# -*- coding: utf-8 -*-
"""
Created on Thu May 26 07:54:50 2022

@author: richard.ramos
"""
import numpy as np
import pandas as pd
import os, glob

from sklearn.ensemble import RandomForestRegressor

import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
# data_estimar = pd.read_excel(r'C:\Users\richard.ramos\35159_147728_DUPREE_VENTA_DIRECTA_S_A\STATS - Documentos\6.- TIM\11.- RCH\5.- COD_PYT\4.- FUN_PYT\dato_estimar_frio.xlsx')
# data = data_estimar.copy()

def modelamiento(data, target, campana_test, brecha, col_features_quant, col_features_categ=[], show=True):
    
    """
    NAME
        modelamiento
        
    Descripcion
        Funcion que realiza las predicciones para una campana usando el modelo Random Forest. Tambien se obtiene los percentiles de las
        predicciones, grafico de correlacion, grafico de importance, grafico de distribucion de errores y una tabla de metricas tanto
        para la data de entrenamiento como la de testeo.
        
    Parametros
    ----------
        data : DataFrame
            Tabla que contiene la informacion de las variables predictoras y la que se busca predecir
            
        target : str
            Nombre de la variable a predecir
            
        campana : str
            Campana la cual se busca predecir. 
                Ejemplo: '202206'
                
        brecha : int
            Numero de campanas de diferencia entre la actual y la que se quiere predecir. 
                Ejemplo: Para obtener las predicciones de la campana '202206', la campana actual seria la 202203 mientras que la ultima
                         campana cerrada seria la 20222 (El filtro para la data de train en este caso es menor a 202203, por ello se
                         pone como brecha el valor de 3 para este ejemplo). 
                    
        col_features_quant : list
            Lista que contiene el nombre de las variables predictoras que sean cuantitativas
            
        col_features_categ : list default []
            Lista que contiene el nombre de las variables predictoras que sean categoricas
            
        show : bool default True
            Si es True te genera los graficos de correlacion, importante y distribucion de errores

    Returns
    -------
    Cuatro DataFrame, un ensemble.-forest y una lista
        El primer objeto es un DataFrame contiene la informacion de la base de testeo (campana a predecir)
        El segundo objeto es un DataFrame contiene la informacion de la base de train
        El tercer objeto es un DataFrame contiene los percentiles para la data de testeo (campana a predecir)
        El cuarto objeto es un DataFrame contiene las metricas MAPE, Adjusted MAPE y RMSE para la data de train y test(campana a predecir)
        El quinto objeto que devuelve es el modelo entrenado
        El sexto objeto es una lista que contiene los nombres de las variables predictoras
        
    Example
    -------
    import MDL as modelos
    modelo = modelos.modelamiento(data=data, 
                                  target='RATIO', 
                                  campana_test='202206', 
                                  brecha=3, 
                                  col_features_quant=['FACT_ANCASH', 'FACT_LIMA', 'FACT_JUNIN', 'FACT_LA.LIBERTAD', 'FACT_HUANCAVELICA', 'FACT_ADV_RGN', 'NUM_CAMP', 'PRECIO_N', 'RATIO_HIST'], 
                                  col_features_categ=['SUBGRUPO', 'SUBUEN'],
                                  show=True)
    """
    
    # Solo testea una campana, en caso se quieran testear varias campanas un for por fuera debe usarse para que cambie dinamicamente el 
    # parametro campana_test
    
    
    # Obtener posicion de la campana, para obtener la campana correcta al restarle la brecha
    lis_camp = ['01','02','03','04','05','06','07','08','09',
                '10','11','12','13','14','15','16','17','18']
        
    anios = list(range(2016,2050))
    anios_str = [str(an) for an in anios]
    
    campanias = []
    
    for a in anios_str:
        anio_c = [a + c for c in lis_camp]
        campanias = campanias + anio_c
    
    camps = [int(cam) for cam in campanias]

    ind = camps.index(int(campana_test))
    

    # Dummies y lista de features
    data_1 = data.copy()
    try:
        data_dummie = pd.get_dummies(data_1[col_features_categ])
        data_1 = pd.concat([data,data_dummie],axis=1)
    except:
        print('No tiene columnas categoricas')
    
    try:
        col_features_0 = [target] + col_features_quant 
        col_features_1 = col_features_quant + data_dummie.columns.tolist() 
    except:
        col_features_0 = [target] + col_features_quant 
        col_features_1 = col_features_quant 
    
    # %% BASE TRAIN Y TEST
    
    train_data = data_1.loc[data_1['CAMP']<int(camps[ind - brecha]), :].reset_index(drop=True)
    test_data = data_1.loc[data_1['CAMP']==int(campana_test), :].reset_index(drop=True)
            
    train_features = np.array(train_data[col_features_1])
    train_labels = np.array(train_data[target])
    test_features = np.array(test_data[col_features_1])
    test_labels = np.array(test_data[target])
    
    # %% CORRELACION
    
    if show:
        mask = np.triu(np.ones_like(np.round(data_1[col_features_0].corr(), 1), dtype=bool))
        
        corrMatrix = np.round(data_1[col_features_0].corr(), 1)
    
        cmap = sns.diverging_palette(250, 10, as_cmap=True)
        res = sns.heatmap(corrMatrix, mask=mask, vmin=-1, yticklabels=1, vmax=1, cmap=cmap, annot=True, annot_kws={'fontsize':7})
        res.set_xticklabels(res.get_xmajorticklabels(), fontsize = 8)
        res.set_yticklabels(res.get_ymajorticklabels(), fontsize = 8)
        plt.show()
    
    # %% MODELO
    rf_quant = RandomForestRegressor(n_estimators=1000, 
                                     random_state=123)
    
    rf_quant.fit(train_features, train_labels)
    
    # %% IMPORTANCE
    if show:
        
        features = col_features_1
        importances = rf_quant.feature_importances_
        indices = np.argsort(importances)
        
        plt.style.use('fivethirtyeight')
        plt.title('Feature Importances')
        plt.barh(range(len(indices)), importances[indices], color='darkblue', align='center')
        plt.yticks(range(len(indices)), [features[i] for i in indices])
        plt.xlabel('Relative Importance')
        plt.show()
    
    # %% Predicciones cuantiles
        
    # Get the predictions of all trees for all observations
    # Each observation has N predictions from the N trees
    pred_Q = pd.DataFrame()
            
    for pred in rf_quant.estimators_:
        # pred = rf_quant.estimators_[0]
        temp = pd.Series(pred.predict(test_features))
        pred_Q = pd.concat([pred_Q,temp],axis=1)
    pred_Q.head()
            
    #-----------QUANTILE SELECCTION----------
            
    quantiles = [0.025,0.1,0.2,0.3,0.4 ,0.50,0.6, 0.7,0.8,0.9 , 0.975]
    RF_test_labels_pred = pd.DataFrame()
            
    for q in quantiles:
        ss = pred_Q.quantile(q=q, axis=1)
        RF_test_labels_pred = pd.concat([RF_test_labels_pred,ss],axis=1,sort=False)
        
    quantiles_names = ['P_'+str(int(n*100)) for n in quantiles]
    quantiles_names[0] = 'LIM_INF'
    quantiles_names[-1] = 'LIM_SUP'
    RF_test_labels_pred.columns=quantiles_names
    
    # %% METRICAS
    
    pred_train = rf_quant.predict(train_features)
    pred_test = rf_quant.predict(test_features)
    
    from sklearn import metrics
    
    # MAPE
    mape_train = metrics.mean_absolute_percentage_error(train_labels, pred_train)*100
    mape_test = metrics.mean_absolute_percentage_error(test_labels, pred_test)*100
    
    #print('Train Mean Absolute Percentage Error: {}'.format(mape_train))
    #print('Test Mean Absolute Percentage Error: {}'.format(mape_test))
    
    # Adj MAPE
    def smape_adjusted(a, f):
        return (1/a.size * np.sum(np.abs(f-a) / (np.abs(a) + np.abs(f))*100))
    
    adj_mape_train = smape_adjusted(train_labels, pred_train)
    adj_mape_test = smape_adjusted(test_labels, pred_test)
    
    #print('Train Simetric Mean Absolute Percentage Error Adjusted: {}'.format(adj_mape_train))
    #print('Test Simectric Mean Absolute Percentage Error Adjusted: {}'.format(adj_mape_test))
    
    # RMSE
    rmse_train = metrics.mean_squared_error(train_labels,pred_train , squared=False)
    rmse_test = metrics.mean_squared_error(test_labels,pred_test , squared=False)
    
    #print('Train RMSE: {}'.format(rmse_train))
    #print('Test RMSE: {}'.format(rmse_test))
    
    metricas_values = {'train': [mape_train, adj_mape_train, rmse_train], 'test':[mape_test, adj_mape_test, rmse_test]}
    metricas = pd.DataFrame(index=['MAPE', 'Adj MAPE', 'RMSE'], data=metricas_values)
    
    if show:
        
        error_train = train_labels - pred_train
        error_test =  test_labels - pred_test
        
        sns.histplot(error_train, fill=False, element='step', color='g', stat='density')
        sns.kdeplot(error_train, color='black')
        sns.rugplot(error_train, color='black');plt.show()
        
        sns.histplot(error_test, fill=False, element='step', color='g', stat='density')
        sns.kdeplot(error_test, color='black')
        sns.rugplot(error_test, color='black');plt.show()                    
    
    
    
    return test_data, train_data, RF_test_labels_pred, metricas, rf_quant, col_features_1
    


# campanasa = '202206'
# col_features_quant = ['FACT_ANCASH', 'FACT_LIMA', 'FACT_JUNIN', 'FACT_LA.LIBERTAD', 'FACT_HUANCAVELICA', 'FACT_ADV_RGN', 'NUM_CAMP', 'PRECIO_N', 'RATIO_HIST']
# col_features_categ = ['SUBGRUPO', 'SUBUEN']
# target='RATIO'
# show = False




    

    
    
    
    
    
    
    