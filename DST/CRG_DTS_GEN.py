# -*- coding: utf-8 -*-
"""
Created on Tue May 24 08:40:39 2022

@author: richard.ramos
"""


# REVISAR ENCODING PARA TODOS LOS ARCHIVOS

import numpy as np
import pandas as pd

import os, glob
import pyarrow
import pyarrow.parquet as pq

# # Parametros:
# compania = 'AZZ' # 'LVV'
# tabla = '1.1'
# pais = 'PER'
# user = 'richard.ramos'
# separador = ','
# camp_inicio = 202117
# camp_final = 202206
# camp_excl = [202119, 202201, 202202]

# Diccionario de países
dic_pais = {'PER':'2.- ','COL':'3.- ','ECU':'4.- ','BOL':'5.- ','GUA':'6.- '}

def carga(compania, tabla, pais, user, camp_inicio='None', camp_final='None', camp_excl=[], separador=','):
    
    """
    NAME
        carguita
        
    Descripcion
        Funcion que permite cargar información de tablas generadas de Azzorti y Livva en todos los países
        
    Parametros
    ----------
        compania : str
            Acepta valores de: 'AZZ' y 'LVV'
        tabla : str
            Primeros digitos con el cual está nombrado una tabla. Ejemplo: '1.1', '0.10'
        pais : str
            Pais del cual se cargara la informacion
        user : str
            Nombre del usuario que consultara
        camp_inicio : int default 'None'
            Campana desde la cual carga la informacion. Si es 'None' cargara desde la minima campana que encuentre
        camp_final : int default 'None'
            Campana hasta la cual carga la informacion. Si es 'None' cargara hasta la maxima campana que encuentre
        camp_excl : list default []
            Campanas a excluir 
        separador : str default ','
            Separador para cuando el archivo a cargar sea txt o csv

    Returns
    -------
    DataFrame
        Un DataFrame de la tabla generada que quiere llamarse de Azzorti o Livva según los parámetros proporcionados
            
            
    Example
    -------
    import CRG_DTS_GEN as carguita
    
    carguita.carga(compania='AZZ', tabla='1.1', pais='PER', user='richard.ramos', 
                   camp_inicio = 202117, camp_final = 202206, camp_excl = [202118, 202201, 202202],  separador=',')
    """
    
    if compania=='AZZ':
        ruta = 'C:/Users/' + user + '/35159_147728_DUPREE_VENTA_DIRECTA_S_A/STATS - Documentos/1.- DTS/' + dic_pais[pais] + pais + '/' + '2.- GEN/'
        os.chdir(ruta)
    
    if compania=='LVV':
        ruta = 'C:/Users/' + user + '/35159_147728_DUPREE_VENTA_DIRECTA_S_A/LVV - STATS - Documentos/1.- DTS/' + dic_pais[pais] + pais + '/' + '2.- GEN/'
        os.chdir(ruta)
    
    files = glob.glob('*.*.- *')
    
    [f.find('.-')for f in files]
    numbers = [f[0:f.find('.-')] for f in files]
    
    for i, num in enumerate(numbers):
        if tabla==num:
            file = files[i]
            
    ruta_1 = ruta + file
    os.chdir(ruta_1)
    
    
    
    table = glob.glob('*_[0-9][0-9][0-9][0-9][0-9][0-9].*')
    
    
    type_files = list(set([t[t.find('.')+1:] for t in table]))
    first_file = list(set([t[t.find('.')+1:] for t in table]))[0]
    
    def len_files(x):
        assert len(x)==1, 'Hay mas de un tipo de archivo con el formato de nombre para las tablas'
        return print('Correcto: Hay solo un tipo de archivo con el formato de nombre para tablas' + ': ' + first_file)

    try:
        len_files(type_files)
    except AssertionError as msg:
        print(msg)
    
    min_camp = int(min([f[f.find('.')-6:f.find('.')] for f in table]))
    max_camp = int(max([f[f.find('.')-6:f.find('.')] for f in table]))
    
    if (camp_inicio=='None') and (camp_final=='None'):
        
        if first_file=='txt':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS TXT IMPORTADOS')
            
        if first_file=='csv':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS CSV IMPORTADOS')
            
        if first_file=='xlsx':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_excel(f, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS XLSX IMPORTADOS')
            
        if first_file.find('parquet')!=-1:
            
            table_1 = [f for f in table if (int(f[f.find('.')-6:f.find('.')]) >= min_camp) & (int(f[f.find('.')-6:f.find('.')]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[f.find('.')-6:f.find('.')]) not in camp_excl]
            
            table_concat = pd.DataFrame()
            for i in range(0, len(table_2)):
                df = pq.read_table(table_2[i], use_threads=True).to_pandas()
                table_concat=pd.concat([table_concat,df])
            print('ARCHIVOS PARQUET IMPORTADOS')
        
    elif (camp_inicio=='None') and (camp_final!='None'):
        
        if first_file=='txt':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS TXT IMPORTADOS')
            
        if first_file=='csv':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS CSV IMPORTADOS')
            
        if first_file=='xlsx':
            table_1 = [f for f in table if (int(f[-10:-4]) >= min_camp) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_excel(f, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS XLSX IMPORTADOS')
            
        if first_file.find('parquet')!=-1:
            
            table_1 = [f for f in table if (int(f[f.find('.')-6:f.find('.')]) >= min_camp) & (int(f[f.find('.')-6:f.find('.')]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[f.find('.')-6:f.find('.')]) not in camp_excl]
            
            table_concat = pd.DataFrame()
            for i in range(0, len(table_2)):
                df = pq.read_table(table_2[i], use_threads=True).to_pandas()
                table_concat=pd.concat([table_concat,df])
            print('ARCHIVOS PARQUET IMPORTADOS')
    
    elif (camp_inicio!='None') and (camp_final=='None'):
        
        if first_file=='txt':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS TXT IMPORTADOS')
            
        if first_file=='csv':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS CSV IMPORTADOS')
            
        if first_file=='xlsx':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= max_camp)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_excel(f, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS XLSX IMPORTADOS')
            
        if first_file.find('parquet')!=-1:
            
            table_1 = [f for f in table if (int(f[f.find('.')-6:f.find('.')]) >= camp_inicio) & (int(f[f.find('.')-6:f.find('.')]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[f.find('.')-6:f.find('.')]) not in camp_excl]
            
            table_concat = pd.DataFrame()
            for i in range(0, len(table_2)):
                df = pq.read_table(table_2[i], use_threads=True).to_pandas()
                table_concat=pd.concat([table_concat,df])
            print('ARCHIVOS PARQUET IMPORTADOS')
        
    else:
        
        if first_file=='txt':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS TXT IMPORTADOS')
            
        if first_file=='csv':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_csv(f, sep =separador, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS CSV IMPORTADOS')
            
        if first_file=='xlsx':
            table_1 = [f for f in table if (int(f[-10:-4]) >= camp_inicio) & (int(f[-10:-4]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[-10:-4]) not in camp_excl]
            table_files = (pd.read_excel(f, na_values=' ', keep_default_na=True, encoding= 'ISO-8859-1') for f in table_2)
            table_concat = pd.concat(table_files, ignore_index=True)
            print('ARCHIVOS XLSX IMPORTADOS')
            
        if first_file.find('parquet')!=-1:
            
            table_1 = [f for f in table if (int(f[f.find('.')-6:f.find('.')]) >= camp_inicio) & (int(f[f.find('.')-6:f.find('.')]) <= camp_final)]
            table_2 = [f for f in table_1 if int(f[f.find('.')-6:f.find('.')]) not in camp_excl]
            
            table_concat = pd.DataFrame()
            for i in range(0, len(table_2)):
                df = pq.read_table(table_2[i], use_threads=True).to_pandas()
                table_concat=pd.concat([table_concat,df])
            print('ARCHIVOS PARQUET IMPORTADOS')

    return table_concat
    
    

# dts = carga(compania='AZZ', tabla='1.1', pais='PER', user='richard.ramos', 
#             camp_inicio = 202117, camp_final = 202206, camp_excl = [202118, 202201, 202202],  separador=',')

# dts.CAMP.unique()
# dts.CAMP.value_counts()
    
    
    
    
    
    
    
    