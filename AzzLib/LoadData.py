# %% Libraries
# pip install --force-reinstall --no-deps git+https://gitlab.com/a6333/library.git
# from AzzLib import LoadData as ld
# pip install selenium
# pip install webdriver_manager
# pip install chromedriver_autoinstaller
import os
import re
import glob
import numpy as np
import pandas as pd
import csv
from string import ascii_letters
import pymssql
from io import BytesIO
from ftplib import FTP
import win32com.client
import subprocess
import datetime
import time
from AzzLib import scrap_consolidado as scrap
import io

conn = pymssql.connect(
    host='23.98.176.101',
    # host= '172.29.0.4',
    user='user_modelos',
    password='Azzorti1956*',
    database='dwh_azzorti'
)
cursor = conn.cursor(as_dict=True)
# %% Constants
# import AzzLib

now = datetime.datetime.now()
name_user = os.getlogin()
root_main = "C:/Users/" + name_user
root_stats_azz = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/STATS - 1.- DTS"
if not os.path.isdir(root_stats_azz):
    root_stats_azz = root_main + '/35159_147728_DUPREE_VENTA_DIRECTA_S_A/STATS - Documentos/1.- DTS'
root_stats_lvv = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/LVV - STATS - Documentos/1.- DTS"
root_supply_azz = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Data/AZZ"
root_supply_lvv = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Data/LVV"

ruta_paises = {"PER": "2.- PER",
               "COL": "3.- COL",
               "ECU": "4.- ECU",
               "BOL": "5.- BOL",
               "GUA": "6.- GUA"}

dict_fac_types = {'MOV': '/1.- ORI/2.- MOV_FAC',
                  'TPA': '/1.- ORI/9.- FCT_PVL',
                  'TZO': '/1.- ORI/6.- FAC_TZO'}

catag_filter = {"PER": [11, 21],
                "COL": [1, 11, 12, 21, 22],
                "ECU": [1, 11],
                "BOL": [1, 11],
                "GUA": [1, 11]}

d_pais_fac = {'PER': 'TPA',
              'COL': 'MOV',
              'ECU': 'MOV',
              'GUA': 'MOV',
              'BOL': 'MOV'}

indof_labels = {"PER": "INDI_OFER",
                "COL": "IND_OFER",
                "ECU": "",
                "BOL": "IND_OFER",
                "GUA": ""}

campanitas_total = np.array([i + j * 100 for j in range(2017, 2025) for i in range(1, 19) if i + j * 100 != 202011])

lines_filter = ['HOGAR', 'ROPA', 'PERSONAL', 'JOYERIA', 'FRAGANCIA', 'JOHNSON', 'COSMETICO', 'NUTRICIONAL', 'ALIMENTO',
                'CALZADO']

validate_lines = pd.DataFrame({
    'LINEA': ['CH CUIDADO HOGAR', 'CP CUIDADO PERSONAL', 'FR FRAGANCIAS', 'HO HOGAR', 'JJ JOHNSON Y JOHNSON',
              'JO JOYERIA', 'RE ROPA EXTERIOR', 'RI ROPA INTERIOR', 'COSMETICOS', 'JOHNSON Y JOHNSON', 'JOYERIA',
              'NUTRICIONALES', 'ROPA', 'FRAGANCIAS', 'ROPA EXTERIOR', 'UN NUTRICIONALES', 'AL ALIMENTO',
              'NU NUTRICIONALES', 'CUIDADO DEL HOGAR', 'CUIDADO PERSONAL', 'HOGAR', 'CA CALZADO', 'CALZADO',
              'SIN PROCESAR'],
    'FINAL_LINEA': ['HOG', 'CUP', 'FRA', 'HOG', 'CUP', 'JOY', 'ROP', 'ROP', 'CUP', 'CUP', 'JOY', 'NUT', 'ROP', 'FRA',
                    'ROP', 'NUT', 'ALI', 'NUT', 'HOG', 'CUP', 'HOG', 'CAL', 'JOY', 'SPR']})

dic_cred_ftp = eval(open(root_supply_azz + '/0.- CODIGO/0.- LIB/credenciales_fobos.txt').read())
user_ftp = dic_cred_ftp['usuario_ftp']
password_ftp = dic_cred_ftp['password_ftp']
host_ftp = dic_cred_ftp['host_ftp']

dic_cred_sap = eval(open(root_supply_azz + '/0.- CODIGO/0.- LIB/credenciales_sap.txt').read())
user_sap = dic_cred_sap['user_sap']
password_sap = dic_cred_sap['password_sap']
con_sap = dic_cred_sap['con_sap']

# Revisar "CALZADO" :C

cnv_lin = pd.read_excel(root_supply_azz + "/1.- CMN/2.- GEN/3.- CLF_MAT/CLF_MAT.xlsx", sheet_name=0)

ini_camp = 201801

pai_lvv = {'PER': 'LPE'}

clase_ord = pd.CategoricalDtype(['42', '50', '63', '36'], ordered=True)


class FacTypeError(Exception):
    pass


# % Functions
def reconnect_sql():
    conn = pymssql.connect(
        host='23.98.176.101',
        # host= '172.29.0.4',
        user='user_modelos',
        password='Azzorti1956*',
        database='dwh_azzorti'
    )
    cursor = conn.cursor(as_dict=True)


def get_tasas_paises():
    tasas_paises = pd.read_excel(
        root_supply_azz + "/1.- CMN/3.- DIG" + "/Tasa_Cambio_Camp_Pais.xlsx")  # Se va a usar este directorio como fuente comun de datos
    tasas_paises = tasas_paises.iloc[:, [0, 4, 3]]
    tasas_paises.columns = ['PAIS', 'CAMP', 'TC']
    return tasas_paises


def apply_orig_cod(df, where_to_replace):
    if not 'd_sust' in globals():
        DataOri(paises=['PER', 'COL', 'ECU', 'BOL', 'GUA'], camp_fin=1).get_sustitutos(origen='sql')

    if 'PER' in df['PAIS'].unique():
        df_per = df[df['PAIS'] == 'PER']
        df_otr = df[df['PAIS'] != 'PER']
        where_to_replace_per = where_to_replace.index[np.isin(where_to_replace.index, df_per.index)]
        sustitutos_i = d_sust['sust_ide'].copy()
        sustitutos_i.columns = ["CODI_ORIG", "CODI_PROD"]
        ## VALIDAR
        sustitutos_i = sustitutos_i.groupby('CODI_PROD', as_index=False).agg('last')
        ##
        df_per = df_per.merge(sustitutos_i, on="CODI_PROD", how="left")
        df_per.index = where_to_replace_per
        df_per.loc[where_to_replace_per, "CODI_PROD"] = df_per.loc[
                                                            where_to_replace_per, ["CODI_ORIG", "CODI_PROD"]].bfill(
            axis=1).iloc[:, 0].astype(str).tolist()
        df_per = df_per.drop("CODI_ORIG", axis=1)
        df = pd.concat([df_per, df_otr])

    sustitutos_s = d_sust['sust_sap'].copy()
    sustitutos_s.columns = ["CODI_ORIG", "CODI_PROD"]
    sustitutos_s = sustitutos_s.groupby('CODI_PROD', as_index=False).agg('last')
    #
    df = pd.merge(df, sustitutos_s, on="CODI_PROD", how="left")
    df.loc[where_to_replace, "CODI_PROD"] = df.loc[where_to_replace, ["CODI_ORIG", "CODI_PROD"]].bfill(axis=1).iloc[:, 0].astype(str)
    df = df.drop("CODI_ORIG", axis=1)

    sustitutos_p = d_sust['sust_xfalt'].copy()
    sustitutos_p = sustitutos_p.loc[:, ['PAIS', "CAMP", "CODI_ORIG", "CODI_PROD"]]
    sustitutos_p.columns = ['PAIS', "CAMP", "CODI_ORIG", "CODI_PROD"]

    # VALIDAR
    sustitutos_p = sustitutos_p.drop_duplicates(['PAIS', 'CAMP', 'CODI_PROD'], keep='last').reset_index(drop=True)

    df = pd.merge(df, sustitutos_p, on=['PAIS', "CAMP", "CODI_PROD"], how="left")
    df.loc[where_to_replace, "CODI_PROD"] = df.loc[where_to_replace, ["CODI_ORIG", "CODI_PROD"]].bfill(axis=1).iloc[:, 0].astype(str)
    df = df.drop("CODI_ORIG", axis=1)
    return df.reset_index(drop=True)


# def get_files_fct_crt(country_now):
#     df = pd.DataFrame()
#     if country_now[-3:] in ['BOL', 'GUA']:
#         df = pd.DataFrame(columns=['CORTE', 'CAMP', 'PAIS', 'PEDIDOS_X_CORTE', 'PEDIDOS_CAMP', 'PR_CORTE'])
#         pass
#     else:
#         df_temp = pd.DataFrame()
#         all_filenames = np.array(glob.glob("*.txt"))
#         all_files_camp = np.array(list(map(lambda x: int(x.strip(ascii_letters + "_.")), all_filenames)))
#         selected_files = all_filenames[all_files_camp > 201900]
#         for f in selected_files:
#             df_temp = pd.read_csv(f, encoding='cp437', sep=',')
#             df = df.append(df_temp)
#         df['PEDIDOS_CAMP'] = \
#         pd.merge(df, df.groupby(['CAMP'], as_index=False)['PEDIDOS_X_CORTE'].sum(), on='CAMP', how='left')[
#             'PEDIDOS_X_CORTE_y'].values
#         df['PR_CORTE'] = df['PEDIDOS_X_CORTE'] / df['PEDIDOS_CAMP']
#     return df


def structure_fac(df_n, pais, col_adi):
    '''Homologar datos de los archivos de facturación.'''
    df_n = df_n.drop(df_n.columns[df_n.columns.str.contains('Unnamed:')], axis=1)

    if pais == "PER":
        df_n = df_n[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA', 'CANT_PEDI',
                     'CANT_DESP', 'CANT_FALT', 'CANT_DEFA', 'NETO_FACT', 'NETO_FALT', 'NETO_DEFA',
                     'PEDIDOS']+col_adi].copy()
        df_n[['NETO_FALT', 'NETO_DEFA', 'CANT_FALT', 'CANT_DEFA']] = df_n[
            ['NETO_FALT', 'NETO_DEFA', 'CANT_FALT', 'CANT_DEFA']].astype('float')
        df_n['NETO_FALT'] = df_n['NETO_FALT'] - df_n['NETO_DEFA']
        df_n['CANT_FALT'] = df_n['CANT_FALT'] - df_n['CANT_DEFA']
        df_n = df_n.drop(["CANT_DEFA", "NETO_DEFA"], axis=1)

    elif pais == "COL":
        if d_pais_fac[pais] == 'MOV':
            col_n = ['TOTAL_FALT_DEVO', 'FALTANTE_DEVO', 'UNID_NOTA']
            df_n[col_n] = df_n[col_n].astype('float')
            df_n['TOTAL_FALT_DEVO'] = df_n['TOTAL_FALT_DEVO'] / df_n['FALTANTE_DEVO']
            df_n['FALTANTE_DEVO'] -= df_n['UNID_NOTA']
            df_n['TOTAL_FALT_DEVO'] = df_n['TOTAL_FALT_DEVO'] * df_n['FALTANTE_DEVO']
            df_n = df_n[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CATALOGO', 'UND_PEDIDAS',
                         'UND_FACTURADAS', 'FALTANTE_DEVO', 'TOTAL_NETO', 'TOTAL_FALT_DEVO', 'TOTAL_PEDIDOS'] +
                        col_adi+['UNIDAD']].copy()
        elif d_pais_fac[pais] == 'TZO':
            df_n = df_n[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA', 'CANT_PEDI',
                        'CANT_DESP', 'CANT_FALT', 'NETO_FACT', 'NETO_FALT', 'PEDIDOS']+col_adi+['CODI_NEGO']].copy()

    elif (pais == "ECU") | (pais == "GUA"):
        df_n = df_n[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CATALOGO', 'UND_PEDIDAS',
                     'UND_FACTURADAS', 'UND_FALTANTE', 'TOTAL_NETO', 'TOTAL_FALTANTE', 'TOTAL_PEDIDO']+col_adi].copy()

    elif pais == "BOL":
        df_n = df_n[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CATALOGO', 'UND_PEDIDAS',
                     'UND_FACTURADAS', 'UND_FALTANTE', 'TOTAL_NETO', 'TOTAL_FALTANTE', 'TOTAL_PEDIDOS']+col_adi].copy()

    if not pais == 'COL':
        df_n.insert(loc=df_n.shape[1], column='UNIDAD', value=np.nan)

    df_n.columns = ['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA', 'UND_PEDIDAS',
                    'UND_FACTURADAS', 'UND_FALTANTES', 'TOTAL_NETO', 'TOTAL_FALTANTE', 'TOTAL_PEDIDOS']+col_adi+['UNIDAD']

    col_n = ['UND_PEDIDAS', 'UND_FACTURADAS', 'UND_FALTANTES', 'TOTAL_NETO', 'TOTAL_FALTANTE', 'TOTAL_PEDIDOS']
    df_n[col_n] = df_n[col_n].astype('float')
    df_n['CAMP'] = df_n['CAMP'].astype('int')

    lineas_adicionales = pd.DataFrame({'LINEA': ['BACK_ORDER'], 'FINAL_LINEA': ['BACK_ORDER']})
    df_n['LINEA'] = df_n.merge(pd.concat([validate_lines, lineas_adicionales], ignore_index=True),
                               how='left')['FINAL_LINEA'].tolist()
    df_n = df_n.drop_duplicates().reset_index(drop=True)
    return df_n


def get_csv_separator(df):
    string = df.columns[0]
    sniffer = csv.Sniffer()
    dialect = sniffer.sniff(string)
    return dialect.delimiter


def apply_filters(df, back_orders=False, filter_pag=False, obj=None):
    country = df.iloc[0, 0]
    cat_col = df.columns[np.isin(df.columns, np.array(['CATALOGO', 'CODI_CATA']))][0]
    df[cat_col] = df[cat_col].astype('int')

    if back_orders:
        df = df.loc[df[cat_col].isin(catag_filter[country] + [3])]
    else:
        df = df.loc[df[cat_col].isin(catag_filter[country])]

    try:
        if indof_labels[country] != "":
            if back_orders:
                df = df.loc[df[indof_labels[country]].isin(['N', 'O', 'B'])]
            else:
                df = df.loc[df[indof_labels[country]].isin(['N', 'O'])]
    except:
        print('No se aplica el filtro IND_OFER/INDI_OFER. =(')

    if filter_pag:
        df = df.reset_index(drop=True)
        if obj.df_pag_cat is None:
            obj.get_pag_cat()
        
        df['CAMP'] = df['CAMP'].astype(int)
        df['CODI_VENT'] = df['CODI_VENT'].str.strip()
        df['CODI_PROD'] = df['CODI_PROD'].str.strip()
        if country in ['BOL', 'ECU', 'GUA']:
            df['TIPO_ZONA'] = 'N'
        df['FILTER_PAG'] = df.merge(obj.df_pag_cat, left_on=['PAIS', 'CODI_VENT', 'CAMP', 'TIPO_ZONA'],
                                    right_on=['PAIS', 'CODIGO_VENT', 'CAMP', 'TIPO_ZONA'], how='left')['PAG']
        df['FILTER_PAG'] = np.where(df['FILTER_PAG'].isnull(),
                                    np.where(df['PAGINA'].astype(float) < 400, 'S', 'N'),
                                    df['FILTER_PAG'])
        df = df[(df['FILTER_PAG'] == 'S') | (df['LINEA'] == 'BACK_ORDER')]
        # df_filtrado = df[(df['FILTER_PAG'] == 'S') | (df['LINEA'] == 'BACK_ORDER')]
        # df_quitados = df[~((df['FILTER_PAG'] == 'S') | (df['LINEA'] == 'BACK_ORDER'))]

    return df.reset_index(drop=True)


def increase_camp_by_n(data, n=1):
    if isinstance(data, pd.DataFrame):
        data['CAMP'] = data.CAMP.map(lambda x: campanitas_total[np.where(campanitas_total == x)[0][0] + n])
    else:
        data = campanitas_total[np.where(campanitas_total == data)[0][0] + n]
    return data


def get_files_consolidation(pais, camp_ini=1, camp_fin=999999, auto_sep=False):
    all_filenames = np.array(glob.glob("*[0-9][0-9][0-9][0-9][0-9][0-9].xls"))
    all_files_camp = np.array(list(map(lambda x: int(x.strip(ascii_letters + "_.")), all_filenames)))
    selected_files = all_filenames[(camp_fin >= all_files_camp) & (all_files_camp >= camp_ini)]

    def find_delimiter(filename, auto_sep_=auto_sep):
        if auto_sep_:
            sniffer = csv.Sniffer()
            with open(filename) as fp:
                delimiter = sniffer.sniff(fp.read(5000)).delimiter
            return delimiter
        else:
            return '\t'

    df = pd.concat([pd.read_csv(file, encoding='cp437', sep=find_delimiter(file), dtype='str') for file in selected_files])
    df = df.dropna(axis=0, how='all')
    df = df.dropna(axis=1, how='all')

    if not ('PAIS' in df):
        df.insert(loc=0, column='PAIS', value=pais)

    if 'LINEA' in df.columns:
        df = df[df.LINEA.str.contains('|'.join(map(re.escape, lines_filter + ['BACK_ORDER', 'SIN PROCESAR'])))]

    # if not 'EST_CAM' in os.getcwd():
    #     not_lines = ['PRE', 'CAT', 'PER']
    #     replace_index = df.loc[(df['LINEA'] == 'SIN PROCESAR') & (df['CODI_NEGO'].isin(not_lines)), :].index
    #     df_aux = df.loc[replace_index, ['CODI_NEGO']].copy()
    #     df_aux = df_aux.merge(cnv_lin[['SUBUEN', 'LINEA']], on='LINEA')
    return df.reset_index(drop=True)


def get_files_falt_anun(root_company, country_now, camp_ini=201900, camp_fin=999999):
    all_filenames = np.array(glob.glob(f'{root_company}/{ruta_paises[country_now]}/1.- ORI/7.- FAL_ANU/*FALTANTE*[0-9][0-9][0-9][0-9][0-9][0-9].xls'))
    all_filenames = np.array([i.replace("\\", "/") for i in all_filenames])
    all_files_camp = np.array(list(map(lambda x: int(x.split('/')[-1].strip(ascii_letters + "_.")), all_filenames)))
    selected_files = all_filenames[(all_files_camp >= camp_ini) & (all_files_camp <= camp_fin)]
    # LOAD
    '''
    for f in selected_files:
        df_temp = pd.read_csv(f, encoding='cp437', sep='\t')
        df = df.append(df_temp)
    '''
    df = pd.concat([pd.read_csv(f, encoding='cp437', sep='\t') for f in selected_files])
    # CLEAN
    if country_now[-3:] in ['PER', 'BOL', 'GUA', 'ECU']:
        if country_now[-3:] == 'ECU':
            df = df.loc[:, ['CAMPANA', 'PRODUCTO', 'CORTE']].drop_duplicates()
        else:
            df = df.loc[:, ['CODI_CAMP', 'CODIGO', 'CORTE']].drop_duplicates()
        df.columns = ['CAMP', 'CODI_PROD', 'CORTE']
        df[['CAMP', 'CORTE']] = df[['CAMP', 'CORTE']].astype(int)
        df['CODI_PROD'] = df['CODI_PROD'].astype(str)

    elif country_now[-3:] == 'COL':
        df = df.loc[:, ['CAMP_DESTINO', 'TIPO_ZONA', 'COD_HIJO', 'CORTE']].drop_duplicates()
        df.columns = ['CAMP', 'TIPO_ZONA', 'CODI_PROD', 'CORTE']
        df[['CAMP', 'CORTE']] = df[['CAMP', 'CORTE']].astype(int)
        df[['TIPO_ZONA', 'CODI_PROD']] = df[['TIPO_ZONA', 'CODI_PROD']].astype(str)
        df = df[df.TIPO_ZONA == 'N'].drop('TIPO_ZONA', axis=1)

    df.insert(loc=0, column='PAIS', value=country_now)

    return df


def get_file_ftp(hostname, username, password, path=None, file=None, sep=',', encoding=None, low_memory=True,
                 dtype=None, thousands=None, decimal='.', engine=None, ruta_imprimir=None, frecuencia_descarga=None,
                 parse_dates=None, date_parser=None, usecols=None):
    """
        Con esta función se lee 1 reporte desde un servidor ftp, indicando:
        :param
            hostname: url o ip a conectarse
            username: usuario para conectarse
            password: contraseña para la conexión
            path: ruta dentro del servidor
            file: nombre del archivo a leer
            frecuencia: 'H' - Hora
                        'D' - Diario
                        'S' - Semanal
                        Sin parámetro fuerza actualización
        :return: retorna un dataframe con el archivo indicado
    """
    ruta_archivos = {
        'zdm_mat_art.csv': fr'C:\Users\{name_user}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Data\AZZ\1.- CMN\1.- ORI\2.- ZDM_ART\zdm_mat_art.csv',
        'ztpla_23.csv': fr'C:\Users\{name_user}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Data\AZZ\1.- CMN\1.- ORI\3.- ZTP_L23\ztpla_23.csv',
        'ZTPLAN_49.csv': fr'C:\Users\{name_user}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Data\AZZ\1.- CMN\1.- ORI\5.- ZTP_L49\ZTPLAN_49.csv'}
    ruta_file = re.split("\\\|/", file)[-1]
    imprimir_onedrive = False
    if ruta_file in ruta_archivos.keys():
        datefile = datetime.datetime.fromtimestamp(os.path.getmtime(ruta_archivos[ruta_file]))
        imprimir_onedrive = True
    else:
        datefile = datetime.datetime(2020, 6, 15, 16, 54, 50, 680416)
    dif = now - datefile
    dif_hours = dif.seconds / 3600 + dif.days * 24

    ftp = FTP(hostname, username, password)

    def actualizar():
        if isinstance(path, str):
            ftp.cwd(path)
        with BytesIO() as zz:
            ftp.retrbinary('RETR ' + file, zz.write)
            zz.seek(0)
            df_zz = pd.read_csv(zz, sep=sep, encoding=encoding, low_memory=low_memory, thousands=thousands,
                                decimal=decimal, engine=engine, dtype=dtype, parse_dates=parse_dates, date_parser=date_parser,
                                usecols=usecols)

        if ruta_imprimir is not None:
            df_zz.to_csv(f'{ruta_imprimir}/{ruta_file}')
        try:
            if imprimir_onedrive:
                df_zz.to_csv(ruta_archivos[ruta_file])
        except:
            None
        print(f"Archivo '{file}' descargado con éxito.")
        return df_zz

    def cargar():
        df_c = pd.read_csv(fr'{ruta_archivos[file]}', sep=sep, encoding=encoding, low_memory=low_memory,
                           thousands=thousands, decimal=decimal, engine=engine, dtype=dtype, parse_dates=parse_dates,
                           date_parser=date_parser, usecols=usecols)
        return df_c

    dateftp = ftp.sendcmd('MDTM ' + file)
    dateftp = datetime.datetime.strptime(dateftp[4:], "%Y%m%d%H%M%S") - datetime.timedelta(hours=5) #Zona horaria

    if frecuencia_descarga is None:
        df = actualizar()
    else:
        if ((dif.days >= 7) and (frecuencia_descarga == 'S')) or (dateftp >= datefile):
            df = actualizar()
        elif ((dif.days >= 1) and (frecuencia_descarga == 'D')) or (dateftp >= datefile):
            df = actualizar()
        elif ((dif_hours >= 1) and (frecuencia_descarga == 'H')) or (dateftp >= datefile):
            df = actualizar()
        else:
            df = cargar()
    return df


def get_sep(file=None):
    try:
        with open(file, 'r', encoding='cp437') as file_read:
            delimiter = csv.Sniffer().sniff(file_read.read(2), delimiters=';,')
        sep = delimiter.delimiter
    except:
        sep = '\t'
    return sep


def get_modification_date(filename):
    date_created = os.path.getmtime(filename)
    date_created = datetime.date.fromtimestamp(date_created)
    return date_created


class SapConnection:
    def __init__(self, ):
        self.path = r"C:\Program Files (x86)\SAP\FrontEnd\SapGui\saplogon.exe"
        subprocess.Popen(self.path)
        time.sleep(5)

        self.SapGuiAuto = win32com.client.GetObject("SAPGUI")
        if not type(self.SapGuiAuto) == win32com.client.CDispatch:
            return

        self.session = None
        self.connection = None

    def close_session(self):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return
        self.session.findById("wnd[0]").close()
        self.session.findById("wnd[1]/usr/btnSPOP-OPTION1").press()
        print("Se ha cerrado la sesión")

    def login(self, user, password, conn, force_entry=False):
        application = self.SapGuiAuto.GetScriptingEngine
        if not type(application) == win32com.client.CDispatch:
            self.SapGuiAuto = None
            return

        self.connection = application.OpenConnection(conn, True)
        if not type(self.connection) == win32com.client.CDispatch:
            application = None
            self.SapGuiAuto = None
            return
        time.sleep(3)

        self.session = self.connection.Children(0)
        if not type(self.session) == win32com.client.CDispatch:
            self.connection = None
            application = None
            self.SapGuiAuto = None
            return

        try:
            self.session.findById("wnd[0]/usr/txtRSYST-BNAME").text = user
            self.session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = password
            self.session.findById("wnd[0]").sendVKey(0)
            self.session.findById("wnd[0]").sendVKey(0)
            if force_entry:
                self.session.findById("wnd[0]/tbar[0]/btn[0]").press()
                self.session.findById("wnd[1]/usr/radMULTI_LOGON_OPT1").select()
                self.session.findById("wnd[1]/tbar[0]/btn[0]").press()

        except Exception as d:
            # print(sys.exc_info()[0])
            print('\n' + repr(d))
            return

        try:
            self.session.findById("wnd[0]/mbar/menu[0]/menu[5]").select()
            # print(type(self.session))
            # self.session.findById("wnd[0]/tbar[1]/btn[34]").press()

        except Exception as d:
            print("No se logró iniciar sesión, verificar credenciales")
            self.session = None
            return

        print("Se ha iniciado sesión con éxito")

    def execute_function(self, fun=None, **kwargs):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return

        if fun is None:
            print("No se ha indicado una función")
            return
        else:
            a = fun(session=self.session, **kwargs)
            print(f"Se ejecuto la funcion {fun.__name__} con éxito")
            return a

    def get_zrepo_list_mate(self, ruta_imprimir=None, variant=None, codes_sap=None, codes_sid=None):
        """
        Metodo para descargar el reporte de zrepo_list_mate de SAP
        :param ruta_imprimir: ruta a una carpeta en donde se descargara el archivo .txt
        :param variant: nombre de la variante que se desea aplicar en SAP
        :param codes_sap: iterable de codigos sap de productos terminados para filtrar el reporte zrepo_list_mate
        :param codes_sid: iterable de codigos sid de productos terminados para filtrar el reporte zrepo_list_mate
        :return:
        """
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "zrepo_list_mate"
        self.session.findById("wnd[0]").sendVKey(0)

        '''
        self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
        self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = "0"
        self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").doubleClickCurrentCell()
        '''

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT') for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte zrepo_list_mate")
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        if codes_sap is not None:
            codes_sap = pd.Series(codes_sap)
            codes_sap.to_clipboard(index=False, header=False)
            self.session.findById("wnd[0]/usr/btn%_SP$00002_%_APP_%-VALU_PUSH").press()
            self.session.findById("wnd[1]/tbar[0]/btn[24]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

        if codes_sid is not None:
            codes_sid = pd.Series(codes_sid)
            codes_sid.to_clipboard(index=False, header=False)
            self.session.findById("wnd[0]/usr/btn%_SP$00005_%_APP_%-VALU_PUSH").press()
            self.session.findById("wnd[1]/tbar[0]/btn[24]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

        # self.session.findById("wnd[0]/usr/ctxtSP$00002-LOW").text = "1005641"
        self.session.findById("wnd[0]/usr/ctxtSP$00002-LOW").caretPosition = 7

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        try:
            self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        except Exception as d:
            print("No se encontraron datos para el reporte zrepo_list_mate con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        # self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "zrepo_list_mate.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 19
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[0]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        print("Reporte zrepo_list_mate descargado con éxito desde SAP")
        return pd.read_csv(ruta_imprimir + 'zrepo_list_mate.txt', encoding='cp1252', sep="\t", thousands=",",
                           decimal=".", low_memory=False, header=3, skipinitialspace=True, quoting=csv.QUOTE_NONE,
                           usecols=lambda x: not (x.__contains__('Unnamed:')),
                           dtype={'Material': str, 'Componente': str, 'NºMaterial antiguo': str, 'Cantidad': float,
                                  'NºMaterial antiguo.1': str, 'Cantidad base': float, 'Calculado': float,
                                  'AlPro': str, 'Lista mat.': str, 'CatVa': str})

    def get_zpp_profit(self, variant=None, ruta_imprimir=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "zpp_profit"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT') for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte zpp_profit")
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        try:
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").text = "999999"
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").caretPosition = 6
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        except Exception as d:
            pass

        try:
            self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        except Exception as d:
            print("No se encontraron datos para el reporte zpp_profit con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        # self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "zpp_profit.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 18
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[0]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        print("Reporte zpp_profit descargado con exito")

        return pd.read_csv(ruta_imprimir + "zpp_profit.txt", encoding='cp1252', skipfooter=0, engine='python',
                           sep="\t", thousands=",", decimal=".", header=3, skipinitialspace=True,
                           quoting=csv.QUOTE_NONE, usecols=lambda x: 'Unnamed:' not in x, dtype=str)

    def get_zpp_leader_list(self, variant=None, ruta_imprimir=None, camp_ini=None, camp_fin=None, paises=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'

        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "zpp_leader_list"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT') for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte zpp_leader_list")
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        if camp_ini is not None:
            self.session.findById("wnd[0]/usr/txtSP$00004-LOW").text = str(camp_ini)

        if camp_fin is not None:
            self.session.findById("wnd[0]/usr/txtSP$00004-HIGH").text = str(camp_fin)

        if paises is not None:
            self.session.findById("wnd[0]/usr/btn%_SP$00001_%_APP_%-VALU_PUSH").press()
            indice = 0
            for pais in paises:
                self.session.findById(
                    f"wnd[1]/usr/tabsTAB_STRIP/tabpSIVA/ssubSCREEN_HEADER:SAPLALDB:3010/tblSAPLALDBSINGLE/txtRSCSEL_255-SLOW_I[1,{indice}]").text = pais
                indice = indice + 1
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        try:
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").text = "999999"
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").caretPosition = 6
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        except Exception as d:
            pass

        try:
            self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        except Exception as d:
            print("No se encontraron datos para el reporte zpp_leader_list con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "zpp_leader_list.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 19
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[0]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        print("Reporte zpp_leader_list descargado con exito")
        return pd.read_csv(ruta_imprimir + "zpp_leader_list.txt", encoding='cp1252', skipfooter=0, engine='python',
                           sep="\t", thousands=",", decimal=".", header=3, skipinitialspace=True,
                           quoting=csv.QUOTE_NONE, usecols=lambda x: 'Unnamed:' not in x, dtype=str)

    def get_me2l(self, variant=None, ruta_imprimir=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'

        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "me2l"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                # modificacion de variante
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").text = ""
                self.session.findById("wnd[1]/usr/txtENAME-LOW").setFocus()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").caretPosition = 0
                self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

                # busqueda de variante
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT')
                           for r in range(rows)]
                row_match = ls_vari.index(variant)

                # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").currentCellRow = 63
                # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").firstVisibleRow = 46
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte me2l")
                self.session.findById("wnd[1]/tbar[0]/btn[12]").press()
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        try:
            self.session.findById("wnd[0]/tbar[1]/btn[45]").press()
        except Exception as d:
            print("No se encontraron datos para el reporte me2l con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        # self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = "C:\Users\luis.pampa\Documents\SAP\"
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "me2l.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 8
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[6]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        print("Reporte me2l descargado con exito")
        return pd.read_csv(ruta_imprimir + "me2l.txt", encoding='cp1252', skipfooter=0, engine='python',
                           sep="\t", thousands=",", decimal=".", header=1, skipinitialspace=True,
                           quoting=csv.QUOTE_NONE, usecols=lambda x: 'Unnamed:' not in x, dtype=str)

    def get_mb5b(self, ruta_imprimir=None, variant=None, fech_ini=None, fech_fin=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'

        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "mb5b"
        self.session.findById("wnd[0]").sendVKey(0)

        '''
        self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
        self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").currentCellRow = 4
        self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = "4"
        self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
        '''

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT') for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte mb5b")
                self.session.findById("wnd[1]/tbar[0]/btn[12]").press()
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        # self.session.findById("wnd[0]/usr/ctxtMATNR-LOW").text = "9001213"
        if fech_ini is not None:
            self.session.findById("wnd[0]/usr/ctxtDATUM-LOW").text = fech_ini
        if fech_fin is not None:
            self.session.findById("wnd[0]/usr/ctxtDATUM-HIGH").text = fech_fin

        self.session.findById("wnd[0]/usr/ctxtDATUM-HIGH").setFocus()
        self.session.findById("wnd[0]/usr/ctxtDATUM-HIGH").caretPosition = 10

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        try:
            self.session.findById("wnd[0]/mbar/menu[0]/menu[1]/menu[2]").select()
            self.session.findById(
                "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
            self.session.findById(
                "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        except:
            print("No se encontraron datos para el reporte mb5b con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "mb5b.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 8
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[2]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        return pd.read_csv(ruta_imprimir + "mb5b.txt", encoding='cp1252', skipfooter=0, engine='python',
                           sep="\t", thousands=",", decimal=".", header=0, skipinitialspace=True,
                           quoting=csv.QUOTE_NONE, dtype=str)

    def get_coois(self, ruta_imprimir=None, variant=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "COOIS"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                # modificacion de variante
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").text = ""
                self.session.findById("wnd[1]/usr/txtENAME-LOW").setFocus()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").caretPosition = 0
                self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

                # busqueda de variante
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT')
                           for r in range(rows)]
                row_match = ls_vari.index(variant)

                # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").currentCellRow = 63
                # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").firstVisibleRow = 46
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte coois")
                self.session.findById("wnd[1]/tbar[0]/btn[12]").press()
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        try:
            self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
            self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").pressToolbarButton(
                "&NAVIGATION_PROFILE_TOOLBAR_EXPAND")
        except:
            print("No se encontraron datos para el reporte coois con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById("wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(10)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "coois.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print(f"Descarga de COOIS_CABE_PREV completada.")
        time.sleep(5)
        df_n = pd.read_csv(ruta_imprimir + 'coois.txt', sep="\t", encoding='cp1252', decimal='.',
                           thousands=',', skiprows=3, dtype='str')
        return df_n

    def get_zpp_plan11(self, ruta_imprimir=None, variant=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "zpp_plan11"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").text = ""
                self.session.findById("wnd[1]/usr/txtENAME-LOW").setFocus()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").caretPosition = 0
                self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT')
                           for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte zpp_plan11")
                self.session.findById("wnd[1]/tbar[0]/btn[12]").press()
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        try:
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").text = "999999"
            self.session.findById("wnd[1]/usr/txtRS38R-DBACC").caretPosition = 6
            self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        except Exception as d:
            pass

        try:
            self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        except Exception as d:
            print("No se encontraron datos para el reporte zpp_plan11 con los filtros indicados")
            self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
            return None

        # self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "zpp_plan11.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 14
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[0]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        print("Reporte zpp_plan11 descargado con exito")
        return pd.read_csv(ruta_imprimir + "zpp_plan11.txt", encoding='cp1252', skipfooter=0, engine='python',
                           sep="\t", thousands=",", decimal=".", header=3, skipinitialspace=True,
                           quoting=csv.QUOTE_NONE, usecols=lambda x: 'Unnamed:' not in x, dtype=str)

    def get_lx02(self, ruta_imprimir=None, variant=None, codes=None, filename=None):
        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "LX02"
        self.session.findById("wnd[0]").sendVKey(0)

        if variant is not None:
            try:
                self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
                self.session.findById("wnd[1]/usr/txtENAME-LOW").text = ""
                self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
                rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
                ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT') for r in range(rows)]
                row_match = ls_vari.index(variant)
                self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
                self.session.findById("wnd[1]/tbar[0]/btn[2]").press()
            except:
                print("No se tiene la variante ", variant, "para el reporte zrepo_list_mate")
                self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
                return None

        # PEGAR CLIPBOARD
        if codes is not None:
            self.session.findById("wnd[0]/tbar[1]/btn[16]").press()
            self.session.findById(
                "wnd[0]/usr/ssub%_SUBSCREEN_%_SUB%_CONTAINER:SAPLSSEL:2001/ssubSUBSCREEN_CONTAINER2:SAPLSSEL:2000/cntlSUB_CONTAINER/shellcont/shellcont/shell/shellcont[1]/shell").expandNode("         48")
            self.session.findById(
                "wnd[0]/usr/ssub%_SUBSCREEN_%_SUB%_CONTAINER:SAPLSSEL:2001/ssubSUBSCREEN_CONTAINER2:SAPLSSEL:2000/cntlSUB_CONTAINER/shellcont/shellcont/shell/shellcont[1]/shell").selectNode("         51")
            self.session.findById(
                "wnd[0]/usr/ssub%_SUBSCREEN_%_SUB%_CONTAINER:SAPLSSEL:2001/ssubSUBSCREEN_CONTAINER2:SAPLSSEL:2000/cntlSUB_CONTAINER/shellcont/shellcont/shell/shellcont[1]/shell").topNode = "         48"
            self.session.findById(
                "wnd[0]/usr/ssub%_SUBSCREEN_%_SUB%_CONTAINER:SAPLSSEL:2001/ssubSUBSCREEN_CONTAINER2:SAPLSSEL:2000/cntlSUB_CONTAINER/shellcont/shellcont/shell/shellcont[1]/shell").doubleClickNode("         51")
            codes.to_clipboard(index=False, header=False)
            self.session.findById(
                "wnd[0]/usr/ssub%_SUBSCREEN_%_SUB%_CONTAINER:SAPLSSEL:2001/ssubSUBSCREEN_CONTAINER2:SAPLSSEL:2000/ssubSUBSCREEN_CONTAINER:SAPLSSEL:1106/btn%_%%DYN001_%_APP_%-VALU_PUSH").press()

            self.session.findById("wnd[1]/tbar[0]/btn[24]").press()
            self.session.findById("wnd[1]/tbar[0]/btn[8]").press()

        # DAR CLIC EN BUSCAR
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()

        self.session.findById("wnd[0]/tbar[1]/btn[9]").press()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "lx02.txt"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 8
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()
        self.session.findById("wnd[0]/mbar/menu[0]/menu[4]").select()

        if filename is not None:
            os.rename(ruta_imprimir + 'lx02.txt', ruta_imprimir + filename)

        print("Reporte LX02 descargado con éxito desde SAP")

    def get_mm60(self, ruta_imprimir=None, read=False):
        if read:
            df_m = pd.read_csv(root_supply_azz + '/1.- CMN/1.- ORI/8.- MM60/mm60.csv', encoding='cp1252', sep="\t",
                               header=1, thousands=",", decimal='.', low_memory=False, skipinitialspace=True,
                               usecols=lambda x: not (x.__contains__('Unnamed:')),
                               dtype={'Material': str, 'Precio': float, 'por': float})
            return df_m

        if not type(self.session) == win32com.client.CDispatch:
            print("No se ha iniciado sesión")
            return None
        if ruta_imprimir is None:
            ruta_imprimir = 'C:/Users/' + os.getlogin() + '/Downloads/'

        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "mm60"
        self.session.findById("wnd[0]").sendVKey(0)
        # self.session.findById("wnd[0]/usr/ctxtMS_WERKS-LOW").text = "PE01"
        # self.session.findById("wnd[0]/usr/ctxtMS_WERKS-LOW").setFocus()
        # self.session.findById("wnd[0]/usr/ctxtMS_WERKS-LOW").caretPosition = 4
        self.session.findById("wnd[0]").sendVKey(0)
        self.session.findById("wnd[0]").sendVKey(8)
        self.session.findById("wnd[0]/tbar[1]/btn[45]").press()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").setFocus()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = ruta_imprimir.replace("/", "\\")
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "mm60.csv"
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").caretPosition = 8
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        self.session.findById("wnd[0]").sendVKey(3)
        self.session.findById("wnd[0]").sendVKey(3)
        print("Reporte mm60 descargado con éxito desde SAP")


class DataOri:
    """

    See Also
    --------

    Returns
    -------

    See Also
    --------

    Notes
    -----

    Examples
    --------

    """

    def __init__(self, paises, company='AZZ', camp_ini=1, camp_fin=999999, filters=True):
        if not isinstance(paises, list):
            print('El argumento "paises" debe ser una lista.\
                  \nEjem: DataOri(paises=["PER", "COL"])')
            pass

        self.paises = [i.upper() for i in paises]
        self.camp_ini = camp_ini
        self.camp_fin = camp_fin
        self.company = company.upper()
        self.root_company = {'AZZ': root_stats_azz, 'LVV': root_stats_lvv}[company.upper()]
        self.ztpla23 = None
        self.zdm_mat_art = None
        self.stock_consolidated = None
        self.sust = {}
        self.recaudacion = None
        self.filters = filters
        self.con_prd = None
        self.zrepo_list_mate = None
        self.est_cam = None
        self.dim_fct_ayn = None
        self.zplan_seg = None
        self.zpp_profit = None
        self.max_camp = max([int(i.replace("\\", "/").split("/")[-1].split("_")[-1][:6]) for i in
                             glob.glob(f"{self.root_company}/{ruta_paises['PER']}/1.- ORI/2.- MOV_FAC/*[0-9][0-9][0-9][0-9][0-9][0-9].xls")])
        self.root_supply = {'AZZ': root_supply_azz, 'LVV': root_supply_lvv}[company.upper()]
        self.faltante = None
        self.df_pag_cat = None

        '''Companias = {AZZ, LVV}
        Paises = {PER, COL, ECU, BOL, GUA}'''

    def get_sustitutos(self, origen='sharepoint'):
        paises = self.paises

        if origen.upper() == 'SQL':
            paises_cadena = "','".join(paises) if self.company == 'AZZ' else "','".join([pai_lvv[pp] for pp in paises])

            # sust_identicos
            query = f"SELECT DISTINCT CAST(TRIM(codi_padr_sid) AS varchar) AS 'CODI_ORIG', \
                             CAST(TRIM(codi_sust_sid) AS varchar) AS 'CODI_PROD' \
                      FROM [dim_general].[dbo].[dim_sust_iden] \
                      WHERE codi_pais IN ('{paises_cadena}') AND LEN(CAST(TRIM(codi_padr_sid) AS varchar)) >= 6"
            sust_identicos = get_file_sql(query=query)
            self.sust['sust_ide'] = sust_identicos

            # sust_xfaltante
            query = f"SELECT DISTINCT codi_camp AS 'CAMP', CAST(TRIM(codi_prod) AS varchar) AS 'CODI_ORIG', \
                             CAST(TRIM(codi_sust) AS varchar) AS 'CODI_PROD', \
                             CAST(TRIM(codi_pais) AS varchar) AS 'PAIS' \
                      FROM [dwh_azzorti].[mer].[sust_falt] \
                      WHERE codi_pais IN ('{paises_cadena}') AND LEN(CAST(TRIM(codi_prod) AS varchar)) >= 6"
            df_sust_xfalt = get_file_sql(query=query)
            self.sust['sust_xfalt'] = df_sust_xfalt

            # sust_sap
            query = "SELECT DISTINCT CAST(TRIM(po.codi_prod) AS varchar) AS 'CODI_ORIG', \
                            CAST(TRIM(ps.codi_prod) AS varchar) AS 'CODI_PROD' \
                     FROM [dim_general].[dbo].[dim_sustitutos] su \
                          LEFT JOIN [dim_general].[dbo].[dim_producto] po \
                                    ON CAST(TRIM(su.codi_padr) AS varchar) = CAST(TRIM(po.codi_sap) AS varchar) \
                          LEFT JOIN [dim_general].[dbo].[dim_producto] ps \
                                     ON CAST(TRIM(su.codi_prod_sap) AS varchar) = CAST(TRIM(ps.codi_sap) AS varchar) \
                     WHERE LEN(TRIM(po.codi_prod)) >= 6"
            df_sust_sap = get_file_sql(query=query)
            df_sust_sap = df_sust_sap[~df_sust_sap['CODI_PROD'].isnull()]
            df_sust_sap = df_sust_sap.drop(df_sust_sap[(df_sust_sap.CODI_PROD == '434855') &
                                                       (df_sust_sap.CODI_ORIG == '275835')].index, axis=0
                                           ).reset_index(drop=True)
            self.sust['sust_sap'] = df_sust_sap

        else:
            # sust_identicos
            root_sust = root_supply_azz + "/1.- CMN/1.- ORI/6.- SUS_ALL/" + self.company + "/"
            sust_identicos = pd.concat([pd.read_csv(root_sust + x, encoding='cp437', sep='|', dtype=str).iloc[:, [0, 2]]
                                        for x in ['SUSTITUTOS_IDENTICOS.txt', 'SUSTITUTOS_IDENTICOS_old.txt']]
                                       ).drop_duplicates()
            sust_identicos.columns = ["CODI_ORIG", "CODI_PROD"]
            sust_identicos = sust_identicos[sust_identicos['CODI_ORIG'].str.len() >= 6].reset_index(drop=True)
            self.sust['sust_ide'] = sust_identicos

            # sust_xfaltante
            root_sust = root_supply_azz + "/1.- CMN/1.- ORI/6.- SUS_ALL/" + self.company + "/SUST_X_FALT_"
            df_sust_xfalt = pd.concat([pd.read_csv(root_sust + pais + ext, encoding='cp437', sep='|').iloc[:,
                                       [0, 1, 4]].set_axis(['CAMP', 'CODI_ORIG', 'CODI_PROD'], axis='columns').assign(
                                      PAIS=pais) for pais in paises for ext in ['.txt', '_OLD.txt']])
            df_sust_xfalt[['CODI_ORIG', 'CODI_PROD']] = df_sust_xfalt[['CODI_ORIG', 'CODI_PROD']].astype(str)
            df_sust_xfalt = df_sust_xfalt.drop_duplicates()
            df_sust_xfalt = df_sust_xfalt[df_sust_xfalt['CODI_ORIG'].str.len() >= 6].reset_index(drop=True)
            self.sust['sust_xfalt'] = df_sust_xfalt

            # sust_sap
            df_sust_sap = pd.read_csv(root_supply_azz + "/1.- CMN/1.- ORI/6.- SUS_ALL/sustitutos.csv", encoding='cp437',
                                      sep='\t')
            df_sust_sap = df_sust_sap.iloc[:, -2:].astype(str).drop_duplicates()
            df_sust_sap.columns = ["CODI_ORIG", "CODI_PROD"]
            df_sust_sap = df_sust_sap.drop(df_sust_sap[(df_sust_sap.CODI_PROD == '434855') &
                                                       (df_sust_sap.CODI_ORIG == '275835')].index, axis=0)
            df_sust_sap = df_sust_sap[df_sust_sap['CODI_ORIG'].str.len() >= 6].reset_index(drop=True)
            self.sust['sust_sap'] = df_sust_sap

        global d_sust
        d_sust = self.sust.copy()

        print(f'Diccionario "d_sust" creado, con los items {list(self.sust.keys())}.\n')

    def get_con_prd(self):
        """
        :return:
        Extrae el archivo más reciente CON_PRD de acuerdo a la compania. (Sharepoint).
        Los valores se almacenan en el objeto con la variable 'con_prod'. ejemp: __nombre_del_objeto__.con_prd
        """
        if self.filters:
            os.chdir(self.root_company + '/1.- CMN/2.- GEN/1.- CON_PRD')
            file = glob.glob('*CON_PRD*.xls*')
        else:
            os.chdir(self.root_company + '/1.- CMN/1.- ORI/1.- CON_PRD')
            file = glob.glob('*CON_PROD*.xlsx*') + glob.glob('*CONSULTA_PRODUCTOS*.xlsx*')
        assert len(file), "Existe más de un archivo CON_PRD."
        df = pd.read_excel(file[0])
        self.con_prd = df

    def get_ztpla23(self, origen='sharepoint', encoding='UTF-8'):
        """
        Extrae la información del ZTPLA23 de los paises, compania y a partir de la campaña inicial,
        teniendo como origen de datos 'sql', 'ftp', 'sharepoint', si no se ingresa por defecto toma 'sharepoint'
        """
        company = self.company
        camp_ini = self.camp_ini
        paises = self.paises
        paises_cadena = "','".join(paises)
        ztpla = ''
        
        cnv_newlin = pd.DataFrame({
            'LINEA': ['HO HOGAR', 'SIN PROCESAR', 'CH CUIDADO HOGAR', 'PR PREMIOS',
                      'MN MATERIAL DE NEGOCIO', 'CP CUIDADO PERSONAL', 'JO JOYERIA',
                      'NU NUTRICIONALES', 'FR FRAGANCIAS', 'JJ JOHNSON Y JOHNSON',
                      'AL ALIMENTO', 'RE ROPA EXTERIOR', 'RI ROPA INTERIOR', 'AZ ALIANZAS', 'CA CALZADO'],
            'NUEVA_LINEA': ['HOG', 'SPR', 'HOG', 'PRE',
                            'MNG', 'CUP', 'JOY',
                            'NUT', 'FRA', 'CUP',
                            'ALI', 'ROP', 'ROP', 'ALZ', 'CAL']})

        tasas_pais = get_tasas_paises()

        if origen.upper() == 'SQL':

            ztpla = 'ztpla_sql'
            compania = " AND LEFT(tipo,1) = 'Y' " if company == 'LVV' else " AND LEFT(tipo,1) != 'Y' "

            if self.filters:

                query = f"""
                         SELECT pais AS 'PAIS', camp_sid AS 'CAMP', line_vta AS 'LINEA', codi_subne AS 'SUBUEN', 
                                codi_prod_sap AS 'CODI_SAP', codi_prod_sid AS 'CODI_PROD', desc_vari AS 'DESCRIPCION', 
                                desc_gene AS 'REFERENCIA', tota_und AS 'UND_ESTIMADAS', venta_total AS 'VT_ESTIMADA',
                                tipo AS 'Tipo_material', clase AS 'CLASE', codi_vehi AS 'VEHICULO'
                         FROM [dwh_azzorti].[mer].[leader_list] 
                         WHERE codi_vehi IN('NAL', 'NALADV', 'CATVIR', 'ADV') 
                               {compania} AND camp_sid >= {camp_ini} AND pais IN ('{paises_cadena}')
                         """

                df_n = get_file_sql(query=query)

                df_n.loc[:, 'LINEA'] = df_n.merge(cnv_newlin, on='LINEA', how='left').NUEVA_LINEA.values
                df_n.insert(loc=2, column='CLASIF_LIN', value=df_n.merge(cnv_lin, how='left').CLASIF.values)

                df_n.CODI_PROD = df_n.CODI_PROD.astype(str)
                df_n[['VEHICULO', 'CLASE']] = df_n[['VEHICULO', 'CLASE']].astype(str).apply(lambda x: x.str.strip())
                del df_n['Tipo_material']

                df_n['CLASE'] = df_n['CLASE'].astype(clase_ord)
                percol_idx = df_n[df_n['PAIS'].isin(['PER', 'COL'])].index
                paises_idx = df_n[df_n['PAIS'].isin(['BOL', 'GUA', 'ECU'])].index
                df_n.loc[percol_idx, 'VEHICULO'] = df_n.loc[percol_idx, 'VEHICULO'].replace({'CATVIR': 'NALADV'})
                df_n.loc[paises_idx, 'VEHICULO'] = df_n.loc[paises_idx, 'VEHICULO'].replace({'CATVIR': 'NAL'})
                df_n = df_n.sort_values(['PAIS', 'CAMP', 'CLASIF_LIN', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD',
                                         'DESCRIPCION', 'REFERENCIA', 'CLASE', 'VEHICULO'], ignore_index=True)
                df_n = df_n.groupby(['PAIS', 'CAMP', 'CLASIF_LIN', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD',
                                     'DESCRIPCION', 'REFERENCIA', 'VEHICULO'], as_index=False
                                    ).agg({'UND_ESTIMADAS': 'sum', 'VT_ESTIMADA': 'sum', 'CLASE': 'first'})
                df_n['VT_ESTIMADA'] = df_n['VT_ESTIMADA'].astype('float') / df_n.merge(tasas_pais, how='left')[
                    'TC'].values
                df_n['UND_ESTIMADAS'] = df_n['UND_ESTIMADAS'].astype('int64')
                df_n['VALOR_NETO_UNIT'] = df_n['VT_ESTIMADA'] / df_n['UND_ESTIMADAS']
                df_n[['CAMP', 'CODI_SAP']] = df_n[['CAMP', 'CODI_SAP']].astype('int64')
                df_n['CLASE'] = df_n['CLASE'].astype(str)

            else:

                query = f"SELECT * FROM [dwh_azzorti].[mer].[leader_list] WHERE \
                        camp_sid >= {camp_ini} \
                        AND pais in ('{paises_cadena}')"
                df_n = get_file_sql(query=query)

        else:

            ztpla = 'ztpla_23.csv' if company == 'AZZ' else 'BOztpla_23.csv'
            sep = '\t' if company == 'AZZ' else ';'
            decimal = '.' if company == 'AZZ' else ','

            if origen.upper() == 'FTP':
                df_n = get_file_ftp(host_ftp, user_ftp, password_ftp, file='ztpla_23.csv', encoding=encoding, sep=sep,
                                    decimal=decimal, low_memory=False)
            else:
                file = root_supply_azz + "/1.- CMN/1.- ORI/3.- ZTP_L23/" + ztpla
                sep = get_sep(file)
                df_n = pd.read_csv(file, encoding=encoding, sep=sep, decimal=decimal, low_memory=False)

            if company == 'LVV':
                df_n = df_n[df_n["Marca"].isin(["LIVVA"])].reset_index(drop=True)
            else:
                df_n = df_n[(df_n["Marca"] != "LIVVA")].reset_index(drop=True)

            if self.filters:

                df_n = df_n.loc[[i in ['NAL', 'NALADV', 'CATVIR', 'ADV', 'SALE'] for i in df_n.VEHI], :].reset_index(
                    drop=True)
                df_n = df_n.loc[[i in paises for i in df_n.Pais], :].reset_index(drop=True)
                df_n['Campana Sid'] = df_n['Campana Sid'].astype('int')
                df_n = df_n[(df_n['Campana Sid'] >= camp_ini)].reset_index(drop=True)
                df_n = df_n.loc[:, ['Pais', 'Campana Sid', 'LINEA_VENTA', 'Sub_nego', 'Material', 'Codigo Sid',
                                    'Nombre Material', 'Nombre Material', 'Total Unidades', 'VALOR_TOTAL', 'Clase']]

                df_n.columns = ['PAIS', 'CAMP', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD', 'DESCRIPCION', 'REFERENCIA',
                                'UND_ESTIMADAS', 'VT_ESTIMADA', 'CLASE']
                df_n.loc[:, 'LINEA'] = df_n.merge(cnv_newlin, on='LINEA', how='left').NUEVA_LINEA.values
                if self.zdm_mat_art is None:
                    self.get_zdm_mat_art()
                df_n['CODI_PROD'] = df_n['CODI_PROD'].astype('str')
                df_n.insert(loc=2, column='CLASIF_LIN', value=df_n.merge(self.zdm_mat_art, how='left', on=['CODI_PROD']
                                                                         ).CLASIF_LIN.values)
                # df_n.insert(loc=2, column='CLASIF_LIN', value=df_n.merge(cnv_lin, how='left').CLASIF.values)
                df_n.columns = ['PAIS', 'CAMP', 'CLASIF_LIN', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD',
                                'DESCRIPCION', 'REFERENCIA', 'UND_ESTIMADAS', 'VT_ESTIMADA', 'CLASE']
                df_n.CODI_PROD = df_n.CODI_PROD.astype(str)
                df_n["REFERENCIA"] = [i.split(",", 1)[0] for i in df_n["REFERENCIA"].values]

                if isinstance(df_n.loc[0, 'VT_ESTIMADA'], str):
                    df_n['VT_ESTIMADA'] = df_n['VT_ESTIMADA'].apply(lambda x: x.replace(',', '.')).astype('float64')

                df_n['CLASE'] = df_n['CLASE'].astype(clase_ord)
                df_n = df_n.sort_values(['PAIS', 'CAMP', 'CLASIF_LIN', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD',
                                         'DESCRIPCION', 'REFERENCIA', 'CLASE'], ignore_index=True)
                df_n = df_n.groupby(['PAIS', 'CAMP', 'CLASIF_LIN', 'LINEA', 'SUBUEN', 'CODI_SAP', 'CODI_PROD',
                                     'DESCRIPCION', 'REFERENCIA'], as_index=False
                                    ).agg({'UND_ESTIMADAS': 'sum', 'VT_ESTIMADA': 'sum', 'CLASE': 'first'})

                df_n['VT_ESTIMADA'] = df_n['VT_ESTIMADA'] / df_n.merge(tasas_pais, how='left')['TC'].values
                df_n['VALOR_NETO_UNIT'] = df_n['VT_ESTIMADA'] / df_n['UND_ESTIMADAS']
                df_n['CLASE'] = df_n['CLASE'].astype(str)

        print(f'Carga del reporte {ztpla} de "{company}" exitosa.')
        self.ztpla23 = df_n

    def get_zdm_mat_art(self, origen='sharepoint', encoding='UTF-8', sep=None):
        '''Extrae la información del ZDM_MAT_ART de la compania seleccionada .'''
        company = self.company

        zdm_mat_art = 'zdm_mat_art.csv' if company == 'AZZ' else 'BOzdm_mat_art.csv'
        if origen.upper() == 'FTP':
            df_n = get_file_ftp(host_ftp, user_ftp, password_ftp, file='zdm_mat_art.csv', encoding=encoding, sep='\t',
                                low_memory=False)
        else:
            file = f'{root_supply_azz}/1.- CMN/1.- ORI/2.- ZDM_ART/{zdm_mat_art}'
            sep = get_sep(file) if sep is None else sep
            df_n = pd.read_csv(f'{root_supply_azz}/1.- CMN/1.- ORI/2.- ZDM_ART/{zdm_mat_art}',
                               encoding=encoding, sep=sep, low_memory=False)

        if self.filters:
            df_n = df_n.loc[:, ["N_Material_ant", "Material", "nombre_material2", "nombre_material2",
                                "Grupo_art_ext", "Tipo_material"]].astype(str)
            # cnv_lin = pd.read_excel(root_supply_azz + "/1.- CMN/2.- GEN/3.- CLF_MAT/CLF_MAT - copia.xlsx", sheet_name=0)
            df_n.insert(loc=2, column='GRUPO', value=df_n.merge(cnv_lin, on=['Grupo_art_ext', 'Tipo_material'], how='left').LINEA)
            df_n.loc[:, "Grupo_art_ext"] = df_n.merge(cnv_lin, on=['Grupo_art_ext', 'Tipo_material'], how='left').SUBUEN
            df_n["CLASIF_LIN"] = df_n.merge(cnv_lin, on=['Grupo_art_ext', 'Tipo_material'], how='left').CLASIF
            df_n.columns = ["CODI_PROD", "CODI_SAP", "LINEA_NUEVA", "DESCRIPCION", "REFERENCIA", "SUBUEN",
                            "TIPO_MATE", "CLASIF_LIN"]  # renombrar las columnas
            # Agregar multiples filtros
            lin_filter = df_n['LINEA_NUEVA']#.isin(['ROP', 'JOY', 'CAL'])
            # cols_talla = df_n['REFERENCIA'].str.contains("T-") & lin_filter
            # cols_coma = df_n['REFERENCIA'].str.contains(",") & cols_talla
            cols_coma = df_n['REFERENCIA'].str.contains(r'^(?=.*,)(?=.*T-)') & lin_filter
            df_n.loc[cols_coma, "REFERENCIA"] = df_n.loc[cols_coma, "REFERENCIA"].apply(lambda x: x.split(',', 1)[0])
            # cols_espacio = df_n['REFERENCIA'].str.contains(" T-") & ~df_n['REFERENCIA'].str.contains("T- ") & lin_filter
            cols_espacio = df_n['REFERENCIA'].str.contains(r'^(?=.* T-)(?!.*T- )') & lin_filter
            # df_n.loc[cols_espacio, "REFERENCIA"] = [i.rsplit(" ", 1)[0] for i in df_n.loc[cols_espacio, "REFERENCIA"].values]
            df_n.loc[cols_espacio, "REFERENCIA"] = df_n.loc[cols_espacio, "REFERENCIA"].apply(lambda x: x.rsplit(" T", 1)[0])
            df_n = df_n.drop_duplicates('CODI_PROD', ignore_index=True)

        print(f'Carga del reporte "{zdm_mat_art}" de "{company}" exitosa.')
        self.zdm_mat_art = df_n
        return self.zdm_mat_art

    def get_est_cam(self, force=False, filters=False):
        """Devuelve el estimado campana del país seleccionado."""

        if (not force) and isinstance(self.est_cam, pd.DataFrame):
            print('Estimado campana ya se ha cargado y se retornarán los datos actuales.\
                  \nUse el argumento "force=True" si desea cargar los datos nuevamente.')
            return self.est_cam

        df_agg = pd.DataFrame()
        for pais in self.paises:
            os.chdir(self.root_company + "/" + ruta_paises[pais] + "/1.- ORI/3.- EST_CAM")
            # auto_sep = self.company == 'LVV'
            df = get_files_consolidation(pais=pais, camp_ini=self.camp_ini, camp_fin=self.camp_fin, auto_sep=True)
            # Previsional
            if not 'ESTRATEGIA' in df.columns:
                df['ESTRATEGIA'] = np.nan

            df['CODIGO_VENTA'] = df['CODIGO_VENTA'].astype(str)
            df = df[df['CODIGO'].apply(lambda x: x.replace('.', '').isdigit())].reset_index(drop=True)
            df['CODIGO'] = df['CODIGO'].astype(float).astype(int).astype(str)
            # Quitando ruido
            df = df[df.CODIGO.str.isdigit()].reset_index(drop=True)
            df['CAMPANA'] = df['CAMPANA'].astype(int)
            # Estrategias
            if 'ESTRATEGIA' in df.columns.tolist():
                strategies = ['PREPED', 'PREM', 'GRANI1', 'GRANI2', 'GRANI3', 'GRANIV', 'GRATIS', 'MATNEG']
                df = df[~df.ESTRATEGIA.isin(strategies)]
                df = df.drop("ESTRATEGIA", axis=1)

            df = df[[i for i in df.columns if "Unnamed: " not in i]]
            df = df.drop_duplicates().reset_index(drop=True)

            # df_agg = df_agg.append(df)
            df_agg = pd.concat([df_agg, df])

        if filters:
            df_agg = df_agg.loc[:, ['PAIS', 'CAMPANA', 'CODIGO', 'CANTIDAD_ESTIMADA']].reset_index(drop=True)
            df_agg.columns = ['PAIS', 'CAMP', 'CODI_PROD', 'UND_ESTIM']
            df_agg['CAMP'] = df_agg['CAMP'].astype('int')
            df_agg['UND_ESTIM'] = df_agg['UND_ESTIM'].astype('float').astype(int)
            df_agg = apply_orig_cod(df_agg, ~df_agg['CODI_PROD'].isna())
            df_agg = df_agg.groupby(['PAIS', 'CAMP', 'CODI_PROD'], as_index=False)['UND_ESTIM'].agg('sum')

        df_agg = df_agg.reset_index(drop=True)
        self.est_cam = df_agg.reset_index(drop=True)
        print('Datos guardados en la variable "ojecto.est_cam".')
        return df_agg.reset_index(drop=True)

    def get_zona_det(self):
        files = [glob.glob(self.root_company + "/" + ruta_paises[i] + "/4.- ADH/8.- ZNA_DET/*[0-9][0-9][0-9][0-9][0-9][0-9].xlsx") for i in self.paises]
        files = np.array([i for lst in files for i in lst])  # Flat
        files_dates = np.array([int(i.split("/")[-1].split("_")[-1][:6]) for i in files])
        files_indate = np.array((files_dates >= self.camp_ini) & (files_dates <= self.camp_fin))
        files_selected = files[files_indate]
        df_n = pd.concat([pd.read_excel(i, dtype='str') for i in files_selected])
        return df_n

    def get_cons_camp(self):
        files = [glob.glob(self.root_supply + "/" + ruta_paises[i] + "/1.- ORI/6.- CON_CAM/*[0-9][0-9][0-9][0-9][0-9][0-9].txt") for i in self.paises]
        files = np.array([i.replace("\\", "/") for lst in files for i in lst])  # Flat
        # pais_camp_ready = np.array([(i.split("/")[-1][:3])+"_"+(i.split("/")[-1].split("_")[-1][:6]) for i in files])
        # camps = campanitas_total[(campanitas_total >= self.camp_ini) & (campanitas_total <= self.camp_fin) & (campanitas_total <= (datetime.datetime.now().year+1)*100)]
        # pais_camp_needed = [str(c)+"_"+p for c in camps for p in self.paises]
        max_camp_disp = pd.DataFrame({'PAIS': [i.split("/")[7][4:] for i in files],
                                      'CAMP': [(i.split("/")[-1].split("_")[-1][:6]) for i in files]})
        max_camp_disp = max_camp_disp.groupby('PAIS').max()
        max_camp_disp = int(max_camp_disp['CAMP'].min())
        if max_camp_disp < self.max_camp:
            obj_scrap = scrap.App(paises=self.paises, camp_ini=increase_camp_by_n(max_camp_disp, 1),
                                  camp_fin=self.max_camp, campana_cons=False, compania=[self.company])
            obj_scrap.submit()
        files_dates = np.array([int(i.split("/")[-1].split("_")[-1][:6]) for i in files])
        files_indate = np.array((files_dates >= self.camp_ini) & (files_dates <= self.camp_fin))
        files_selected = files[files_indate]

        df_agg = pd.DataFrame()
        for p in self.paises:
            files_pais = [i for i in files_selected if ruta_paises[p] in i]
            df_n = pd.concat([pd.read_csv(i, sep=open(i, "r").read(5)[-1], dtype='str') for i in files_pais])
            df_n.insert(loc=0, column='PAIS', value=p)
            # df_agg = df_agg.append(df_n)
            df_agg = pd.concat([df_agg, df_n])

        df_agg['PEDIDOS'] = df_agg['PEDIDOS'].astype('float64')
        df_agg = df_agg.groupby(['PAIS', 'CAMP', 'ZONA'], as_index=False)['PEDIDOS'].sum()
        return df_agg

    def get_pedidos_x_corte(self):

        df_cons = self.get_cons_camp()
        df_det = self.get_zona_det()
        df_det = df_det[['PAIS', 'CAMP', 'ZONA', 'CORTE']].drop_duplicates().reset_index(drop=True)

        df_n = df_cons.merge(df_det, on=['PAIS', 'CAMP', 'ZONA'], how='left')
        if df_n['CORTE'].isna().sum() > 0:
            print("Se pierden datos porque no se encuentra match en las Zona -> Corte")
        df_n = df_n[~df_n['CORTE'].isna()]
        df_n = df_n.groupby(['PAIS', 'CAMP', 'CORTE'], as_index=False)['PEDIDOS'].sum()
        ### Aux camp
        # self.get_zdm_mat_art(origen='FTP')
        df_aux = df_n.groupby(['PAIS', 'CAMP'], as_index=False)['PEDIDOS'].sum()
        df_aux.to_csv(self.root_supply + f'/1.- CMN/2.- GEN/4.- FLT_ANU/PED_CMP.csv', index=False)
        df_n = df_n.rename(columns={'PEDIDOS': 'PEDIDOS_X_CORTE'})
        # ###
        # paises = self.paises
        # root_company = self.root_company
        # camp_ini = self.camp_ini
        # camp_fin = self.camp_fin
        #
        # all_filenames = []
        # for pais in paises:
        #     os.chdir(root_company)
        #     all_filenames = all_filenames + glob.glob(f"{ruta_paises[pais]}/2.- GEN/0.6.- FCT_CRT_STD/*FCT*.txt")
        #
        # all_filenames = np.array(all_filenames)
        # all_files_camp = np.array(
        #     list(map(lambda x: int(x.split('\\')[-1].strip(ascii_letters + "_.")), all_filenames)))
        # selected_files = all_filenames[(all_files_camp >= camp_ini) & (all_files_camp <= camp_fin)]
        # # LOAD
        # df = pd.concat([pd.read_csv(f, encoding='cp437', sep=',') for f in selected_files])
        return df_n

    def get_falt_anun(self):
        camp_ini = self.camp_ini
        camp_fin = self.camp_fin
        paises_cadena = "','".join(self.paises) if self.company == 'AZZ' else "','".join(
            [pai_lvv[pp] for pp in self.paises])

        cursor.execute(f"SELECT DISTINCT codi_pais, codi_cort, codi_camp, tipo_zona, codi_prod, codi_hijo\
                           FROM\
                               (SELECT dr.*, dc.codi_camp\
                               FROM\
                                    [dim_general].[dbo].[dim_region] dr \
                               INNER JOIN\
                                    [dim_general].[dbo].[dim_campana] dc \
                               ON dr.cons_camp = dc.cons_camp\
                               AND dc.codi_camp >= {camp_ini}\
                               AND dc.codi_camp <= {camp_fin}\
                               AND dr.codi_pais IN ('{paises_cadena}')) rslt \
                           LEFT JOIN\
                           (SELECT codi_prod, codi_hijo, cons_area\
                           FROM [dwh_azzorti].[mer].[falt_anun]) dwh\
                                   ON TRIM(dwh.cons_area) = TRIM(rslt.cons_area)\
                           WHERE codi_prod IS NOT NULL")
        cons_area = pd.DataFrame(cursor.fetchall())
        cons_area = cons_area.applymap(lambda x: x.strip() if isinstance(x, str) else x)

        if cons_area.empty:  # Retorna un DF con las columnas si tenemos errores en la consulta
            # cons_area = pd.DataFrame(columns=['PAIS', 'CAMP', 'CORTE', 'CODI_PROD', 'PR_FALT_ANUN', 'UND_ESTIM', 'LINEA', 'SUBUEN', 'CLASIF_LIN'])
            print('Datos de Faltante Anunciado no disponibles en SQL.')
            cons_area = pd.DataFrame(columns=['codi_pais', 'codi_camp', 'codi_cort', 'codi_hijo'])
            # return cons_area
        df_flt = cons_area[['codi_pais', 'codi_camp', 'codi_cort', 'codi_hijo']].copy()
        df_flt.columns = ['PAIS', 'CAMP', 'CORTE', 'CODI_PROD']
        df_flt = df_flt.drop_duplicates().applymap(lambda x: x.strip() if isinstance(x, str) else x)
        df_flt = df_flt[~df_flt['CODI_PROD'].isna()]

        df_ped_x_corte = self.get_pedidos_x_corte()
        df_ped_x_corte[['CAMP', 'CORTE']] = df_ped_x_corte[['CAMP', 'CORTE']].astype('int64')
        ###############
        df_ped_x_corte['PEDIDOS_CAMP_PAIS'] = df_ped_x_corte.groupby(['CAMP', 'PAIS'], as_index=False)[
            'PEDIDOS_X_CORTE'].transform('sum')
        df_ped_x_corte['PR_FALT_ANUN'] = df_ped_x_corte['PEDIDOS_X_CORTE'] / df_ped_x_corte['PEDIDOS_CAMP_PAIS']

        # Consultar datos de sharepoint para los que no están en SQL y que tenemos información de pedidos_x_corte
        # paises_ped_x_c = df_ped_x_corte.groupby('PAIS', as_index=False)['CAMP'].max()
        # paises_ped_x_c = paises_ped_x_c[paises_ped_x_c['CAMP'] == paises_ped_x_c['CAMP'].max()]['PAIS'].values
        paises_ped_x_c = df_ped_x_corte['PAIS'].unique()
        if len(self.paises) > len(paises_ped_x_c):
            print(
                f'Pedidos por corte incompletos en los paises {np.array(self.paises)[~np.isin(self.paises, paises_ped_x_c)]}')

        paises_flt_anun = df_flt.groupby('PAIS', as_index=False).agg({'CAMP': ['max', 'min']})
        paises_flt_anun = paises_flt_anun[
            (paises_flt_anun[('CAMP', 'max')] >= self.max_camp) & (paises_flt_anun[('CAMP', 'min')] <= self.camp_ini)][
            'PAIS'].unique()
        if len(self.paises) > len(paises_flt_anun):
            print(
                f'Falt_anun incompletos en SQL, paises {np.array(self.paises)[~np.isin(self.paises, paises_flt_anun)]}')

        paises_sp = paises_ped_x_c[~np.isin(paises_ped_x_c, paises_flt_anun)]
        try:
            df_flt_sp = pd.concat(
                [get_files_falt_anun(self.root_company, p, camp_ini=self.camp_ini, camp_fin=self.camp_fin) for p in
                 paises_sp])
        except:
            df_flt_sp = pd.DataFrame()

        df_flt = df_flt[~df_flt['PAIS'].isin(paises_sp)]
        # df_flt = df_flt.append(df_flt_sp).reset_index(drop=True)
        df_flt = pd.concat([df_flt, df_flt_sp], ignore_index=True)
        ###
        df_flt = df_flt.merge(df_ped_x_corte, on=['PAIS', 'CAMP', 'CORTE'], how='left')
        df_flt = df_flt.drop(['PEDIDOS_X_CORTE', 'PEDIDOS_CAMP_PAIS'], axis=1)
        df_flt = df_flt[~df_flt['PR_FALT_ANUN'].isna()]
        df_flt = df_flt.groupby(['PAIS', 'CAMP', 'CORTE', 'CODI_PROD'], as_index=False).agg(
            {'PR_FALT_ANUN': 'sum'})

        df_flt = increase_camp_by_n(df_flt, 1)

        # Juntar con el estimado_cam
        df_est = self.get_est_cam(filters=True)

        df_flt = df_flt.merge(df_est, on=['PAIS', 'CAMP', 'CODI_PROD'], how='left')
        if not isinstance(self.zdm_mat_art, pd.DataFrame):
            self.get_zdm_mat_art()
        df_zdm_aux = self.zdm_mat_art[['CODI_PROD', 'LINEA_NUEVA', 'SUBUEN', 'CLASIF_LIN']].drop_duplicates('CODI_PROD',
                                                                                                            keep='last').reset_index(
            drop=True)
        df_flt = df_flt.merge(df_zdm_aux, on='CODI_PROD', how='left').rename(columns={'LINEA_NUEVA': 'LINEA'})
        df_flt = df_flt[~df_flt['LINEA'].isna()]

        df_flt.to_csv(self.root_supply + f'/1.- CMN/2.- GEN/4.- FLT_ANU/FLT_ANU.csv', index=False)

        return df_flt.reset_index(drop=True)

    def structure_fac(self, df, pais, col_adi=None):
        if col_adi is None:
            col_adi = []
        if 'd_sust' not in globals():
            self.get_sustitutos(origen='sql')
        df_n = structure_fac(df_n=df, pais=pais, col_adi=col_adi)
        return df_n

    def get_fac(self, fac_type=None, with_back_order=False, only_back_order=False, falt_anunc=True, con_tipo_zona=False,
                filter_pag=False):
        """Devuelve la facturación del país seleccionado. Ejemp: "PER"
        Tipos de facturación {TPV}
        Origenes disponibles = {Sharepoint, SQL}
        'fac_type' por defecto devuelve {PER:TPA, otros:MOV}"""

        if fac_type is None:
            raise FacTypeError

        paises = self.paises
        camp_ini = self.camp_ini
        camp_fin = self.camp_fin
        root_company = self.root_company
        company = self.company

        fac_type = fac_type.upper()
        if fac_type not in ['MOV', 'TPA', 'TZO', 'MIX']:
            print(f'"{fac_type}" no es un tipo de factura válido.')
            pass

        df_fac = pd.DataFrame()
        for pais in paises:
            if fac_type == 'MIX':
                os.chdir(root_company + "/" + ruta_paises[pais] + dict_fac_types[d_pais_fac[pais]])
            else:
                os.chdir(root_company + "/" + ruta_paises[pais] + dict_fac_types[fac_type])
            df_n = get_files_consolidation(pais=pais, camp_ini=camp_ini, camp_fin=camp_fin)
            df_n = apply_filters(df_n, back_orders=any([with_back_order, only_back_order]), filter_pag=filter_pag, obj=self)
            if con_tipo_zona:
                if pais in ['PER', 'COL']:
                    try:
                        df_n = self.structure_fac(df=df_n, pais=pais, col_adi=['TIPO_ZONA'])
                    except:
                        df_n = self.structure_fac(df=df_n, pais=pais)
                else:
                    df_n = self.structure_fac(df=df_n, pais=pais)
            else:
                df_n = self.structure_fac(df=df_n, pais=pais)
            df_fac = pd.concat([df_fac, df_n])

        if con_tipo_zona and ('TIPO_ZONA' in df_fac.columns):
            df_fac['TIPO_ZONA'] = df_fac['TIPO_ZONA'].fillna('N')
            df_fac = df_fac.groupby(['PAIS', 'CAMP', 'CODI_PROD'],
                                    as_index=False).agg({'LINEA': 'last',
                                                         'CODI_VENT': 'last',
                                                         'DESCRIPCION': 'last',
                                                         'CODI_CATA': lambda x: "".join(np.unique(str(x))),
                                                         'UND_PEDIDAS': 'sum',
                                                         'UND_FACTURADAS': 'sum',
                                                         'UND_FALTANTES': 'sum',
                                                         'TOTAL_NETO': 'sum',
                                                         'TOTAL_FALTANTE': 'sum',
                                                         'TOTAL_PEDIDOS': 'sum',
                                                         'TIPO_ZONA': lambda x: "".join(np.unique(x)),
                                                         'UNIDAD': 'last'})
        else:
            df_fac = df_fac.groupby(['PAIS', 'CAMP', 'CODI_PROD'],
                                    as_index=False).agg({'LINEA': 'last',
                                                         'CODI_VENT': 'last',
                                                         'DESCRIPCION': 'last',
                                                         'CODI_CATA': lambda x: "".join(np.unique(str(x))),
                                                         'UND_PEDIDAS': 'sum',
                                                         'UND_FACTURADAS': 'sum',
                                                         'UND_FALTANTES': 'sum',
                                                         'TOTAL_NETO': 'sum',
                                                         'TOTAL_FALTANTE': 'sum',
                                                         'TOTAL_PEDIDOS': 'sum',
                                                         'UNIDAD': 'last'})

        # LIMPIAR SIN PROCESAR CON ZDM_MAT_ART
        if self.zdm_mat_art is None:
            self.get_zdm_mat_art()
        print('Variable "zdm_mat_art" creada.')
        df_zdm = self.zdm_mat_art[['CODI_PROD', 'LINEA_NUEVA']].copy().drop_duplicates().reset_index(drop=True)
        # AISLAR BACK ORDERS
        df_bor = df_fac[df_fac.LINEA == 'BACK_ORDER'].drop('UNIDAD', axis=1)
        df_fac = df_fac[df_fac.LINEA != 'BACK_ORDER']
        df_fac_bu = df_fac[df_fac.LINEA != 'BACK_ORDER'].copy()

        try:
            df_fac = df_fac.merge(df_zdm, on='CODI_PROD', how='left')
            df_fac.loc[(~df_fac.LINEA_NUEVA.isna()), 'LINEA'] = df_fac.loc[(~df_fac.LINEA_NUEVA.isna()), 'LINEA_NUEVA']
            df_fac.loc[(df_fac.LINEA == 'SPR') & (~df_fac.UNIDAD.isna()), 'LINEA'] = df_fac.loc[
                (df_fac.LINEA == 'SPR') & (~df_fac.UNIDAD.isna()), 'UNIDAD']
            # LIMPIAR CON LA FACTURACIÓN

            df_codi_spr = df_fac[df_fac.LINEA == 'SPR'][['CODI_PROD']].reset_index(drop=True)
            df_sin_spr = df_fac[df_fac.LINEA != 'SPR'][['CODI_PROD', 'LINEA']].reset_index(drop=True)
            df_codi_spr = df_codi_spr.merge(df_sin_spr, on='CODI_PROD', how='left').fillna('SPR')

            df_codi_aux = df_codi_spr.copy()
            df_codi_aux.insert(loc=2, column='COUNT', value=
            df_codi_spr.merge(df_codi_spr.groupby(['CODI_PROD', 'LINEA'], as_index=False)['LINEA'].count(),
                              on='CODI_PROD', how='left')['LINEA_y'].tolist())
            df_codi_aux = df_codi_aux.sort_values('COUNT', ascending=False)
            df_codi_aux = df_codi_aux.groupby('CODI_PROD', as_index=False).agg({'LINEA': 'first'}).rename(
                columns={'LINEA': 'LINEA_SPR'})

            df_fac = df_fac.merge(df_codi_aux, on='CODI_PROD', how='left')
            df_fac.loc[~df_fac['LINEA_SPR'].isna(), 'LINEA'] = df_fac.loc[~df_fac['LINEA_SPR'].isna(), 'LINEA_SPR']
            df_fac = df_fac.drop(['LINEA_NUEVA', 'LINEA_SPR', 'UNIDAD'], axis=1)
        except:
            print("No se pudo reclasificar las lineas de los productos 'SPR'.")
            df_fac = df_fac_bu

        # df_fac = df_fac.reset_index(drop=True).append(df_bor.reset_index(drop=True))
        df_fac = pd.concat([df_fac.reset_index(drop=True), df_bor.reset_index(drop=True)])

        if falt_anunc:
            try:
                self.get_falt_anun()
                # df_fac = df_fac.merge(df_falt_anun, on=['PAIS', 'CAMP', 'CODI_PROD'], how='left')
            except:
                print('¡¡¡Archivos FALT_ANUN no disponibles!!!')

        if not (with_back_order or only_back_order):
            df_fac = df_fac[df_fac.LINEA != 'BACK_ORDER'].reset_index(drop=True)
            return df_fac

        if fac_type in ['MOV', 'TPA', 'MIX']:
            if only_back_order:
                df_bor = df_fac[df_fac.LINEA == 'BACK_ORDER']
                df_bor = df_bor.rename(columns={'UND_PEDIDAS': 'BO_UND_PEDIDAS',
                                                'UND_FACTURADAS': 'BO_UND_FACTURADAS'})
                return df_bor

            elif with_back_order:
                df_bor = df_fac[(df_fac['LINEA'] == 'BACK_ORDER')].reset_index(drop=True)
                df_fac = df_fac[(df_fac['LINEA'] != 'BACK_ORDER')].reset_index(drop=True)

                df_bor = increase_camp_by_n(df_bor, -1)
                df_bor = df_bor.drop_duplicates().reset_index(drop=True)
                assert df_bor.duplicated().sum() == 0, "Duplicados en la facturación. [get_fac]"
                # GET LINEA AND CODI_CATA
                df_linea = df_fac[['PAIS', 'CAMP', 'LINEA', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION',
                                   'CODI_CATA']].copy().drop_duplicates().reset_index(drop=True)
                df_codicata = df_linea.groupby(['PAIS', 'CAMP', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION'],
                                               as_index=False).agg({'CODI_CATA': 'first'}).reset_index(drop=True)

                df_bor = df_bor.drop(['CODI_CATA', 'LINEA'], axis=1)
                df_bor = df_bor.merge(df_codicata, on=['PAIS', 'CAMP', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION'],
                                      how='left')
                df_bor = df_bor.merge(df_linea,
                                      on=['PAIS', 'CAMP', 'CODI_VENT', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA'],
                                      how='left')

                df_bor = \
                df_bor.groupby(['PAIS', 'CAMP', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA', 'LINEA'], as_index=False)[
                    ['UND_PEDIDAS', 'UND_FACTURADAS']].agg('sum')
                df_bor = df_bor.rename(columns={'UND_PEDIDAS': 'BO_UND_PEDIDAS',
                                                'UND_FACTURADAS': 'BO_UND_FACTURADAS'})

                df_fac = df_fac.merge(df_bor, on=['PAIS', 'CAMP', 'CODI_PROD', 'DESCRIPCION', 'CODI_CATA', 'LINEA'],
                                      how='left')

            else:
                df_fac = df_fac[(df_fac['LINEA'] != 'BACK_ORDER')].reset_index(drop=True)

        # Adicion de columna de clase extraido de leader_list
        compania_filter = " LEFT(tipo,1) = 'Y' " if self.company == 'LVV' else " LEFT(tipo,1) != 'Y' "
        camp_ini = self.camp_ini
        camp_max = df_fac['CAMP'].max()
        paises = self.paises
        paises_cadena = "','".join(paises)
        
        query = f"""SELECT DISTINCT pais AS PAIS, camp_sid AS CAMP, codi_venta AS CODI_VENT, clase AS CLASE
                   FROM [dwh_azzorti].[mer].[leader_list] 
                   WHERE {compania_filter} AND camp_sid >= {camp_ini} AND camp_sid <= {camp_max} AND pais IN ('{paises_cadena}')
                """
        df_query = get_file_sql(query=query)
        # self.df_query = df_query
        df_query['CLASE'] = df_query['CLASE'].astype(str).astype(clase_ord)
        df_query['CAMP'] = df_query['CAMP'].astype(int)
        df_query['CODI_VENT'] = df_query['CODI_VENT'].astype(str)
        df_query = df_query.sort_values(['PAIS', 'CAMP', 'CODI_VENT', 'CLASE'], ignore_index=True)
        df_query = df_query.groupby(['PAIS', 'CAMP', 'CODI_VENT'], as_index=False).agg({'CLASE': 'first'})
        df_query['CLASE'] = df_query['CLASE'].astype(str)
        df_fac['CLASE'] = df_fac.merge(df_query, on=['PAIS', 'CAMP', 'CODI_VENT'], how='left')['CLASE']
        # y = df_fac.groupby(['PAIS', 'CAMP'], as_index=False).agg({'CODI_VENT': 'count', 'CLASE': lambda x: x.isnull().sum()})
        
        return df_fac

    def get_stock_sql(self):
        files = glob.glob(root_supply_azz+r'/4.- ECU/1.- ORI/1.- INV_CAM/*/*_INVENTARIO_CAMPANA*.*',
                          recursive=True)
        files = [i for i in files if re.search(r"_(\d{6}).txt", i)]
        max_camp_stock = np.unique([int(i.split(".")[-2][-6:]) for i in files]).max() if files else 202118
        if not np.where(campanitas_total == self.max_camp)[0][0] > np.where(campanitas_total == max_camp_stock)[0][0]:
            return

        df_stock = self.get_stock_consolidated_v2(origen='sql', camp_ini=increase_camp_by_n(max_camp_stock, 1))
        df_stock = df_stock['inv_base']
        col_obj = [i for i in df_stock.columns if isinstance(df_stock.loc[0, i], str)]
        df_stock[col_obj] = df_stock[col_obj].applymap(lambda x: x.strip())
        df_stock = df_stock.drop_duplicates(['PAIS', 'CAMP', 'CODI_SID']).reset_index(drop=True)
        df_stock = df_stock.groupby(['PAIS', 'CAMP', 'CODI_SID', 'CODI_SAP', 'FECH_INI', 'FECH_FIN'], as_index=False)['STOCK_INI'].sum()
        df_stock['STOCK_INI'] = df_stock['STOCK_INI'].astype('float64')
        df_stock.rename(columns={'FECH_INI': 'FECHA_INICIO', 'FECH_FIN': 'FECHA_FIN', 'STOCK_INI': 'STOCK_INICIO'},
                        inplace=True)

        # Agregar stock fin, tomando el inicio de la campaña siguiente
        df_stock_fin = increase_camp_by_n(df_stock.copy(), -1).rename(columns={'STOCK_INICIO': 'STOCK_FIN'})[
            ['PAIS', 'CAMP', 'CODI_SID', 'STOCK_FIN']]
        df_stock['STOCK_FIN'] = df_stock.merge(df_stock_fin, on=['PAIS', 'CAMP', 'CODI_SID'], how='left')['STOCK_FIN']

        # b = Ld.get_file_sql(query=query1)

        for year in df_stock.CAMP.astype('str').str[:4].unique():
            df_y = df_stock[df_stock.CAMP.astype('str').str[:4] == year].copy()
            for camp in df_y.CAMP.unique():
                df_c = df_y[df_y.CAMP == camp].copy()
                for pais in df_c.PAIS.unique():
                    if pais in ['PER', 'COL']:
                        continue
                    df = df_c[df_c.PAIS == pais].copy().drop('PAIS', axis=1)
                    cur_path = f"{root_supply_azz}/{ruta_paises[pais]}/1.- ORI/1.- INV_CAM/{year}"
                    if not os.path.exists(cur_path):
                        os.mkdir(cur_path)
                    df.to_csv(f"{cur_path}/{pais}_INVENTARIO_CAMPANA_{camp}.txt", index=False, encoding='cp437',
                              sep='\t')

    def get_stock_consolidated(self):
        """
        Consolida la data de stocks que está en la carpeta de suministros.
        El origen para Perú y Colombia es de descargas recurrentes MB51 de SAP.
        El origen de paises es de SQL, transacción de stocks disponibles.
        """
        paises = self.paises
        camp_ini = self.camp_ini
        camp_fin = self.camp_fin

        self.get_stock_sql()

        df_agg = pd.DataFrame()
        for pais in paises:
            os.chdir(root_supply_azz + "/" + ruta_paises[pais] + "/1.- ORI/1.- INV_CAM")
            all_filenames = np.array(glob.glob("*/*INVENTARIO_CAMPANA_**[0-9][0-9].*"))
            all_filenames = np.array([i.replace("\\", '/') for i in all_filenames])
            all_files_camp = np.array(
                list(map(lambda x: int(x.split("/")[-1].strip(ascii_letters + "_.")), all_filenames)))
            selected_files = all_filenames[(camp_fin >= all_files_camp) & (all_files_camp >= camp_ini)]
            df_n = pd.DataFrame()
            for file in selected_files:
                df = pd.read_csv(file, encoding='cp437', sep='\t', dtype='str', usecols=lambda x: not (x.__contains__('Unnamed:')))
                df['CAMP'] = re.findall('\d+', file.split('/')[-1])[0]
                df_n = pd.concat([df_n, df], axis=0, ignore_index=True)
                # print(pais, file)
            df_n.insert(loc=0, column='PAIS', value=pais)
            df_n.columns = df_n.columns.str.strip()
            # CLEAN
            if pais in ['PER', 'COL']:
                df_n = df_n.loc[:, ['PAIS', 'CAMP', 'Material', 'De fecha', 'A fecha', 'Stock inicial',
                                    'Stock de cierre']].astype(str)
                df_n.columns = ['PAIS', 'CAMP', 'CODI_SAP', 'FECHA_INICIO', 'FECHA_FIN',
                                'STOCK_INICIO', 'STOCK_FIN']
            df_n = df_n.apply(lambda x: x.str.strip())
            df_n['CAMP'] = df_n['CAMP'].astype("int64")
            cols = ['STOCK_INICIO', 'STOCK_FIN']
            df_n[cols] = df_n[cols].apply(lambda x: x.str.replace(",", "").astype(float))
            df_n = df_n.loc[:, ['PAIS', 'CAMP', 'CODI_SAP', 'STOCK_INICIO', 'STOCK_FIN']]
            df_n = df_n[~(df_n['CODI_SAP'] == 'nan')]

            df_agg = pd.concat([df_agg, df_n], axis=0, ignore_index=True)

        files = glob.glob(root_supply_azz + "/**/*INVENTARIO_CAMPANA_OLD*", recursive=True)
        df_p = pd.concat([pd.read_csv(i) for i in files])
        df_p = df_p.loc[:, [i for i in df_p.columns if "Unnamed: " not in i]]
        df_p = df_p[~df_p['CODI_SAP'].isna()].reset_index(drop=True)
        df_p['CODI_SAP'] = df_p['CODI_SAP'].astype('int64').astype('str')

        df_agg = pd.concat([df_agg, df_p], axis=0, ignore_index=True)
        self.stock_consolidated = df_agg

    def get_payment_collection(self):
        """
        Extrae el reporte de recaudación
        A partir de la campaña inicial indicada, y para los paises indicados
        :return:
        """
        camp_ini = self.camp_ini
        camp_fin = self.camp_fin
        paises = self.paises
        paises_cadena = "','".join(paises)

        query = "SELECT t1.codi_camp, t1.codi_pais, t1.dias_pago, t1.val_ing,\
                       SUM(t1.val_ing) OVER(PARTITION BY t2.cons_camp ORDER BY t1.dias_pago) as val_ing_acum,\
                       SUM(t1.val_ing) OVER(PARTITION BY t2.cons_camp ORDER BY t1.dias_pago)/t2.val_doc as rec_acum\
                       FROM\
                       (SELECT\
                       rd.camp_fact, dc.codi_camp, dc.codi_pais, rd.dias_pago, SUM(rd.valo_ingr) as val_ing\
                       FROM [dwh_azzorti].[fina].[hechos_recau_dia] as rd\
                       LEFT JOIN\
                       [dim_general].[dbo].[dim_campana] as dc \
                       ON dc.cons_camp = rd.camp_fact \
                        WHERE rd.dias_pago <= 1500 \
                        AND dc.codi_camp >= {} \
                        AND dc.codi_camp <= {} \
                        AND dc.codi_pais IN ('{}')\
                       GROUP BY rd.camp_fact, dc.codi_camp, dc.codi_pais, rd.dias_pago) as t1\
                       LEFT JOIN\
                       (SELECT ec.cons_camp, sum(ec.valo_docu) as val_doc \
                       FROM [dwh_azzorti].[fina].[hechos_edad_cartera] as ec\
                       GROUP BY ec.cons_camp) as t2 \
                       ON t1.camp_fact = t2.cons_camp\
                       ORDER BY t1.codi_pais, t1.codi_camp, t1.dias_pago".format(camp_ini, camp_fin, paises_cadena)
        cursor.execute(query)
        df_recaud = pd.DataFrame(cursor.fetchall())
        print('Carga del reporte de recaudación exitosa.')
        self.recaudacion = df_recaud

    def get_stock_consolidated_v2(self, origen='sql', camp_ini=None):
        if camp_ini is None:
            camp_ini = self.camp_ini
        paises = self.paises
        camp_fin = self.camp_fin

        if origen == 'sql':
            if self.filters:
                paises_cadena = "','".join(paises)

                query1 = f"""SELECT bs.pais AS 'PAIS', 
                                   nv.pais AS 'PAIS_DES',
                                   bs.campana AS 'CAMP', 
                                   bs.fecha_ini AS 'FECH_INI', bs.fecha_fin AS 'FECH_FIN', 
                                   nv.codi_prod AS 'CODI_SID', nv.codi_sap AS 'CODI_SAP',
                                   bs.fech_m AS 'FECH_M', 

                                   SUM(nv.stock + nv.ctrl + nv.trasl) AS 'STOCK_INI', 
                                   SUM(nv.comp_impo + nv.trans_impo + nv.sin_nac + nv.naci) AS 'OIMP',
                                   SUM(nv.comp_naci) AS 'OCOM'
                            FROM
                                (
                                SELECT ca.codi_pais AS pais, ca.codi_camp AS campana, ca.fech_inic AS fecha_ini, 
                                       ca.fech_fina AS fecha_fin, MAX(iv.fech_dia) AS fech_m 
                                FROM [dim_general].[dbo].[dim_campana] ca LEFT JOIN [inv].[base_inventarios] iv 
                                     ON ca.codi_pais = iv.pais_orig AND ca.fech_inic >= iv.fech_dia 
                                WHERE ca.codi_pais IN ('{paises_cadena}') AND ca.codi_camp >= {camp_ini} AND ca.codi_camp <= {camp_fin} AND 
                                       (iv.pais = iv.pais_orig OR iv.pais = 'DPR')
                                GROUP BY ca.codi_pais,  ca.codi_camp, ca.fech_inic, ca.fech_fina
                                ) bs
                            LEFT JOIN [inv].[base_inventarios] nv 
                                ON bs.pais = nv.pais_orig AND bs.fech_m = nv.fech_dia
                            WHERE nv.pais = bs.pais
                            GROUP BY bs.pais, bs.campana, bs.fecha_ini, bs.fecha_fin, 
                                   nv.codi_prod, nv.codi_sap,
                                   bs.fech_m, 
                                   nv.pais
                        """

                query1 = f"""SELECT nv.pais_orig AS 'PAIS_DES', nv.fech_dia as FECHA_INI,
                                                   nv.codi_prod AS 'CODI_SID', nv.codi_sap AS 'CODI_SAP',

                                                   SUM(nv.stock + nv.ctrl + nv.trasl) AS 'STOCK_INI',
                                                   SUM(nv.comp_impo + nv.trans_impo + nv.sin_nac + nv.naci) AS 'OIMP',
                                                   SUM(nv.comp_naci) AS 'OCOM'
                                            FROM [inv].[base_inventarios] nv
                                            WHERE nv.pais_orig IN ('ECU', 'BOL', 'GUA')
                                                AND nv.fech_dia >= '20230330'
                                                AND nv.fech_dia <= '20230330'
                                            GROUP BY nv.codi_prod, nv.codi_sap, nv.pais_orig, nv.fech_dia
                                            """

                # print("Cambiar las fechas/input de campañas del stock.")

                cursor.execute(query1)
                df_prb1 = pd.DataFrame(cursor.fetchall())
                # cursor.execute(query2)
                # df_prb2 = pd.DataFrame(cursor.fetchall())
                # self.stock_consolidated = df_prb
                dict = {'inv_base': df_prb1}  #, 'inv_det': df_prb2}
            else:
                paises_cadena = "','".join(paises)

                query1 = "SELECT ca.codi_camp AS campana, iv.* \
                            FROM [inv].[base_inventarios] iv LEFT JOIN [dim_general].[dbo].[dim_campana] ca \
                                ON ca.codi_pais = iv.pais_orig AND ca.fech_inic <= iv.fech_dia AND ca.fech_fina >= iv.fech_dia \
                            WHERE ca.codi_pais IN ('{}') AND ca.codi_camp >= {} AND ca.codi_camp <= {} \
                            ".format(paises_cadena, camp_ini, camp_fin)

                query2 = "SELECT ca.codi_camp AS campana, iv.* \
                            FROM [inv].[deta_inve] iv LEFT JOIN [dim_general].[dbo].[dim_campana] ca \
                                ON ca.codi_pais = iv.pais_orig AND ca.fech_inic <= iv.fecha AND ca.fech_fina >= iv.fecha \
                            WHERE ca.codi_pais IN ('{}') AND ca.codi_camp >= {} AND ca.codi_camp <= {} \
                         ".format(paises_cadena, camp_ini, camp_fin)

                cursor.execute(query1)
                df_prb1 = pd.DataFrame(cursor.fetchall())
                cursor.execute(query2)
                df_prb2 = pd.DataFrame(cursor.fetchall())
                # self.stock_consolidated = df_prb
                dict = {'inv_base': df_prb1, 'det_inv': df_prb2}
        else:
            dict = None

        return dict

    def get_zrepo_list_mate(self, origen='sharepoint', force=None, ultima_lista=False):
        """
            Con esta función se obtiene el reporte de ZREPO_LIST_MATE, que tiene el desagredado de
            materiales por componentes.
            :param
                origen: puede ser 'sharepoint' (si no se especifica) o 'sql'
                force: en caso se necesite descargar el actual reporte de SAP de ZREPO_LIST_MATE de SAP colocar 'True',
                       en caso no se necesite colocar 'False',
                       si no se especifica, descargará el actual reporte de SAP en caso la ultima actualización en el
                       compartido sea anterior a la fecha actual
            :return:
                retorna un dataframe en self.zrepo_list_mate
        """
        if origen.upper() == 'SQL':
            if self.filters:
                query = "SELECT TRIM(mt.codi_prod_sap) AS 'Material', \
                                 CASE \
                                    WHEN zm.desc_corta IS NULL THEN TRIM(zmg.desc_corta) \
                                    ELSE TRIM(zm.desc_corta) \
                                 END AS 'Texto breve de material', \
                                 TRIM(mt.codi_comp_sap) AS 'Componente', \
                                 TRIM(mt.nomb_comp_sap) AS 'Texto breve de componente', \
                                 CAST(mt.cant_base_com AS float) AS 'Cantidad base', \
                                 CAST(ROUND(mt.cant_comp_sap / mt.cant_base_com,5) AS float) AS 'Calculado', \
                                 CAST(mt.cant_comp_sap AS float) AS 'Cantidad', \
                                 TRIM(mt.UM_comp) AS 'UM', \
                                 TRIM(mt.UM_mate) AS 'UMB', \
                                 CASE \
                                    WHEN zc.codi_prod IS NULL THEN TRIM(mt.codi_comp_sap) \
                                    ELSE TRIM(zc.codi_prod) \
                                 END AS 'Componente antiguo', \
                                 CASE \
                                    WHEN zm.codi_prod IS NULL THEN TRIM(mt.codi_prod_sap) \
                                    ELSE TRIM(zm.codi_prod) \
                                 END AS 'Material antiguo', \
                                 TRIM(mt.list_mate_sap) AS 'LMatAl' \
                             FROM [dim_general].[dbo].[dim_mate_comp_sap] mt \
                                 LEFT JOIN [dim_general].[dbo].[dim_producto] zc ON mt.codi_comp_sap = zc.codi_sap \
                                 LEFT JOIN [dim_general].[dbo].[dim_producto] zm ON mt.codi_prod_sap = zm.codi_sap \
                                 LEFT JOIN \
                                    (SELECT * FROM (SELECT ROW_NUMBER() OVER(PARTITION BY codi_gene_sap \
                                     ORDER BY desc_corta DESC ) AS r, codi_gene_sap, desc_corta \
                                     FROM [dim_general].[dbo].[dim_producto] ) AS r WHERE r.r=1)\
                                    zmg \
                                    ON mt.codi_prod_sap = zmg.codi_gene_sap \
                             "
            else:
                query = "SELECT * FROM [dim_general].[dbo].[dim_mate_comp_sap]"
            df_zz = get_file_sql(query=query)

        else:
            root_zrlm = root_supply_azz + "/1.- CMN/1.- ORI/1.- ZRL_MAT/"
            if (force is True) | ((force is None) & (get_modification_date(
                    (root_zrlm + 'zrepo_list_mate.txt').replace('/', '\\')) != datetime.date.today())):
                obj_sap = SapConnection()
                obj_sap.login(user=user_sap, password=password_sap, conn=con_sap)
                obj_sap.get_zrepo_list_mate(ruta_imprimir=root_zrlm)
                obj_sap.close_session()

            df_zz = pd.read_csv(root_zrlm + 'zrepo_list_mate.txt', encoding='cp1252',
                                sep="\t", thousands=",", decimal=".", low_memory=False, header=3, skipinitialspace=True,
                                quoting=csv.QUOTE_NONE, usecols=lambda x: not (x.__contains__('Unnamed:')),
                                dtype={'Material': str, 'Componente': str, 'NºMaterial antiguo': str, 'Cantidad': float,
                                       'NºMaterial antiguo.1': str, 'Cantidad base': float, 'Calculado': float,
                                       'AlPro': str, 'Lista mat.': str, 'CatVa': str})
            if self.filters:
                df_zz = df_zz[~df_zz['Material'].isnull()].drop_duplicates(ignore_index=True)
                df_zz.columns = ['Material', 'Texto breve de material', 'Componente', 'Texto breve de componente',
                                 'Cantidad base', 'Calculado', 'Cantidad', 'Centro', 'UtlLMat', 'AlPro', 'TpP', 'Pos',
                                 'UM', 'UMB', 'Componente antiguo', 'Material antiguo', 'CatVa', 'Lista mat', 'LMatAl']

        if ultima_lista:
            df_zz = df_zz.sort_values(['LMatAl', 'Calculado'], ascending=[False, True]).drop_duplicates(
                ['Material', 'Componente'], keep='first')
        self.zrepo_list_mate = df_zz

    def get_dim_fct_ayn(self):
        if self.company == 'AZZ':
            df_zz = pd.read_csv(root_stats_azz + '/1.- CMN/2.- GEN/8.- TAB_EGL/0.- SID_REG_EGLLVE/DIM_FCT_AYN.csv',
                                low_memory=False)
        else:
            df_zz = pd.read_csv(root_stats_lvv + '/1.- CMN/2.- GEN/2.- TAB_EGL/DIM_FCT_AYN.csv', low_memory=False)

        df_zz = df_zz.loc[[i in self.paises for i in df_zz['pais']], :].reset_index(drop=True)
        df_zz['camp'] = df_zz['camp'].astype('int')
        df_zz = df_zz[(df_zz['camp'] >= self.camp_ini) & (df_zz['camp'] <= self.camp_fin)].reset_index(drop=True)

        self.dim_fct_ayn = df_zz

    def get_zplan_seg(self):
        """
            Con esta función se obtiene el reporte de ZPLAN_SEG del dwh_azzorti, filtrado por las campañas y paises del
            objeto, en caso 'filters' sea True, filtrará las ULTI_VER.
            :param
                None
            :return:
                retorna un dataframe en self.zplan_seg
        """

        paises_cadena = "','".join(self.paises) if self.company == 'AZZ' else "','".join(
            [pai_lvv[pp] for pp in self.paises])

        if self.filters:
            query = f"SELECT zz.PAIS, CAST(zz.CAMP AS bigint) AS 'CAMP', zz.LINEA, zz.SUBUEN, zz.PEDIDOS, \
                             CAST(zz.PPP_PED AS float) AS 'PPP_PED', CAST(zz.UPP AS float) AS 'UPP', ss.codi_nego \
                          FROM [dwh_azzorti].[inv].[zplan_seg] zz LEFT JOIN [dim_general].[dbo].[dim_subnego] ss \
                               ON zz.SUBUEN = ss.subb_nego \
                          WHERE zz.ULTI_VER ='X' AND CAST(zz.CAMP AS bigint) >= {self.camp_ini} \
                                AND zz.PAIS IN ('{paises_cadena}') \
                                "
        else:
            query = f"SELECT * \
                          FROM [dwh_azzorti].[inv].[zplan_seg] \
                          WHERE CAST(CAMP AS bigint) >= {self.camp_ini} AND PAIS IN ('{paises_cadena}')"
        con = pymssql.connect(
            host='23.98.176.101',
            user='user_modelos',
            password='Azzorti1956*',
            database='dwh_azzorti'
        )
        cur = con.cursor(as_dict=True)
        cur.execute(query)
        df_zz = pd.DataFrame(cur.fetchall())
        con.close()
        self.zplan_seg = df_zz

    def get_zpp_profit(self, force=None, layout=None):
        """
            Con esta función se obtiene el reporte de ZPP_PROFIT.
            :param
                force: en caso se necesite descargar el actual reporte de SAP de ZPP_PROFIT de SAP colocar 'True',
                       en caso no se necesite colocar 'False',
                       si no se especifica, descargará el actual reporte de SAP en caso la ultima actualización en el
                       compartido sea anterior a la fecha actual
            :return:
                retorna un dataframe en self.zpp_profit
        """
        layout = '/NCB' if layout is None else layout
        root_zprf = root_supply_azz + "/1.- CMN/1.- ORI/10.- ZPP_PRF/"

        if (force is True) | ((force is None) & (
                get_modification_date((root_zprf + 'zpp_profit.txt').replace('/', '\\')) != datetime.date.today())):
            obj_sap = SapConnection()
            obj_sap.login(user=user_sap, password=password_sap, conn=con_sap)
            obj_sap.get_zpp_profit(ruta_imprimir=root_zprf, variant='/PRB_ZPP')
            obj_sap.close_session()

        df_zz = pd.read_csv(root_zprf + 'zpp_profit.txt', encoding='cp1252', skipfooter=1, engine='python',
                            sep="\t", thousands=",", decimal=".", header=3, skipinitialspace=True,
                            quoting=csv.QUOTE_NONE, usecols=lambda x: not (x.__contains__('Unnamed:')),
                            dtype={'Fecha': str, 'Temporada': str, 'Total_Pedidos': float, 'UMP': str,
                                   'Pedi_Sumi.': float, 'Version': str, 'Campaña': int,
                                   'Pais': str, 'Livva.': str})

        # Filtro pais
        df_zz = df_zz[df_zz['Pais'].isin(self.paises)]
        # Filtro campana
        df_zz = df_zz[df_zz['Campaña'] >= self.camp_ini]
        df_zz = df_zz[df_zz['Campaña'] <= self.camp_fin]
        # Filtro compania
        if self.company == 'AZZ':
            df_zz = df_zz[df_zz['Livva.'] != 'X']
        else:
            df_zz = df_zz[df_zz['Livva.'] == 'X']

        if self.filters:
            df_zz['Fecha'] = pd.to_datetime(df_zz['Fecha'], dayfirst=True).dt.date
            df_zz.rename(columns={'Pedi_Sumi.': 'Pedi_Sumi', 'Campaña': 'Camp', 'Livva.': 'Compania'}, inplace=True)
            df_zz['Compania'] = np.where(df_zz['Compania'] == 'X', 'LVV', 'AZZ')
            df_zz.sort_values(['Fecha'], ascending=False, inplace=True)
            df_zz = df_zz.groupby(['Camp', 'Pais', 'Compania'], as_index=False).agg({'Pedi_Sumi': 'first',
                                                                                     'Fecha': 'first'})
        self.zpp_profit = df_zz

    def get_stock_per_day(self, fech_ini, fech_fin=None):
        """
        Con esta función se obtiene el stock de los paises del objeto por rango de dias indicados con fuente de datos
        SQL
        :param:
            fech_ini: texto, fecha de inicio de consulta del stock (obligatorio) en formato yyyy-mm-dd
            fech_fin: texto, fecha de fin de consulta del stock (opcional) en formato yyyy-mm-dd

        :return: retorna un dataframe con el inventario en el rango de dias indicado
        :example: obj.get_stock_per_day(fech_ini='2022-05-29', fech_fin='2022-06-02')
        """
        if fech_ini is None:
            print("Se debe indicar una fecha de referencia")
            return None
        paises_cadena = "','".join(self.paises)
        q_fech = f" = '{fech_ini}'" if fech_fin is None else f" >= '{fech_ini}' AND fech_dia <= '{fech_fin}'"
        query = f"SELECT * \
                 FROM [dwh_azzorti].[inv].[base_inventarios] \
                 WHERE pais_orig IN ('{paises_cadena}') AND fech_dia {q_fech}"
        return get_file_sql(query=query)

    def get_pedidos_x_camp(self):
        df_e = self.get_est_cam(filters=False, force=True)
        df_e = df_e[df_e['TIPO_ZONA'].isin(['A', 'N'])].reset_index(drop=True)
        # df_e = df_e[['CAMPANA', 'PAIS', 'TIPO_ZONA', 'PEDIDOS']].drop_duplicates()
        df_e['PEDIDOS'] = df_e['PEDIDOS'].astype(float)
        df_e = df_e.groupby(['CAMPANA', 'PAIS', 'TIPO_ZONA'], as_index=False).agg({'PEDIDOS': lambda x: x.unique().sum()})

        df_e2 = df_e.groupby(['CAMPANA', 'PAIS'], as_index=False).agg({'PEDIDOS': lambda x: x.unique().sum(),
                                                                       'TIPO_ZONA': lambda x: "".join(np.unique(np.sort(x)))})
        df_e2 = df_e2[df_e2.TIPO_ZONA == 'AN'].reset_index(drop=True)
        df_e = pd.concat([df_e, df_e2], ignore_index=True)
        df_e.rename(columns={'CAMPANA': 'CAMP'}, inplace=True)

        '''
        dict_path_dtp = {'AZZ': '/1.- CMN/2.- GEN/8.- TAB_EGL/0.- SID_REG_EGLLVE/DIM_TOT_PED.csv',
                         'LVV': '/1.- CMN/2.- GEN/2.- TAB_EGL/DIM_TOT_PED.csv'}
        df_dim_tot_ped = pd.read_csv(f"{self.root_company}{dict_path_dtp[self.company]}")
        '''
        # PROVISIONAL DE PEDIDOS REALES
        paises = self.paises
        camp_ini = self.camp_ini
        camp_fin = self.camp_fin
        root_company = self.root_company
        fac_type = 'MIX'
        col_keep = {'PER': ['PEDIDOS'],
                    'COL': ['PEDIDOS'],
                    'ECU': ['TOTAL_PEDIDO'],
                    'BOL': ['TOTAL_PEDIDOS'],
                    'GUA': ['TOTAL_PEDIDO']}

        df_fac = pd.DataFrame()
        for pais in paises:
            if fac_type == 'MIX':
                os.chdir(root_company + "/" + ruta_paises[pais] + dict_fac_types[d_pais_fac[pais]])
            else:
                os.chdir(root_company + "/" + ruta_paises[pais] + dict_fac_types[fac_type])

            df_n = get_files_consolidation(pais=pais, camp_ini=camp_ini, camp_fin=camp_fin)
            if pais in ['ECU', 'GUA', 'BOL']:
                df_n['TIPO_ZONA'] = 'N'
            df_n = df_n[['PAIS', 'CAMP', 'TIPO_ZONA']+col_keep[pais]].drop_duplicates().reset_index(drop=True)
            df_n = df_n.rename(columns={col_keep[pais][0]: 'PEDIDOS'})
            df_fac = df_fac.append(df_n)
        df_fac['PEDIDOS'] = df_fac['PEDIDOS'].astype(float)

        df_f = df_fac.groupby(['CAMP', 'PAIS', 'TIPO_ZONA'], as_index=False).agg({'PEDIDOS': lambda x: x.unique().sum()})
        df_f2 = df_fac.groupby(['CAMP', 'PAIS'], as_index=False).agg({'PEDIDOS': lambda x: x.unique().sum(),
                                                                      'TIPO_ZONA': lambda x: "".join(
                                                                          np.unique(np.sort(x)))})
        df_f2 = df_f2[df_f2.TIPO_ZONA == 'AN'].reset_index(drop=True)

        df_f = pd.concat([df_f, df_f2], ignore_index=True)
        df_f['CAMP'] = df_f['CAMP'].astype(int)
        df_e['CAMP'] = df_e['CAMP'].astype(int)
        df_e = df_f.merge(df_e, on=['CAMP', 'PAIS', 'TIPO_ZONA'], how='left', suffixes=['_REAL', '_ESTIM'])
        return df_e

    def get_pag_cat(self):
        dir_ori = os.getcwd()
        df_pag_cat = pd.DataFrame()
        for pais in self.paises:
            path_pag_cat = f"{self.root_company}/{ruta_paises[pais]}/1.- ORI/10.- PAG_CTL/"
            os.chdir(path_pag_cat)
            all_filenames = np.array(glob.glob("*[0-9][0-9][0-9][0-9][0-9][0-9].xls"))
            all_files_camp = np.array(list(map(lambda x: int(x.strip(ascii_letters + "_.")), all_filenames)))
            selected_files = all_filenames[(self.camp_fin >= all_files_camp) & (all_files_camp >= self.camp_ini)]
            x = pd.concat([pd.read_csv(f, sep='\t', encoding='cp437').assign(CAMP=int(f.strip(ascii_letters + "_."))) for f in selected_files]).assign(PAIS=pais)
            df_pag_cat = pd.concat([df_pag_cat, x], ignore_index=True)
        df_pag_cat['CODIGO_VENT'] = df_pag_cat['CODIGO_VENT'].astype(str).str.strip()
        # df_pag_cat['CODIGO_PRODUCTO'] = df_pag_cat['CODIGO_PRODUCTO'].astype(str).str.strip()
        df_pag_cat = df_pag_cat.drop_duplicates(ignore_index=True)
        df_pag_cat = df_pag_cat.rename(columns={'NUMERO_PAGINA': 'N', 'PAGINA_ADVANCE': 'A'})
        df_pag_cat = df_pag_cat.melt(id_vars=['PAIS', 'CAMP', 'CODIGO_VENT'], value_vars=['N', 'A'],
                                     var_name='TIPO_ZONA', value_name='PAG')
        df_pag_cat = df_pag_cat[~df_pag_cat['PAG'].isnull()].reset_index(drop=True)
        df_pag_cat = df_pag_cat.drop_duplicates(ignore_index=True)
        df_pag_cat = df_pag_cat[df_pag_cat['PAG'] > 0].reset_index(drop=True)
        df_pag_cat['PAG'] = np.where(df_pag_cat['PAG'] < 400, 'S', 'N')
        df_pag_cat = df_pag_cat.sort_values(['PAG'], ascending=False, ignore_index=True)
        df_pag_cat = df_pag_cat.groupby(['PAIS', 'CAMP', 'CODIGO_VENT', 'TIPO_ZONA'], as_index=False).agg({'PAG': 'first'})
        # df_pag_cat = df_pag_cat[df_pag_cat['PAG'].str.len() == 1].reset_index(drop=True)
        os.chdir(dir_ori)
        self.df_pag_cat = df_pag_cat


def get_exec_query(host, user, password, database, query=None):
    conn = pymssql.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )
    cursor = conn.cursor(as_dict=True)
    cursor.execute(query)
    df_zz = pd.DataFrame(cursor.fetchall())
    conn.close()
    return df_zz


def get_file_sql(host='23.98.176.101', user='user_modelos', password='Azzorti1956*', database='dwh_azzorti', query=None,
                 table=None, ruta_imprimir=None):
    """
    Funcion que permite ejecutar una consulta en base de datos SQL SERVER
    :param host: host de la base de datos, por defecto '23.98.176.101
    :param user: usuario para acceder a la base de datos, por defeto 'user_modelos'
    :param password: contraseña para acceder a la base de datos, por defecto 'Azzorti1956*'
    :param database: nombre de la base de datos que se desea consultar por defecto 'dwh_azzorti'
    :param query: sentencia SQL para consultar datos
    :param table: nombre de la tabla a la que se desea consultar los datos
    :param ruta_imprimir: ruta de la carpeta en donde se dese descargar en formato la tabla o sentencia consultada
    :return: dataframe con los datos consultados
    """
    if query is not None:
        df_q = get_exec_query(host, user, password, database, query)
    elif table is not None:
        df_q = get_exec_query(host, user, password, database, query=f"SELECT * FROM {table}")
    else:
        return
    if ruta_imprimir is not None:
        name = table.rsplit(".[")[-1][:-1] if table is not None else 'query'
        df_q.to_csv(f'{ruta_imprimir}/{name}.csv', index=False)
        print(f"Archivo '{name}' descargado con éxito.")
    return df_q


def get_all_tables_sql():
    """
    Funcion para obtener todas las tablas de las bases de datos de SQL de dim_general y dwh_azzorti
    :return: Dataframe con el listado de tablas
    """
    q = "SELECT [Tables].name AS Tabla, \
                CONCAT('[',DB_NAME(),'].[',SCHEMA_NAME(schema_id),'].[',[Tables].name,']') AS 'Tabla_Ubi', \
                [Tables].max_column_id_used AS 'Columnas', SUM([Partitions].[rows]) AS 'Filas' \
         FROM sys.tables AS [Tables] \
              JOIN sys.partitions AS [Partitions] \
              ON [Tables].[object_id] = [Partitions].[object_id] \
              AND [Partitions].index_id IN ( 0, 1 ) \
         GROUP BY SCHEMA_NAME(schema_id), [Tables].name, [Tables].max_column_id_used ORDER BY [Tables].name"
    return pd.concat([get_file_sql(database=x, query=q) for x in ['dwh_azzorti', 'dim_general']], axis=0,
                     ignore_index=True)


def get_dim_prod(codes):
    """
    Funcion que retorna los campos de CODIGO_SAP, UEN, SUBUEN, TIPO_ARTICULO
    :param codes: lista de CODIGOS SID (CODI_PROD)
    :return: dataframe
    """
    l1k = [codes[i:i + 1000] for i in range(0, len(codes), 1000)]
    list_codes_j = """');
                      INSERT INTO #temp_producto (CODI_PROD)
                      VALUES ('""".join(["'), ('".join(c) for c in l1k])
    query = f"""
            USE dim_general

            CREATE TABLE #temp_producto
            (CODI_PROD VARCHAR(50))

            INSERT INTO #temp_producto (CODI_PROD)
            VALUES ('{list_codes_j}');

            SELECT tp.CODI_PROD , pr.codi_sap AS 'CODI_SAP',  LEFT(pr.id_subnego,3) AS 'UEN', 
                   RIGHT(pr.id_subnego,3) AS 'SUBUEN', pr.tipo_arti AS 'TIPO_ARTI', pr.desc_compl AS 'DESCRIPCION',
                   pr.desc_corta AS 'REFERENCIA'
            FROM #temp_producto tp LEFT JOIN [dim_general].[dbo].[dim_producto] pr
            ON tp.CODI_PROD = pr.codi_prod
            """
    df = get_file_sql(query=query).drop_duplicates('CODI_PROD', ignore_index=True)
    return df


def get_cost_hist(df_camp_cod_sid):
    """
    Esta funcion retorna los datos de la tabla fina.costos para una cierta cantidad de codigos dados por campaña y pais
    :param df_camp_cod_sid: Dataframe que contiene las columnas 'PAIS', 'CAMP', 'CODI_PROD'. Ejm: 'PER', 202218, '53433'
    :return: Retorna el mismo dataframe con las columnas de costos, en donde el costo es el costo histórico mas cercano
    del cierre de la campaña del pais ingresado
    """
    # df_camp_cod_sid = pd.read_clipboard(sep='\t')
    df_camp_cod_sid = df_camp_cod_sid.drop_duplicates(ignore_index=True)
    for c in ['PAIS', 'CODI_PROD']:
        df_camp_cod_sid[c] = df_camp_cod_sid[c].astype(str).str.strip()
    df_camp_cod_sid['CAMP'] = df_camp_cod_sid['CAMP'].astype(int)

    paises = df_camp_cod_sid['PAIS']
    camps = df_camp_cod_sid['CAMP']
    codes = df_camp_cod_sid['CODI_PROD']
    l1k = [zip(paises[i:i + 1000], camps[i:i + 1000], codes[i:i + 1000]) for i in range(0, len(codes), 1000)]
    l1k = [[f"'{pais}', {camp}, '{code}'" for pais, camp, code in z] for z in l1k]

    list_codes_j = """);
                      INSERT INTO #temp_costos (PAIS, CAMP, CODI_PROD)
                      VALUES (""".join(["), (".join(c) for c in l1k])

    query = f"""
            USE dwh_azzorti

            CREATE TABLE #temp_costos
            (PAIS VARCHAR(5), CAMP INT, CODI_PROD VARCHAR(20))

            INSERT INTO #temp_costos (PAIS, CAMP, CODI_PROD)
            VALUES ({list_codes_j});

            SELECT bs.PAIS, bs.CAMP, bs.CODI_PROD, bs.FECH_INI_CAMP, bs.FECH_FIN_CAMP, bs.fech_cier_costos,
                    ct.*
            FROM
                (
                    SELECT tp.PAIS AS PAIS, tp.CAMP AS CAMP, tp.CODI_PROD AS CODI_PROD, ca.fech_inic AS FECH_INI_CAMP, 
                           ca.fech_fina AS FECH_FIN_CAMP, MAX(co.fech_cier) AS fech_cier_costos
                    FROM #temp_costos tp LEFT JOIN [dim_general].[dbo].[dim_campana] ca 
                         ON tp.CAMP = ca.codi_camp AND tp.PAIS = ca.codi_pais
                         LEFT JOIN [dwh_azzorti].[fina].[costos] co
                         ON ca.codi_pais = co.codi_pais AND tp.CODI_PROD = co.codi_prod AND ca.fech_fina >= co.fech_cier 
                    WHERE co.codi_pais IS NOT NULL
                    GROUP BY tp.PAIS, tp.CAMP, tp.CODI_PROD, ca.fech_inic, ca.fech_fina
                ) bs
            LEFT JOIN [dwh_azzorti].[fina].[costos] ct
            ON bs.PAIS = ct.codi_pais AND bs.CODI_PROD = ct.codi_prod AND bs.fech_cier_costos = ct.fech_cier
            """

    df_r = get_file_sql(query=query)

    df_camp_cod_sid_res = df_camp_cod_sid.merge(df_r, on=['PAIS', 'CAMP', 'CODI_PROD'], how='left')
    df_camp_cod_sid_res = df_camp_cod_sid_res[df_camp_cod_sid_res['fech_cier_costos'].isnull()][
        ['PAIS', 'CAMP', 'CODI_PROD']].drop_duplicates(ignore_index=True)
    if len(df_camp_cod_sid_res) > 0:
        paises = df_camp_cod_sid_res['PAIS']
        camps = df_camp_cod_sid_res['CAMP']
        codes = df_camp_cod_sid_res['CODI_PROD']
        l1k = [zip(paises[i:i + 1000], camps[i:i + 1000], codes[i:i + 1000]) for i in range(0, len(codes), 1000)]
        l1k = [[f"'{pais}', {camp}, '{code}'" for pais, camp, code in z] for z in l1k]

        list_codes_j = """);
                          INSERT INTO #temp_costos (PAIS, CAMP, CODI_PROD)
                          VALUES (""".join(["), (".join(c) for c in l1k])

        query = f"""
                USE dwh_azzorti
    
                CREATE TABLE #temp_costos
                (PAIS VARCHAR(5), CAMP INT, CODI_PROD VARCHAR(20))
    
                INSERT INTO #temp_costos (PAIS, CAMP, CODI_PROD)
                VALUES ({list_codes_j});
    
                SELECT bs.PAIS, bs.CAMP, bs.CODI_PROD, bs.FECH_INI_CAMP, bs.FECH_FIN_CAMP, bs.fech_cier_costos,
                        ct.*
                FROM
                    (
                        SELECT tp.PAIS AS PAIS, tp.CAMP AS CAMP, tp.CODI_PROD AS CODI_PROD, ca.fech_inic AS FECH_INI_CAMP, 
                               ca.fech_fina AS FECH_FIN_CAMP, MIN(co.fech_cier) AS fech_cier_costos
                        FROM #temp_costos tp LEFT JOIN [dim_general].[dbo].[dim_campana] ca 
                             ON tp.CAMP = ca.codi_camp AND tp.PAIS = ca.codi_pais
                             LEFT JOIN [dwh_azzorti].[fina].[costos] co
                             ON ca.codi_pais = co.codi_pais AND tp.CODI_PROD = co.codi_prod AND ca.fech_fina <= co.fech_cier 
                        WHERE co.codi_pais IS NOT NULL
                        GROUP BY tp.PAIS, tp.CAMP, tp.CODI_PROD, ca.fech_inic, ca.fech_fina
                    ) bs
                LEFT JOIN [dwh_azzorti].[fina].[costos] ct
                ON bs.PAIS = ct.codi_pais AND bs.CODI_PROD = ct.codi_prod AND bs.fech_cier_costos = ct.fech_cier
                """

        df_r_res = get_file_sql(query=query)
    else:
        df_r_res = pd.DataFrame()

    df_r = pd.concat([df_r, df_r_res], ignore_index=True)

    df_r = df_r.sort_values(['PAIS', 'CAMP', 'CODI_PROD', 'centro', 'tipo_valo']).drop_duplicates(
        ['PAIS', 'CAMP', 'CODI_PROD'], keep='first', ignore_index=True)

    df_camp_cod_sid = df_camp_cod_sid.merge(df_r, on=['PAIS', 'CAMP', 'CODI_PROD'], how='left')
    return df_camp_cod_sid

