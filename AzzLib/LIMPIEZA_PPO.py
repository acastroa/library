# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 12:39:49 2021

@author: liamd
"""
from functools import reduce
import pandas as pd
import numpy as np
import os
import re
import glob
from string import ascii_letters

name_user = os.getlogin()
root_modelos = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/PUBLIC - STATS - Documentos/6.- DAT"
root_stats = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/STATS - 1.- DTS"
root_planos = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/PUBLIC - STATS - Documentos/3.- PPO"
root_suministros = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Data"


paises_uno = ["1.- PER", "2.- COL"]
paises_dos = ["3.- ECU", "5.- GUA"]
paises_dos = ["3.- ECU", "4.- BOL", "5.- GUA"]
linea_ruta = "1.- ROE"
linea_ruta = "2.- ROI"

def comparar_matseg_antdos(pais_ahora = paises_uno + paises_dos,
                           linea_ahora = ['1.- ROE', '2.- ROI', '4.- JOY'],
                           anticipos=['1.1.- ADV_NAC', '1.2.- MAT_SEG', '2.1.- PRE_NAC', '3.1.- PED_NAC','1.1.- ANT_UNO','1.5.- ANT_UPC',"2.1.- ANT_DOS"],
                           name = 'Observaciones'):
    anticipos1 = ['1.1.- ADV_NAC', '1.2.- MAT_SEG', '2.1.- PRE_NAC', '3.1.- PED_NAC']
    anticipos1 = [i for i in anticipos if i in anticipos1]
    cols = np.array(['COMPANIA', 'PAIS', 'LINEA', 'MODELO', 'VERSION', 'TIPO_ZONA_DESTINO',
       'CAMP_ORIGEN', 'CAMP_DESTINO', 'CODI_VENT', 'DESCRIPCION','LIM_INF',
       'Q_10', 'Q_20', 'Q_30', 'Q_40', 'Q_50', 'Q_60', 'Q_70', 'Q_80', 'Q_90',
       'LIM_SUP', 'Q_RG3', 'TOT_PED_EST', 'Q_MKT','TOT_PED_MKT', 'Q_REAL',
       'Q_FALT', 'TOT_PED_REAL', 'FALT_ANUN', 'PERCENTIL_ELEGIDO', 'COMENTARIO'])
    qs = ['LIM_INF','Q_10', 'Q_20', 'Q_30', 'Q_40', 'Q_50',
          'Q_60', 'Q_70', 'Q_80', 'Q_90','LIM_SUP']
    linea_dict = {'1.- ROE':'RE ROPA EXTERIOR',
                  '2.- ROI':'RI ROPA INTERIOR',
                  '4.- JOY':'JO JOYERIA'}
    
    dict_df = {}
    dict_obs = {}
    for anticipo in anticipos1:
        anticipo_nomb = re.sub('[^a-zA-Z+_]+', '', anticipo)
        all_filenames_now = []
        
        if anticipo == '1.2.- MAT_SEG':
            anticipo_nomb = 'MATSEG'
            
        df_total = pd.DataFrame()
        df_obs = pd.DataFrame(columns=['columns_sob','columns_fal','pais','modelo','linea','tipo_zona','campana','Q_s'])
        for pais in pais_ahora:
            for linea in linea_ahora:
                os.chdir(root_planos + "/" + pais + "/" + linea + "/" + anticipo)
                all_filenames_now = [pais + "/" + linea + "/" + anticipo + "/" + x for x in glob.glob("*.xlsx")]
                for f in all_filenames_now:
                    os.chdir(root_planos)
                    df = pd.read_excel(f)
                    qs_lst = []
                    try:
                        camp_file = int(re.split("_",f)[-1].strip(ascii_letters+"._"))
                    except:
                        print("Camañana no reconocida: "+f)
                        continue

                    try:
                        assert all(df.columns == cols)
                        if df.duplicated(['COMPANIA', 'PAIS', 'LINEA', 'MODELO', 'VERSION', 'TIPO_ZONA_DESTINO', 'CAMP_ORIGEN', 'CAMP_DESTINO', 'DESCRIPCION']).any():
                            df_obs.loc[f, 'duplicados'] = 'DUPLICADDOS'
                        df_t = df[(df['Q_50'] > 50) & ((df['Q_10'] - df['LIM_INF']) > 5)].copy()
                        df_t['CODI_VENT'] = df_t['CODI_VENT'].astype('str')
                        if (df_t['Q_50']/(df_t['Q_10'])).max() > 20:
                            df_obs.loc[f, 'ratio'] = ", ".join(df_t[(df_t['Q_50'] / df_t['Q_10']) > 20].CODI_VENT)
                        if (df['PAIS'].unique()[0] != pais[-3:]) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'pais'] = df['PAIS'].unique()[0]
                        if (df['MODELO'].unique()[0] != anticipo_nomb) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'modelo'] = df['MODELO'].unique()[0]
                        if (df['LINEA'].unique()[0] != linea_dict[linea]) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'linea'] = df['LINEA'].unique()[0]
                        if df['TIPO_ZONA_DESTINO'].unique()[0] != 'N':
                            df_obs.loc[f,'tipo_zona'] = df['TIPO_ZONA_DESTINO'].unique()[0]
                        if (df['CAMP_DESTINO'].iloc[0] != camp_file) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'campana'] = df['CAMP_DESTINO'].iloc[0]
                        for q in qs:
                            if np.isnan(df[q].iloc[0]):
                                qs_lst.append(q)
                        df_obs.loc[f,'Q_s'] = ", ".join(qs_lst)
                            
                    except:
                        cols_sobrantes = df.columns[~np.isin(df.columns, cols)]
                        cols_contenidas = df.columns[np.isin(df.columns, cols)]
                        cols_faltantes = cols[~np.isin(cols, df.columns)]
                        if cols_sobrantes.tolist() == cols_faltantes.tolist(): #Orden de columnas
                            df_obs.loc[f,'comentario'] = 'Orden de columnas'
                        df = df[cols_contenidas]
                        df = pd.concat([df, pd.DataFrame(columns = cols_faltantes)], axis = 1)
                            
                        df_obs.loc[f,'columns_sob'] = ", ".join(cols_sobrantes)
                        df_obs.loc[f,'columns_fal'] = ", ".join(cols_faltantes)
                        print('Diferentes columnas: '+f)
                        
                    df.insert(loc=df.shape[1], column = 'DIR', value = f)
                    df_total = df_total.append(df)
                    
        dict_df[anticipo] = df_total
        dict_obs[anticipo] = df_obs
        exec("df_"+anticipo_nomb.lower() +'=' "dict_df["+"'"+anticipo+"'"+"]")
        exec("df_obs_"+anticipo_nomb.lower() +'=' "dict_obs["+"'"+anticipo+"'"+"].reset_index()")
# =============================================================================
    #CMN
    anticipos2 = ['1.1.- ANT_UNO','1.5.- ANT_UPC',"2.1.- ANT_DOS"]
    anticipos2 = [i for i in anticipos if i in anticipos2]
    pais_ant = {'1.1.- ANT_UNO':paises_uno,'1.5.- ANT_UPC':paises_dos,"2.1.- ANT_DOS":paises_uno+paises_dos}
    
    
    for anticipo in anticipos2:
        pais_cmn = pais_ant[anticipo]
        if ('4.- JOY' in linea_ahora):
            if len(linea_ahora) > 1:
                print("Ejecutar la función con Joyería y Ropa por separado")
                break
            pais_cmn = pais_ahora
        
        anticipo_nomb = re.sub('[^a-zA-Z+_]+', '', anticipo)
        all_filenames_now = []
        df_total = pd.DataFrame()
        df_obs = pd.DataFrame(columns=['columns_sob','columns_fal','pais','modelo','linea','tipo_zona','campana','Q_s'])
        for pais in pais_cmn:
            for linea in linea_ahora:
                os.chdir(root_planos + "/0.- CMN/" + linea + "/" + anticipo + "/" + pais)
                all_filenames_now = [linea + "/" + anticipo + "/" + pais + "/" + x for x in glob.glob("*.xlsx")]
                
                for f in all_filenames_now:
                    os.chdir(root_planos + "/0.- CMN/")
                    df = pd.read_excel(f)
                    qs_lst = []
                    try:
                        camp_file = int(re.split("_",f)[-1].strip(ascii_letters+"._"))
                    except:
                        continue
                    
                    try:
                        assert all(df.columns == cols)
                        if df.duplicated(['COMPANIA', 'PAIS', 'LINEA', 'MODELO', 'VERSION', 'TIPO_ZONA_DESTINO', 'CAMP_ORIGEN', 'CAMP_DESTINO', 'DESCRIPCION']).any():
                            df_obs.loc[f, 'duplicados'] = 'DUPLICADDOS'
                        df_t = df[(df['Q_50'] > 50) & ((df['Q_10'] - df['LIM_INF']) > 5)].copy()
                        df_t['CODI_VENT'] = df_t['CODI_VENT'].astype('str')
                        if (df_t['Q_50'] / (df_t['Q_10'])).max() > 20:
                            df_obs.loc[f, 'ratio'] = ", ".join(df_t[(df_t['Q_50'] / df_t['Q_10']) > 20].CODI_VENT)
                        if (df['PAIS'].unique()[0] != pais[-3:]) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'pais'] = df['PAIS'].unique()[0]
                        if (df['MODELO'].unique()[0] != anticipo_nomb) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'modelo'] = df['MODELO'].unique()[0]
                        if (df['LINEA'].unique()[0] != linea_dict[linea]) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'linea'] = df['LINEA'].unique()[0]
                        if df['TIPO_ZONA_DESTINO'].unique()[0] != 'N':
                            df_obs.loc[f,'tipo_zona'] = df['TIPO_ZONA_DESTINO'].unique()[0]
                        if (df['CAMP_DESTINO'].iloc[0] != camp_file) or (df['LINEA'].unique().shape[0] > 1):
                            df_obs.loc[f,'campana'] = df['CAMP_DESTINO'].iloc[0]
                            
                        for q in qs:
                            if np.isnan(df[q].iloc[0]):
                                qs_lst.append(q)
                        df_obs.loc[f, 'Q_s'] = ", ".join(qs_lst)
                    except:
                        cols_sobrantes = df.columns[~np.isin(df.columns, cols)]
                        cols_contenidas = df.columns[np.isin(df.columns, cols)]
                        cols_faltantes = cols[~np.isin(cols, df.columns)]
                        if cols_sobrantes.tolist() == cols_faltantes.tolist(): #Orden de columnas
                            df_obs.loc[f, 'comentario'] = 'Orden de columnas'
                        df = df[cols_contenidas]
                        df = pd.concat([df, pd.DataFrame(columns=cols_faltantes)], axis=1)
                            
                        df_obs.loc[f, 'columns_sob'] = ", ".join(cols_sobrantes)
                        df_obs.loc[f, 'columns_fal'] = ", ".join(cols_faltantes)
                        print('Diferentes columnas: '+f)
                    df.insert(loc=df.shape[1], column='DIR', value=f)
                    df_total = df_total.append(df)
                    
        dict_df[anticipo] = df_total
        dict_obs[anticipo] = df_obs
        exec("df_"+anticipo_nomb.lower() +'=' "dict_df["+"'"+anticipo+"'"+"]")
        exec("df_obs_"+anticipo_nomb.lower() +'=' "dict_obs["+"'"+anticipo+"'"+"].reset_index()")


    writer = pd.ExcelWriter(fr'C:\Users\{name_user}\OneDrive\Documentos\{name}.xlsx')
    for i in dict_obs.keys():
        dict_obs[i].to_excel(writer, i[-7:])
    writer.save()
            
    return [dict_df, dict_obs]

# obs = comparar_matseg_antdos()
obs_joy = comparar_matseg_antdos(pais_ahora=paises_uno + paises_dos,
                                 linea_ahora=['1.- ROE'],
                                 anticipos=['1.1.- ADV_NAC', '2.1.- PRE_NAC', '1.2.- MAT_SEG'],
                                 name='Observaciones_roe2')
