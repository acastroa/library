"""
Created on Sat Nov  6 18:43:38 2021

@author: Monica
"""
# -*- coding: utf-8 -*-

# %% LIBRERIAS
import os
import pandas as pd
# %pip install selenium
# %pip install chromedriver-autoinstaller==0.3.1
from selenium import webdriver
import time
import pandas as pd
import os
from pathlib import Path
import glob
# from selenium.webdriver.chrome.options import Options
import chromedriver_autoinstaller
import os
from pathlib import Path
import shutil

import tkinter as tk


# from tkinter import tk

# os.chdir(str(Path.home() / "Downloads"))

class App(tk.Tk):

    def __init__(self):

        super().__init__()
        self.title('SELENA')
        # self.subtitle('LIVVA')
        self.geometry("500x120")
        # self.configure(background='#747f91')

        # self.var_pais = tk.StringVar()
        self.var_usuario = tk.StringVar()
        self.var_contrasena = tk.StringVar()

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        # self.columnconfigure(2, weight=1)

        self.create_widgets()

    def create_widgets(self):

        padding = {'padx': 5, 'pady': 5}

        # label
        tk.Label(self, text='Generador:').grid(column=0, row=0, **padding)
        # tk.Label(self, text='Ruta exportar:').grid(column=0, row=1, **padding)

        # # OptionMenu: Paises
        # pais_optionmenu = tk.OptionMenu(self,
        #                                  self.var_pais,
        #                                  self.list_paises[0],
        #                                  *self.list_paises)

        # pais_optionmenu.grid(column=1, row=0, **padding)

        # Entry: Usuario
        usuario_entry = tk.Entry(self,
                                 textvariable=self.var_usuario)
        usuario_entry.grid(column=1, row=0, **padding)
        usuario_entry.focus()

        # Entry: Contrasena
        # contrasena_entry = tk.Entry(self,
        #                              textvariable = self.var_contrasena)
        # contrasena_entry.grid(column=1, row=1, **padding)
        # contrasena_entry.focus()

        # Button
        submit_button = tk.Button(self, text='GENERAR', command=self.submit)
        submit_button.grid(column=2, row=0, **padding)

        # Output label
        self.output_pais_label = tk.Label(self)
        self.output_pais_label.grid(column=0, row=3, columnspan=3, **padding)

    def submit(self):

        " ---------------- AQUI ------------------------------"
        import numpy as np
        from random import random
        nombre_ruta = "pruebachromedriver7" + str(random())
        # %%% ya creados
        # PARAMETROS
        os.chdir(self.var_usuario.get())
        requisitos = pd.read_excel("REQUISITOS.xlsx", sheet_name="REQUISITOS")
        requisitos = requisitos.sort_values(by=["COMPANIA", "PAIS"])
        requisitos["CAMPANA"] = [i[:-2] if i[-2:] == ".0" else i for i in requisitos["CAMPANA"].apply(str)]

        # WEB SCRAPPING
        usuario = requisitos["USUARIO"][0:1][0]
        contrasena = str(requisitos["CONTRASENA"][0:1][0])

        time_to_export = 10  # segundos
        time_to_charge = 6
        # %% LIBRERIAS

        # %% PARAMETROS PREDEFINIDOS

        os.chdir(str(Path.home() / "Downloads"))
        # list_subfolders = [name for name in os.listdir(".") if os.path.isdir(name) and name == nombre_ruta]
        # list_subfolders = [name for name in os.listdir("/chromedriver") if os.path.isdir(name) and name == "*pruebachromedriver"]

        time.sleep(5)

        # if len(list_subfolders) == 1:
        #     shutil.rmtree(str(Path.home() / "Downloads" / nombre_ruta))
        # else:
        #     ""

        try:
            files = glob.glob("pruebachromedriver*")

            for i in files:
                print(i)
                shutil.rmtree(str(Path.home() / "Downloads" / i))
        except:
            print("no hay")

        os.makedirs("./" + nombre_ruta)
        os.chdir(str(Path.home() / "Downloads" / nombre_ruta))

        chromedriver_autoinstaller.install(cwd=True)  # Check if the current version of chromedriver exists
        # and if it doesn't exist, download it automatically,
        # then add chromedriver to path

        # %%%% definiendo compania pais
        dict_urls = {"LVV-PER": "https://intranet.livva.pe/desarrollo/reportes/",
                     "AZZ-PER": "https://intranet.dupree.pe/desarrollo/reportes/",
                     "AZZ-BOL": "https://intranet.azzorti.bo/desarrollo/reportes/index.php",
                     "AZZ-ECU": "https://intranet.azzorti.com/desarrollo/reportes/",
                     "AZZ-COL": "https://intranet.dupree.co/desarrollo/reportes/",
                     "AZZ-GUA": "https://intranet.azzorti.gt/desarrollo/reportes/"}

        arr_comp = requisitos.COMPANIA.unique()

        # %%% COMPANIA
        for compania in arr_comp:
            print(compania)
            # compania = "AZZ"
            # filtrar los de solo una compania
            df_requisitos_0 = requisitos[requisitos["COMPANIA"] == compania]
            arr_pais = df_requisitos_0.PAIS.unique()
            # filtrar los de solo un pais
            # %%% PAIS
            for pais in arr_pais:
                print(pais)
                # pais= "BOL"
                df_requisitos = df_requisitos_0[df_requisitos_0["PAIS"] == pais]

                compania_pais = compania + "-" + pais
                print(dict_urls[compania_pais])
                # FILTRAR SOLO UN REPORTE

                # %%% REPORTES
                reporte_estimado = df_requisitos[df_requisitos.REPORTE == "ESTIMADO_CAMPANA"]

                if len(reporte_estimado) != 0:
                    print(reporte_estimado.REPORTE.reset_index(drop=True)[0])

                    chrome_options = webdriver.ChromeOptions()
                    prefs = {'download.default_directory': str(Path.home() / "Downloads" / nombre_ruta)}
                    # prefs = {'download.default_directory' : ruta_to_export}
                    chrome_options.add_experimental_option('prefs', prefs)

                    browser = webdriver.Chrome(
                        str(Path.home() / "Downloads" / nombre_ruta / os.listdir(".")[0] / "chromedriver.exe"),
                        chrome_options=chrome_options)

                    # browser.minimize_window()

                    if compania == "LVV":
                        url_text = browser.get("https://intranet.livva.pe/desarrollo/reportes/")

                    else:
                        url_text = browser.get("https://intranet.dupree.co/desarrollo/reportes/")
                    # browser.minimize_window()
                    " AQUI"
                    time.sleep(time_to_charge)
                    if compania == "LVV":
                        browser.find_elements_by_id('botonint')[0].click()
                        time.sleep(time_to_charge)
                    else:
                        print("no es livva")

                    time.sleep(time_to_charge)
                    # iniciar sesion
                    browser.find_elements_by_name('usua')[0].send_keys(usuario)
                    browser.find_elements_by_name("clav")[0].send_keys(contrasena)
                    browser.find_elements_by_name('autenticar')[0].click()
                    time.sleep(time_to_charge)
                    "AQUI"

                else:
                    "no es estimado"

                # i = "FACTURA_ASESORA"

                # %%% CAMPAÑAS

                try:
                    campanas = reporte_estimado.CAMPANA.unique()
                    for camp_i in campanas:
                        print(camp_i)
                        campanita = camp_i

                        df_requisitos_con_estimado = reporte_estimado[reporte_estimado["CAMPANA"] == campanita]

                        if len(reporte_estimado) != 0:
                            print(reporte_estimado.REPORTE.reset_index(drop=True)[0])

                            report = reporte_estimado.REPORTE.reset_index(drop=True)[0]

                            "AQUI"
                            # %%%% REPORTE ESPECIFICADO

                            browser.find_element_by_xpath('//input[@value="' + report + '"]').click()
                            time.sleep(5)

                            # %%%% CAMPANA

                            try:
                                campana = browser.find_elements_by_name("_g_codi_camp")[0]
                                campana.clear()
                                campana.send_keys(campanita)
                            except:
                                print("no tiene campana")

                            try:
                                campana = browser.find_elements_by_name("_g_camp_orig")[0]
                                campana.clear()
                                campana.send_keys(campanita)
                            except:
                                print("no tiene campana")

                            try:
                                campana = browser.find_elements_by_name("_g_CODI_CAMP")[0]
                                campana.clear()
                                campana.send_keys(campanita)
                            except:
                                print("no tiene campana")

                            time.sleep(time_to_charge)

                            try:
                                pais_path = browser.find_elements_by_id("_g_codi_pais")[0]
                                pais_path.clear()
                                pais_path.send_keys(pais)
                            except:
                                print("no tiene pais")

                            time.sleep(time_to_charge)

                            # %%%% exportar
                            print("exportando")
                            browser.find_elements_by_name('tipo_sali')[1].click()
                            time.sleep(time_to_export)

                            # browser.find_elements_by_class_name('btn1')[0].click()

                            browser.find_elements_by_xpath("//input[@value='(G)enerar']")[0].click()

                            time.sleep(time_to_export)

                            try:
                                # EXPORTAR
                                time.sleep(time_to_export)
                                browser.find_element_by_xpath("//a[@href='" + usuario + "_" + report + ".xls']").click()
                                time.sleep(time_to_export)
                                # VOLVER A REPORTES
                                print("retornando")
                                regresar = \
                                browser.find_elements_by_xpath("//input[@value='Volver a Lista De Reportes']")[
                                    0].click()

                            except:
                                print("es Col o Gua o Ecu")
                                time.sleep(time_to_export)
                                # %%% REGRESAR
                                print("retornando")
                                regresar = browser.find_elements_by_xpath("//input[@value='(R)etornar']")[0]
                                time.sleep(time_to_charge)
                                regresar.click()

                            # "AQUI"
                            # print("importando")
                            # os.chdir(str(Path.home() / "Downloads"/ nombre_ruta))
                            # reporte_name = glob.glob("*" + report +".xls")
                            # time.sleep(time_to_charge)
                            # reporte_des = pd.read_csv(reporte_name[0], sep = "\t")
                            # os.remove(reporte_name[0])
                            # ruta_to_export = df_requisitos_con_estimado["RUTA_EXPORTAR"].reset_index(drop=True)[0]
                            # os.chdir(ruta_to_export)
                            # reporte_des.to_csv( pais+ "_"+ report + "_"+  str(campanita) + '.xls', sep = "\t")
                            # time.sleep(time_to_charge)

                            import shutil
                            time.sleep(time_to_charge)
                            print("copiando")
                            ruta_import = str(Path.home() / "Downloads" / nombre_ruta)
                            # ruta_import =r"C:\Users\monica.huaman\35159_147728_DUPREE_VENTA_DIRECTA_S_A\LVV - STATS - Documentos\6.- TIM\8.- MNC\7.- COD_PYT\3.- WEB_SCR\1.- DSH_LVV\DATOS\ORI"
                            os.chdir(ruta_import)
                            reporte_name = glob.glob("*" + report + ".xls")
                            # ruta_to_export = r"C:\Users\monica.huaman\35159_147728_DUPREE_VENTA_DIRECTA_S_A\LVV - STATS - Documentos\6.- TIM\8.- MNC\7.- COD_PYT\3.- WEB_SCR\1.- DSH_LVV\DATOS\ORI\mover"
                            ruta_to_export = ruta_to_export = \
                            df_requisitos_con_estimado["RUTA_EXPORTAR"].reset_index(drop=True)[0]
                            reporte_name_new = pais + "_" + report + "_" + str(campanita) + '.xls'
                            shutil.move(ruta_import + "//" + reporte_name[0],
                                        ruta_to_export + "//" + reporte_name_new)


                except:
                    print("")

                if len(reporte_estimado) != 0:
                    print(reporte_estimado.REPORTE.reset_index(drop=True)[0])
                    browser.close()
                else:
                    ("no es estimado")

                    # %% SI NO TIENE ESTIMADO
                os.chdir(str(Path.home() / "Downloads" / nombre_ruta))

                chrome_options = webdriver.ChromeOptions()
                prefs = {'download.default_directory': str(Path.home() / "Downloads" / nombre_ruta)}
                # prefs = {'download.default_directory' : ruta_to_export}
                chrome_options.add_experimental_option('prefs', prefs)

                browser = webdriver.Chrome(
                    str(Path.home() / "Downloads" / nombre_ruta / os.listdir(".")[0] / "chromedriver.exe"),
                    chrome_options=chrome_options)
                # browser.minimize_window()

                url_text = browser.get(dict_urls[compania_pais])
                # browser.minimize_window()
                # %%% INICIAR SESION
                time.sleep(5)
                " AQUI"
                # time.sleep(time_to_charge)
                if compania == "LVV":
                    browser.find_elements_by_id('botonint')[0].click()
                    time.sleep(time_to_charge)
                else:
                    print("no es livva")

                time.sleep(time_to_charge)
                # iniciar sesion
                browser.find_elements_by_name('usua')[0].send_keys(usuario)
                browser.find_elements_by_name("clav")[0].send_keys(contrasena)
                browser.find_elements_by_name('autenticar')[0].click()
                time.sleep(time_to_charge)
                "AQUI"

                reporte_sin_estimado = df_requisitos[df_requisitos.REPORTE != "ESTIMADO_CAMPANA"]
                # reporte_sin_estimado = [i for i in reportes_a_descargar if i != "ESTIMADO_CAMPANA"]

                campanas_sin_estimado = reporte_sin_estimado.CAMPANA.unique()

                for campanita in campanas_sin_estimado:
                    print(campanita)
                    # campanita = "202117"
                    df_requisitos_sin_estimado_0 = reporte_sin_estimado[
                        reporte_sin_estimado["CAMPANA"].apply(str) == str(campanita)]
                    time.sleep(3)
                    for report in df_requisitos_sin_estimado_0.REPORTE.unique():

                        print(report)

                        # report = "PREPEDIDOS_CAMPANA"
                        df_requisitos_sin_estimado = df_requisitos_sin_estimado_0[
                            df_requisitos_sin_estimado_0["REPORTE"] == report]
                        print(df_requisitos_sin_estimado)
                        print(int(df_requisitos_sin_estimado["TIEPO_EXPORT_SEG"].reset_index(drop=True)[0]))
                        " AQUI"
                        # %%%% REPORTE ESPECIFICADO
                        # time.sleep(time_to_charge)
                        browser.find_element_by_xpath('//input[@value="' + report + '"]').click()
                        time.sleep(time_to_charge)

                        # %%%% CAMPANA

                        try:
                            campana = browser.find_elements_by_name("_g_codi_camp")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("no tiene campana")

                        try:
                            campana = browser.find_elements_by_id("_g_codi_camp")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("no tiene campana")

                        try:
                            campana = browser.find_elements_by_id("_g_camp")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("no tiene campana")

                        try:
                            campana = browser.find_elements_by_name("_g_INV_PRCA~CODI_CAMP")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("_g_INV_PRCA~CODI_CAMP")

                        try:
                            campana = browser.find_elements_by_name("_g_inv_prca~codi_camp")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("_g_INV_PRCA~CODI_CAMP")

                        try:
                            campana = browser.find_elements_by_id("_g_impo_glob~camp_orig")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("_g_impo_glob~camp_orig")

                        # _g_INV_PRCA~CODI_CAMP

                        try:
                            campana = browser.find_elements_by_name("_g_camp_orig")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("no tiene campana")

                        try:
                            campana = browser.find_elements_by_name("_g_CODI_CAMP")[0]
                            campana.clear()
                            campana.send_keys(campanita)
                        except:
                            print("no tiene campana")

                        time.sleep(time_to_charge)

                        try:
                            pais_path = browser.find_elements_by_id("_g_codi_pais")[0]
                            pais_path.clear()
                            pais_path.send_keys(pais)
                        except:
                            print("no tiene pais")

                        time.sleep(time_to_charge)

                        # %%%% exportar
                        print("exportando")
                        browser.find_elements_by_name('tipo_sali')[1].click()
                        time.sleep(time_to_export)
                        # time.sleep(int(df_requisitos_sin_estimado["TIEPO_EXPORT_SEG"].reset_index(drop=True)[0]))
                        # browser.find_elements_by_class_name('btn1')[0].click()

                        browser.find_elements_by_xpath("//input[@value='(G)enerar']")[0].click()

                        time.sleep(time_to_export)

                        try:
                            # EXPORTAR
                            time.sleep(int(df_requisitos_sin_estimado["TIEPO_EXPORT_SEG"].reset_index(drop=True)[0]))
                            browser.find_element_by_xpath("//a[@href='" + usuario + "_" + report + ".xls']").click()
                            # time.sleep(int(df_requisitos_sin_estimado["TIEPO_EXPORT_SEG"].reset_index(drop=True)[0]))
                            # VOLVER A REPORTES
                            print("retornando")
                            regresar = browser.find_elements_by_xpath("//input[@value='Volver a Lista De Reportes']")[
                                0].click()

                        except:
                            print("es Col o Gua o Ecu")
                            time.sleep(int(df_requisitos_sin_estimado["TIEPO_EXPORT_SEG"].reset_index(drop=True)[0]))
                            # %%% REGRESAR



                            print("retornando")
                            regresar = browser.find_elements_by_xpath("//input[@value='(R)etornar']")[0]
                            time.sleep(time_to_charge)
                            regresar.click()

                        # "AQUI"
                        # time.sleep(time_to_charge)
                        # print("importando")
                        # os.chdir(str(Path.home() / "Downloads"/ nombre_ruta))
                        # reporte_name = glob.glob("*" + report +".xls")
                        # time.sleep(time_to_charge)
                        # reporte_des = pd.read_csv(reporte_name[0], sep = "\t")
                        # os.remove(reporte_name[0])
                        # ruta_to_export = df_requisitos_sin_estimado["RUTA_EXPORTAR"].reset_index(drop=True)[0]
                        # os.chdir(ruta_to_export)
                        # reporte_des.to_csv(pais+ "_"+ report + "_"+  str(campanita) + '.xls', sep = "\t", index =  False)
                        # time.sleep(time_to_charge)

                        import shutil
                        time.sleep(time_to_charge)
                        print("copiando")
                        ruta_import = str(Path.home() / "Downloads" / nombre_ruta)
                        # ruta_import =r"C:\Users\monica.huaman\35159_147728_DUPREE_VENTA_DIRECTA_S_A\LVV - STATS - Documentos\6.- TIM\8.- MNC\7.- COD_PYT\3.- WEB_SCR\1.- DSH_LVV\DATOS\ORI"
                        os.chdir(ruta_import)
                        reporte_name = glob.glob("*" + report + ".xls")
                        # ruta_to_export = r"C:\Users\monica.huaman\35159_147728_DUPREE_VENTA_DIRECTA_S_A\LVV - STATS - Documentos\6.- TIM\8.- MNC\7.- COD_PYT\3.- WEB_SCR\1.- DSH_LVV\DATOS\ORI\mover"
                        ruta_to_export = df_requisitos_sin_estimado["RUTA_EXPORTAR"].reset_index(drop=True)[0]
                        reporte_name_new = pais + "_" + report + "_" + str(campanita) + '.xls'
                        shutil.move(ruta_import + "//" + reporte_name[0],
                                    ruta_to_export + "//" + reporte_name_new)

                browser.close()

        browser.quit()
        os.chdir(str(Path.home() / "Downloads"))

        "----------------- AQUI ------------------------------"

        self.output_pais_label.config(text="Listo")

        self.mainloop()


if __name__ == "__main__":
    app = App()
    app.mainloop()


