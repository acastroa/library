import time
import pandas as pd
import os
import numpy as np
from pathlib import Path
import glob
# from selenium.webdriver.chrome.options import Options
# import chromedriver_autoinstaller
import os
from pathlib import Path
# from random import random
import shutil
#pip install webdriver-manager
import io
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
# from selenium.webdriver.common.by import By
# import bs4 as bs

ruta_paises = {"PER": "2.- PER",
               "COL": "3.- COL",
               "ECU": "4.- ECU",
               "BOL": "5.- BOL",
               "GUA": "6.- GUA"}

root_data = str(Path().home()) + r"\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Data"
campanitas_total = np.array([i + j * 100 for j in range(2017, 2025) for i in range(1, 19) if i + j * 100 != 202011])
user = "erickson_sandoval"
password = "72496955"


def scroll_down(browser=None):
    SCROLL_PAUSE_TIME = 3

    # Get scroll height
    last_height = browser.execute_script("return document.body.scrollHeight")

    while True:
        # Scroll down to bottom
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = browser.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

    # posts = browser.find_elements_by_class_name("post-text")

class App():

    def __init__(self, paises=['PER'], camp_ini=000000, camp_fin=999999, user=user, password=password, compania=['AZZ'],
                 reportes=['CONSOLIDADO'], campana_cons=True):
        self.paises = paises
        self.campanas = campanitas_total[(campanitas_total >= camp_ini) & (campanitas_total <= camp_fin)]
        self.campanas_agg = str(self.campanas.min()) + ".." + str(self.campanas.max())
        self.user = user
        self.password = password
        self.compania = compania
        self.reportes = reportes
        self.campana_cons = campana_cons
        self.data_final = {}

    def submit(self):
        root_chrome = str(Path.home() / "Downloads" / "pruebachromedriver7")
        try:
            os.makedirs(root_chrome)
            s = Service(ChromeDriverManager(path=root_chrome).install())
            print("Instalando Chrome Driver para Scraping. ")
        except Exception as e:
            ls = glob.glob(root_chrome + "/**/*.exe", recursive=True) + glob.glob(root_chrome + "/.*/**/*.exe",
                                                                                  recursive=True)
            s = Service(ls[0])

        # PARAMETROS
        # requisitos = pd.read_excel("REQUISITOS.xlsx", sheet_name="REQUISITOS")
        # requisitos = requisitos.sort_values(by=["COMPANIA", "PAIS"])
        # requisitos["CAMPANA"] = [i[:-2] if i[-2:] == ".0" else i for i in requisitos["CAMPANA"].apply(str)]

        # WEB SCRAPPING
        usuario = user  # requisitos["USUARIO"][0:1][0]
        contrasena = password  # str(requisitos["CONTRASENA"][0:1][0])

        time_to_export = 6  # segundos
        time_to_charge = 4

        # chromedriver_autoinstaller.install(cwd=True)  # Check if the current version of chromedriver exists
        # and if it doesn't exist, download it automatically,
        # then add chromedriver to path

        try:
            files = glob.glob("pruebachromedriver*")
            for i in files:
                print(i)
                shutil.rmtree(str(Path.home() / "Downloads" / i))
        except:
            print("no hay")

        # %%%% definiendo compania pais
        dict_urls = {"LVV-PER": "https://intranet.livva.pe/desarrollo/reportes/",
                     "AZZ-PER": "https://intranet.dupree.pe/desarrollo/reportes/",
                     "AZZ-BOL": "https://intranet.azzorti.bo/desarrollo/reportes/index.php",
                     "AZZ-ECU": "https://intranet.azzorti.com/desarrollo/reportes/",
                     "AZZ-COL": "https://intranet.dupree.co/desarrollo/reportes/",
                     "AZZ-GUA": "https://intranet.azzorti.gt/desarrollo/reportes/"}

        # arr_comp = requisitos.COMPANIA.unique()

        # %%% COMPANIA
        for compania in self.compania:
            print(compania)
            # df_requisitos_0 = requisitos[requisitos["COMPANIA"] == compania]
            # arr_pais = df_requisitos_0.PAIS.unique()
            for pais in self.paises:
                print(pais)
                # df_requisitos = df_requisitos_0[df_requisitos_0["PAIS"] == pais]

                compania_pais = compania + "-" + pais
                # print(dict_urls[compania_pais])

                # reporte_estimado = df_requisitos[df_requisitos.REPORTE == "ESTIMADO_CAMPANA"]

                root_out_country_b = root_data + f"\\{compania}\\{ruta_paises[pais]}\\1.- ORI\\6.- CON_CAM"
                if "temp" in os.listdir(root_out_country_b):
                    os.rmdir(root_out_country_b+"/temp")
                    os.mkdir(root_out_country_b + "\\temp")
                else:
                    os.mkdir(root_out_country_b+"\\temp")
                root_out_country = root_out_country_b+"\\temp"

                chrome_options = webdriver.ChromeOptions()
                # prefs = {'download.default_directory': str(Path.home() / "Downloads" / nombre_ruta)}
                prefs = {'download.default_directory': root_out_country}
                chrome_options.add_experimental_option('prefs', prefs)

                browser = webdriver.Chrome(service=s, options=chrome_options)

                url_text = browser.get(dict_urls[compania_pais])
                browser.minimize_window()
                # %%% INICIAR SESION
                time.sleep(time_to_charge)
                if compania == "LVV":
                    browser.find_elements(by="id", value='botonint')[0].click()
                    time.sleep(time_to_charge)

                # iniciar sesion
                browser.find_element(by='name', value='usua').send_keys(self.user)
                browser.find_element(by='name', value="clav").send_keys(self.password)
                browser.find_element(by='name', value='autenticar').click()
                time.sleep(time_to_charge)

                # reporte_sin_estimado = df_requisitos[df_requisitos.REPORTE != "ESTIMADO_CAMPANA"]
                # reporte_sin_estimado = [i for i in reportes_a_descargar if i != "ESTIMADO_CAMPANA"]

                # campanas_sin_estimado = reporte_sin_estimado.CAMPANA.unique()

                for report in self.reportes:
                    print(" "+report)

                    for campanita in self.campanas:
                        campanita = str(campanita)
                        print(" "+campanita)
                        # df_requisitos_sin_estimado_0 = reporte_sin_estimado[
                        #     reporte_sin_estimado["CAMPANA"].apply(str) == str(campanita)]

                        if (campanita == str(self.campanas[0])) or (compania == 'LVV') or (pais == 'PER'): #La primera vez
                            browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()
                            time.sleep(time_to_charge)
                        # %%%% CAMPANA

                        xpath_camp = [i for i in browser.find_elements(by='xpath', value='//*[@id]') if
                                      "camp" in i.get_attribute('id')]
                        if len(xpath_camp) == 0:
                            xpath_camp = [i for i in browser.find_elements(by='xpath', value='.//*') if i.get_attribute('name') != None]
                            xpath_camp = [i for i in xpath_camp if "camp" in i.get_attribute('name')]

                        assert len(xpath_camp) == 1, "Se tiene más de un campo donde se debe agregar campaña."

                        if self.campana_cons:
                            browser.find_element(by='name', value=xpath_camp[0].get_attribute('name')).send_keys(self.campanas_agg)
                        else:
                            browser.find_element(by='name', value=xpath_camp[0].get_attribute('name')).clear()
                            browser.find_element(by='name', value=xpath_camp[0].get_attribute('name')).send_keys(campanita)

                        # try:
                        #     pais_path = browser.find_element(by='id', value="_g_codi_pais")
                        #     pais_path.clear()
                        #     pais_path.send_keys(pais)
                        #     time.sleep(time_to_charge)
                        # except:
                        #     print("no tiene pais")

                        # %%%% exportar
                        print(" exportando")
                        browser.find_elements(by='name', value='tipo_sali')[1].click()
                        try:
                            browser.find_element(by='xpath', value='//*[@id="boto_gene"]').click()
                        except:
                            browser.find_element(by='xpath', value='/ html / body / center / form / table / tbody / tr[3] / td / input[1]').click()
                            time.sleep(time_to_export)
                            while "CONSOLIDADO" in browser.current_url:
                                browser.back()
                                time.sleep(0.2)
                            time.sleep(2)
                            browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()
                            time.sleep(time_to_charge) #problemas de ecu

                        time.sleep(time_to_export)
                        try:
                            xpath_export = [i for i in browser.find_elements(by='xpath', value='.//*') if i.get_attribute('href') is not None]
                            xpath_export = [i for i in xpath_export if (report in i.get_attribute('href') and (".xls" in i.get_attribute('href')))]
                            xpath_export[0].click()
                            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[3]/td[3]/font/a/img').click()
                            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[4]/td[2]/font/a').click()
                            time.sleep(1)
                        except:
                            print(" No requiere acceder al reporte manualmente")
                        time.sleep(2)
                        filename = max([root_out_country + "\\" + f for f in os.listdir(root_out_country)], key=os.path.getctime)
                        shutil.move(filename, os.path.join(root_out_country_b, pais + "_" + report + "_" + str(campanita) + '.txt'))
                        # os.rename(filename, root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt')

                        # scroll_down(browser=browser)

                        # data_string = browser.find_element(by='xpath', value='.//pre').text
                        # data = io.StringIO(data_string)
                        # table = pd.read_csv(data, sep='|')
                        #
                        # if self.campana_cons:
                        #     self.data_final[compania_pais] = table.copy()
                        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + self.campanas_agg.replace("..", "_") + '.txt', index=False)
                        # else:
                        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt', index=False)

                        while "temporales" in browser.current_url:
                            browser.back()
                            time.sleep(0.2)
                        time.sleep(2)

                    if self.campana_cons:
                        break

                browser.close()

                os.rmdir(root_out_country) #Eliminar carpeta temporal

                print(f"    Pais {pais} completadon con éxito.")
                browser.quit()

    def limpieza_campanas(self, root_now=None, report=None):
        files = glob.glob(f"{root_now}/{self.user}*.*")
        pais = root_now.split("\\")[-3][-3:]
        for f in files:
            file = open(f, "r")
            data = io.StringIO(file.read(50))
            table = pd.read_csv(data, sep='\t')
            campanita = str(table['CAMP'][0])
            print(campanita)
            os.rename(f, root_now + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt')



# self = App(paises=['ECU'], camp_ini=201901, camp_fin=202203, campana_cons=False)
# self.submit()

