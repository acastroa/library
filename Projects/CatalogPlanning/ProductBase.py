import pandas as pd
import numpy as np
import os
import glob
from AzzLib import LoadData as Ld
import datetime as dt


class Camp:
    def __init__(self, value):
        self.value = value
        self.anio = int(value / 100)
        self.month = value % 100
        self._camps = [x for x in range(1, 19)]
        self._indice = self._camps.index(self.month)

    def __add__(self, camp_int):
        indice = self._indice + camp_int
        if indice < 0:
            indice = 18 - indice
            anios = -int(indice / 18)
            indice = -indice % 18
        else:
            anios = int(indice / 18)
            indice = indice % 18

        return Camp((self.anio + anios) * 100 + self._camps[indice])

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value

    def __gt__(self, other):
        return self.value > other.value

    def __int__(self):
        return int(self.value)

    def __str__(self):
        return str(self.value)


class ProductBase:
    def __init__(self, camps_retro=27, pais='PER', aggregate_codivent=False):
        self.camps_retro = camps_retro
        self.pais = pais
        self.df_prod_base = None
        self.aggregate_codivent = aggregate_codivent
        self.df_sust = None

    def get_products_base(self):
        q_inv_pai = f'''
                    SELECT dt.fech_dia_max AS 'FECHA_CONSULTA', dt.pais_orig AS 'PAIS_ORIGEN', iv.pais AS 'PAIS', 
                           TRIM(iv.codi_prod) AS 'CODI_PROD',
                           TRIM(iv.codi_sap) AS 'CODI_SAP',
                           TRIM(pr.desc_compl) AS 'NOMBRE_SID',
                           TRIM(pr.tipo_arti) AS 'TIPO_ARTI_SAP',
                           TRIM(sb.nomb_line) AS 'LINEA',
                           TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO',
                           TRIM(pr.desc_corta) AS 'REFERENCIA',
                           TRIM(pr.desc_talla) AS 'TALLA',
                           SUM(CAST(iv.stock AS float)) AS 'STOCK'
                    FROM (
                          SELECT pais_orig, MAX(fech_dia) AS 'fech_dia_max'
                          FROM [dwh_azzorti].[inv].[base_inventarios] 
                          WHERE pais_orig IN ('{self.pais}') GROUP BY pais_orig 
                         ) dt LEFT JOIN [dwh_azzorti].[inv].[base_inventarios] iv 
                         ON dt.fech_dia_max = iv.fech_dia AND dt.pais_orig = iv.pais_orig 
                         LEFT JOIN [dim_general].[dbo].[dim_producto] pr
                         ON iv.codi_prod = pr.codi_prod AND iv.codi_sap = pr.codi_sap
                         LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                         ON pr.cons_subgrup = sb.cons_subgrup
                    WHERE iv.tipo_mate = 'PT' AND iv.stock > 0 AND iv.pais = '{self.pais}'
                    GROUP BY dt.fech_dia_max, dt.pais_orig, iv.pais, iv.codi_prod, iv.codi_sap, pr.desc_compl,
                             pr.tipo_arti, sb.nomb_line, sb.nomb_subgrupo, pr.desc_corta, pr.desc_talla
                    '''
        df_stock_act = Ld.get_file_sql(query=q_inv_pai)
        df_stock_act[['CODI_SAP', 'CODI_PROD']] = df_stock_act[['CODI_SAP', 'CODI_PROD']].astype(str)

        fecha_inve = df_stock_act.iloc[0][0]

        query = f'''
                SELECT codi_camp
                FROM [dim_general].[dbo].[dim_campana]
                WHERE fech_inic <= '{fecha_inve}' AND fech_fina >= '{fecha_inve}' AND codi_pais = '{self.pais}'
                '''
        camp_act = int(Ld.get_file_sql(query=query).iloc[0][0])
        camp_ini = int(Camp(camp_act) + (-27))
        if self.pais == 'PER':
            oazz = Ld.DataOri(paises=['PER'], company='AZZ', filters=True, camp_ini=1)
            olvv = Ld.DataOri(paises=['PER'], company='LVV', filters=True, camp_ini=1)

            # CARGA DE TABLA FACTURACION
            z = olvv.get_zdm_mat_art(sep=';')
            df_fac = pd.concat([oazz.get_fac(fac_type='TPA', falt_anunc=False, with_back_order=True, con_tipo_zona=True),
                                olvv.get_fac(fac_type='TPA', falt_anunc=False, with_back_order=True, con_tipo_zona=True)], ignore_index=True)
        else:
            oazz = Ld.DataOri(paises=[self.pais], company='AZZ', filters=True, camp_ini=1)

            # CARGA DE TABLA FACTURACION
            df_fac = oazz.get_fac(fac_type='MOV', falt_anunc=False, with_back_order=True)

        # CARGA DE CODIGOS SUSTITUTOS

        # oazz.get_sustitutos(origen='SQL')
        if self.pais != 'PER':
            paises_cadena = "','".join(['PER', 'LPE'])
        else:
            paises_cadena = "','".join(['PER', 'LPE'])
        # sust_identicos
        query = f"""
                 SELECT DISTINCT CAST(TRIM(si.codi_padr_sid) AS varchar) AS 'CODI_ORIG',
                        TRIM(si.codi_sust_sid) AS 'CODI_PROD',
                        TRIM(si.codi_padr_sap) AS 'CODI_ORIG_SAP',
                        TRIM(si.codi_sust_sap) AS 'CODI_PROD_SAP',
                        TRIM(pr.desc_compl) AS 'NOMBRE_SID_ORIG',
                        TRIM(pr.tipo_arti) AS 'TIPO_ARTI_SAP_ORIG',
                        TRIM(sb.nomb_line) AS 'LINEA_ORIG',
                        TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO_ORIG',
                        TRIM(pr.desc_corta) AS 'REFERENCIA_ORIG',
                        TRIM(pr.desc_talla) AS 'TALLA_ORIG'
                 FROM [dim_general].[dbo].[dim_sust_iden] si LEFT JOIN [dim_general].[dbo].[dim_producto] pr
                      ON si.codi_padr_sid = pr.codi_prod AND si.codi_padr_sap = pr.codi_sap
                      LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                      ON pr.cons_subgrup = sb.cons_subgrup
                 WHERE si.codi_pais IN ('{paises_cadena}') 
                 """
        # AND LEN(CAST(TRIM(si.codi_padr_sid) AS varchar)) >= 6
        df_sust_ide = Ld.get_file_sql(query=query)
        try:
            df_sust_ide[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']] = df_sust_ide[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']].astype(str)
        except:
            pass
        # sust_sap
        query = """
                SELECT DISTINCT CAST(TRIM(po.codi_prod) AS varchar) AS 'CODI_ORIG', 
                       TRIM(ps.codi_prod) AS 'CODI_PROD',
                       TRIM(su.codi_padr) AS 'CODI_ORIG_SAP',
                       TRIM(su.codi_prod_sap) AS 'CODI_PROD_SAP',
                       TRIM(po.desc_compl) AS 'NOMBRE_SID_ORIG',
                       TRIM(po.tipo_arti) AS 'TIPO_ARTI_SAP_ORIG',
                       TRIM(sb.nomb_line) AS 'LINEA_ORIG',
                       TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO_ORIG',
                       TRIM(po.desc_corta) AS 'REFERENCIA_ORIG',
                       TRIM(po.desc_talla) AS 'TALLA_ORIG'
                FROM [dim_general].[dbo].[dim_sustitutos] su 
                     LEFT JOIN [dim_general].[dbo].[dim_producto] po 
                            ON CAST(TRIM(su.codi_padr) AS varchar) = CAST(TRIM(po.codi_sap) AS varchar) 
                     LEFT JOIN [dim_general].[dbo].[dim_producto] ps 
                            ON CAST(TRIM(su.codi_prod_sap) AS varchar) = CAST(TRIM(ps.codi_sap) AS varchar) 
                     LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                            ON po.cons_subgrup = sb.cons_subgrup 
                WHERE LEN(TRIM(po.codi_prod)) >= 6
                """
        df_sust_sap = Ld.get_file_sql(query=query)
        df_sust_sap[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']] = df_sust_sap[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']].astype(str)
        df_sust_sap = df_sust_sap[~df_sust_sap['CODI_PROD'].isnull()].reset_index(drop=True)
        df_sust_sap = df_sust_sap.drop(df_sust_sap[(df_sust_sap.CODI_PROD == '434855') &
                                                   (df_sust_sap.CODI_ORIG == '275835')].index, axis=0
                                       ).reset_index(drop=True)
        df_sust_sap = df_sust_sap[~df_sust_sap['CODI_PROD'].isin(['1255004', '773876', '582663', '246978', '953446',
                                                                  '432352'])].reset_index(drop=True)

        df_sust = pd.concat([df_sust_ide, df_sust_sap]).drop_duplicates(['CODI_PROD'], ignore_index=True).drop_duplicates(['CODI_PROD_SAP'], ignore_index=True)

        df_stock_act = df_stock_act.merge(df_sust, on='CODI_PROD', how='left')
        df_stock_act['CODI_ORIG'] = np.where(df_stock_act['CODI_ORIG'].isna(), df_stock_act['CODI_PROD'],
                                             df_stock_act['CODI_ORIG'])
        df_stock_act['CODI_ORIG_SAP'] = np.where(df_stock_act['CODI_ORIG_SAP'].isna(), df_stock_act['CODI_SAP'],
                                                 df_stock_act['CODI_ORIG_SAP'])
        cols_orig = ['NOMBRE_SID', 'TIPO_ARTI_SAP', 'LINEA', 'SUBGRUPO', 'REFERENCIA', 'TALLA']
        for col in cols_orig:
            df_stock_act[col + '_ORIG'] = np.where(df_stock_act[col + '_ORIG'].isna(), df_stock_act[col],
                                                   df_stock_act[col + '_ORIG'])
        del df_stock_act['CODI_PROD_SAP']
        df_stock_act = df_stock_act.groupby(['PAIS_ORIGEN', 'FECHA_CONSULTA', 'CODI_ORIG', 'CODI_ORIG_SAP',
                                             'NOMBRE_SID_ORIG', 'TIPO_ARTI_SAP_ORIG', 'LINEA_ORIG', 'SUBGRUPO_ORIG', 'REFERENCIA_ORIG', 'TALLA_ORIG'],
                                            as_index=False).agg({'STOCK': 'sum'})

        df_fac = df_fac.merge(df_sust[['CODI_ORIG', 'CODI_PROD']], on='CODI_PROD', how='left')
        df_fac['CODI_ORIG'] = np.where(df_fac['CODI_ORIG'].isna(), df_fac['CODI_PROD'], df_fac['CODI_ORIG'])

        df_fac = df_fac.sort_values(['PAIS', 'CAMP', 'CODI_VENT', 'CODI_ORIG'], ascending=False
                                    ).drop_duplicates(['CODI_ORIG'], ignore_index=True)

        df_fac_past = df_fac[df_fac['CAMP'] >= camp_ini].reset_index(drop=True)
        # df_fac = df_fac.drop(['CODI_ORIG_SAP', 'CODI_PROD_SAP'], axis=1)

        # CRUCE CON Z15 Y LL
        path_boztpla15 = Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/BOztplan_15_' + str(fecha_inve) + '.csv'
        if not os.path.isfile(path_boztpla15):
            # fecha_inve_dat = dt.datetime.strptime(fecha_inve, '%Y-%m-%d')
            fecha_inve_dat = dt.datetime(fecha_inve.year, fecha_inve.month, fecha_inve.day)

            dates_str = [f[-14:-4] for f in glob.glob(Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/*.csv')]
            dates_dat = [fecha_inve_dat - dt.datetime.strptime(f, '%Y-%m-%d') if fecha_inve_dat > dt.datetime.strptime(f, '%Y-%m-%d') else dt.timedelta(999) for f in dates_str]
            min_date = min(dates_dat)
            fecha_inve = [f for f, g in zip(dates_str, dates_dat) if g == min_date][0]
            path_boztpla15 = Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/BOztplan_15_' + str(
                fecha_inve) + '.csv'

        df_ztpla15 = pd.read_csv(path_boztpla15, sep=';', low_memory=False, dtype={'Producto_Original': str},
                                 usecols=['Indicador_movimiento', 'Pais_Origen', 'Num_Documento', 'Producto_Original',
                                          'Cantidad_Proyectada'])
        df_ztpla15['Producto_Original'] = df_ztpla15['Producto_Original'].str.strip()
        df_ztpla15['Producto_Original'] = df_ztpla15['Producto_Original'].astype(str)

        df_ztpla15 = df_ztpla15[(df_ztpla15['Indicador_movimiento'] == 'FACT') & (df_ztpla15['Pais_Origen'] == self.pais)
                                & (df_ztpla15['Num_Documento'] == camp_act)].reset_index(drop=True)

        df_ztpla15 = df_ztpla15.merge(df_sust[['CODI_PROD_SAP', 'CODI_ORIG_SAP']].rename(columns={'CODI_PROD_SAP': 'Producto_Original'}),
                                      on='Producto_Original', how='left')
        df_ztpla15['CODI_ORIG_SAP'] = np.where(df_ztpla15['CODI_ORIG_SAP'].isnull(), df_ztpla15['Producto_Original'],
                                               df_ztpla15['CODI_ORIG_SAP'])

        df_ztpla15 = df_ztpla15.groupby(['CODI_ORIG_SAP', 'Pais_Origen', 'Num_Documento'], as_index=False
                                        ).agg({'Cantidad_Proyectada': 'sum'})
        df_ztpla15.columns = ['CODI_ORIG_SAP', 'PAIS', 'CAMPANA', 'PROY_FACT_CAMP_ACT']

        q_ll = f"""
               SELECT TRIM(codi_prod_sap) AS 'CODI_PROD_SAP', CAST(tota_und AS float) AS 'tota_und'
               FROM [dwh_azzorti].[mer].[leader_list]
               WHERE pais = '{self.pais}' AND camp_sid {'>=' if df_ztpla15['PROY_FACT_CAMP_ACT'].sum() == 0 else '>'} {str(camp_act)}
               """
        df_ztpla23 = Ld.get_file_sql(query=q_ll)
        df_ztpla23['CODI_PROD_SAP'] = df_ztpla23['CODI_PROD_SAP'].astype(str)
        df_ztpla23 = df_ztpla23.merge(df_sust, on='CODI_PROD_SAP', how='left')
        df_ztpla23['CODI_ORIG_SAP'] = np.where(df_ztpla23['CODI_ORIG_SAP'].isnull(), df_ztpla23['CODI_PROD_SAP'],
                                               df_ztpla23['CODI_ORIG_SAP'])

        df_ztpla23 = df_ztpla23.groupby(['CODI_ORIG_SAP'], as_index=False).agg({'tota_und': 'sum'})
        # FIN DE CRUZE
        # df_final = df_stock_act.merge(df_con_prod, on='CODI_ORIG', how='left')
        df_final = df_stock_act[df_stock_act['STOCK'] > 0].reset_index(drop=True)
        
        # Adicion de codgios facturados del pasado
        df_fac_past['FECHA_CONSULTA'] = fecha_inve
        df_fac_past['STOCK'] = 0
        df_fac_past = df_fac_past[['PAIS', 'FECHA_CONSULTA', 'CODI_ORIG', 'STOCK']]
        df_fac_past = df_fac_past[~df_fac_past['CODI_ORIG'].isin(df_final['CODI_ORIG'].to_list())].reset_index(drop=True)

        df_fac_past = df_fac_past.rename(columns={'PAIS': 'PAIS_ORIGEN'})

        l1k = [df_fac_past['CODI_ORIG'][i:i + 1000] for i in range(0, len(df_fac_past['CODI_ORIG']), 1000)]
        list_codes_j = """');
                          INSERT INTO #temp_producto (CODI_PROD)
                          VALUES ('""".join(["'), ('".join(c) for c in l1k])
        query = f"""
                USE dim_general

                CREATE TABLE #temp_producto
                (CODI_PROD VARCHAR(50))

                INSERT INTO #temp_producto (CODI_PROD)
                VALUES ('{list_codes_j}');

                SELECT tp.CODI_PROD AS CODI_ORIG,
                        pr.codi_sap AS 'CODI_ORIG_SAP', 
                        TRIM(pr.desc_compl) AS 'NOMBRE_SID_ORIG',
                        pr.tipo_arti AS 'TIPO_ARTI_SAP_ORIG', 
                        TRIM(sb.nomb_line) AS 'LINEA_ORIG',
                        TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO_ORIG',
                        TRIM(pr.desc_corta) AS 'REFERENCIA_ORIG',
                        TRIM(pr.desc_talla) AS 'TALLA_ORIG'
                FROM #temp_producto tp LEFT JOIN [dim_general].[dbo].[dim_producto] pr
                ON tp.CODI_PROD = pr.codi_prod
                LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                ON pr.cons_subgrup = sb.cons_subgrup
                """
        df = Ld.get_file_sql(query=query).drop_duplicates('CODI_ORIG', ignore_index=True)
        df_fac_past = df_fac_past.merge(df, on=['CODI_ORIG'], how='left')

        # Agrupacion de Dataframe Final
        df_final = pd.concat([df_final, df_fac_past], ignore_index=True)

        df_final['COMPANIA'] = np.where(df_final['TIPO_ARTI_SAP_ORIG'].str[0] == 'Y',
                                        'LVV',
                                        np.where(df_final['TIPO_ARTI_SAP_ORIG'].str[0] == 'Z', 'AZZ', np.nan))
        df_final = df_final.merge(df_ztpla15[['CODI_ORIG_SAP', 'PROY_FACT_CAMP_ACT']], on='CODI_ORIG_SAP', how='left')
        df_final = df_final.merge(df_ztpla23, on='CODI_ORIG_SAP', how='left')
        df_final[['PROY_FACT_CAMP_ACT', 'tota_und', 'STOCK']] = df_final[['PROY_FACT_CAMP_ACT', 'tota_und', 'STOCK'
                                                                          ]].astype(float).fillna(0)

        df_final = df_final.merge(df_fac[['CODI_ORIG', 'CODI_VENT', 'CAMP']], on='CODI_ORIG', how='left')
        df_final = df_final.rename(columns={'CAMP': 'ULT_CAMP'})
        df_final['CODI_VENT'] = np.where(df_final['CODI_VENT'].isna(), df_final['CODI_ORIG'], df_final['CODI_VENT'])
        df_final['PROYECTADO'] = df_final['PROY_FACT_CAMP_ACT'] + df_final['tota_und']
        df_final = df_final.rename(columns={'CODI_ORIG': 'CODI_PROD', 'CODI_ORIG_SAP': 'CODI_SAP',
                                            'NOMBRE_SID_ORIG': 'NOMBRE_SID', 'TIPO_ARTI_SAP_ORIG': 'TIPO_ARTI_SAP',
                                            'LINEA_ORIG': 'LINEA', 'SUBGRUPO_ORIG': 'SUBGRUPO',
                                            'REFERENCIA_ORIG': 'REFERENCIA', 'TALLA_ORIG': 'TALLA'})
        # carga de tipo de lenta rotacion
        if self.pais == 'PER':
            ls_lr = Ld.root_supply_azz + f'/2.- PER/1.- ORI/7.- LEN_ROT/LENTA ROTACION.xlsx'
            df_lr = pd.read_excel(ls_lr, sheet_name=0, dtype={'CODIGOS': str})
            df_lr = df_lr.rename(columns={'CODIGOS': 'CODI_SAP'})
            df_final['LENTA_ROTACION'] = df_final.merge(df_lr, on='CODI_SAP', how='left')['CATE_ROT']
        else:
            df_final['LENTA_ROTACION'] = None
        df_final['REF_2'] = np.where(df_final['REFERENCIA'].str.contains('T-') &
                                         df_final['TIPO_ARTI_SAP'].isin(
                                             ['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                         df_final['REFERENCIA'].apply(lambda x: x.split("T-")[0]), np.nan)
        df_final['REFERENCIA'] = np.where(df_final['REFERENCIA'].str.contains('T-') &
                                              df_final['TIPO_ARTI_SAP'].isin(
                                                  ['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                              df_final['REF_2'], df_final['REFERENCIA'])
        df_final['REFERENCIA'] = df_final['REFERENCIA'].str.strip()

        if self.aggregate_codivent:
            df_final_agg = df_final.groupby(['FECHA_CONSULTA', 'PAIS_ORIGEN', 'CODI_VENT'], as_index=False
                                            ).agg({'NOMBRE_SID': 'first', 'LINEA': 'first', 'SUBGRUPO': 'first',
                                                  'TIPO_ARTI_SAP': 'first', 'COMPANIA': 'first', 'STOCK': 'sum',
                                                   'PROYECTADO': 'sum', 'CODI_PROD': 'count', 'LENTA_ROTACION': 'first',
                                                   'REFERENCIA': 'first', 'TALLA': 'first'
                                                   })
            df_final_agg = df_final_agg.rename(columns={'CODI_PROD': 'CANT_CODS'})

        else:
            df_final_agg = df_final.groupby(['FECHA_CONSULTA', 'PAIS_ORIGEN', 'CODI_SAP', 'CODI_PROD',
                                             'CODI_VENT'], as_index=False
                                            ).agg({'NOMBRE_SID': 'first', 'LINEA': 'first', 'SUBGRUPO': 'first',
                                                   'TIPO_ARTI_SAP': 'first', 'COMPANIA': 'first', 'STOCK': 'sum',
                                                   'PROYECTADO': 'sum', 'REFERENCIA': 'first', 'TALLA': 'first', 'ULT_CAMP': 'last'
                                                   })
        df_final_agg['STOCK-PROYECTADO'] = df_final_agg['STOCK'] - df_final_agg['PROYECTADO']
        df_final_agg['STOCK-PROYECTADO'] = np.where(df_final_agg['STOCK-PROYECTADO'] < 0, 0,
                                                    df_final_agg['STOCK-PROYECTADO'])
        df_final_agg['REF_2'] = np.where(df_final_agg['REFERENCIA'].str.contains('T-') &
                                           df_final_agg['TIPO_ARTI_SAP'].isin(['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                           df_final_agg['REFERENCIA'].apply(lambda x: x.split("T-")[0]), np.nan)
        df_final_agg['REFERENCIA'] = np.where(df_final_agg['REFERENCIA'].str.contains('T-') &
                                              df_final_agg['TIPO_ARTI_SAP'].isin(['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                              df_final_agg['REF_2'], df_final_agg['REFERENCIA'])
        df_final_agg['REFERENCIA'] = df_final_agg['REFERENCIA'].str.strip()
        del df_final_agg['REF_2']

        # inventario base sin filtrar
        self.df_prod_base = df_final_agg.copy()
        self.df_sust = df_sust

