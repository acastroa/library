import time
import pandas as pd
import os
import numpy as np
from pathlib import Path
import glob
# from selenium.webdriver.chrome.options import Options
# import chromedriver_autoinstaller
import os
from pathlib import Path
# from random import random
import shutil
#pip install webdriver-manager
import io
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
# from selenium.webdriver.common.by import By
# import bs4 as bs

ruta_paises = {"PER": "2.- PER",
               "COL": "3.- COL",
               "ECU": "4.- ECU",
               "BOL": "5.- BOL",
               "GUA": "6.- GUA"}

campanitas_total = np.array([i + j * 100 for j in range(2017, 2025) for i in range(1, 19) if i + j * 100 != 202011])


def scroll_down(browser=None):
    SCROLL_PAUSE_TIME = 3

    # Get scroll height
    last_height = browser.execute_script("return document.body.scrollHeight")

    while True:
        # Scroll down to bottom
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = browser.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

    # posts = browser.find_elements_by_class_name("post-text")


class ScrapReportes:

    def __init__(self):
        self.data_final = {}
        self.dict_urls = {"LVV-PER": "https://intranet.livva.pe/desarrollo/reportes/",
                          "AZZ-PER": "https://intranet.dupree.pe/desarrollo/reportes/",
                          "AZZ-BOL": "https://intranet.azzorti.bo/desarrollo/reportes/index.php",
                          "AZZ-ECU": "https://intranet.azzorti.com/desarrollo/reportes/",
                          "AZZ-COL": "https://intranet.dupree.co/desarrollo/reportes/",
                          "AZZ-GUA": "https://intranet.azzorti.gt/desarrollo/reportes/"}
        self.time_to_export = 6  # segundos
        self.time_to_charge = 4
        self.browser = None
        self.root_out_country = None
        self.root_out_country_b = None
        self.pais = None
        self.user = None
        self.password = None
        self.compania = None

    def submit(self, compania='AZZ', pais='BOL', user='', password=''):
        self.pais = pais
        self.user = user
        self.password = password
        self.compania = compania
        root_chrome = str(Path.home() / "Downloads" / "pruebachromedriver7")
        try:
            os.makedirs(root_chrome)
            s = Service(ChromeDriverManager(path=root_chrome).install())
            print("Instalando Chrome Driver para Scraping. ")
        except Exception as e:
            ls = glob.glob(root_chrome + "/**/*.exe", recursive=True) + glob.glob(root_chrome + "/.*/**/*.exe",
                                                                                  recursive=True)
            s = Service(ls[0])

        # chromedriver_autoinstaller.install(cwd=True)  # Check if the current version of chromedriver exists
        # and if it doesn't exist, download it automatically,
        # then add chromedriver to path

        try:
            files = glob.glob("pruebachromedriver*")
            for i in files:
                print(i)
                shutil.rmtree(str(Path.home() / "Downloads" / i))
        except:
            print("no hay chromedriver")

        # %%%% definiendo compania pais
        dict_urls = self.dict_urls

        # %%% COMPANIA
        compania = self.compania
        pais = self.pais
        time_to_export = self.time_to_export
        time_to_charge = self.time_to_charge

        print(compania)
        # df_requisitos_0 = requisitos[requisitos["COMPANIA"] == compania]
        # arr_pais = df_requisitos_0.PAIS.unique()

        print(pais)
        # df_requisitos = df_requisitos_0[df_requisitos_0["PAIS"] == pais]

        compania_pais = compania + "-" + pais
        # print(dict_urls[compania_pais])

        # reporte_estimado = df_requisitos[df_requisitos.REPORTE == "ESTIMADO_CAMPANA"]

        root_out_country_b = f'C:\\Users\\{os.getlogin()}\\Downloads'
        if "temp" in os.listdir(root_out_country_b):
            os.rmdir(root_out_country_b+"/temp")
            os.mkdir(root_out_country_b + "\\temp")
        else:
            os.mkdir(root_out_country_b+"\\temp")
        root_out_country = root_out_country_b+"\\temp"

        chrome_options = webdriver.ChromeOptions()
        # prefs = {'download.default_directory': str(Path.home() / "Downloads" / nombre_ruta)}
        prefs = {'download.default_directory': root_out_country}
        chrome_options.add_experimental_option('prefs', prefs)

        browser = webdriver.Chrome(service=s, options=chrome_options)

        url_text = browser.get(dict_urls[compania_pais])
        # browser.minimize_window()
        # %%% INICIAR SESION
        time.sleep(time_to_charge)
        if compania == "LVV":
            browser.find_elements(by="id", value='botonint')[0].click()
            time.sleep(time_to_charge)

        # iniciar sesion
        browser.find_element(by='name', value='usua').send_keys(self.user)
        browser.find_element(by='name', value="clav").send_keys(self.password)
        browser.find_element(by='name', value='autenticar').click()
        time.sleep(time_to_charge)
        self.browser = browser
        self.root_out_country = root_out_country
        self.root_out_country_b = root_out_country_b

        # FIN DE INICIO DE SESION
        print("Se ha iniciado sesion a la intranet")

    def get_entradas_bodegas(self, fecha='01-01-2022..12-31-2022', bodega='1|4|6|71', file_name=None,
                             ruta_imprimir=None):
        compania = self.compania
        pais = self.pais
        browser = self.browser
        time_to_charge = self.time_to_charge
        time_to_export = self.time_to_export
        root_out_country = self.root_out_country
        root_out_country_b = self.root_out_country_b
        report = 'ENTRADAS_BODEGAS'

        browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()

        time.sleep(time_to_charge)
        # TEXTBOX FECHA(*)
        browser.find_element(by='name', value='_g_INV_GLOB~FECH_MOVI').clear()
        browser.find_element(by='name', value='_g_INV_GLOB~FECH_MOVI').send_keys(fecha)

        # TEXTBOX BODEGA(*)
        browser.find_element(by='name', value='_g_INV_DETA~CODI_BODE').clear()
        browser.find_element(by='name', value='_g_INV_DETA~CODI_BODE').send_keys(bodega)

        # try:
        #     pais_path = browser.find_element(by='id', value="_g_codi_pais")
        #     pais_path.clear()
        #     pais_path.send_keys(pais)
        #     time.sleep(time_to_charge)
        # except:
        #     print("no tiene pais")

        # %%%% exportar
        print(" exportando entrada bodegas")
        browser.find_elements(by='name', value='tipo_sali')[1].click()
        try:
            browser.find_element(by='xpath', value='//*[@id="boto_gene"]').click()
        except:
            browser.find_element(by='xpath', value='/ html / body / center / form / table / tbody / tr[3] / td / input[1]').click()
            time.sleep(time_to_export)
            while "CONSOLIDADO" in browser.current_url:
                browser.back()
                time.sleep(0.2)
            time.sleep(2)
            browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()
            time.sleep(time_to_charge) #problemas de ecu

        time.sleep(time_to_export)
        try:
            xpath_export = [i for i in browser.find_elements(by='xpath', value='.//*') if i.get_attribute('href') is not None]
            xpath_export = [i for i in xpath_export if (report in i.get_attribute('href') and (".xls" in i.get_attribute('href')))]
            xpath_export[0].click()
            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[3]/td[3]/font/a/img').click()
            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[4]/td[2]/font/a').click()
            time.sleep(1)
        except:
            print(" No requiere acceder al reporte manualmente")

        # periodo de descarga de archivo
        time.sleep(30)
        filename = None
        for k in range(10):
            filename = max([root_out_country + "\\" + f for f in os.listdir(root_out_country)], key=os.path.getctime)
            if filename[-4:] == '.xls':
                print("se ha descargado el archivo")
                break
            time.sleep(20)

        if file_name is None:
            file_name = report
        shutil.move(filename, os.path.join(root_out_country_b, file_name + '.txt'))

        # mover archivo a carpeta deseada
        if ruta_imprimir is not None:
            shutil.move(os.path.join(root_out_country_b, file_name + '.txt'),
                        ruta_imprimir.replace('/', '\\') + file_name + '.txt')
        # os.rename(filename, root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt')

        # scroll_down(browser=browser)

        # data_string = browser.find_element(by='xpath', value='.//pre').text
        # data = io.StringIO(data_string)
        # table = pd.read_csv(data, sep='|')
        #
        # if self.campana_cons:
        #     self.data_final[compania_pais] = table.copy()
        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + self.campanas_agg.replace("..", "_") + '.txt', index=False)
        # else:
        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt', index=False)

        browser.find_element(by='xpath', value='//*[@id="boto_reto"]').click()
        '''
        while "temporales" in browser.current_url:
            browser.back()
            time.sleep(0.2)
        '''
        time.sleep(5)

    def get_orden_x_nume_fact(self, fecha='2022-01..2022-12', file_name=None, ruta_imprimir=None):
        compania = self.compania
        pais = self.pais
        browser = self.browser
        time_to_charge = self.time_to_charge
        time_to_export = self.time_to_export
        root_out_country = self.root_out_country
        root_out_country_b = self.root_out_country_b
        report = 'ORDEN_X_NUME_FACT'

        browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()

        time.sleep(time_to_charge)
        # TEXTBOX FECHA(*)
        browser.find_element(by='name', value='_g_IG~FECH_MOVI').clear()
        browser.find_element(by='name', value='_g_IG~FECH_MOVI').send_keys(fecha)

        # try:
        #     pais_path = browser.find_element(by='id', value="_g_codi_pais")
        #     pais_path.clear()
        #     pais_path.send_keys(pais)
        #     time.sleep(time_to_charge)
        # except:
        #     print("no tiene pais")

        # %%%% exportar
        print(" exportando orden_x_nume_fact")
        browser.find_elements(by='name', value='tipo_sali')[1].click()
        try:
            browser.find_element(by='xpath', value='//*[@id="boto_gene"]').click()
        except:
            browser.find_element(by='xpath', value='/ html / body / center / form / table / tbody / tr[3] / td / input[1]').click()
            time.sleep(time_to_export)
            while "CONSOLIDADO" in browser.current_url:
                browser.back()
                time.sleep(0.2)
            time.sleep(2)
            browser.find_element(by='xpath', value='//input[@value="' + report + '"]').click()
            time.sleep(time_to_charge) #problemas de ecu

        time.sleep(time_to_export)
        try:
            xpath_export = [i for i in browser.find_elements(by='xpath', value='.//*') if i.get_attribute('href') is not None]
            xpath_export = [i for i in xpath_export if (report in i.get_attribute('href') and (".xls" in i.get_attribute('href')))]
            xpath_export[0].click()
            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[3]/td[3]/font/a/img').click()
            # browser.find_element(by='xpath', value='/html/body/center/table/tbody/tr[4]/td[2]/font/a').click()
            time.sleep(1)
        except:
            print(" No requiere acceder al reporte manualmente")

        # periodo de descarga de archivo
        time.sleep(30)
        filename = None
        for k in range(10):
            filename = max([root_out_country + "\\" + f for f in os.listdir(root_out_country)], key=os.path.getctime)
            if filename[-4:] == '.xls':
                print("se ha descargado el archivo")
                break
            time.sleep(20)

        if file_name is None:
            file_name = report
        shutil.move(filename, os.path.join(root_out_country_b, file_name + '.txt'))
        # os.rename(filename, root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt')
        if ruta_imprimir is not None:
            shutil.move(os.path.join(root_out_country_b, file_name + '.txt'),
                        ruta_imprimir.replace('/', '\\') + file_name + '.txt')
        # scroll_down(browser=browser)

        # data_string = browser.find_element(by='xpath', value='.//pre').text
        # data = io.StringIO(data_string)
        # table = pd.read_csv(data, sep='|')
        #
        # if self.campana_cons:
        #     self.data_final[compania_pais] = table.copy()
        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + self.campanas_agg.replace("..", "_") + '.txt', index=False)
        # else:
        #     table.to_csv(root_out_country + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt', index=False)

        browser.find_element(by='xpath', value='//*[@id="boto_reto"]').click()
        '''
        while "temporales" in browser.current_url:
            browser.back()
            time.sleep(0.2)
        '''
        time.sleep(5)

    def close_browser(self):
        self.browser.close()
        os.rmdir(self.root_out_country) #Eliminar carpeta temporal
        print(f"Se ha cerrado la sesion del web scrap")
        self.browser.quit()

    def limpieza_campanas(self, root_now=None, report=None):
        files = glob.glob(f"{root_now}/{self.user}*.*")
        pais = root_now.split("\\")[-3][-3:]
        for f in files:
            file = open(f, "r")
            data = io.StringIO(file.read(50))
            table = pd.read_csv(data, sep='\t')
            campanita = str(table['CAMP'][0])
            print(campanita)
            os.rename(f, root_now + "\\" + pais + "_" + report + "_" + str(campanita) + '.txt')


# self = App(paises=['ECU'], camp_ini=201901, camp_fin=202203, campana_cons=False)
# self.submit()

# self = App(paises=['BOL'], camp_ini=202212, camp_fin=202213, campana_cons=False, compania=['AZZ'])
# obj_scrap.submit()
