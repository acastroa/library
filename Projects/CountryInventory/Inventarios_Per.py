import pandas as pd
import numpy as np
import os
import glob
from AzzLib import LoadData as Ld
import datetime as dt


class Inventarios:
    def __init__(self):
        self.df_con_prod = None
        self.df_inv = None
        self.df_inv_agg = None
        self.df_fac = None
        self.df_stock_act = None
        self.df_sust = None
        self.df_ztpla23 = None
        self.df_ztpla15 = None
        self.df_inv_resumen = None
        self.df_inv_base = None

    def get_inv_per(self, aggregate=True, max_camp_inv=0.5, max_stock_proy=100, company=None):
        """
        Funcion que obtiene el inventario de PER, junto a la clasificación de tipo de salida para catalogo o no
        :param aggregate: Por defecto True, agrega los SKUs por CODI_VENT, en caso contrario los agrega por CODI_PROD
        :param max_camp_inv:  permite indicar el maximo de valor de Campañas de Inventario para que un CODI_VENT sea considerado como NO CATALOGO
        :param max_stock_proy: permite indicar el maximo de valor de Stock Proyectado para que un CODI_VENT sea considerado como NO CATALOGO
        :param company: Por defecto None, puede tomar los valores 'AZZ', 'LVV' permite indicar si se debe filtrar una compania o no, en caso se indique 'LVV', solo se mostrara de la linea ROPA
        :return: Dataframe con la informacion del inventario y la clasificacion
        """
        q_inv_pai = '''
                    SELECT dt.fech_dia_max AS 'FECHA_CONSULTA', dt.pais_orig AS 'PAIS_ORIGEN', iv.pais AS 'PAIS', 
                           TRIM(iv.codi_prod) AS 'CODI_PROD',
                           TRIM(iv.codi_sap) AS 'CODI_SAP',
                           TRIM(pr.desc_compl) AS 'NOMBRE_SID',
                           TRIM(pr.tipo_arti) AS 'TIPO_ARTI_SAP',
                           TRIM(sb.nomb_line) AS 'LINEA',
                           TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO',
                           TRIM(pr.desc_corta) AS 'REFERENCIA',
                           TRIM(pr.desc_talla) AS 'TALLA',
                           SUM(CAST(iv.stock AS float)) AS 'STOCK'
                    FROM (
                          SELECT pais_orig, MAX(fech_dia) AS 'fech_dia_max'
                          FROM [dwh_azzorti].[inv].[base_inventarios] 
                          WHERE pais_orig IN ('PER') GROUP BY pais_orig 
                         ) dt LEFT JOIN [dwh_azzorti].[inv].[base_inventarios] iv 
                         ON dt.fech_dia_max = iv.fech_dia AND dt.pais_orig = iv.pais_orig 
                         LEFT JOIN [dim_general].[dbo].[dim_producto] pr
                         ON iv.codi_prod = pr.codi_prod AND iv.codi_sap = pr.codi_sap
                         LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                         ON pr.cons_subgrup = sb.cons_subgrup
                    WHERE iv.tipo_mate = 'PT' AND iv.stock > 0 AND iv.pais = 'PER'
                    GROUP BY dt.fech_dia_max, dt.pais_orig, iv.pais, iv.codi_prod, iv.codi_sap, pr.desc_compl,
                             pr.tipo_arti, sb.nomb_line, sb.nomb_subgrupo, pr.desc_corta, pr.desc_talla
                    '''
        df_stock_act = Ld.get_file_sql(query=q_inv_pai)
        df_stock_act[['CODI_SAP', 'CODI_PROD']] = df_stock_act[['CODI_SAP', 'CODI_PROD']].astype(str)

        fecha_inve = df_stock_act.iloc[0][0]

        query = f'''
                SELECT codi_camp
                FROM [dim_general].[dbo].[dim_campana]
                WHERE fech_inic <= '{fecha_inve}' AND fech_fina >= '{fecha_inve}' AND codi_pais = 'PER'
                '''
        camp_act = int(Ld.get_file_sql(query=query).iloc[0][0])

        oazz = Ld.DataOri(paises=['PER'], company='AZZ', filters=True, camp_ini=1)
        olvv = Ld.DataOri(paises=['PER'], company='LVV', filters=True, camp_ini=1)

        # CARGA DE TABLA FACTURACION
        z = olvv.get_zdm_mat_art(sep=';')
        df_fac = pd.concat([oazz.get_fac(fac_type='TPA', falt_anunc=False, with_back_order=True),
                            olvv.get_fac(fac_type='TPA', falt_anunc=False, with_back_order=True)], ignore_index=True)

        # CARGA DE CODIGOS SUSTITUTOS

        # oazz.get_sustitutos(origen='SQL')
        paises_cadena = "','".join(['PER', 'LPE'])

        # sust_identicos
        query = f"""
                 SELECT DISTINCT CAST(TRIM(si.codi_padr_sid) AS varchar) AS 'CODI_ORIG',
                        TRIM(si.codi_sust_sid) AS 'CODI_PROD',
                        TRIM(si.codi_padr_sap) AS 'CODI_ORIG_SAP',
                        TRIM(si.codi_sust_sap) AS 'CODI_PROD_SAP',
                        TRIM(pr.desc_compl) AS 'NOMBRE_SID_ORIG',
                        TRIM(pr.tipo_arti) AS 'TIPO_ARTI_SAP_ORIG',
                        TRIM(sb.nomb_line) AS 'LINEA_ORIG',
                        TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO_ORIG',
                        TRIM(pr.desc_corta) AS 'REFERENCIA_ORIG',
                        TRIM(pr.desc_talla) AS 'TALLA_ORIG'
                 FROM [dim_general].[dbo].[dim_sust_iden] si LEFT JOIN [dim_general].[dbo].[dim_producto] pr
                      ON si.codi_padr_sid = pr.codi_prod AND si.codi_padr_sap = pr.codi_sap
                      LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                      ON pr.cons_subgrup = sb.cons_subgrup
                 WHERE si.codi_pais IN ('{paises_cadena}') AND LEN(CAST(TRIM(si.codi_padr_sid) AS varchar)) >= 6
                 """
        df_sust_ide = Ld.get_file_sql(query=query)
        df_sust_ide[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']] = df_sust_ide[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']].astype(str)

        # sust_sap
        query = """
                SELECT DISTINCT CAST(TRIM(po.codi_prod) AS varchar) AS 'CODI_ORIG', 
                       TRIM(ps.codi_prod) AS 'CODI_PROD',
                       TRIM(su.codi_padr) AS 'CODI_ORIG_SAP',
                       TRIM(su.codi_prod_sap) AS 'CODI_PROD_SAP',
                       TRIM(po.desc_compl) AS 'NOMBRE_SID_ORIG',
                       TRIM(po.tipo_arti) AS 'TIPO_ARTI_SAP_ORIG',
                       TRIM(sb.nomb_line) AS 'LINEA_ORIG',
                       TRIM(sb.nomb_subgrupo) AS 'SUBGRUPO_ORIG',
                       TRIM(po.desc_corta) AS 'REFERENCIA_ORIG',
                       TRIM(po.desc_talla) AS 'TALLA_ORIG'
                FROM [dim_general].[dbo].[dim_sustitutos] su 
                     LEFT JOIN [dim_general].[dbo].[dim_producto] po 
                            ON CAST(TRIM(su.codi_padr) AS varchar) = CAST(TRIM(po.codi_sap) AS varchar) 
                     LEFT JOIN [dim_general].[dbo].[dim_producto] ps 
                            ON CAST(TRIM(su.codi_prod_sap) AS varchar) = CAST(TRIM(ps.codi_sap) AS varchar) 
                     LEFT JOIN [dim_general].[dbo].[dim_subgrupo] sb
                            ON po.cons_subgrup = sb.cons_subgrup 
                WHERE LEN(TRIM(po.codi_prod)) >= 6
                """
        df_sust_sap = Ld.get_file_sql(query=query)
        df_sust_sap[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']] = df_sust_sap[['CODI_ORIG', 'CODI_PROD', 'CODI_ORIG_SAP', 'CODI_PROD_SAP']].astype(str)
        df_sust_sap = df_sust_sap[~df_sust_sap['CODI_PROD'].isnull()].reset_index(drop=True)
        df_sust_sap = df_sust_sap.drop(df_sust_sap[(df_sust_sap.CODI_PROD == '434855') &
                                                   (df_sust_sap.CODI_ORIG == '275835')].index, axis=0
                                       ).reset_index(drop=True)
        df_sust_sap = df_sust_sap[~df_sust_sap['CODI_PROD'].isin(['1255004', '773876', '582663', '246978', '953446',
                                                                  '432352'])].reset_index(drop=True)

        df_sust = pd.concat([df_sust_ide, df_sust_sap]).drop_duplicates(['CODI_PROD'], ignore_index=True).drop_duplicates(['CODI_PROD_SAP'], ignore_index=True)

        df_stock_act = df_stock_act.merge(df_sust, on='CODI_PROD', how='left')
        df_stock_act['CODI_ORIG'] = np.where(df_stock_act['CODI_ORIG'].isna(), df_stock_act['CODI_PROD'],
                                             df_stock_act['CODI_ORIG'])
        df_stock_act['CODI_ORIG_SAP'] = np.where(df_stock_act['CODI_ORIG_SAP'].isna(), df_stock_act['CODI_SAP'],
                                                 df_stock_act['CODI_ORIG_SAP'])
        cols_orig = ['NOMBRE_SID', 'TIPO_ARTI_SAP', 'LINEA', 'SUBGRUPO', 'REFERENCIA', 'TALLA']
        for col in cols_orig:
            df_stock_act[col + '_ORIG'] = np.where(df_stock_act[col + '_ORIG'].isna(), df_stock_act[col],
                                                   df_stock_act[col + '_ORIG'])
        del df_stock_act['CODI_PROD_SAP']
        df_stock_act = df_stock_act.groupby(['PAIS_ORIGEN', 'FECHA_CONSULTA', 'CODI_ORIG', 'CODI_ORIG_SAP',
                                             'NOMBRE_SID_ORIG', 'TIPO_ARTI_SAP_ORIG', 'LINEA_ORIG', 'SUBGRUPO_ORIG', 'REFERENCIA_ORIG', 'TALLA_ORIG'],
                                            as_index=False).agg({'STOCK': 'sum'})

        df_fac = df_fac.merge(df_sust[['CODI_ORIG', 'CODI_PROD']], on='CODI_PROD', how='left')
        df_fac['CODI_ORIG'] = np.where(df_fac['CODI_ORIG'].isna(), df_fac['CODI_PROD'], df_fac['CODI_ORIG'])
        df_fac = df_fac.sort_values(['PAIS', 'CAMP', 'CODI_VENT', 'CODI_ORIG'], ascending=False
                                    ).drop_duplicates(['CODI_ORIG'])
        # df_fac = df_fac.drop(['CODI_ORIG_SAP', 'CODI_PROD_SAP'], axis=1)

        # CRUCE CON Z15 Y LL
        path_boztpla15 = Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/BOztplan_15_' + str(fecha_inve) + '.csv'
        if not os.path.isfile(path_boztpla15):
            # fecha_inve_dat = dt.datetime.strptime(fecha_inve, '%Y-%m-%d')
            fecha_inve_dat = dt.datetime(fecha_inve.year, fecha_inve.month, fecha_inve.day)

            dates_str = [f[-14:-4] for f in glob.glob(Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/*.csv')]
            dates_dat = [fecha_inve_dat - dt.datetime.strptime(f, '%Y-%m-%d') if fecha_inve_dat > dt.datetime.strptime(f, '%Y-%m-%d') else dt.timedelta(999) for f in dates_str]
            min_date = min(dates_dat)
            fecha_inve = [f for f, g in zip(dates_str, dates_dat) if g == min_date][0]
            path_boztpla15 = Ld.root_supply_azz + '/1.- CMN/1.- ORI/7.- ZTP_L15/hist_BOztplan_15/BOztplan_15_' + str(
                fecha_inve) + '.csv'

        df_ztpla15 = pd.read_csv(path_boztpla15, sep=';', low_memory=False, dtype={'Producto_Original': str},
                                 usecols=['Indicador_movimiento', 'Pais_Origen', 'Num_Documento', 'Producto_Original',
                                          'Cantidad_Proyectada'])
        df_ztpla15['Producto_Original'] = df_ztpla15['Producto_Original'].str.strip()
        df_ztpla15['Producto_Original'] = df_ztpla15['Producto_Original'].astype(str)

        df_ztpla15 = df_ztpla15[(df_ztpla15['Indicador_movimiento'] == 'FACT') & (df_ztpla15['Pais_Origen'] == 'PER')
                                & (df_ztpla15['Num_Documento'] == camp_act)].reset_index(drop=True)

        df_ztpla15 = df_ztpla15.merge(df_sust[['CODI_PROD_SAP', 'CODI_ORIG_SAP']].rename(columns={'CODI_PROD_SAP': 'Producto_Original'}),
                                      on='Producto_Original', how='left')
        df_ztpla15['CODI_ORIG_SAP'] = np.where(df_ztpla15['CODI_ORIG_SAP'].isnull(), df_ztpla15['Producto_Original'],
                                               df_ztpla15['CODI_ORIG_SAP'])

        df_ztpla15 = df_ztpla15.groupby(['CODI_ORIG_SAP', 'Pais_Origen', 'Num_Documento'], as_index=False
                                        ).agg({'Cantidad_Proyectada': 'sum'})
        df_ztpla15.columns = ['CODI_ORIG_SAP', 'PAIS', 'CAMPANA', 'PROY_FACT_CAMP_ACT']

        q_ll = f"""
               SELECT TRIM(codi_prod_sap) AS 'CODI_PROD_SAP', CAST(tota_und AS float) AS 'tota_und'
               FROM [dwh_azzorti].[mer].[leader_list]
               WHERE pais = 'PER' AND camp_sid {'>=' if df_ztpla15['PROY_FACT_CAMP_ACT'].sum() == 0 else '>'} {str(camp_act)}
               """
        df_ztpla23 = Ld.get_file_sql(query=q_ll)
        df_ztpla23['CODI_PROD_SAP'] = df_ztpla23['CODI_PROD_SAP'].astype(str)
        df_ztpla23 = df_ztpla23.merge(df_sust, on='CODI_PROD_SAP', how='left')
        df_ztpla23['CODI_ORIG_SAP'] = np.where(df_ztpla23['CODI_ORIG_SAP'].isnull(), df_ztpla23['CODI_PROD_SAP'],
                                               df_ztpla23['CODI_ORIG_SAP'])

        df_ztpla23 = df_ztpla23.groupby(['CODI_ORIG_SAP'], as_index=False).agg({'tota_und': 'sum'})

        # FIN DE CRUZE
        # df_final = df_stock_act.merge(df_con_prod, on='CODI_ORIG', how='left')
        df_final = df_stock_act[df_stock_act['STOCK'] > 0].reset_index(drop=True)
        df_final['COMPANIA'] = np.where(df_final['TIPO_ARTI_SAP_ORIG'].str[0] == 'Y',
                                        'LVV',
                                        np.where(df_final['TIPO_ARTI_SAP_ORIG'].str[0] == 'Z', 'AZZ', np.nan))
        df_final = df_final.merge(df_ztpla15[['CODI_ORIG_SAP', 'PROY_FACT_CAMP_ACT']], on='CODI_ORIG_SAP', how='left')
        df_final = df_final.merge(df_ztpla23, on='CODI_ORIG_SAP', how='left')
        df_final[['PROY_FACT_CAMP_ACT', 'tota_und', 'STOCK']] = df_final[['PROY_FACT_CAMP_ACT', 'tota_und', 'STOCK'
                                                                          ]].astype(float).fillna(0)

        df_final = df_final.merge(df_fac[['CODI_VENT', 'CODI_ORIG']], on='CODI_ORIG', how='left')
        df_final['CODI_VENT'] = np.where(df_final['CODI_VENT'].isna(), df_final['CODI_ORIG'], df_final['CODI_VENT'])
        df_final['PROYECTADO'] = df_final['PROY_FACT_CAMP_ACT'] + df_final['tota_und']
        df_final = df_final.rename(columns={'CODI_ORIG': 'CODI_PROD', 'CODI_ORIG_SAP': 'CODI_SAP',
                                            'NOMBRE_SID_ORIG': 'NOMBRE_SID', 'TIPO_ARTI_SAP_ORIG': 'TIPO_ARTI_SAP',
                                            'LINEA_ORIG': 'LINEA', 'SUBGRUPO_ORIG': 'SUBGRUPO',
                                            'REFERENCIA_ORIG': 'REFERENCIA', 'TALLA_ORIG': 'TALLA'})
        # carga de tipo de lenta rotacion
        ls_lr = Ld.root_supply_azz + '/2.- PER/1.- ORI/7.- LEN_ROT/LENTA ROTACION.xlsx'
        df_lr = pd.read_excel(ls_lr, sheet_name=0, dtype={'CODIGOS': str})
        df_lr = df_lr.rename(columns={'CODIGOS': 'CODI_SAP'})
        df_final['LENTA_ROTACION'] = df_final.merge(df_lr, on='CODI_SAP', how='left')['CATE_ROT']
        df_final['REF_2'] = np.where(df_final['REFERENCIA'].str.contains('T-') &
                                         df_final['TIPO_ARTI_SAP'].isin(
                                             ['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                         df_final['REFERENCIA'].apply(lambda x: x.split("T-")[0]), np.nan)
        df_final['REFERENCIA'] = np.where(df_final['REFERENCIA'].str.contains('T-') &
                                              df_final['TIPO_ARTI_SAP'].isin(
                                                  ['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                              df_final['REF_2'], df_final['REFERENCIA'])
        df_final['REFERENCIA'] = df_final['REFERENCIA'].str.strip()

        if aggregate:
            df_final_agg = df_final.groupby(['FECHA_CONSULTA', 'PAIS_ORIGEN', 'CODI_VENT'], as_index=False
                                            ).agg({'NOMBRE_SID': 'first', 'LINEA': 'first', 'SUBGRUPO': 'first',
                                                  'TIPO_ARTI_SAP': 'first', 'COMPANIA': 'first', 'STOCK': 'sum',
                                                   'PROYECTADO': 'sum', 'CODI_PROD': 'count', 'LENTA_ROTACION': 'first',
                                                   'REFERENCIA': 'first', 'TALLA': 'first'
                                                   })
            df_final_agg = df_final_agg.rename(columns={'CODI_PROD': 'CANT_CODS'})

        else:
            df_final_agg = df_final.groupby(['FECHA_CONSULTA', 'PAIS_ORIGEN', 'CODI_SAP', 'CODI_PROD',
                                             'CODI_VENT'], as_index=False
                                            ).agg({'NOMBRE_SID': 'first', 'LINEA': 'first', 'SUBGRUPO': 'first',
                                                   'TIPO_ARTI_SAP': 'first', 'COMPANIA': 'first', 'STOCK': 'sum',
                                                   'PROYECTADO': 'sum', 'REFERENCIA': 'first', 'TALLA': 'first'
                                                   })
        df_final_agg['STOCK-PROYECTADO'] = df_final_agg['STOCK'] - df_final_agg['PROYECTADO']
        df_final_agg['STOCK-PROYECTADO'] = np.where(df_final_agg['STOCK-PROYECTADO'] < 0, 0,
                                                    df_final_agg['STOCK-PROYECTADO'])
        df_final_agg['REF_2'] = np.where(df_final_agg['REFERENCIA'].str.contains('T-') &
                                           df_final_agg['TIPO_ARTI_SAP'].isin(['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                           df_final_agg['REFERENCIA'].apply(lambda x: x.split("T-")[0]), np.nan)
        df_final_agg['REFERENCIA'] = np.where(df_final_agg['REFERENCIA'].str.contains('T-') &
                                              df_final_agg['TIPO_ARTI_SAP'].isin(['YPRE', 'YPRI', 'ZPRE', 'ZPRI', 'ZPCA', 'ZPJO']),
                                              df_final_agg['REF_2'], df_final_agg['REFERENCIA'])
        df_final_agg['REFERENCIA'] = df_final_agg['REFERENCIA'].str.strip()
        del df_final_agg['REF_2']
        # inventario base sin filtrar
        self.df_inv_base = df_final_agg.copy()
        df_final_agg = df_final_agg[df_final_agg['STOCK-PROYECTADO'] > 0].reset_index(drop=True)

        # FILTRO DE COMPANY
        if company == 'LVV':
            df_final_agg_bu = df_final_agg.copy()
            df_final_agg = df_final_agg[(df_final_agg['COMPANIA'] == 'LVV') &
                                        (df_final_agg['TIPO_ARTI_SAP'].isin(['YPRE', 'YPRI']))].reset_index(drop=True)
        if company == 'AZZ':
            df_final_agg = df_final_agg[df_final_agg['COMPANIA'] == 'AZZ'].reset_index(drop=True)

        # ADICION DE NUEVA COLUMNA
        # ultima salida
        df_final_agg['ULT_CAMP'] = df_final_agg.merge(df_fac.drop_duplicates('CODI_VENT', ignore_index=True), on='CODI_VENT', how='left')['CAMP']
        df_fac['FACTOR'] = df_fac['UND_PEDIDAS'] / df_fac['TOTAL_PEDIDOS']
        df_final_agg['ULT_FACTOR'] = df_final_agg.merge(df_fac.drop_duplicates('CODI_VENT', ignore_index=True), on='CODI_VENT', how='left')['FACTOR']
        df_final_agg['ULT_UND_PEDIDAS'] = df_final_agg.merge(df_fac.drop_duplicates('CODI_VENT', ignore_index=True), on='CODI_VENT', how='left')['UND_PEDIDAS']
        df_final_agg['CAMP_INV'] = df_final_agg['STOCK-PROYECTADO'] / df_final_agg['ULT_UND_PEDIDAS']
        tipo_tallas = ['ZPRI', 'ZPRE', 'YPRE', 'YPRI', 'ZPCA', 'YPCA', 'ZPJO', 'YPJO']
        df_final_agg['REF_aux'] = np.where(df_final_agg['TIPO_ARTI_SAP'].isin(tipo_tallas),
                                           df_final_agg['REFERENCIA'],
                                           df_final_agg['CODI_VENT'])
        x_ref = df_final_agg.groupby(['REF_aux'], as_index=False).agg({'ULT_CAMP': lambda x: x.dropna().max(),
                                                                       'CAMP_INV': lambda x: pd.Series([y for y in x if y != np.inf], dtype=float).dropna().min()})

        x_ref = x_ref[(x_ref['ULT_CAMP'].isnull()) | (x_ref['CAMP_INV'].isnull())]
        df_final_agg_no_data = df_final_agg[df_final_agg['REF_aux'].isin(x_ref['REF_aux'])].reset_index(drop=True)
        df_final_agg = df_final_agg[~(df_final_agg['REF_aux'].isin(x_ref['REF_aux']))].reset_index(drop=True)
        qs = df_final_agg['CAMP_INV'].quantile([.20, .5, .75]).values

        if max_camp_inv < qs[0]:
            corte_1 = max_camp_inv
            corte_2 = qs[0]
            tramo_0 = f"De 0 a {max_camp_inv:,.2f}"
            tramo_1 = f"De {max_camp_inv:,.2f} a q20"
            tramo_2 = f"De q20 a mas"
        else:
            corte_1 = qs[0]
            corte_2 = max_camp_inv
            tramo_0 = f"De 0 a q20"
            tramo_1 = f"De q20 a {max_camp_inv:,.2f}"
            tramo_2 = f"De {max_camp_inv:,.2f} a mas"

        df_final_agg['TRAMO'] = np.where(df_final_agg['CAMP_INV'] < corte_1, tramo_0,
                                         np.where(df_final_agg['CAMP_INV'] < corte_2, tramo_1,
                                                  np.where(df_final_agg['CAMP_INV'] >= corte_2, tramo_2,
                                                           np.nan)))

        # min 0.5 y q20

        df_final_agg_f = df_final_agg[df_final_agg['CAMP_INV'] < min(max_camp_inv, qs[0])].reset_index(drop=True)
        df_final_agg_nf = df_final_agg[~(df_final_agg['CAMP_INV'] < min(max_camp_inv, qs[0]))].reset_index(drop=True)
        df_final_agg_f = df_final_agg_f.sort_values(['LENTA_ROTACION', 'CAMP_INV'], ignore_index=True)

        qss = max(df_final_agg_f['STOCK-PROYECTADO'].quantile(.95), max_stock_proy)
        df_final_agg_nf = pd.concat([df_final_agg_nf, df_final_agg_f[~(df_final_agg_f['STOCK-PROYECTADO'] < qss)].reset_index(drop=True)], ignore_index=True)

        df_final_agg_nf['REQ_SKU'] = None
        df_final_agg_f = df_final_agg_f[df_final_agg_f['STOCK-PROYECTADO'] < qss].reset_index(drop=True)
        df_final_agg_f['REQ_SKU'] = 'X'
        df_final_agg_nf_ = df_final_agg_nf[df_final_agg_nf['REF_aux'].isin(df_final_agg_f['REF_aux'])].reset_index(drop=True)
        df_final_agg_nf = df_final_agg_nf[~df_final_agg_nf['REF_aux'].isin(df_final_agg_f['REF_aux'])].reset_index(drop=True)
        df_final_agg_f = pd.concat([df_final_agg_f, df_final_agg_nf_], ignore_index=True)

        df_final_agg_f['SALIDA'] = df_final_agg_f.groupby(['REF_aux'])['REQ_SKU'].transform(lambda x: 'CATALOGO REF INC' if len(set(x)) > 1 else 'NO CATALOGO')
        df_final_agg_nf['SALIDA'] = 'CATALOGO'

        df_final_agg_no_data['REQ_SKU'] = 'X'
        df_final_agg_no_data['SALIDA'] = 'NO CATALOGO'
        df_final_agg_no_data = df_final_agg_no_data.drop(['REF_aux'], axis=1)
        df_final_ = pd.concat([df_final_agg_f, df_final_agg_nf, df_final_agg_no_data], ignore_index=True).drop(['ULT_CAMP', 'ULT_FACTOR', 'ULT_UND_PEDIDAS', 'CAMP_INV', 'REF_aux'], axis=1)

        df_final_res = df_final_.groupby(['SALIDA', 'TIPO_ARTI_SAP', 'REFERENCIA'], as_index=False).agg({'STOCK-PROYECTADO': 'sum', 'CODI_VENT': 'count'}).sort_values(['CODI_VENT', 'STOCK-PROYECTADO'], ascending=False)
        df_final_res = df_final_res.groupby(['SALIDA', 'TIPO_ARTI_SAP'], as_index=False).agg({'STOCK-PROYECTADO': 'mean', 'CODI_VENT': 'mean', 'REFERENCIA': 'count'})

        self.df_stock_act = df_stock_act
        self.df_fac = df_fac
        self.df_sust = df_sust
        self.df_ztpla15 = df_ztpla15
        self.df_ztpla23 = df_ztpla23
        self.df_inv = df_final
        self.df_inv_agg = df_final_
        self.df_inv_resumen = df_final_res


