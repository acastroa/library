from io import BytesIO
from ftplib import FTP
import pandas as pd
import numpy as np
import os
import datetime
from Code.sap_rpa import SAP
from Code.MDVP_auth import MDVPv1

class XLocIndexer:
    def __init__(self, frame):
        self.frame = frame

    def __getitem__(self, key):
        row, col = key
        return self.frame.iloc[row][col]


pd.core.indexing.IndexingMixin.xloc = property(lambda frame: XLocIndexer(frame))


class MDVPv2(MDVPv1, SAP):
    name_user = os.getlogin()
    root_main = "C:/Users/" + name_user
    root_input = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/" \
                             "Projects/01. Supply/08. MDVP/Files/1. INPUT/"

    # ftp_today = datetime.datetime.strftime(datetime.datetime.today(), "%Y%m%d")
    print('FECHA_TEST___FECHA_TEST___FECHA_TEST___FECHA_TEST___FECHA_TEST___')
    ftp_today = '20220303'
    d_cred = eval(open(root_input + 'credenciales.txt').read())
    user_ftp, password_ftp, host_ftp = d_cred['usuario_ftp'], d_cred['password_ftp'], d_cred['host_ftp']

    def __init__(self, weeks=24, prop_amortiguador=0.5, capacity_relax=1.01):
        super().__init__()
        self.df_firme = None
        self.df_reacciones = None
        self.df_planeado = None
        self.df_amortiguador = None
        self.df_final = None
        self.d_recursos = None
        self.df_cap = None
        self.tipos_prenda = None
        self.calendar = None
        self.cap_disp = None
        self.today = datetime.datetime.strptime(MDVPv2.ftp_today, "%Y%m%d")
        self.weeks = weeks
        self.days = weeks * 7
        self.prop_amortiguador = prop_amortiguador
        self.capacity_relax = capacity_relax

        self.get_parametros()

    def get_parametros(self):
        df_amt = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Amortiguador').iloc[:, :2]
        df_amt.columns = ['FAMILIA_PROD_LARGO', 'AMORTIGUADOR']
        self.df_amortiguador = df_amt.copy()

        tipos_prenda = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Tipos Prenda').iloc[:, 0].values
        self.tipos_prenda = [i.strip().upper() for i in tipos_prenda]

        # Recursos
        df_rec = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Recursos')
        festivos = np.array(df_rec['Festivos'].dropna().values, dtype='datetime64[D]')
        # self.festivos = [pd.to_datetime(i).to_pydatetime() for i in festivos]
        self.calendar = np.busdaycalendar(weekmask='1111110', holidays=festivos)

        df_rec.dropna(how='all', inplace=True)
        df_rec.dropna(axis=1, how='all', inplace=True)
        df_rec.columns = df_rec.columns.map(lambda x: x.strip().upper())
        df_tot = df_rec.loc[
            df_rec['PROVEEDOR'] == 'TOTAL CAPACIDAD DIARIA', df_rec.columns[df_rec.columns.isin(tipos_prenda)]]
        self.d_recursos = df_tot.iloc[0].to_dict()

        self.df_cap = pd.DataFrame({'CAPACIDAD': self.d_recursos}).reset_index().rename(columns={'index': 'FAMILIA'})

        df_reac = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Reacciones')
        df_reac.columns = ['FAMILIA', 'FAMILIA_PROD_LARGO', 'MINU_CONF', 'FECHA_PROMETIDA']
        df_reac = df_reac.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')
        df_reac['START_DATE'] = np.busday_offset(df_reac['FECHA_PROMETIDA'].values.astype('datetime64[D]'),
                                                 df_reac['AMORTIGUADOR'] * self.prop_amortiguador, roll='forward',
                                                 busdaycal=self.calendar)
        df_reac = df_reac.groupby(['FAMILIA', 'START_DATE'], as_index=False)['MINU_CONF'].sum()
        df_reac.set_index(['FAMILIA', 'START_DATE'], inplace=True)
        self.df_reacciones = df_reac.copy()

    def get_from_ftp(self, path=None, file=None, sep=';', encoding='cp437', low_memory=True,
                     thousands=None, decimal='.', engine=None):
        """
            Con esta función se lee 1 reporte desde un servidor ftp, indicando:
            :param
                hostname: url o ip a conectarse
                username: usuario para conectarse
                password: contraseña para la conexión
                path: ruta dentro del servidor
                file: nombre del archivo a leer
            :return: retorna un dataframe con el archivo indicado
        """

        ftp = FTP(MDVPv2.d_cred['host_ftp'], MDVPv2.d_cred['usuario_ftp'], MDVPv2.d_cred['password_ftp'])
        file = ftp.nlst(file)[-1]
        # file = ftp.nlst(file)[0]
        if isinstance(path, str):
            ftp.cwd(path)
        with BytesIO() as zz:
            ftp.retrbinary('RETR ' + file, zz.write)
            zz.seek(0)
            # df = pd.read_csv(zz, encoding="cp437", sep=";")
            self.df_firme = pd.read_csv(zz, sep=sep, encoding=encoding, low_memory=low_memory, thousands=thousands,
                                        decimal=decimal,
                                        engine=engine)
            self.df_firme.to_csv(f'{MDVPv2.root_input}/{file[9:]}')
        print(f"Archivo {file} descargado con éxito de {MDVPv2.d_cred['host_ftp']}.")
        return self.df_firme

    def smooth_backwards(self, idx_row=None, row_data=None, sooner=True):
        sooner = {True: -1, False: 0}[sooner]

        posible_dates = self.cap_disp.loc[row_data['FAMILIA'], self.today:row_data['START_DATE']].iloc[:-1]
        posible_dates = posible_dates[(posible_dates.values >= row_data['MINU_CONF_AUX'])]

        if posible_dates.shape[0] >= 1:
            idx_date_suav = posible_dates.index[sooner]
            self.df_final.loc[idx_row, 'START_DATE'] = idx_date_suav[1]
            self.df_final.loc[idx_row, 'LOTE'] = -row_data['LOTE']
            self.cap_disp.loc[idx_date_suav] -= np.round(row_data['MINU_CONF_AUX'], 2)
            return True
        return False

    def smooth(self, row=None, sooner=True):
        row_1 = self.df_final.iloc[row].copy()
        row_2 = self.df_final.iloc[row].copy()
        row_1['LOTE'] = 1
        idx_row = self.df_final.iloc[row].name
        # Debido a la relajación de la capacidad pueden haber negativos al recalcular el MINU_CONF_AUX en row_1
        # Intentar suavizar hacia atrás antes de dividir la orden
        if not self.smooth_backwards(idx_row=idx_row, row_data=row_1, sooner=sooner):  # No se logró suavizar el row completo
            # Row1
            row_1['MINU_CONF_AUX'] = np.round(row_1['CAPACIDAD'] - (row_1['MINU_CONF_ACUM'] - row_1['MINU_CONF_AUX']), 2)
            self.df_final.iloc[row] = row_1
            # Row2
            row_2['MINU_CONF_AUX'] = np.round(row_2['MINU_CONF_AUX'] - row_1['MINU_CONF_AUX'], 2)
            row_2['LOTE'] = row_1['LOTE'] + 1
            row_2.name = row_1.name+0.1
            self.df_final.loc[row_2.name] = row_2
            self.df_final.sort_index(inplace=True)

            self.cap_disp.loc[row_1['FAMILIA'], row_1['START_DATE']] -= row_1['MINU_CONF_AUX']
            # Suavizar hacia atrás parcialmente
            if not self.smooth_backwards(idx_row=row_2.name, row_data=row_2, sooner=sooner):  # No se logró suavizar el row parcial
                # Posponer fechas similares al valor dividido
                row_fltr = (self.df_final['FAMILIA'] == row_2['FAMILIA']) & (self.df_final['START_DATE'] == row_2['START_DATE'])
                idx = self.df_final.loc[row_2.name:].loc[row_fltr, 'START_DATE'].index
                new_date = np.busday_offset(self.df_final.loc[idx, 'START_DATE'].values[0].astype('datetime64[D]'),
                                            offsets=1, roll='forward', busdaycal=self.calendar)
                self.df_final.loc[idx, 'START_DATE'] = new_date
                self.cap_disp.loc[row_2['FAMILIA'], new_date] -= row_2['MINU_CONF_AUX']

        self.df_final.sort_values(['FAMILIA', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                  ascending=[True, True, True, True], inplace=True)
        self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()

    def generate(self):
        # Plan
        df_plan = MDVPv1.resultado_prev(self, fam_y_sam=True, force=True)
        df_plan['FAMILIA_PROD'] = np.where(df_plan['OPER'].isna(), df_plan['FAMILIA'].str[:] + "-SP",
                                           df_plan['FAMILIA'].str[:] + "-CP")
        df_plan['MINU_CONF'] = df_plan['SAM'] * df_plan['CANT_REAL']
        self.df_planeado = df_plan.copy()

        # Firme
        file = f'/History/PRODUCTION_ORDERS_{MDVPv2.ftp_today}*.csv'
        # file = f'/History/PRODUCTION_ORDERS_20220303*.csv'
        df_data = self.get_from_ftp(file=file, encoding='cp437', sep=';')
        df_data[df_data.select_dtypes(['object']).columns] = df_data.select_dtypes(['object']).apply(lambda x: x.str.strip())
        df_data['SKU'] = df_data['SKU'].astype(str)
        df_fam = MDVPv1.get_familia(self, df_data[['SKU']], with_catalogo=True)
        # Familia Larga
        df_fam['FAMILIA'] = df_fam['FAMILIA'].apply(lambda x: x.replace('+', '/'))
        df_fam['FAMILIA'] = np.where(df_fam['FAMILIA'].isin(['PANTY/TOP', 'PLANO', 'PUNTO BASICO']),
                                     df_fam['FAMILIA'] + " " + df_fam['CATALOGO'],
                                     df_fam['FAMILIA'])
        df_fam.drop('CATALOGO', axis=1, inplace=True)

        df_data['FAMILIA'] = df_data.merge(df_fam, on='SKU', how='left')['FAMILIA'].values
        df_data['FAMILIA_PROD_LARGO'] = np.where(df_data['FAMILIA_PROD'].str[-2:] == 'SP', df_data['FAMILIA'] + '-SP',
                                                df_data['FAMILIA'] + '-CP')
        if df_data['FAMILIA_PROD_LARGO'].str.contains('VALIDAR').sum() > 0:
            print("Eliminando productos que no tienen FAMILIA PRODUCCION.")
            df_data = df_data[~df_data['FAMILIA_PROD_LARGO'].str.contains('VALIDAR')].reset_index(drop=True)

        df_data['AMORTIGUADOR'] = df_data.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')[
            'AMORTIGUADOR'].values

        # df_ol = df_ol.sort_values(['FAMILIA', 'FECHA_PROMETIDA', 'SKU', 'MINU_CONF'], ascending=[True, True, True, True]).reset_index(drop=True)
        # df_ol['ACUM_MIN'] = df_ol['MINU_CONF'].cumsum()

        df_data['AMORTIGUADOR_REAL'] = np.ceil(df_data['AMORTIGUADOR'] * self.prop_amortiguador)

        df_data[['FECHA_ACTU', 'FECHA_PROMETIDA']] = df_data[['FECHA_ACTU', 'FECHA_PROMETIDA']].astype('datetime64[D]')
        fecha_fin = df_data['FECHA_PROMETIDA'].values.astype('datetime64[D]')
        df_data['START_DATE'] = np.busday_offset(fecha_fin, -df_data['AMORTIGUADOR_REAL'], roll='forward',
                                                 busdaycal=self.calendar)
        df_data['START_DATE'] = np.maximum(df_data['START_DATE'], df_data['FECHA_ACTU'])
        df_data['START_DATE_ANTERIOR'] = df_data['START_DATE']
        df_data['RELATIVO'] = df_data['SKU'].str[:7] + '_' + df_data['PAIS'] + '_' + df_data['CAMPANA'] + '_' + df_data['START_DATE'].astype(str)
        # No indigo
        df_data = df_data[~df_data['FAMILIA'].str.contains('INDIGO')]

        # Array de fechas
        # max_dates = df_fir.groupby('FAMILIA')['START_DATE'].max().reset_index().to_records(index=False)
        # dias_habil2 = {familia: [np.busday_offset(np.datetime64(df_fir['START_DATE'].min(), 'D'),
        # i, roll='forward', busdaycal=self.calendar)
        #                 for i in range(np.busday_count(np.datetime64(df_fir['START_DATE'].min(), 'D'),
        #                                                np.datetime64(max_familia, 'D'), busdaycal=self.calendar)+60)]
        #                for (familia, max_familia) in max_dates)}
        # dias_habil2 = pd.DataFrame(dias_habil2)
        df_fir = df_data[df_data['ESTADO'] == 'Fabricacion'].copy().reset_index(drop=True)
        df_pre = df_data[df_data['ESTADO'] == 'Previsional'].copy().reset_index(drop=True)
        df_fir = self.clean(df_n=df_fir)
        df_pre = self.clean(df_n=df_pre)

        dias_habil = pd.Series(
            [np.busday_offset(np.datetime64(df_data['START_DATE'].min(), 'D'), i, roll='forward',
                              busdaycal=self.calendar)
             for i in range(np.busday_count(np.datetime64(df_data['START_DATE'].min(), 'D'),
                                            np.datetime64(df_data['START_DATE'].max(), 'D'),
                                            busdaycal=self.calendar) + 60)], dtype='datetime64[D]')

        cap_disp = pd.DataFrame({'FAMILIA': np.repeat(df_data['FAMILIA'].unique(), len(dias_habil)),
                                 'DIA': np.tile(dias_habil, df_data['FAMILIA'].unique().shape[0])})
        cap_disp = cap_disp.merge(self.df_cap, on='FAMILIA', how='left').rename(
            columns={'CAPACIDAD': 'CAPACIDAD_DISPONIBLE'})
        cap_disp.set_index(['FAMILIA', 'DIA'], inplace=True)
        cap_disp.sort_index(inplace=True)
        cap_disp = cap_disp.iloc[:, 0]

        # # Generar
        # df_fir['RELATIVO'] = df_fir['SKU'].str[:7] + '_' + df_fir['PAIS'] + '_' + df_fir['CAMPANA'] + '_' + df_fir[
        #     'START_DATE'].astype(str)
        # # df_fir['ACUM_RELATIVO'] = df_fir.groupby(['RELATIVO'])['MINU_CONF'].transform(sum)
        # # df_fir['ACUM_RELATIVO'] = df_fir.groupby(['RELATIVO'])['MINU_CONF'].cumsum()
        # df_cap = pd.DataFrame({'CAPACIDAD': self.d_recursos}).reset_index().rename(columns={'index': 'FAMILIA'})
        # df_fir['CAPACIDAD'] = df_fir.merge(df_cap, on='FAMILIA', how='left')['CAPACIDAD'].values
        #
        # # df_fir['rolled'] = df_fir['CAPACIDAD'] - df_fir['MINU_CONF_ACUM']
        # df_fir.set_index('RELATIVO', inplace=True)
        #
        # # Cuando la producción en una sola orden (ROW) excede capacidad diaria
        # df_fir['N_EXCESO'] = (df_fir['MINU_CONF'] // df_fir['CAPACIDAD']).astype('int64')
        # df_fir['MINU_CONF_AUX'] = df_fir.apply(
        #     lambda x: [x['MINU_CONF'] % x['CAPACIDAD']] + [x['CAPACIDAD'] for i in range(x['N_EXCESO'])], axis=1)
        # df_fir['LOTE'] = df_fir.apply(lambda x: [i for i in range(1, x['N_EXCESO'] + 2)], axis=1)
        # df_fir = df_fir.explode(['MINU_CONF_AUX', 'LOTE'])
        # df_fir[['MINU_CONF_AUX', 'LOTE']] = np.round(df_fir[['MINU_CONF_AUX', 'LOTE']].astype('float64'), 2)
        # # Agregar días que se posponen a cada row, 2 días por cada día a full capacidad
        # df_fir['DIAS_POSPONER'] = np.where(df_fir['MINU_CONF_AUX'] == df_fir['CAPACIDAD'], 1, 0)
        # df_fir['DIAS_POSPONER'] = df_fir.groupby(['FAMILIA'])['DIAS_POSPONER'].transform('cumsum') * 2
        # # Re asignar el valor a las ROWS que ocupan full capacidad
        # df_fir.reset_index(inplace=True)
        # df_fir.set_index('RELATIVO', append=True, inplace=True)  # Segundo index
        # idx = df_fir[~(df_fir['DIAS_POSPONER'].eq(df_fir['DIAS_POSPONER'].shift().fillna(0))) & (
        #             df_fir['DIAS_POSPONER'] != 0)].index
        # df_fir.loc[idx, 'DIAS_POSPONER'] = df_fir.loc[idx, 'DIAS_POSPONER'] - 1  # Los días que están full
        # df_fir['START_DATE'] = np.busday_offset(df_fir['START_DATE'].values.astype('datetime64[D]'),
        #                                         df_fir['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        # df_fir['DIAS_POSPONER'] = 0
        # df_fir['RELATIVO'] = df_fir['SKU'].str[:7] + '_' + df_fir['PAIS'] + '_' + df_fir['CAMPANA'] + '_' + df_fir[
        #     'START_DATE'].astype(str)
        # df_fir = df_fir.droplevel('RELATIVO')
        # df_fir.set_index('RELATIVO', append=True, inplace=True)
        #
        # df_fir = df_fir.sort_values(['FAMILIA', 'START_DATE', 'START_DATE_ANTERIOR', 'MINU_CONF'],
        #                             ascending=[True, True, True, True])
        # df_fir['MINU_CONF_ACUM'] = df_fir.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        # df_fir.reset_index(inplace=True)
        # df_fir['LOTE'] = np.nan

        self.df_final = df_fir.copy()
        self.cap_disp = cap_disp.copy()

        row = 0
        # for row in range(200):
        while True:
            # print(row)
            if row >= self.df_final.shape[0]:
                df_final = self.df_final.copy()
                break
            xidx = self.df_final.index[row]
            cap_idx = (self.df_final.loc[xidx, 'FAMILIA'], np.datetime64(self.df_final.loc[xidx, 'START_DATE'], 'D'))
            if self.df_final.loc[xidx, 'MINU_CONF_ACUM'] >= self.df_final.loc[xidx, 'CAPACIDAD']*self.capacity_relax:
                # print(str(row)+'_'+str(xidx), end=', ')
                self.smooth(row)
                row += 1
                xidx2 = self.df_final.index[row]
                print(str(row) + ': ' + str(xidx) + ' a ' + str(xidx2))
                # self.cap_disp.loc[cap_idx] = 0
                continue

            elif self.df_final.loc[xidx, 'MINU_CONF_ACUM'] >= self.df_final.loc[xidx, 'CAPACIDAD']:
                # break
                row_fltr = (self.df_final['FAMILIA'] == self.df_final.loc[xidx, 'FAMILIA']) & (self.df_final['START_DATE'] == self.df_final.loc[xidx, 'START_DATE'])
                idx = self.df_final.iloc[row+1:].loc[row_fltr, 'START_DATE'].index
                if idx.shape[0] > 0:
                    new_date = np.busday_offset(self.df_final.loc[idx, 'START_DATE'].values[0].astype('datetime64[D]'),
                                                offsets=1, roll='forward', busdaycal=self.calendar)
                    self.df_final.loc[idx, 'START_DATE'] = new_date
                self.df_final.sort_values(['FAMILIA', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                          ascending=[True, True, True, True], inplace=True)
                self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()

            xidx2 = self.df_final.index[row]
            print(str(row) + ': ' + str(xidx) + ' a ' + str(xidx2))
            self.df_final.loc[xidx, 'LOTE'] = 0
            self.cap_disp.loc[cap_idx] -= np.round(self.df_final.loc[xidx, 'MINU_CONF_AUX'], 2)
            # Lote 0: Nada, -1: adelantado completo, 1y2: Dividido
            row += 1


        # PREVISIONALES
        start_row = df_final.shape[0]
        df_pre.index = np.arange(start_row, start_row+df_pre.shape[0])
        df_pre['INDEX_ANTERIOR'] = df_pre.index
        self.df_final = self.df_final.append(df_pre)

        for row in range(start_row, max(df_pre.index)):
            xidx = self.df_final.index[row]
            cap_idx = (self.df_final.loc[xidx, 'FAMILIA'], np.datetime64(self.df_final.loc[xidx, 'START_DATE'], 'D'))
            if self.df_final.xloc[xidx, ]




        #
        # dfdf = self.df_final.copy()
        # cap_val = dfdf.groupby(['FAMILIA', 'START_DATE'])[['MINU_CONF_ACUM', 'CAPACIDAD']].max()
        # cap_val['DISP'] = cap_val.CAPACIDAD - cap_val.MINU_CONF_ACUM
        # cap_val.index.names = ['FAMILIA', 'DIA']
        # cap_val = cap_val.merge(self.cap_disp, on=['FAMILIA', 'DIA'], how='left')


    def clean(self, df_n):
        df = df_n.copy()
        df['RELATIVO'] = df['SKU'].str[:7] + '_' + df['PAIS'] + '_' + df['CAMPANA'] + '_' + df['START_DATE'].astype(str)
        df['CAPACIDAD'] = df.merge(self.df_cap, on='FAMILIA', how='left')['CAPACIDAD'].values
        # df.set_index('RELATIVO', inplace=True)
        # Cuando la producción en una sola orden (ROW) excede capacidad diaria
        df['N_EXCESO'] = (df['MINU_CONF'] // df['CAPACIDAD']).astype('int64')
        df['MINU_CONF_AUX'] = df.apply(
            lambda x: [x['MINU_CONF'] % x['CAPACIDAD']] + [x['CAPACIDAD'] for i in range(x['N_EXCESO'])], axis=1)
        df['LOTE'] = df.apply(lambda x: [i for i in range(1, x['N_EXCESO'] + 2)], axis=1)
        df = df.explode(['MINU_CONF_AUX', 'LOTE'])
        df[['MINU_CONF_AUX', 'LOTE']] = np.round(df[['MINU_CONF_AUX', 'LOTE']].astype('float64'), 2)
        # Agregar días que se posponen a cada row, 2 días por cada día a full capacidad
        df['DIAS_POSPONER'] = np.where(df['MINU_CONF_AUX'] == df['CAPACIDAD'], 1, 0)
        df['DIAS_POSPONER'] = df.groupby(['FAMILIA'])['DIAS_POSPONER'].transform('cumsum') * 2
        # Re asignar el valor a las ROWS que ocupan full capacidad
        df.reset_index(inplace=True, drop=True)
        # df.set_index('RELATIVO', append=True, inplace=True)  # Segundo index
        idx = df[~(df['DIAS_POSPONER'].eq(df['DIAS_POSPONER'].shift().fillna(0))) & (df['DIAS_POSPONER'] != 0)].index
        df.loc[idx, 'DIAS_POSPONER'] = df.loc[idx, 'DIAS_POSPONER'] - 1  # Los días que están full
        df['START_DATE'] = np.busday_offset(df['START_DATE'].values.astype('datetime64[D]'),
                                            df['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        df.drop('DIAS_POSPONER', axis=1, inplace=True)
        # df['RELATIVO'] = df['SKU'].str[:7] + '_' + df['PAIS'] + '_' + df['CAMPANA'] + '_' + df['START_DATE'].astype(str)
        # df = df.droplevel('RELATIVO')
        # df.set_index('RELATIVO', append=True, inplace=True)
        df.sort_values(['FAMILIA', 'START_DATE', 'RELATIVO', 'MINU_CONF'],
                       ascending=[True, True, True, True], inplace=True)
        df.reset_index(drop=True, inplace=True)
        df.reset_index(inplace=True)
        df = df.rename(columns={'index': 'INDEX_ANTERIOR'})
        df['MINU_CONF_ACUM'] = df.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        df['LOTE'] = np.nan
        return df

    # dfdf.to_excel(rf'C:\Users\liamd\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Projects\01. Supply\08. MDVP\Files\2. OUTPUT\data_suav.xlsx')
    # df_fir.to_excel(rf'C:\Users\liamd\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Projects\01. Supply\08. MDVP\Files\2. OUTPUT\data_sin_suav.xlsx')












        # dfdf = self.df_final.copy()
        # # 20 unidades mínimo
        # test = pd.DataFrame({'A': ['a', 'b', 'c'], 'B': [1, 2, 3]})
        # pd.DataFrame(np.insert(test.values, 1, values=[2, 3], axis=0))
        #
        # while True:
        #     if not np.array(df_ol['MINU_CONF_ACUM'] > (df_ol['CAPACIDAD'] * self.capacity_relax)).any():
        #         print(iter)
        #         break
        #     iter += 1
        #     if iter % 5 == 0:
        #         print(iter)
        #     # df_ol['DIA_SIG'] = df_ol['START_DATE'].shift(-1)
        #     # df_ol['POST_AUX'] = np.where(df_ol['MINU_CONF_ACUM'] >= df_ol['CAPACIDAD'], 1, 0)
        #     df_ol['DIAS_POSPONER'] = (
        #                 df_ol['MINU_CONF_ACUM'] // (df_ol['CAPACIDAD'] * self.capacity_relax)).replace(0,
        #                                                                                                         np.nan)
        #     # Primera row con la nueva fecha
        #     idx = df_ol[df_ol['DIAS_POSPONER'].isna()].drop_duplicates(['FAMILIA', 'START_DATE'], keep='first').index
        #     df_ol.loc[idx, 'DIAS_POSPONER'] = 0
        #     df_ol['DIAS_POSPONER'] = df_ol.groupby(['FAMILIA', 'START_DATE'])['DIAS_POSPONER'].ffill().fillna(0).astype(
        #         'int64')
        #     # Auxiliar de acumulado de días a mover
        #     # df_aux = df_ol[df_ol['DIAS_POSPONER'] != 0][['FAMILIA', 'START_DATE', 'DIAS_POSPONER']].drop_duplicates().reset_index(drop=True)
        #     # df_aux['DIAS_POSPONER2'] = df_aux.groupby(['FAMILIA', 'START_DATE'])['DIAS_POSPONER'].transform('cumsum')
        #     # df_ol['DIAS_POSPONER1'] = df_ol.merge(df_aux, on=['FAMILIA', 'START_DATE', 'DIAS_POSPONER'], how='left')['DIAS_POSPONER2'].values
        #     # df_ol['DIAS_POSPONER1'] = df_ol.groupby(['FAMILIA', 'START_DATE'])['DIAS_POSPONER1'].ffill().fillna(0).astype('int64')
        #     # df_ol['START_DATE'] = np.busday_offset(df_ol['START_DATE'].values.astype('datetime64[D]'), df_ol['DIAS_POSPONER1'], roll='forward', busdaycal=self.calendar)
        #
        #     df_ol['START_DATE'] = np.busday_offset(df_ol['START_DATE'].values.astype('datetime64[D]'),
        #                                            df_ol['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        #     df_ol['RELATIVO'] = df_ol['SKU'].str[:7] + '_' + df_ol['PAIS'] + '_' + df_ol['CAMPANA'] + '_' + df_ol[
        #         'START_DATE'].astype(str)
        #     df_ol = df_ol.droplevel('RELATIVO')
        #     df_ol.set_index('RELATIVO', append=True, inplace=True)
        #     df_ol = df_ol.sort_values(['FAMILIA', 'START_DATE', 'START_DATE_ANTERIOR', 'MINU_CONF'],
        #                               ascending=[True, True, True, True])
        #     df_ol['MINU_CONF_ACUM'] = df_ol.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        #     # df_ol['rolled'] = df_ol['CAPACIDAD'] - df_ol['MINU_CONF_ACUM']
        #
        #     # df_ol['DIAS_POSPONER'] = np.where(df_ol['POST_AUX'].eq(df_ol['POST_AUX'].shift().fillna(0)), 1, 0)
        #     # df_ol['DIAS_POSPONER1'] = np.where((df_ol['DIAS_POSPONER'] == 0) & (df_ol['POST_AUX'] == 1), 1, 0)
        #     # df_ol['DIAS_POSPONER2'] = df_ol.groupby(['FAMILIA'])['DIAS_POSPONER1'].transform('cumsum')
        #
        # # faltan mandar los indivuduales agrupados para el día siguiente
        # # Agregarle un día a
