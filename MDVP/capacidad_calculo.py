from ftplib import FTP
from io import BytesIO
import pandas as pd
import numpy as np
import os
import datetime
import sys
from pathlib import Path
sys.path.append(str(Path(os.getcwd()).parent.absolute()))
from Code.sap_rpa import SAP
from Code.main import MDVPv2
import matplotlib.pyplot as plt


obj = MDVPv2()

file = f'/History/PRODUCTION_ORDERS_{obj.ftp_today}*.csv'
# file = fr'/History/PRODUCTION_ORDERS_20220711*.csv'
df_data = obj.get_from_ftp(file=file, encoding='cp437', sep=';')
df_data = df_data[df_data['NUMERO_OPERACION'].isin([90, 0])].reset_index(drop=True)
df_data[df_data.select_dtypes(['object']).columns] = df_data.select_dtypes(['object']).apply(lambda x: x.str.strip())
df_data = df_data[df_data['DESTINO'] != '-'].reset_index(drop=True)
df_data['SKU'] = df_data['SKU'].astype(str)
df_fam = obj.get_familia(df_data[['SKU']], with_catalogo=True, with_sam=True)
# Familia Larga
df_fam['FAMILIA'] = df_fam['FAMILIA'].apply(lambda x: x.replace('+', '/'))
df_fam.drop('CATALOGO', axis=1, inplace=True)

date_start = file.split('*')[0].split('_')[-1]
date_start = datetime.datetime.strptime(date_start, "%Y%m%d")
date_start = min(obj.today, date_start)

df_data = df_data.merge(df_fam, on='SKU', how='left')
df_data['FAMILIA_PROD_LARGO'] = np.where(df_data['FAMILIA_PROD'].str[-2:] == 'SP', df_data['FAMILIA'] + '-SP',
                                         df_data['FAMILIA'] + '-CP')
df_data['MINU_TOTA'] = df_data['MINU_CONF'] + df_data['MINU_EMPA']
df_data['FECHA_PROMETIDA'] = df_data['FECHA_PROMETIDA'].apply(lambda x: datetime.datetime.strptime(x, "%d/%m/%Y"))

# Escenario 1
df_minutos = df_data.groupby(['FECHA_PROMETIDA', 'FAMILIA'], as_index=False)['MINU_TOTA'].sum()
df_minutos = df_minutos.reset_index(drop=True)

df_minutos.sort_values(['FAMILIA', 'FECHA_PROMETIDA'], ascending=[True, True], inplace=True)
df_minutos = df_minutos[~(df_minutos['FAMILIA'] == 'VALIDAR')].reset_index(drop=True)
df_minutos = df_minutos[~(df_minutos['FAMILIA'].str.contains('INDIGO'))].reset_index(drop=True)

dias_habil_past = pd.Series(
    [np.busday_offset(np.datetime64(df_minutos.FECHA_PROMETIDA.min(), 'D'), i, roll='forward',
                      busdaycal=obj.calendar)
     for i in range(np.busday_count(np.datetime64(df_minutos.FECHA_PROMETIDA.min(), 'D'),
                                    np.datetime64(df_minutos['FECHA_PROMETIDA'].max(), 'D'),
                                    busdaycal=obj.calendar))], dtype='datetime64[D]', name='FECHA_PROMETIDA')

day_zero = dias_habil_past[dias_habil_past==obj.today].index[0]
dias_transcurridos = pd.Series(np.array(list(range(-day_zero, 0)) + list(range(1, dias_habil_past.shape[0]-day_zero+1))), name='DIAS_TRANSCURRIDOS')
# dias_transcurridos = pd.Series(dias_habil_past - obj.today, name='DIAS_TRANSCURRIDOS').astype('timedelta64[D]')
# dias_transcurridos = dias_transcurridos.where(dias_transcurridos<0, dias_transcurridos+1)
df_dias = pd.concat([dias_habil_past, dias_transcurridos], axis=1)
df_dias = pd.concat(df_dias.assign(FAMILIA=f) for f in df_minutos['FAMILIA'].unique())

df_minutos_con_vencidos = df_minutos.copy()
df_minutos = df_minutos[df_minutos['FECHA_PROMETIDA'] >= date_start].reset_index(drop=True)


def get_cap_teo(df1):
    df = df1.merge(df_dias, on=['FAMILIA', 'FECHA_PROMETIDA'], how='right')
    # df = df.loc[(df['DIAS_TRANSCURRIDOS'] > 0) | (df['FAMILIA'].notna()), :]
    df['MINU_TOTA'] = df['MINU_TOTA'].fillna(0)
    df['MINU_TOTA_ACUM'] = df.groupby(['FAMILIA'])['MINU_TOTA'].cumsum()
    df['MINU_TOTA_ACUM'] = df.groupby(['FAMILIA'])['MINU_TOTA_ACUM'].ffill()
    df['CAPACIDAD_TEO'] = df['MINU_TOTA_ACUM'] / df['DIAS_TRANSCURRIDOS']
    df['CAPACIDAD_TEO'] = df.groupby('FAMILIA')['CAPACIDAD_TEO'].transform('max')
    df['CAPACIDAD_DISPONIBLE'] = df.merge(obj.df_cap, on='FAMILIA', how='left')['CAPACIDAD']
    df[['CAPACIDAD_TEO', 'CAPACIDAD_DISPONIBLE']] = df[['CAPACIDAD_TEO', 'CAPACIDAD_DISPONIBLE']].where(df['DIAS_TRANSCURRIDOS'] >= 1, 0)
    df['CAPACIDAD_TEO_ACUM'] = df.groupby('FAMILIA')['CAPACIDAD_TEO'].cumsum()
    df['CAPACIDAD_DISPONIBLE_ACUM'] = df.groupby('FAMILIA')['CAPACIDAD_DISPONIBLE'].cumsum()

    # df_cap_teo = df.loc[df.groupby(['FAMILIA'])['CAPACIDAD_TEO'].idxmax()][['FECHA_PROMETIDA', 'FAMILIA', 'DIAS_TRANSCURRIDOS', 'CAPACIDAD_TEO']]
    # df_cap_teo = df_cap_teo.merge(obj.df_cap, on='FAMILIA', how='left')
    # df_cap_teo['STATUS'] = np.where(df_cap_teo['CAPACIDAD'] >= df_cap_teo['CAPACIDAD_TEO'], 'SUFICIENTE', 'INSUFICIENTE')

    # df['CAPACIDAD_TEO'] = df.merge(df_cap_teo[['FAMILIA', 'CAPACIDAD_TEO']], on='FAMILIA', how='left')['CAPACIDAD_TEO_y']
    #
    # cap_disp = pd.DataFrame({'FAMILIA': np.repeat(df_data['FAMILIA'].unique(), len(dias_habil_past)),
    #                          'FECHA_PROMETIDA': np.tile(dias_habil_past, df_data['FAMILIA'].unique().shape[0])}).merge(
    #     obj.df_cap, on='FAMILIA', how='left').rename(columns={'CAPACIDAD': 'CAPACIDAD_DISPONIBLE'})
    # cap_disp = cap_disp.merge(df[['FECHA_PROMETIDA', 'FAMILIA', 'MINU_TOTA', 'MINU_TOTA_ACUM']], on=['FECHA_PROMETIDA', 'FAMILIA'], how='left')
    # cap_disp['MINU_TOTA_ACUM'] = cap_disp.groupby(['FAMILIA'])['MINU_TOTA_ACUM'].ffill()
    # cap_disp['DIAS_TRANSCURRIDOS'] = cap_disp.merge(df_dias, on='FECHA_PROMETIDA', how='left')['DIAS_TRANSCURRIDOS'].values
    #
    # cap_disp['CAPACIDAD_TEO'] = cap_disp.merge(df_cap_teo[['FAMILIA', 'CAPACIDAD_TEO']], on='FAMILIA', how='left')['CAPACIDAD_TEO'].values
    # cap_disp[['CAPACIDAD_TEO', 'CAPACIDAD_DISPONIBLE']] = cap_disp[['CAPACIDAD_TEO', 'CAPACIDAD_DISPONIBLE']].where(cap_disp.DIAS_TRANSCURRIDOS >= 1, 0)
    # cap_disp['CAPACIDAD_ACUM'] = cap_disp.groupby('FAMILIA')['CAPACIDAD_DISPONIBLE'].cumsum()
    # cap_disp['CAPACIDAD_TEO_ACUM'] = cap_disp.groupby(['FAMILIA'])['CAPACIDAD_TEO'].cumsum()
    #
    # cap_disp = cap_disp[~(cap_disp['FAMILIA'] == 'VALIDAR')].reset_index(drop=True)
    # cap_disp = cap_disp[~(cap_disp['FAMILIA'].str.contains('INDIGO'))].reset_index(drop=True)
    return df
    return cap_disp


cap_disp = get_cap_teo(df_minutos)
cap_disp['ESCENARIO'] = 'SIN VENCIDOS'
cap_disp_vencidos = get_cap_teo(df_minutos_con_vencidos)
cap_disp_vencidos['ESCENARIO'] = 'CON VENCIDOS'

cap_disp_agg = pd.concat([cap_disp, cap_disp_vencidos], ignore_index=True)
cap_disp_agg.to_excel(obj.root_output + 'CAPACIDAD_TEO.xlsx', index=False)

#
# #GRAPHS
#
# df_graph = df_cap_teo[['FAMILIA', 'CAPACIDAD_TEO', 'CAPACIDAD']]
# df_graph.columns = ['FAMILIA', 'CAPACIDAD REQUERIDA', 'CAPACIDAD ACTUAL']
# df_graph = df_graph.set_index('FAMILIA').round(0)
# df_graph.plot.bar(rot=0)
#
# # GRAFICO DE FAMILIAS CON CAPACIDAD INSUFICIENTE
# familias = df_cap_teo[df_cap_teo['STATUS'] == 'INSUFICIENTE'].FAMILIA.unique()
# cap_disp = cap_disp[cap_disp.FAMILIA.isin(familias)]
# fig = plt.figure()
# gs = fig.add_gridspec(cap_disp.FAMILIA.unique().shape[0], hspace=0)
# axs = gs.subplots(sharex=True, sharey=False)
# for i in cap_disp.FAMILIA.unique():
#     df_pb = cap_disp[(cap_disp.FAMILIA == i) & (cap_disp.DIAS_TRANSCURRIDOS <= 60)].copy()
#     df_pb = df_pb.loc[:df_pb[df_pb.MINU_TOTA_ACUM.notna()].index.max()]
#     pos = np.where(cap_disp.FAMILIA.unique() == i)[0][0]
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.MINU_TOTA, label='MIN PRODUCCION')
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.CAPACIDAD_DISPONIBLE, label='CAPACIDAD ACTUAL')
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.CAPACIDAD_TEO, label='CAPACIDAD REQUERIDA')
#     axs[pos].text(x=df_pb.DIAS_TRANSCURRIDOS.max(), y=0.5, s=i, horizontalalignment='right', verticalalignment='bottom')
#     # Minutos vencidos
#     df_mv = df_minutos_vencidos[df_minutos_vencidos.FAMILIA == i].copy()
#     axs[pos].bar(x=0, height=df_mv.MINU_TOTA.sum(), color='r')
#     # axs.label_outer()
# plt.legend()
#     # plt.show()
#
#
#
#
# #ESCENARIO2
# df_minutos2 = df_data.groupby(['FECHA_PROMETIDA', 'FAMILIA'], as_index=False)['MINU_TOTA'].sum()
# df_minutos2 = df_minutos2[~(df_minutos2['FAMILIA'] == 'VALIDAR')].reset_index(drop=True)
# df_minutos2 = df_minutos2[~(df_minutos2['FAMILIA'].str.contains('INDIGO'))].reset_index(drop=True)
# df_minutos2.sort_values(['FAMILIA', 'FECHA_PROMETIDA'], ascending=[True, True], inplace=True)
# #Quitando fechas pasadas no cumplidas
# df_minutos2['MINU_TOTA_ACUM'] = df_minutos2.groupby(['FAMILIA'])['MINU_TOTA'].cumsum()
#
# #CAPACIDAD
# dias_habil2 = pd.Series(
#     [np.busday_offset(np.datetime64(date_start, 'D'), i, roll='forward',
#                       busdaycal=obj.calendar)
#      for i in range(np.busday_count(np.datetime64(date_start, 'D'),
#                                     np.datetime64(df_minutos2['FECHA_PROMETIDA'].max(), 'D'),
#                                     busdaycal=obj.calendar))], dtype='datetime64[D]')
#
# ######################################*******************############################
# dias_trans = pd.Series(np.array(range(1, dias_habil2.shape[0]+1)))
# dias_horizonte = np.array(range(1, dias_habil2.shape[0]+1)).max()/15
# dias_horizonte = pd.Series(np.tile([i for i in range(1, 16)], int(dias_horizonte)))
# grupo_horizonte = pd.Series(np.array(range(dias_habil2.shape[0]))//15+1)
# df_dias2 = pd.concat([dias_habil2, dias_trans, dias_horizonte, grupo_horizonte], axis=1)
# df_dias2.columns = ['FECHA_PROMETIDA', 'DIAS_TRANSCURRIDOS', 'DIAS_TRANSCURRIDOS_GRUPO', 'GRUPO']
# df_minutos2 = df_minutos2.merge(df_dias2, on='FECHA_PROMETIDA', how='left')
# df_minutos2['CAPACIDAD_TEO'] = df_minutos2['MINU_TOTA_ACUM'] / df_minutos2['DIAS_TRANSCURRIDOS_GRUPO']
#
# df_cap_teo2 = df_minutos2.groupby(['FAMILIA', 'GRUPO'], as_index=False)['CAPACIDAD_TEO'].max()
# df_cap_teo2 = df_cap_teo2.merge(obj.df_cap, on='FAMILIA', how='left')
# df_cap_teo2['STATUS'] = np.where(df_cap_teo2['CAPACIDAD'] >= df_cap_teo2['CAPACIDAD_TEO'], 'SUFICIENTE', 'INSUFICIENTE')
#
# ########################################################################################
#
# dias_trans = pd.Series(np.array(range(1, dias_habil2.shape[0]+1)))
# # dias_horizonte = np.array(range(1, dias_habil2.shape[0]+1)).max()/15
# # dias_horizonte = pd.Series(np.tile([i for i in range(1, 16)], int(dias_horizonte)))
# # grupo_horizonte = pd.Series(np.array(range(dias_habil2.shape[0]))//15+1)
# df_dias2 = pd.concat([dias_habil2, dias_trans], axis=1)
# df_dias2.columns = ['FECHA_PROMETIDA', 'DIAS_TRANSCURRIDOS']
#
# df_minutos2 = df_minutos2.merge(df_dias2, on='FECHA_PROMETIDA', how='left')
# df_agg = pd.DataFrame()
# for f in df_minutos2['FAMILIA'].unique():
#     df = df_minutos2[df_minutos2['FAMILIA'] == f].copy()
#     # for g in range(3):
#     g = 1
#     while True:
#         df['GRUPO'] = g
#         g += 1
#         df['DIAS_TRANSCURRIDOS_RELA'] = df.merge(df_dias2, on='FECHA_PROMETIDA', how='left')['DIAS_TRANSCURRIDOS_y'].values
#         df['MINU_TOTA_ACUM_RELA'] = df['MINU_TOTA'].cumsum()
#         df['CAPACIDAD_TEO'] = df['MINU_TOTA_ACUM_RELA'] / df['DIAS_TRANSCURRIDOS_RELA']
#         cap_teo_max = df['CAPACIDAD_TEO'].max()
#         if df.shape[0] >= 5:
#             idx = df[df['CAPACIDAD_TEO'] == cap_teo_max].index.max()
#             df.loc[:idx, 'CAPACIDAD_TEO'] = cap_teo_max
#         else:
#             df['CAPACIDAD_TEO'] = cap_teo_max
#             df_agg = pd.concat([df_agg, df])
#             break
#         df_agg = pd.concat([df_agg, df.loc[:idx]])
#         df = df.loc[idx+1:]
#
# # df_minutos2['CAPACIDAD_TEO'] = df_minutos2['MINU_TOTA_ACUM'] / df_minutos2['DIAS_TRANSCURRIDOS']
# df_minutos2 = df_agg[['FECHA_PROMETIDA', 'FAMILIA', 'MINU_TOTA', 'MINU_TOTA_ACUM',
#                       'DIAS_TRANSCURRIDOS', 'GRUPO', 'CAPACIDAD_TEO']].copy()
# df_cap_teo2 = df_minutos2.groupby(['FAMILIA', 'GRUPO'], as_index=False)['CAPACIDAD_TEO'].max()
# df_cap_teo2 = df_cap_teo2.merge(obj.df_cap, on='FAMILIA', how='left')
# df_cap_teo2['STATUS'] = np.where(df_cap_teo2['CAPACIDAD'] >= df_cap_teo2['CAPACIDAD_TEO'], 'SUFICIENTE', 'INSUFICIENTE')
#
# ######################################*******************############################
#
# cap_disp2 = pd.DataFrame({'FAMILIA': np.repeat(df_minutos2['FAMILIA'].unique(), len(dias_habil2)),
#                          'FECHA_PROMETIDA': np.tile(dias_habil2, df_minutos2['FAMILIA'].unique().shape[0])})
# cap_disp2 = cap_disp2.merge(obj.df_cap, on='FAMILIA', how='left').rename(columns={'CAPACIDAD': 'CAPACIDAD_DISPONIBLE'})
# cap_disp2 = cap_disp2.merge(df_dias2, on='FECHA_PROMETIDA', how='left')
# cap_disp2['CAPACIDAD_ACUM'] = cap_disp2.groupby(['FAMILIA'])['CAPACIDAD_DISPONIBLE'].cumsum()
# cap_disp2 = cap_disp2.merge(df_minutos2[['FECHA_PROMETIDA', 'FAMILIA', 'GRUPO', 'MINU_TOTA', 'MINU_TOTA_ACUM']], on=['FECHA_PROMETIDA', 'FAMILIA'], how='left')
# # cap_disp2 = cap_disp2.merge(df_dias2, on='FECHA_PROMETIDA', how='left')
#
# cap_disp2 = cap_disp2.merge(df_cap_teo2[['FAMILIA', 'GRUPO', 'CAPACIDAD_TEO']], on=['FAMILIA', 'GRUPO'], how='left')
# cap_disp2['CAPACIDAD_TEO'] = cap_disp2.groupby('FAMILIA')['CAPACIDAD_TEO'].transform(lambda v: v.ffill())
# cap_disp2['CAPACIDAD_TEO_ACUM'] = cap_disp2.groupby(['FAMILIA'])['CAPACIDAD_TEO'].cumsum()
# cap_disp2['MINU_TOTA'] = cap_disp2['MINU_TOTA'].fillna(0)
# cap_disp2['MINU_TOTA_ACUM'] = cap_disp2.groupby('FAMILIA')['MINU_TOTA'].cumsum()
# # Máximo numero de grupos para analizar
# grupos = df_cap_teo2.GRUPO.unique().shape[0]
# cap_disp2 = cap_disp2[cap_disp2['GRUPO'] <= grupos].reset_index(drop=True)
#
# # Graficas
# cap_disp2 = cap_disp2[cap_disp2['FAMILIA'].isin(['BABYDOLL', 'BOXER'])].reset_index(drop=True)
# fig = plt.figure()
# gs = fig.add_gridspec(cap_disp2.FAMILIA.unique().shape[0], hspace=0)
# axs = gs.subplots(sharex=True, sharey=False)
# for i in cap_disp2.FAMILIA.unique():
#     df_pb = cap_disp2[(cap_disp2.FAMILIA == i) & (cap_disp2.DIAS_TRANSCURRIDOS <= 90)].copy()
#     df_pb = df_pb.loc[:df_pb[df_pb.MINU_TOTA_ACUM.notna()].index.max()]
#     # df_pb = df_pb.ffill()
#     pos = np.where(cap_disp2.FAMILIA.unique() == i)[0][0]
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.MINU_TOTA, label='PRODUCCIÓN ACUM.')
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.CAPACIDAD_DISPONIBLE, label='CAPACIDAD ACTUAL ACUM.')
#     axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.CAPACIDAD_TEO, label='CAPACIDAD REQUERIDA ACUM.')
#     # axs[pos].plot(df_pb.DIAS_TRANSCURRIDOS, df_pb.MINU_TOTA, label='PRODUCCIÓN REAL.')
#     axs[pos].text(x=df_pb.DIAS_TRANSCURRIDOS.max(), y=0.5, s=i, horizontalalignment='right', verticalalignment='bottom')
#     # axs.label_outer()
# plt.legend()