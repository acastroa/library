import pandas as pd
import win32com.client
import sys
import subprocess
import time
from tkinter import *
from tkinter import messagebox
import os

name_user = os.getlogin()
root_input = rf'C:\Users\{name_user}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Projects\01. Supply\08. MDVP\Files\1. INPUT\\'
root_output = rf'C:\Users\{name_user}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Projects\01. Supply\08. MDVP\Files\2. OUTPUT\\'


class SAP:
    def __init__(self, root_in=root_input, root_out=root_input, read_only=False, year=time.localtime().tm_year):
        super(SAP, self).__init__()
        self.root_in = root_in
        self.root_out = root_out
        self.read_only = read_only
        self.year = year
        # SAP
        # self.open()
        self.SapGuiAuto = None
        self.path = None
        self.session = None

    def open(self):
        self.path = r"C:\Program Files (x86)\SAP\FrontEnd\SapGui\sapLogon.exe"
        subprocess.Popen(self.path)
        time.sleep(5)

        self.SapGuiAuto = win32com.client.GetObject("SAPGUI")
        if not type(self.SapGuiAuto) == win32com.client.CDispatch:
            print("Sesion no iniciada.")
            return

    def exit(self):
        if not type(self.SapGuiAuto) == win32com.client.CDispatch:
            print("Sesion no iniciada.")
            return
        self.session.findById("wnd[0]").close()
        self.session.findById("wnd[1]/usr/btnSPOP-OPTION1").press()
        self.session = None

    def open_and_login(self, conexion="500", user='JCASAS', password='Jhon1214724616'):
   # def open_and_login(self, conexion="erpprod", user='MLOPEZ', password='Planprod20222'):
        # conexión = 'erpprod'
        # password = "Concentrado1234"
        self.open()
        application = self.SapGuiAuto.GetScriptingEngine
        if not type(application) == win32com.client.CDispatch:
            self.SapGuiAuto = None
            return

        self.connection = application.OpenConnection(conexion, True)
        if not type(self.SapGuiAuto) == win32com.client.CDispatch:
            application = None
            self.SapGuiAuto = None
            return
        time.sleep(5)

        self.session = self.connection.Children(0)
        if not type(self.session) == win32com.client.CDispatch:
            self.connection = None
            application = None
            self.SapGuiAuto = None
            return

        try:
            self.session.findById("wnd[0]/usr/txtRSYST-MANDT").text = "500"
            self.session.findById("wnd[0]/usr/txtRSYST-BNAME").text = user
            self.session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = password
            self.session.findById("wnd[0]/usr/txtRSYST-LANGU").text = "ES"
            self.session.findById("wnd[0]").sendVKey(0)
        except:
            print(sys.exc_info()[0])
        time.sleep(1)
        print("Sesion SAP iniciada con éxito.")

    def exporter(self, name=None):
        i, e = 0, ''
        while True:
            if i > 10:
                print(f"Error descargando: {name}.")
                df_n = pd.DataFrame()
                break
            try:
                time.sleep(10)
                self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton(
                    "&MB_EXPORT")
                self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
                self.session.findById(
                    "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
                self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
                time.sleep(10)
                self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
                self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = f"{name}.txt"
                time.sleep(0.5)
                self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
                time.sleep(0.5)
                self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
                self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
                print(f"Descarga de {name} completada.")
                time.sleep(5)
                df_n = pd.read_csv(self.root_in + f'{name}.txt', sep="\t", encoding='cp1252', decimal='.',
                                   thousands=',', skiprows=5, dtype='str')
                break
            except Exception as e:
                print(f"Esperando descarga de {name}.")
                i += 1

        return df_n

    def get_variant(self, variant=''):
        self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
        rows = self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").RowCount
        ls_vari = [self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").GetCellValue(r, 'VARIANT')
                   for r in range(rows)]
        row_match = ls_vari.index(variant)
        self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = str(row_match)
        self.session.findById("wnd[1]/tbar[0]/btn[2]").press()

    def get_zpp_leader_list(self, variant='ZPP LEADER LIS'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZPP_LEADER_LIST"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").selectedRows = "2"
        # self.session.findById("wnd[1]/usr/cntlALV_CONTAINER_1/shellcont/shell").doubleClickCurrentCell()
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        time.sleep(30)
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(45)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "ZPP_LEADER_LIST_ADV.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print("Descarga de ZPP_LEADER_LIST completada.")
        time.sleep(5)
        df_n = pd.read_csv(self.root_in + 'ZPP_LEADER_LIST_ADV.txt', sep="\t", encoding='cp1252', decimal='.', thousands=',',
                           skiprows=5, dtype='str')
        return df_n

    def get_zdm_pp_comp(self, variant='COMPONENTES'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZDM_PP_COMP"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        time.sleep(10)
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(10)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "ZDM_PP_COMP.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print("Descarga de ZDM_PP_COMP completada.")
        time.sleep(5)
        df_n = pd.read_csv(self.root_in + 'ZDM_PP_COMP.txt', sep="\t", encoding='cp1252', decimal='.',
                           thousands=',', skiprows=5, dtype='str')
        return df_n

    def get_zdm_hoja_ruta_puesto(self, variant='/JCASAS'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZDM_HOJA_RUTA_PUESTO"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        time.sleep(10)
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCONTAINER/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById(
            "wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(10)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "ZDM_PP_HRUTAS.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print("Descarga de ZDM_PP_HRUTAS completada.")
        time.sleep(5)
        df_n = pd.read_csv(self.root_in + 'ZDM_PP_HRUTAS.txt', sep="\t", encoding='cp1252', decimal='.',
                           thousands=',', skiprows=5, dtype='str')
        return df_n

    def get_zmd04(self, variant='INV.'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZMD04"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        time.sleep(5)
        self.session.findById("wnd[0]/mbar/menu[0]/menu[3]/menu[2]").select()
        self.session.findById("wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(1)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = "ZMD04.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print("Descarga de ZMD04 completada.")
        time.sleep(5)
        df_n = pd.read_csv(self.root_out + '/ZMD04.txt', sep="\t", encoding='cp1252', decimal='.', thousands=',',
                           skiprows=5, dtype='str')
        return df_n

    def get_zdm_mat_art(self, variant='EC1'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZDM_MAT_ART"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/usr/ctxtSP$00001-LOW").text = "*"
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        df = self.exporter("ZDM_MAT_ART")
        return df

    def get_zrepo_list_mate(self, variant='COMPONENTES'):
        print("Esta transacción será obsoleta, actualizar a ZLIST_MATE.")
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZREPO_LIST_MATE"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        df = self.exporter("ZREPO_LIST_MATE")
        return df

    def get_zpp_plan_oprev(self):
        for _ in [str(self.year)+'A', str(self.year-1)]:
            self.session.findById("wnd[0]").maximize()
            self.session.findById("wnd[0]").iconify()
            self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZPP_PLAN_OPREV"
            self.session.findById("wnd[0]").sendVKey(0)
            self.get_variant(variant=_)
            self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
            df = self.exporter(f"ZPP_PLAN_OPREV_{_}")
        return df

    def get_zpp_sustitutos(self, variant='SUST.'):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "ZPP_SUSTITUTOS"
        self.session.findById("wnd[0]").sendVKey(0)
        self.get_variant(variant=variant)
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        df = self.exporter("ZPP_SUSTITUTOS")
        return df

    def get_coois(self):
        self.session.findById("wnd[0]").maximize()
        self.session.findById("wnd[0]").iconify()
        self.session.findById("wnd[0]/tbar[0]/okcd").text = "COOIS"
        self.session.findById("wnd[0]").sendVKey(0)
        self.session.findById("wnd[0]/tbar[1]/btn[17]").press()
        self.session.findById("wnd[1]/usr/txtV-LOW").text = "PREV"
        self.session.findById("wnd[1]/usr/txtENAME-LOW").text = "" #JCASAS
        self.session.findById("wnd[1]/tbar[0]/btn[8]").press()
        self.session.findById("wnd[0]/tbar[1]/btn[8]").press()
        self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").pressToolbarButton("&NAVIGATION_PROFILE_TOOLBAR_EXPAND")
        self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").pressToolbarContextButton("&MB_EXPORT")
        self.session.findById("wnd[0]/usr/cntlCUSTOM/shellcont/shell/shellcont/shell").selectContextMenuItem("&PC")
        self.session.findById("wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[1,0]").select()
        self.session.findById("wnd[1]/tbar[0]/btn[0]").press()
        time.sleep(10)
        self.session.findById("wnd[1]/usr/ctxtDY_PATH").text = self.root_in
        self.session.findById("wnd[1]/usr/ctxtDY_FILENAME").text = f"COOIS_CABE_PREV.txt"
        time.sleep(0.5)
        self.session.findById("wnd[1]/tbar[0]/btn[11]").press()
        time.sleep(0.5)
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()  # Exit
        self.session.findById("wnd[0]/tbar[0]/btn[15]").press()
        print(f"Descarga de COOIS_CABE_PREV completada.")
        time.sleep(5)
        df_n = pd.read_csv(self.root_in + f'COOIS_CABE_PREV.txt', sep="\t", encoding='cp1252', decimal='.',
                           thousands=',', skiprows=5, dtype='str')
        return df_n

    def get_all_sap_reports(self):
        self.open_and_login()
        self.get_zpp_leader_list()
        self.get_zdm_pp_comp()
        self.get_zdm_hoja_ruta_puesto()
        self.get_zmd04()
        self.get_zdm_mat_art()
        self.get_zrepo_list_mate()
        self.get_zpp_plan_oprev()
        self.get_zpp_sustitutos()
        self.get_coois()
        print("Todos los reportes desacargados exitosamente.")


if __name__ == '__main__':
    SAP().get_all_sap_reports()
    
# if __name__ == '__main__':
#     window = Tk()
#     window.geometry('200x60')
#     btn = Button(window, text="Login SAP", command= lambda : SapGui().sapLogin())
#     btn.pack()
#     mainloop()


