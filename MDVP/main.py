import glob
from io import BytesIO
from ftplib import FTP
import pandas as pd
import numpy as np
import shutil
import os
import datetime
import sys
from pathlib import Path
sys.path.append(str(Path(os.getcwd()).parent.absolute()))
from Code.sap_rpa import SAP
from Code.MDVP_auth import MDVPv1


class MDVPv2(MDVPv1, SAP):
    name_user = os.getlogin()
    root_main = "C:/Users/" + name_user
    root_input = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/" \
                             "Projects/01. Supply/08. MDVP/Files/1. INPUT/"
    root_output = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/" \
                             "Projects/01. Supply/08. MDVP/Files/2. OUTPUT/"
    root_input_focuss = root_main + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/" \
                                   "Projects/01. Supply/08. MDVP/Files/1. INPUT/Focuss/"

    ftp_today = datetime.datetime.strftime(datetime.datetime.today(), "%Y%m%d")
    # print('FECHA_TEST___FECHA_TEST___FECHA_TEST___FECHA_TEST___FECHA_TEST___')
    # ftp_today = '20220303'
    d_cred = eval(open(root_input + 'credenciales.txt').read())
    user_ftp, password_ftp, host_ftp = d_cred['usuario_ftp'], d_cred['password_ftp'], d_cred['host_ftp']

    def __init__(self, weeks=24, prop_amortiguador=0.5, capacity_relax=1.01):
        super().__init__()
        self.df_firme = None
        self.df_reacciones = None
        self.df_planeado = None
        self.df_amortiguador = None
        self.df_final = None
        self.d_recursos = None
        self.df_cap = None
        self.tipos_prenda = None
        self.calendar = None
        self.cap_disp = None
        self.today = datetime.datetime.strptime(MDVPv2.ftp_today, "%Y%m%d")
        self.weeks = weeks
        self.days = weeks * 7
        self.prop_amortiguador = prop_amortiguador
        self.capacity_relax = capacity_relax
        self.cap_var = None
        self.df_data_prod = None

        self.get_parametros()
        self.get_capacity()

    def get_parametros(self):
        df_amt = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Amortiguador').iloc[:, :2]
        df_amt.columns = ['FAMILIA_PROD_LARGO', 'AMORTIGUADOR']
        self.df_amortiguador = df_amt.copy()

        tipos_prenda = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Tipos Prenda').iloc[:, 0].values
        self.tipos_prenda = [i.strip().upper() for i in tipos_prenda]

        # Recursos
        df_fest = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Festivos')
        festivos = np.array(df_fest['Festivos'].dropna().values, dtype='datetime64[D]')
        self.calendar = np.busdaycalendar(weekmask='1111110', holidays=festivos)

        # df_rec.dropna(how='all', inplace=True)
        # df_rec.dropna(axis=1, how='all', inplace=True)
        # df_rec.columns = df_rec.columns.map(lambda x: x.strip().upper())
        # df_tot = df_rec.loc[
        #     df_rec['PROVEEDOR'] == 'TOTAL CAPACIDAD DIARIA', df_rec.columns[df_rec.columns.isin(tipos_prenda)]]
        # self.d_recursos = df_tot.iloc[0].to_dict()
        #
        # self.df_cap = pd.DataFrame({'CAPACIDAD': self.d_recursos}).reset_index().rename(columns={'index': 'FAMILIA'})
        # self.df_cap = self.df_cap[self.df_cap['CAPACIDAD'] > 0].reset_index(drop=True)

        df_cap_var = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Capacidad Adicional')
        df_cap_var.columns = ['FAMILIA', 'INI', 'FIN', 'CAPACIDAD']
        inter_days = np.busday_count(df_cap_var['INI'].values.astype('datetime64[D]'),
                                     df_cap_var['FIN'].values.astype('datetime64[D]'), busdaycal=self.calendar) + 1
        df_cap_var['DAYS'] = inter_days

        vector = [[df_cap_var.iloc[i, 0],  np.busday_offset(np.datetime64(df_cap_var.iloc[i, 1], 'D'), d, roll='forward', busdaycal=self.calendar), df_cap_var.iloc[i, 3]] for i in range(len(df_cap_var)) for d in range(df_cap_var.iloc[i, 4])]
        vector_cap = np.round([df_cap_var.iloc[i, 3] for i in range(len(df_cap_var)-1) for d in range(df_cap_var.iloc[i, 4])], 2)
        df_vector = pd.DataFrame(vector, columns=['FAMILIA', 'DIA', 'CAPACIDAD'])

        self.cap_var = df_vector.set_index(['FAMILIA', 'DIA']).iloc[:, 0]
        # df_reac = pd.read_excel(MDVPv2.root_input + 'Parametros.xlsx', sheet_name='Reacciones')
        # df_reac.columns = ['FAMILIA', 'FAMILIA_PROD_LARGO', 'MINU_CONF', 'FECHA_PROMETIDA']
        # df_reac = df_reac.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')
        # df_reac['START_DATE'] = np.busday_offset(df_reac['FECHA_PROMETIDA'].values.astype('datetime64[D]'),
        #                                          df_reac['AMORTIGUADOR'] * self.prop_amortiguador, roll='forward',
        #                                          busdaycal=self.calendar)
        # df_reac = df_reac.groupby(['FAMILIA', 'START_DATE'], as_index=False)['MINU_CONF'].sum()
        # df_reac.set_index(['FAMILIA', 'START_DATE'], inplace=True)
        # self.df_reacciones = df_reac.copy()

    def get_capacity(self):
        # ACTUALIZAR ARCHIVOS DE DROPBOX
        for f in ['CONTROL_*', 'Control de Carga*']:
            file_db = glob.glob(f'C:/Cloud/Dropbox/Dupree/Calculo Diario/{f}')[0]
            file_sp = glob.glob(MDVPv2.root_input+f)[0]
            datefile = datetime.datetime.fromtimestamp(os.path.getmtime(file_db))
            if os.path.getmtime(file_db) > os.path.getmtime(file_sp):
                os.remove(file_sp)
                shutil.copy(file_db, MDVPv2.root_input)

        df_entradas = pd.read_excel(glob.glob(MDVPv2.root_input+'CONTROL_*')[0], sheet_name='ENTRADAS',
                                    converters={'CODISAP': str, 'POS': str})
        fecha_lim = datetime.datetime.today() - datetime.timedelta(days=90)
        # dias_lab = np.busday_count(begindates=np.datetime64(fecha_lim).astype('datetime64[D]'),
        #                            enddates=np.datetime64(datetime.datetime.today()).astype('datetime64[D]'), busdaycal=self.calendar)
        df_entradas = df_entradas[df_entradas['FECHA'] >= fecha_lim]
        df_entradas = df_entradas[df_entradas['ORIGEN'] != 'PC']
        df_entradas_prov = df_entradas.copy()
        df_entradas = df_entradas.groupby(['CODISAP', 'DOC'], as_index=False)['UNDS'].sum()
        df_entradas.rename(columns={'FECHA': 'DIAS_TRABAJO'}, inplace=True)
        df_entradas = MDVPv1.get_familia(self, df=df_entradas, col_name='CODISAP', with_sam=True)

        df_control = pd.read_excel(glob.glob(MDVPv2.root_input + 'CONTROL_*')[0], sheet_name='CONTROL',
                                   converters={'CODISAP': str})
        df_control = df_control[['OP', 'PROV GENERAL']].rename(columns={'OP': 'ORDEN', 'PROV GENERAL': 'PROVEEDOR'})

        df_entradas.rename(columns={'DOC': 'ORDEN'}, inplace=True)
        df_entradas = df_entradas.merge(df_control, on='ORDEN', how='left')
        df_entradas['MINS'] = df_entradas['UNDS']*df_entradas['SAM']
        # Calcular la producción diaria promedio por proveedor
        df_fechas_prov = df_entradas_prov[['FECHA', 'DOC']].merge(df_control, left_on='DOC', right_on='ORDEN', how='left')
        df_dias_proveedor = df_fechas_prov.groupby(['PROVEEDOR'], as_index=False)['FECHA'].nunique().rename(columns={'FECHA': 'DIAS'})
        df_dias_proveedor = df_dias_proveedor.merge(df_entradas.groupby(['PROVEEDOR'], as_index=False)['MINS'].sum(), on='PROVEEDOR', how='left')
        df_dias_proveedor['PROD_DIA_PROM'] = df_dias_proveedor['MINS'] / df_dias_proveedor['DIAS']
        df_dias_proveedor.set_index('PROVEEDOR', inplace=True)
        df_dias_proveedor.drop(['DIAS', 'MINS'], axis=1, inplace=True)

        # Calcular la distribución de la capacidad
        df_distribution = df_entradas.groupby(['FAMILIA', 'PROVEEDOR'], as_index=False)['MINS'].sum()
        df_distribution = df_distribution.pivot(index='PROVEEDOR', columns='FAMILIA', values='MINS')
        cols = df_distribution.columns
        df_distribution['TOTAL'] = df_distribution.sum(axis=1)
        df_distribution[cols] = df_distribution[cols].div(df_distribution['TOTAL'], axis=0)
        # Agregar la información de las Jefes y Capacidad diaria
        df_recursos = pd.read_excel(glob.glob(MDVPv2.root_input+'Control de Carga*')[0], sheet_name='Recursos')
        df_recursos = df_recursos.groupby(['PROVEEDOR']).agg({'JEFE': 'first', 'CAPACIDAD DIARIA': 'max'})

        df_distribution = df_distribution.merge(df_recursos, left_index=True, right_index=True, how='inner')
        df_distribution = df_distribution.merge(df_dias_proveedor, left_index=True, right_index=True, how='left')

        # Agregar capacidad variada por análisis
        df_cap_edit = pd.read_excel(MDVPv2.root_input+'/Parametros.xlsx', sheet_name='Recursos Ajuste', index_col=0)
        df_distribution.update(df_cap_edit.fillna(0), overwrite=True)
        df_distribution = df_distribution.apply(lambda x: x.replace(0, np.nan))

        df_cap_fam = (df_distribution[cols].multiply(df_distribution['CAPACIDAD DIARIA'], axis=0)).sum(axis=0)
        df_cap_fam = df_cap_fam.reset_index()
        df_cap_fam.columns = ['FAMILIA', 'CAPACIDAD']
        self.df_cap = df_cap_fam.copy()

        df_distribution = pd.concat([df_distribution, df_cap_fam.set_index('FAMILIA').transpose()])
        df_distribution.index.name = 'PROVEEDOR'

        with pd.ExcelWriter(MDVPv2.root_input+"Parametros.xlsx", engine="openpyxl", mode="a", if_sheet_exists="replace") as writer:
            df_distribution.to_excel(writer, 'Recursos', index=True)

    def get_from_ftp(self, path=None, file=None, sep=';', encoding='cp437', low_memory=True,
                     thousands=None, decimal='.', engine=None):
        """
            Con esta función se lee 1 reporte desde un servidor ftp, indicando:
            :param
                hostname: url o ip a conectarse
                username: usuario para conectarse
                password: contraseña para la conexión
                path: ruta dentro del servidor
                file: nombre del archivo a leer
            :return: retorna un dataframe con el archivo indicado
        """

        ftp = FTP(MDVPv2.d_cred['host_ftp'], MDVPv2.d_cred['usuario_ftp'], MDVPv2.d_cred['password_ftp'])
        try:
            file = ftp.nlst(file)[-1]
            if file in os.listdir(MDVPv2.root_input_focuss):
                pd.read_csv(file, sep=sep, encoding=encoding, low_memory=low_memory, thousands=thousands,
                            decimal=decimal,
                            engine=engine)
        except Exception as e:
            file = ftp.nlst('History/PRODUCTION_ORDERS_202*')[-1]
            print(f"Archivo con fecha actual no encontrado, cargando el último disponible.\n{str(e)}")
        # file = ftp.nlst(file)[0]
        if isinstance(path, str):
            ftp.cwd(path)
        with BytesIO() as zz:
            ftp.retrbinary('RETR ' + file, zz.write)
            zz.seek(0)
            # df = pd.read_csv(zz, encoding="cp437", sep=";")
            self.df_firme = pd.read_csv(zz, sep=sep, encoding=encoding, low_memory=low_memory, thousands=thousands,
                                        decimal=decimal,
                                        engine=engine)
            self.df_firme.to_csv(f'{MDVPv2.root_input_focuss}/{file[9:]}', index=False)
        print(f"Archivo {file} descargado con éxito de {MDVPv2.d_cred['host_ftp']}.")
        return self.df_firme

    def get_reacciones(self):
        df_r = pd.read_excel(MDVPv2.root_input + 'Reserva reacción fecha.xlsx', thousands=',', sheet_name=0)
        df_r = df_r.dropna()
        df_r.loc[:, df_r.dtypes == 'O'] = df_r.loc[:, df_r.dtypes == 'O'].applymap(lambda x: x.strip())
        df_r['PROMEDIO'] = df_r['PROMEDIO'].astype('float64')
        df_r['FECHA'] = df_r['FECHA'].astype('datetime64[D]')
        df_r['FAMILIA_PROD_LARGO'] = df_r['FAMILIA'].astype('str') + '-' + df_r['OPER'].astype('str')
        df_r = df_r.merge(self.df_cap, on='FAMILIA', how='left')
        df_r['CAPACIDAD'] = df_r['CAPACIDAD'].fillna(0)
        df_r = df_r.groupby(['FAMILIA', 'FECHA', 'FAMILIA_PROD_LARGO', 'CAPACIDAD'], as_index=False)['PROMEDIO'].sum()
        df_r.rename(columns={'PROMEDIO': 'MINU_TOT', 'FECHA': 'FECHA_PROMETIDA'}, inplace=True)
        df_r.sort_values(['FAMILIA', 'FECHA_PROMETIDA'], ascending=[True, True], inplace=True)
        df_r['AMORTIGUADOR'] = df_r.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')['AMORTIGUADOR']
        df_r['AMORTIGUADOR_MED'] = np.ceil(df_r['AMORTIGUADOR']*self.prop_amortiguador)
        # df_r['MINU_CONF_ACUM'] = df_r.groupby(['FAMILIA', 'FECHA'])['MINU_CONF_AUX'].cumsum()

        df_r['N_EXCESO'] = (df_r['MINU_TOT'] // df_r['CAPACIDAD']).fillna(0).replace([np.inf, -np.inf], 0).astype('int64')
        df_r['MINU_CONF_AUX'] = df_r.apply(lambda x: [x['MINU_TOT'] % x['CAPACIDAD']] + [x['CAPACIDAD'] for i in range(x['N_EXCESO'])], axis=1)
        df_r['LOTE'] = df_r.apply(lambda x: [i for i in range(1, x['N_EXCESO'] + 2)], axis=1)
        df_r = df_r.explode(['MINU_CONF_AUX', 'LOTE'])
        df_r[['MINU_CONF_AUX', 'CAPACIDAD', 'LOTE']] = np.round(df_r[['MINU_CONF_AUX', 'CAPACIDAD', 'LOTE']].astype('float64'), 2)
        # Agregar días que se posponen a cada row, 2 días por cada día a full capacidad
        df_r['DIAS_POSPONER'] = np.where(df_r['MINU_CONF_AUX'] == df_r['CAPACIDAD'], 1, 0)
        df_r['DIAS_POSPONER'] = df_r.groupby(['FAMILIA'])['DIAS_POSPONER'].transform('cumsum') * 2
        # Re asignar el valor a las ROWS que ocupan full capacidad
        df_r.reset_index(inplace=True, drop=True)
        # df.set_index('RELATIVO', append=True, inplace=True)  # Segundo index
        idx = df_r[~(df_r['DIAS_POSPONER'].eq(df_r['DIAS_POSPONER'].shift().fillna(0))) & (df_r['DIAS_POSPONER'] != 0)].index
        df_r.loc[idx, 'DIAS_POSPONER'] = df_r.loc[idx, 'DIAS_POSPONER'] - 1  # Los días que están full
        df_r['START_DATE'] = np.busday_offset(df_r['FECHA_PROMETIDA'].values.astype('datetime64[D]'), -df_r['AMORTIGUADOR'].values.astype('int64'),
                                              roll='forward', busdaycal=self.calendar)
        df_r['START_DATE'] = np.busday_offset(df_r['START_DATE'].values.astype('datetime64[D]'),
                                              df_r['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        df_r = df_r[df_r['START_DATE'] >= np.datetime64(self.today, 'D')]
        df_r.drop('DIAS_POSPONER', axis=1, inplace=True)
        df_r['ESTADO'] = 'Reaccion'
        return df_r.reset_index(drop=True)

    def get_previsional_plan(self):
        df_plan = MDVPv1.resultado_prev(self, fam_y_sam=True, force=True)
        df_pp = MDVPv1.cargue(self, force=True)[['OPREV', 'FECHAPAIS']].drop_duplicates().reset_index(drop=True)
        df_pp.rename(columns={'FECHAPAIS': 'FECHA_PROMETIDA'}, inplace=True)
        df_plan = df_plan[df_plan.OPREV.astype('str').str.startswith('9')].reset_index(drop=True)
        df_plan = df_plan[~df_plan['FAMILIA'].str.contains('INDIGO')].reset_index(drop=True)
        # df_plan['FAMILIA'] = np.where(df_plan['FAMILIA'].isin(['PANTY/TOP', 'PLANO', 'PUNTO BASICO']),
        #                               df_plan['FAMILIA'].astype('str') + " " + df_plan['CATALOGO'].astype('str'),
        #                               df_plan['FAMILIA'].astype('str'))
        df_plan['FAMILIA_PROD_LARGO'] = np.where(df_plan['OPER'].isna(), df_plan['FAMILIA'].str[:] + "-SP",
                                      df_plan['FAMILIA'].str[:] + "-CP")
        df_plan.drop(['OPER', 'CATALOGO'], axis=1, inplace=True)
        df_plan['MINU_TOT'] = df_plan['SAM'] * df_plan['CANT_REAL']
        df_plan.dropna(subset=['MINU_TOT'], inplace=True)
        df_plan.drop(['ANO', 'TIPO', 'PRIORIDAD', 'CANT_REAL', 'UND_DISP',
                      '% DE COBERTURA SKU', '% DE COBERTURA REF', 'SAM', 'FPROV'], axis=1, inplace=True)
        # Insertar fecha de compromiso
        df_plan.rename(columns={'TEMA': 'PAIS', 'TEMP': 'CAMPANA', 'LIN': 'LINEA',
                                'CODISAP': 'SKU', 'NAMESAP': 'DESC_SKU', 'CANT_REAL_OM': 'CANT_TOTAL',
                                'FAMILIA_PROD': 'FAMILIA_PROD_LARGO'}, inplace=True)
        df_plan = df_plan.merge(df_pp, on='OPREV', how='left').rename(columns={'OPREV': 'ORDEN_PREV'})
        df_plan['FECHA_PROMETIDA'] = df_plan['FECHA_PROMETIDA'].fillna(np.datetime64(self.today))
        df_plan.loc[:, df_plan.dtypes == 'O'] = df_plan.loc[:, df_plan.dtypes == 'O'].astype('str').applymap(lambda x: x.strip())

        df_plan = df_plan.merge(self.df_cap, on='FAMILIA', how='left')
        df_plan['CAPACIDAD'] = df_plan['CAPACIDAD'].fillna(0)
        df_plan.sort_values(['FAMILIA', 'FECHA_PROMETIDA'], ascending=[True, True], inplace=True)
        df_plan['AMORTIGUADOR'] = df_plan.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')['AMORTIGUADOR']
        df_plan['AMORTIGUADOR_MED'] = np.ceil(df_plan['AMORTIGUADOR'] * self.prop_amortiguador)
        # df_plan['MINU_CONF_ACUM'] = df_plan.groupby(['FAMILIA', 'FECHA'])['MINU_CONF_AUX'].cumsum()

        df_plan['N_EXCESO'] = (df_plan['MINU_TOT'] // df_plan['CAPACIDAD']).fillna(0).replace([np.inf, -np.inf], 0).astype('int64')
        df_plan['MINU_CONF_AUX'] = df_plan.apply(
            lambda x: [x['MINU_TOT'] % x['CAPACIDAD']] + [x['CAPACIDAD'] for i in range(x['N_EXCESO'])], axis=1)
        df_plan['LOTE'] = df_plan.apply(lambda x: [i for i in range(1, x['N_EXCESO'] + 2)], axis=1)
        df_plan = df_plan.explode(['MINU_CONF_AUX', 'LOTE'])
        df_plan[['MINU_CONF_AUX', 'CAPACIDAD', 'LOTE']] = np.round(
            df_plan[['MINU_CONF_AUX', 'CAPACIDAD', 'LOTE']].astype('float64'), 2)
        # Agregar días que se posponen a cada row, 2 días por cada día a full capacidad
        df_plan['DIAS_POSPONER'] = np.where(df_plan['MINU_CONF_AUX'] == df_plan['CAPACIDAD'], 1, 0)
        df_plan['DIAS_POSPONER'] = df_plan.groupby(['FAMILIA'])['DIAS_POSPONER'].transform('cumsum') * 2
        # Re asignar el valor a las ROWS que ocupan full capacidad
        df_plan.reset_index(inplace=True, drop=True)
        # df.set_index('RELATIVO', append=True, inplace=True)  # Segundo index
        idx = df_plan[
            ~(df_plan['DIAS_POSPONER'].eq(df_plan['DIAS_POSPONER'].shift().fillna(0))) & (df_plan['DIAS_POSPONER'] != 0)].index
        df_plan.loc[idx, 'DIAS_POSPONER'] = df_plan.loc[idx, 'DIAS_POSPONER'] - 1  # Los días que están full
        df_plan['START_DATE'] = np.busday_offset(df_plan['FECHA_PROMETIDA'].values.astype('datetime64[D]'),
                                                 -df_plan['AMORTIGUADOR_MED'].values.astype('int64'),
                                                 roll='forward', busdaycal=self.calendar)
        df_plan['START_DATE'] = np.busday_offset(df_plan['START_DATE'].values.astype('datetime64[D]'),
                                                 df_plan['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        df_plan['START_DATE'] = np.maximum(df_plan['START_DATE'], np.datetime64(self.today, 'D'))
        df_plan['START_DATE_ANTERIOR'] = df_plan['START_DATE'].copy()
        df_plan.drop('DIAS_POSPONER', axis=1, inplace=True)
        df_plan['LOTE'] = np.nan
        df_plan['ESTADO'] = 'Previsional Plan'
        self.df_planeado = df_plan.copy()
        return df_plan.reset_index(drop=True)

    def smooth_forward(self, idx_row=None, row_data=None):
        posible_dates = np.ceil(self.cap_disp.loc[row_data['FAMILIA'], row_data['START_DATE']:])
        posible_dates = posible_dates[(posible_dates.values >= row_data['MINU_CONF_AUX'])]

        if posible_dates.shape[0] >= 1:
            idx_date_suav = posible_dates.index[0]
            self.df_final.loc[idx_row, 'START_DATE'] = idx_date_suav[1]
            self.df_final.loc[idx_row, 'LOTE'] = 3
            self.cap_disp.loc[idx_date_suav] -= np.round(row_data['MINU_CONF_AUX'], 2)
            return True
        return False

    def smooth_backwards(self, idx_row=None, row_data=None, sooner=True):
        sooner = {True: -1, False: 0}[sooner]

        posible_dates = np.ceil(self.cap_disp.loc[row_data['FAMILIA'], self.today:row_data['START_DATE']].iloc[:-1])
        posible_dates = posible_dates[(posible_dates.values >= row_data['MINU_CONF_AUX'])]

        if posible_dates.shape[0] >= 1:
            idx_date_suav = posible_dates.index[sooner]
            self.df_final.loc[idx_row, 'START_DATE'] = idx_date_suav[1]
            self.df_final.loc[idx_row, 'LOTE'] = -row_data['LOTE']
            self.cap_disp.loc[idx_date_suav] -= np.round(row_data['MINU_CONF_AUX'], 2)
            print(f'{idx_row} Backwards Normal')
            self.df_final.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                      ascending=[True, True, True, True, True], inplace=True)
            self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
            return True
        return False

    def smooth_backwards_sliced(self, idx_row=None, row_data=None, sooner=True, days_slices=2):
        sooner = {True: False, False: True}[sooner]

        posible_dates = np.ceil(self.cap_disp.loc[row_data['FAMILIA'], self.today:row_data['START_DATE']])
        posible_dates.sort_index(level=1, ascending=sooner, inplace=True)
        for ref_day in range(posible_dates.shape[0]):
            for days in range(1, days_slices+1):
                if posible_dates.iloc[ref_day:ref_day+days].sum() >= row_data['MINU_CONF_AUX']:
                    # print(f'{ref_day} - {days}')
                    # Asignar al df y capacidad
                    selected = posible_dates.iloc[ref_day:ref_day + days]
                    selected = selected[selected > 0]
                    cap_left = pd.Series([max(i - row_data['MINU_CONF_AUX'], 0) for i in selected.cumsum().values], index=selected.index)
                    cap_used = selected - cap_left
                    for ind in range(len(selected)):
                        new_idx = idx_row + ind/10
                        self.df_final.loc[new_idx] = row_data
                        self.df_final.loc[new_idx, 'START_DATE'] = selected.index.get_level_values(1)[ind]
                        self.df_final.loc[new_idx, 'MINU_CONF_AUX'] = np.round(cap_used[ind], 2)
                        self.df_final.loc[new_idx, 'LOTE'] = -(ind + 11)
                        self.cap_disp.loc[selected.index[ind]] = np.round(cap_left[ind], 2)

                    print(f'{idx_row} Backwards Sliced')

                    self.df_final.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                              ascending=[True, True, True, True, True], inplace=True)
                    self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()

                    return True
        return False

    def smooth_forward_both(self, idx_row=None, row_data=None, days_slices=2):

        posible_dates = np.ceil(self.cap_disp.loc[row_data['FAMILIA'], row_data['START_DATE']:])
        posible_dates.sort_index(level=1, ascending=True, inplace=True)
        for ref_day in range(posible_dates.shape[0]):
            for days in range(1, days_slices+1):
                if posible_dates.iloc[ref_day:ref_day+days].sum() >= row_data['MINU_CONF_AUX']:
                    # print(f'{ref_day} - {days}')
                    # Asignar al df y capacidad
                    selected = posible_dates.iloc[ref_day:ref_day + days]
                    selected = selected[selected > 0]
                    cap_left = pd.Series([max(i - row_data['MINU_CONF_AUX'], 0) for i in selected.cumsum().values], index=selected.index)
                    cap_used = selected - cap_left
                    for ind in range(len(selected)):
                        new_idx = idx_row + ind/10
                        self.df_final.loc[new_idx] = row_data
                        self.df_final.loc[new_idx, 'START_DATE'] = selected.index.get_level_values(1)[ind]
                        self.df_final.loc[new_idx, 'MINU_CONF_AUX'] = cap_used[ind]
                        self.df_final.loc[new_idx, 'LOTE'] = (ind + 11)
                        self.cap_disp.loc[selected.index[ind]] = cap_left[ind]

                    print(f'{idx_row} Forward')
                    self.df_final.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                              ascending=[True, True, True, True, True], inplace=True)
                    self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()

                    return True
        return False

    def smooth2(self, idx_row=None, sooner=True):
        row_1 = self.df_final.loc[idx_row].copy()
        row_1['LOTE'] = 1
        # idx_row = self.df_final.iloc[row].name
        # if not self.smooth_backwards(idx_row=idx_row, row_data=row_1, sooner=sooner):
        # if row_1['ESTADO'] == 3:  # Previsional Planeado
        #     sooner = False

        if not self.smooth_backwards_sliced(idx_row=idx_row, row_data=row_1, sooner=sooner):
            self.smooth_forward_both(idx_row=idx_row, row_data=row_1)
                # self.smooth_forward(idx_row=idx_row, row_data=row_1)

    def smooth(self, row=None, sooner=True):
        row_1 = self.df_final.iloc[row].copy()
        row_2 = self.df_final.iloc[row].copy()
        row_1['LOTE'] = 1
        idx_row = self.df_final.iloc[row].name
        rows_to_skip = 1
        # Debido a la relajación de la capacidad pueden haber negativos al recalcular el MINU_CONF_AUX en row_1
        # Intentar suavizar hacia atrás antes de dividir la orden
        if not self.smooth_backwards(idx_row=idx_row, row_data=row_1, sooner=sooner):  # No se logró suavizar el row completo
        # Row1
            row_1['MINU_CONF_AUX'] = np.round(row_1['CAPACIDAD'] - (row_1['MINU_CONF_ACUM'] - row_1['MINU_CONF_AUX']), 2)
            if row_1['MINU_CONF_AUX'] == 0:
                self.smooth_backwards(idx_row=idx_row, row_data=row_1, sooner=sooner)
                rows_to_skip = 1
                row_fltr = (self.df_final['FAMILIA'] == row_1['FAMILIA']) & (
                            self.df_final['START_DATE'] == row_1['START_DATE'])
                idx = self.df_final.loc[row_1.name:].loc[row_fltr, 'START_DATE'].index
                new_date = np.busday_offset(self.df_final.loc[idx, 'START_DATE'].values.astype('datetime64[D]'),
                                            offsets=1, roll='forward', busdaycal=self.calendar)
                self.df_final.loc[idx, 'START_DATE'] = new_date
                self.cap_disp.loc[row_1['FAMILIA'], new_date] -= row_1['MINU_CONF_AUX']
                self.df_final.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                          ascending=[True, True, True, True, True], inplace=True)
                self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
                return rows_to_skip

        self.df_final.loc[row_1.name] = row_1
        # Row2
        row_2['MINU_CONF_AUX'] = np.round(row_2['MINU_CONF_AUX'] - row_1['MINU_CONF_AUX'], 2)
        row_2['LOTE'] = row_1['LOTE'] + 1
        row_2.name = row_1.name+0.1
        self.df_final.loc[row_2.name] = row_2
        self.df_final.sort_index(inplace=True)
        self.cap_disp.loc[row_1['FAMILIA'], row_1['START_DATE']] -= row_1['MINU_CONF_AUX']
        rows_to_skip = 2
        # Suavizar hacia atrás parcialmente
        if not self.smooth_backwards(idx_row=row_2.name, row_data=row_2, sooner=sooner):  # No se logró suavizar el row parcial
            # Posponer fechas similares al valor dividido
            if row_2['ESTADO'] == 'Previsional':
                self.smooth_forward(idx_row=row_2.name, row_data=row_2)
                rows_to_skip = 1
            else:
                row_fltr = (self.df_final['FAMILIA'] == row_2['FAMILIA']) & (self.df_final['START_DATE'] == row_2['START_DATE'])
                idx = self.df_final.loc[row_2.name:].loc[row_fltr, 'START_DATE'].index
                new_date = np.busday_offset(self.df_final.loc[idx, 'START_DATE'].values.astype('datetime64[D]'),
                                            offsets=1, roll='forward', busdaycal=self.calendar)
                self.df_final.loc[idx, 'START_DATE'] = new_date
                self.cap_disp.loc[row_2['FAMILIA'], new_date] -= row_2['MINU_CONF_AUX']

        self.df_final.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                                  ascending=[True, True, True, True, True], inplace=True)
        self.df_final['MINU_CONF_ACUM'] = self.df_final.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        return rows_to_skip

    def get_file_production(self):
        if self.df_data_prod is None:
            file = f'/History/PRODUCTION_ORDERS_{MDVPv2.ftp_today}*.csv'
            # file = fr'/History/PRODUCTION_ORDERS_20220729*.csv'
            # file = f'/History/PRODUCTION_ORDERS_20220303*.csv'
            df_data = self.get_from_ftp(file=file, encoding='cp437', sep=';')
            self.df_data_prod = df_data.copy()

    def generate(self):
        # Firme
        self.get_file_production()
        df_data = self.df_data_prod.copy()
        df_data = df_data[df_data['NUMERO_OPERACION'].isin([90, 0])].reset_index(drop=True)
        df_data[df_data.select_dtypes(['object']).columns] = df_data.select_dtypes(['object']).apply(lambda x: x.str.strip())
        df_data['SKU'] = df_data['SKU'].astype(str)
        df_fam = MDVPv1.get_familia(self, df_data[['SKU']], with_catalogo=True)
        # Familia Larga
        df_fam['FAMILIA'] = df_fam['FAMILIA'].apply(lambda x: x.replace('+', '/'))
        # df_fam['FAMILIA'] = np.where(df_fam['FAMILIA'].isin(['PANTY/TOP', 'PLANO', 'PUNTO BASICO']),
        #                              df_fam['FAMILIA'] + " " + df_fam['CATALOGO'],
        #                              df_fam['FAMILIA'])
        df_fam.drop('CATALOGO', axis=1, inplace=True)

        df_data['FAMILIA'] = df_data.merge(df_fam, on='SKU', how='left')['FAMILIA'].values
        df_data['FAMILIA_PROD_LARGO'] = np.where(df_data['FAMILIA_PROD'].str[-2:] == 'SP', df_data['FAMILIA'] + '-SP',
                                                 df_data['FAMILIA'] + '-CP')
        if df_data['FAMILIA_PROD_LARGO'].str.contains('VALIDAR').sum() > 0:
            print("Eliminando productos que no tienen FAMILIA PRODUCCION.")
            df_data = df_data[~df_data['FAMILIA_PROD_LARGO'].str.contains('VALIDAR')].reset_index(drop=True)

        df_data['AMORTIGUADOR'] = df_data.merge(self.df_amortiguador, on='FAMILIA_PROD_LARGO', how='left')[
            'AMORTIGUADOR'].values

        df_data['AMORTIGUADOR_MED'] = np.ceil(df_data['AMORTIGUADOR'] * self.prop_amortiguador)

        df_data[['FECHA_ACTU', 'FECHA_PROMETIDA']] = df_data[['FECHA_ACTU', 'FECHA_PROMETIDA']].apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y'))
        fecha_fin = df_data['FECHA_PROMETIDA'].values.astype('datetime64[D]')
        df_data['START_DATE'] = np.busday_offset(fecha_fin, -df_data['AMORTIGUADOR_MED'], roll='forward',
                                                 busdaycal=self.calendar)
        df_data['START_DATE'] = np.maximum(df_data['START_DATE'], df_data['FECHA_ACTU'])
        df_data['START_DATE_ANTERIOR'] = df_data['START_DATE']
        df_data['RELATIVO'] = df_data['SKU'].str[:7] + '_' + df_data['PAIS'] + '_' + df_data['CAMPANA'] + '_' + df_data['START_DATE'].astype(str)
        # No indigo
        df_data = df_data[~df_data['FAMILIA'].str.contains('INDIGO')]

        # CREATE FULL DF
        df_fir = df_data.copy()
        df_fir = self.clean(df_n=df_fir)
        df_reac = self.get_reacciones()
        df_plan = self.get_previsional_plan()
        cols = df_fir.columns
        df_fir = df_reac.append([df_fir, df_plan]).loc[:, cols]
        # df_fir = df_fir.append([df_plan]).loc[:, cols]
        df_fir = df_fir[df_fir['MINU_CONF_AUX'] >= 1]
        df_fir['ESTADO'] = df_fir['ESTADO'].replace({'Reaccion': 0, 'Fabricacion': 1, 'Previsional': 2, 'Previsional Plan': 3})
        df_fir.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'INDEX_ANTERIOR', 'RELATIVO'],
                           ascending=[True, True, True, True, True], inplace=True)
        df_fir['MINU_CONF_ACUM'] = df_fir.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        df_fir = df_fir.reset_index(drop=True)
        df_fir['INDEX_ANTERIOR'] = np.arange(df_fir.shape[0])

        # CAPACIDAD
        dias_habil = pd.Series(
            [np.busday_offset(np.datetime64(df_fir['START_DATE'].min(), 'D'), i, roll='forward',
                              busdaycal=self.calendar)
             for i in range(np.busday_count(np.datetime64(self.today, 'D'),
                                            np.datetime64(df_fir['START_DATE'].max(), 'D'),
                                            busdaycal=self.calendar) + 60)], dtype='datetime64[D]')

        cap_disp = pd.DataFrame({'FAMILIA': np.repeat(df_data['FAMILIA'].unique(), len(dias_habil)),
                                 'DIA': np.tile(dias_habil, df_data['FAMILIA'].unique().shape[0])})
        cap_disp = cap_disp.merge(self.df_cap, on='FAMILIA', how='left').rename(
            columns={'CAPACIDAD': 'CAPACIDAD_DISPONIBLE'})
        cap_disp.set_index(['FAMILIA', 'DIA'], inplace=True)
        cap_disp.sort_index(inplace=True)
        cap_disp = cap_disp.iloc[:, 0]
        cap_disp = np.round(cap_disp, 2)
        #Agregar la capacidad Variable
        cap_disp.loc[self.cap_var[self.cap_var.index.isin(cap_disp.index)].index] = self.cap_var[self.cap_var.index.isin(cap_disp.index)].values
        df_cap_disp = cap_disp.reset_index(drop=False).rename(columns={'DIA': 'START_DATE'})
        # QUITANDO LAS REACCONES A LA CAPACIDAD DISPONIBLE
        df_reac_s = df_reac[['FAMILIA', 'START_DATE', 'MINU_CONF_AUX']].drop_duplicates().reset_index(drop=True)
        df_reac_s.set_index(['FAMILIA', 'START_DATE'], inplace=True)
        df_reac_s = df_reac_s['MINU_CONF_AUX']
        df_reac_s.index.names = ['FAMILIA', 'DIA']
        cap_disp = cap_disp.add(-df_reac_s, fill_value=0)

        # self.df_final = df_fir[df_fir['ESTADO'] != 'Reaccion'].reset_index(drop=True).copy()
        self.df_final = df_fir.reset_index(drop=True).copy()
        self.cap_disp = cap_disp.copy()

        while any(self.df_final.LOTE.isna()):
            # if xidx >= 300:
            #     df_final = self.df_final.copy()
            #     break
            xidx = self.df_final['LOTE'].notna().idxmin()#self.df_final.index[row]
            cap_idx = (self.df_final.loc[xidx, 'FAMILIA'], np.datetime64(self.df_final.loc[xidx, 'START_DATE'], 'D'))
            if self.df_final.loc[xidx, 'MINU_CONF_ACUM'] >= self.cap_disp.loc[cap_idx]:#*self.capacity_relax:
                if np.isnan(self.df_final.loc[xidx, 'LOTE']):  # No ha sido asignado antes
                    self.smooth2(xidx)
                continue

            self.df_final.loc[xidx, 'LOTE'] = 0
            self.cap_disp.loc[cap_idx] -= np.round(self.df_final.loc[xidx, 'MINU_CONF_AUX'], 2)
        else:
            df_final = self.df_final.copy()
            # Lote 0: Nada, -1: adelantado completo, 1y2: Dividido

        # df_reac_w_index = df_fir[df_fir['ESTADO'] == 'Reaccion'].reset_index(drop=True).copy()
        # df_final = df_final.append([df_reac_w_index]).loc[:, cols].reset_index(drop=True)
        df_fir['CAPACIDAD'] = df_fir.merge(df_cap_disp, on=['FAMILIA', 'START_DATE'], how='left')['CAPACIDAD_DISPONIBLE']
        df_final['CAPACIDAD'] = df_final.merge(df_cap_disp, on=['FAMILIA', 'START_DATE'], how='left')['CAPACIDAD_DISPONIBLE']
        df_final['ESTADO'] = df_final['ESTADO'].replace({0: 'Reaccion', 1: 'Fabricacion', 2: 'Previsional', 3: 'Previsional Plan'})
        df_final[['CANT_TOTAL', 'CANT_PEND', 'CANT_NETA']] = df_final[['CANT_TOTAL', 'CANT_PEND', 'CANT_NETA']].apply(
            lambda x: x * (df_final['MINU_CONF_AUX'] / df_final['MINU_TOT']))
        df_final['FECHA_PROMETIDA_AJUST'] = np.busday_offset(df_final['START_DATE'].values.astype('datetime64[D]'),
                                                             df_final['AMORTIGUADOR_MED'], busdaycal=self.calendar,
                                                             roll='forward')

        ids = df_final[~((df_final['FECHA_LIBERACION'].isna()) | (df_final['FECHA_LIBERACION'] == ''))].index
        df_final.loc[ids, 'FECHA_LIBERACION'] = df_final.loc[ids, 'FECHA_LIBERACION'].apply(lambda x: datetime.datetime.strptime(x, "%d/%m/%Y"))
        df_final['FECHA_LIBERACION_AJUST'] = np.busday_offset(df_final['START_DATE'].values.astype('datetime64[D]'),
                                                              -df_final['AMORTIGUADOR_MED'], busdaycal=self.calendar,
                                                              roll='forward')

        root_print = rf'C:\Users\{os.getlogin()}\35159_147728_DUPREE_VENTA_DIRECTA_S_A\CADENA DE SUMINISTROS - Documentos\Projects\01. Supply\08. MDVP\Files\2. OUTPUT\\'
        df_cap_disp.to_parquet(root_print+'cap_disp.gzip', compression='gzip')
        df_final.to_excel(root_print+'data_suav.xlsx')
        df_fir.to_excel(root_print+'data_sin_suav.xlsx')

    def clean(self, df_n):
        df = df_n.copy()
        # df['REFERENCIA'] = df['SKU'].str[:7]
        # df_sam = MDVPv1.get_sam()
        # df = df.merge(df_sam, on='REFERENCIA', how='left')
        df['MINU_TOT'] = df['MINU_CONF'].fillna(0) + df['MINU_EMPA'].fillna(0)
        df = df[df['MINU_TOT'] >= 1].reset_index(drop=True)
        df['RELATIVO'] = df['SKU'].str[:7] + '_' + df['PAIS'] + '_' + df['CAMPANA'] + '_' + df['START_DATE'].astype(str)
        # self.cap_var
        df['CAPACIDAD'] = df.merge(self.df_cap, on='FAMILIA', how='left')['CAPACIDAD'].values
        # df.set_index('RELATIVO', inplace=True)
        # Cuando la producción en una sola orden (ROW) excede capacidad diaria
        df['N_EXCESO'] = (df['MINU_TOT'] // df['CAPACIDAD']).fillna(0).replace([np.inf, -np.inf], 0).astype('int64')
        df['MINU_CONF_AUX'] = df.apply(
            lambda x: [x['MINU_TOT'] % x['CAPACIDAD']] + [x['CAPACIDAD'] for i in range(x['N_EXCESO'])], axis=1)
        df['LOTE'] = df.apply(lambda x: [i for i in range(1, x['N_EXCESO'] + 2)], axis=1)
        df = df.explode(['MINU_CONF_AUX', 'LOTE'])
        df[['MINU_CONF_AUX', 'LOTE']] = np.round(df[['MINU_CONF_AUX', 'LOTE']].astype('float64'), 2)
        # Agregar días que se posponen a cada row, 2 días por cada día a full capacidad
        df['DIAS_POSPONER'] = np.where(df['MINU_CONF_AUX'] == df['CAPACIDAD'], 1, 0)
        df['DIAS_POSPONER'] = df.groupby(['FAMILIA'])['DIAS_POSPONER'].transform('cumsum') * 2
        # Re asignar el valor a las ROWS que ocupan full capacidad
        df.reset_index(inplace=True, drop=True)
        # df.set_index('RELATIVO', append=True, inplace=True)  # Segundo index
        idx = df[~(df['DIAS_POSPONER'].eq(df['DIAS_POSPONER'].shift().fillna(0))) & (df['DIAS_POSPONER'] != 0)].index
        df.loc[idx, 'DIAS_POSPONER'] = df.loc[idx, 'DIAS_POSPONER'] - 1  # Los días que están full
        df['START_DATE'] = np.busday_offset(df['START_DATE'].values.astype('datetime64[D]'),
                                            df['DIAS_POSPONER'], roll='forward', busdaycal=self.calendar)
        df.drop('DIAS_POSPONER', axis=1, inplace=True)
        # df['RELATIVO'] = df['SKU'].str[:7] + '_' + df['PAIS'] + '_' + df['CAMPANA'] + '_' + df['START_DATE'].astype(str)
        # df = df.droplevel('RELATIVO')
        # df.set_index('RELATIVO', append=True, inplace=True)
        df.sort_values(['FAMILIA', 'ESTADO', 'START_DATE', 'RELATIVO'],
                       ascending=[True, True, True, True], inplace=True)
        df.reset_index(drop=True, inplace=True)
        df.reset_index(inplace=True)
        df = df.rename(columns={'index': 'INDEX_ANTERIOR'})
        df['MINU_CONF_ACUM'] = df.groupby(['FAMILIA', 'START_DATE'])['MINU_CONF_AUX'].cumsum()
        df['LOTE'] = np.nan
        df[['MINU_CONF_AUX', 'CAPACIDAD']] = np.round(df[['MINU_CONF_AUX', 'CAPACIDAD']], 2)
        return df


if __name__ == '__main__':
    MDVPv2().generate()
