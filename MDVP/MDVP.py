# -*- coding: utf-8 -*-
"""
Created on Mon May 31 10:13:19 2021

@author: liamd
"""
import pandas as pd
import numpy as np
import os
import re
import glob
from datetime import datetime

name_user = os.getlogin()
root_main = "C:/Cloud/Dropbox/Dupree/Tablas/"
root_diario = "C:/Cloud/Dropbox/Dupree/Calculo Diario/"
root_suministros = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Data"
root_project = "C:/Users/" + name_user + "/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP"
root_plan = "C:/Users/jhonathan.lopez/OneDrive - 35159_147728_DUPREE_VENTA_DIRECTA_S_A/PLAN_PROD/VALIDACIONES/NUEVA MDVP/AJUSTE PEDIDO/"

df_cronograma = pd.read_csv(root_main+'/BD_CRONOGRAMA.txt', sep='\t')
os.chdir(root_main)
all_files = glob.glob('*.txt')
now = datetime.now()
time_now = pd.Timestamp.now()
date_now = pd.Timestamp(time_now.year, time_now.month, time_now.day)

# =============================================================================
# PARAMETROS
# =============================================================================
def parametros():
    global df_ignorados
    # df_ignorados = pd.read_excel(root_project + '/inputs_mdvp.xlsx', sheet_name='IGNORADOS')
    df_ignorados = pd.read_excel(root_main + 'inputs_mdvp.xlsx', sheet_name='IGNORADOS')
    df_ignorados = df_ignorados.loc[:,['PREVISIONAL']].drop_duplicates().reset_index(drop=True)
    df_ignorados['PREVISIONAL_'] = df_ignorados['PREVISIONAL']
    
    global parametros_dict
    parametros_dict = {}
    # df = pd.read_excel(root_project + '/inputs_mdvp.xlsx', sheet_name='PARAMETROS')
    df = pd.read_excel(root_main + 'inputs_mdvp.xlsx', sheet_name='PARAMETROS')
    df_ppais = df.iloc[0:6, 0:2]
    df_ptipo = df.iloc[0:5, 3:5].rename(columns={'PUNTAJE.1':'PUNTAJE'})
    df_lead_min = df.iloc[0:5, 6:8].rename(columns={'PUNTAJE.2':'PUNTAJE'})
    df_lead_min['LEAD_MIN'] = df_lead_min['LEAD_MIN'].astype(int)
    df_lead_min = df_lead_min.sort_values('PUNTAJE').set_index('LEAD_MIN')    
    
    df_lead_due_max = df.iloc[0:6, 9:11].rename(columns={'PUNTAJE.3':'PUNTAJE'})
    df_lead_due_max['LEAD_DUE_MAX'] = df_lead_due_max['LEAD_DUE_MAX'].astype(int)
    df_lead_due_max = df_lead_due_max.sort_values('PUNTAJE').set_index('LEAD_DUE_MAX')
    
    df_lead_due_ind = df.iloc[0:6, 12:14].rename(columns={'PUNTAJE.4':'PUNTAJE'})
    df_lead_due_ind['LEAD_DUE_IND'] = df_lead_due_ind['LEAD_DUE_IND'].astype(int)
    df_lead_due_ind = df_lead_due_ind.sort_values('PUNTAJE').set_index('LEAD_DUE_IND')
    
    df_lead_due_proc = df.iloc[0:6, 15:17].rename(columns={'PUNTAJE.5':'PUNTAJE'})
    df_lead_due_proc['LEAD_DUE_PROC'] = df_lead_due_proc['LEAD_DUE_PROC'].astype(int)
    df_lead_due_proc = df_lead_due_proc.sort_values('PUNTAJE').set_index('LEAD_DUE_PROC')
    
    df_ptpmt = df.iloc[0:2, 18:20].rename(columns={'PUNTAJE.6':'PUNTAJE'})
    df_ptpmt['TPMT'] = df_ptpmt['TPMT'].astype(int).astype('str')
    horizonte_liberacion = df.iloc[10, 0]
    parametros_dict = {'df_ppais':df_ppais,
                       'df_ptipo':df_ptipo,
                       'df_lead_min':df_lead_min,
                       'df_lead_due_max':df_lead_due_max,
                       'df_lead_due_ind':df_lead_due_ind,
                       'df_lead_due_proc':df_lead_due_proc,
                       'df_ptpmt':df_ptpmt,
                       'horizonte_liberacion':horizonte_liberacion}

# =============================================================================
# Insertar ZPP_LEADER_LIST_ADV
# =============================================================================
def zpp_leader_list_adv():
    df = pd.read_csv(root_main + 'ZPP_LEADER_LIST_ADV.txt', sep='\t', encoding='cp1252', skiprows=4, dtype='str')
    df = df.query("Material == Material & Material != ''")
    df = df.drop(['Unnamed: 0', 'Unnamed: 1'], axis=1)
    df[['Und-Advance', 'Und-Nacional']] = df[['Und-Advance', 'Und-Nacional']].astype(str).applymap(lambda x: x.strip().replace(',','')).astype('float')
    df = df.groupby(['Pais', 'Año-Camp', 'Campaña', 'Material'], as_index=False)[['Und-Advance', 'Und-Nacional']].sum()
    df['COLECCION'] = np.where((df['Und-Nacional'] > 0) & (df['Und-Advance'] > 0), 'ADV-NAL',
                               np.where(df['Und-Advance'] > 0, 'ADV', 'NAL'))
    df['Año-Camp'] = df['Año-Camp'].apply(lambda x: x.strip()[-4:])
    df = df.rename(columns = {'Pais':'TEMA',
                              'Año-Camp':'ANO',
                              'Campaña':'TEMP',
                              'Material':'CODISAP'})
    df['IDC'] = (df['TEMA'].astype(str)+
                 df['ANO'].astype(str)+
                 df['TEMP'].astype(str)+
                 df['CODISAP'].astype(str))
    df = df.drop_duplicates().reset_index(drop=True)
    global df_zpp_leader_list_adv
    df_zpp_leader_list_adv = df.copy()
    return df

# =============================================================================
# Insertar el ZPP_PLAN_OPREV_2020'
# =============================================================================
def zpp_plan_oprev():
    zpp_plan_oprev_files = [i for i in all_files if 'ZPP_PLAN_OPREV' in i]
    df = pd.DataFrame()
    for f in zpp_plan_oprev_files:
        df_temp = pd.read_csv(root_main + f, sep='\t', encoding='cp1252', skiprows=5)
        df = df.append(df_temp)    
    
    df = df.loc[:,['País', 'Año', 'Unnamed: 3', 'Campaña', 'Usuario',
                   'Fecha crea Prev.', 'Material Prev.', 'Ord.prev.',
                   'Cant. Prev.', 'Fin_extrem']]
    df = df.query("(País != 'País') & (`Ord.prev.` == `Ord.prev.`) & (Año != 2019)")
    df = df.drop(['Unnamed: 3','Usuario','Cant. Prev.'], axis=1)
    df[['Fecha crea Prev.','Fin_extrem']] = df[['Fecha crea Prev.','Fin_extrem']].applymap(lambda x: x.strip().replace(' ', ''))
    df[['Fecha crea Prev.','Fin_extrem']] = df[['Fecha crea Prev.','Fin_extrem']].applymap(lambda x: x.replace('00.00.0000', '01.01.1990'))
    df['Fecha crea Prev.'] = pd.to_datetime(df['Fecha crea Prev.'], format='%d.%m.%Y')
    df['Fin_extrem'] = pd.to_datetime(df['Fin_extrem'], format='%d.%m.%Y')
    df = df.drop_duplicates()
    df = df.groupby(['País','Año','Campaña','Fin_extrem','Material Prev.','Ord.prev.'],
                    as_index=False)[['Fecha crea Prev.']].max()
    df = df.groupby(['País','Año','Campaña','Fecha crea Prev.','Material Prev.','Ord.prev.'],
                    as_index=False)[['Fin_extrem']].max()
    df = df.rename(columns={'País':'TEMA',
                            'Año':'ANO',
                            'Campaña':'TEMP',
                            'Material Prev.':'CODISAP',
                            'Ord.prev.':'OPREV',
                            'Fecha crea Prev.':'FPROV',
                            'Fin_extrem':'FCOMP'})
    df['IDC'] = (df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+df['CODISAP'].astype(str))
    df = df.reset_index(drop=True)
    #Insertar ZPP_LEADER_LIST_ADV
    df_zpp_leader_list_adv = zpp_leader_list_adv()
    df = df.merge(df_zpp_leader_list_adv[['IDC','COLECCION']].reset_index(drop=True), on='IDC', how='left')
    df = df.rename(columns = {'COLECCION':'COLE'})
    df = df.sort_values('FPROV', ascending=True).loc[:,['TEMA','ANO','TEMP','CODISAP','OPREV','FPROV','FCOMP','IDC','COLE']]
    return df.reset_index(drop=True)

# =============================================================================
# #ZDM_MAT_ART
# =============================================================================
def zdm_mat_art():
    df = pd.read_csv(root_main + "ZDM_MAT_ART.TXT", sep="\t", encoding='cp1252', skiprows=5, dtype='str')
    df = df.rename(columns = {'Grupo artíc. ext.':'LIN',
                              'Texto breve de material':'NAMESAP',
                              'Material':'CODISAP'})
    df = df[['CODISAP','NAMESAP','LIN']]
    df['LINPROD'] = np.where((df['LIN'] == 'MSC') & (df['NAMESAP'].apply(lambda x: re.split(",T|, T",x)[0][-1].isdigit())),
                             np.where(df['NAMESAP'].str.contains('S2,'),
                                      df['LIN'], 'IND'),
                             df['LIN'])
    assert df.duplicated().sum() == 0, "Duplicados en ZDM_MAT_ART"
    return df.reset_index(drop=True)

# =============================================================================
# BD_CRONOGRAMA
# =============================================================================
def bd_cronograma():
    df = pd.read_csv(root_main+"BD_CRONOGRAMA.TXT", sep='\t', encoding='cp1252', skiprows=0, dtype='str')
    df['FECHA'] = pd.to_datetime(df['FECHA'], format='%d/%m/%Y')
    return df.reset_index(drop=True)
    
# =============================================================================
# ZDM_PP_HRUTAS
# =============================================================================
def zdm_pp_hrutas():
    df = pd.read_csv(root_main+"ZDM_PP_HRUTAS.txt", sep='\t', encoding='cp1252', skiprows=6, dtype='str')
    df = df[~df.Material.isna()]
    #PI
    df_pi = df.drop(['Unnamed: 0','Texto breve de material','Op.','ValPref','Un.','Cantidad base','Unnamed: 8', 'UM'], axis=1).copy()
    df_pi = df_pi[(df_pi.Material.str[0] == '1') | (df_pi.Material.str[0] == '2')]
    df_pi['Texto breve operación'] = df_pi['Texto breve operación'].str.replace(" ", "").str.replace("CORTE", "")
    df_pi = df_pi[~((df_pi['Texto breve operación'].str.contains("CNF|CONF|INT|EMP|SESG|TELA|MAQ|MIX")) |
              (df_pi['Texto breve operación'].isna()) |
              (df_pi['Texto breve operación'] == ''))]
    df_pi = df_pi.drop(['Texto breve operación'], axis=1)
    df_pi['OPER'] = "P.I."
    df_pi = df_pi.rename(columns = {'Material':'REF'})
    df_pi = df_pi.drop_duplicates()
    global df_zdm_pp_hrutas
    df_zdm_pp_hrutas = df_pi.copy()

    #ESTAMPADO & SUBLIMADO
    df_es = df[['Material','Texto breve operación']].copy()
    df_es = df_es.rename(columns={'Material':'REFERENCIA'})
    df_es = df_es.drop_duplicates().reset_index(drop=True).dropna()
    df_es = df_es[(df_es['Texto breve operación'].str.startswith('EST')) | (df_es['Texto breve operación'].str.startswith('SUB'))]
    df_es['OPERACION'] = np.where(df_es['Texto breve operación'].str.endswith('MADO'),
                                  'SUB',
                                  'EST')
    df_es = df_es.drop('Texto breve operación', axis=1).drop_duplicates().reset_index(drop=True)
    df_es = df_es.sort_values('OPERACION', ascending=True)
    df_es = df_es.drop_duplicates('REFERENCIA').reset_index(drop=True)
    global df_est_sub
    df_est_sub = df_es.copy()
    
    #TIEMPOS DE CONFECCION Y EMPAQUE
    df_ce = df[['Material','Texto breve operación','ValPref','Cantidad base']].copy().drop_duplicates().reset_index(drop=True)
    df_ce = df_ce.dropna(subset=['Texto breve operación']).reset_index(drop=True)
    df_ce = df_ce[df_ce['Texto breve operación'].str.contains('CNF|CONF|EMPA')]
    df_ce[['ValPref','Cantidad base']] =df_ce[['ValPref','Cantidad base']].fillna('0').applymap(lambda x: x.strip().replace(',','')).astype('float64') 
    df_ce['SAM'] = df_ce['ValPref'] / df_ce['Cantidad base']
    df_ce = df_ce.drop(['ValPref','Cantidad base'], axis=1).reset_index(drop=True)
    df_ce = df_ce.groupby('Material', as_index=False)['SAM'].sum()
    df_ce = df_ce.rename(columns={'Material':'REFERENCIA'})
    global df_tiempos_confeccion
    df_tiempos_confeccion = df_ce.copy()
    
    return df.reset_index(drop=True)

# =============================================================================
# CONSULTA
# =============================================================================
def consulta():
    # df = pd.read_excel(root_project + '/inputs_mdvp.xlsx', sheet_name='CONSULTA', dtype='str')
    df = pd.read_excel(root_main + 'inputs_mdvp.xlsx', sheet_name='CONSULTA', dtype='str')
    df = df.sort_values(['TEMP','PRODUCTO'], ascending=[True, True])
    df = df.reset_index(drop=True).reset_index(drop=False).rename(columns={'index':'INDICE'})
    df['INDICE'] = df['INDICE'] + 1
    df['OPREV'] = df['INDICE'] + 9000000
    df['FPROV'] = date_now
    df = df.rename(columns={'AÑO':'ANO','CODIGO':'CODISAP','PRODUCTO':'NAMESAP','CANTIDAD':'CANT_REAL'})
    global df_zdm_mat_art
    df_zdm_mat_art = zdm_mat_art()
    df_zdm_mat_art_aux = df_zdm_mat_art[['CODISAP', 'LINPROD']].drop_duplicates().reset_index(drop=True)
    df_zdm_mat_art_aux = df_zdm_mat_art_aux.rename(columns={'LINPROD':'LIN'})
    df = df.merge(df_zdm_mat_art_aux, on='CODISAP', how='left')
    df['CLAVEDATE'] = np.where(df['TEMA'] == 'COL',
                               df['TEMA'].astype(str) + df['ANO'].astype(str) + df['TEMP'].astype(str) + 'FACT',
                               df['TEMA'].astype(str) + df['ANO'].astype(str) + df['TEMP'].astype(str) + 'AER')
    global df_bd_cronograma
    df_bd_cronograma = bd_cronograma()
    df_bd_cronograma_aux = df_bd_cronograma.copy()
    df_bd_cronograma_aux = df_bd_cronograma_aux[['CLAVE', 'FECHA']].drop_duplicates().reset_index(drop=True)
    df_bd_cronograma_aux = df_bd_cronograma_aux.rename(columns={'CLAVE':'CLAVEDATE', 'FECHA':'FECHAPAIS'})
    df = df.merge(df_bd_cronograma_aux, on='CLAVEDATE', how='left')
    df['TIPO'] = 'REA'
    df['PLEAD'] = np.where((date_now - df['FPROV']) < pd.Timedelta(days=5),
                           0,
                           np.where((date_now - df['FPROV']) < pd.Timedelta(days=10),
                                    2,
                                    np.where((date_now - df['FPROV']) < pd.Timedelta(days=20),
                                             3,
                                             np.where((date_now - df['FPROV']) < pd.Timedelta(days=30),
                                                      4,
                                                      5))))
    df['REF'] = df['CODISAP'].str[:7]
    df_zdm_pp_hrutas_aux = df_zdm_pp_hrutas.copy()
    df_zdm_pp_hrutas_aux = df_zdm_pp_hrutas.copy()
    df = df.merge(df_zdm_pp_hrutas_aux, on='REF', how='left')
    df['PDUE'] = np.where(df['LIN'] == 'IND',
                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                  parametros_dict['df_lead_due_ind'].loc[10][0],
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=20),
                                           parametros_dict['df_lead_due_ind'].loc[20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=30),
                                                    parametros_dict['df_lead_due_ind'].loc[30][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=40),
                                                             parametros_dict['df_lead_due_ind'].loc[40][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=80),
                                                                      parametros_dict['df_lead_due_ind'].loc[80][0],
                                                                      parametros_dict['df_lead_due_ind'].loc[100][0]))))),
                         np.where(((df['OPER'] == 'P.I.') | (df['NAMESAP'].str[:4] == 'POLO')),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                           parametros_dict['df_lead_due_proc'].loc[0][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                    parametros_dict['df_lead_due_proc'].loc[10][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=14),
                                                             parametros_dict['df_lead_due_proc'].loc[14][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=21),
                                                                      parametros_dict['df_lead_due_proc'].loc[21][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_proc'].loc[39][0],
                                                                               parametros_dict['df_lead_due_proc'].loc[100][0]))))),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=-20),
                                           parametros_dict['df_lead_due_max'].loc[-20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                                    parametros_dict['df_lead_due_max'].loc[0][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                             parametros_dict['df_lead_due_max'].loc[10][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=19),
                                                                      parametros_dict['df_lead_due_max'].loc[19][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_max'].loc[39][0],
                                                                               parametros_dict['df_lead_due_max'].loc[100][0])))))))
    df['TPMT'] = df['CODISAP'].str[:2]
    df_ppais_aux = parametros_dict['df_ppais']
    df_ptipo_aux = parametros_dict['df_ptipo']
    df = df.reset_index(drop=True)
    df.insert(loc=len(df.columns), column = 'PPAIS_PUNTAJE', value = df.merge(df_ppais_aux, left_on='TEMA', right_on='PAIS', how='left')['PUNTAJE'].tolist())
    df.insert(loc=len(df.columns), column = 'PTIPO_PUNTAJE', value = df.merge(df_ptipo_aux, on='TIPO', how='left')['PUNTAJE'].tolist())
    df['PUNTAJE'] = df['PLEAD'].astype('float64') + df['PDUE'].astype('float64') + df['PPAIS_PUNTAJE'].astype('float64') + df['PTIPO_PUNTAJE'].astype('float64')
    df['FCOMP'] = df['FECHAPAIS']
    df['CANT_REAL'] = df['CANT_REAL'].astype('float64')
    df = df.sort_values(['PUNTAJE','FCOMP','REF','CANT_REAL'], ascending=[False, True, True, True])
    df = df.drop(['INDICE','CLAVEDATE'], axis=1)
    df = df.loc[:,["TEMA", "ANO", "TEMP", "OPREV", "CODISAP", "NAMESAP", "LIN", "CANT_REAL", "FPROV", "FCOMP", "FECHAPAIS", "TIPO", "PLEAD", "REF", "OPER", "PDUE", "PPAIS_PUNTAJE", "PTIPO_PUNTAJE", "PUNTAJE"]]
    df = df.reset_index(drop=True)
    global df_consulta
    df_consulta = df.copy()
    return df

# =============================================================================
# CARGUE_DOC
# =============================================================================

def cargue():
    # df = pd.read_excel(root_project + '/inputs_mdvp.xlsx', sheet_name='CONSULTA', dtype='str')
    df = pd.read_excel(root_main + 'inputs_mdvp.xlsx', sheet_name='CARGUE_DOC', dtype='str')
    df = df.sort_values(['TEMP','PRODUCTO'], ascending=[True, True])
    df = df.reset_index(drop=True).reset_index(drop=False).rename(columns={'index':'INDICE'})
    df['INDICE'] = df['INDICE'] + 1
    df['OPREV'] = df['INDICE'] + 9001000
    df['FPROV'] = date_now
    df = df.rename(columns={'AÑO':'ANO','CODIGO':'CODISAP','PRODUCTO':'NAMESAP','CANTIDAD':'CANT_REAL'})
    global df_zdm_mat_art
    df_zdm_mat_art = zdm_mat_art()
    df_zdm_mat_art_aux = df_zdm_mat_art[['CODISAP', 'LINPROD']].drop_duplicates().reset_index(drop=True)
    df_zdm_mat_art_aux = df_zdm_mat_art_aux.rename(columns={'LINPROD':'LIN'})
    df = df.merge(df_zdm_mat_art_aux, on='CODISAP', how='left')    
    df['IDC'] = (df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+df['CODISAP'].astype(str))
    df = df.reset_index(drop=True)
    #Insertar ZPP_LEADER_LIST_ADV
    df_zpp_leader_list_adv = zpp_leader_list_adv()
    df = df.merge(df_zpp_leader_list_adv[['IDC','COLECCION']].reset_index(drop=True), on='IDC', how='left')
    df = df.rename(columns = {'COLECCION':'COLE'})
    df['COLE'] = df['COLE'].fillna('NAL')
    df['CLAVEDATEDPR'] = (df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'DOC C.A.2')
    #Insertar BD_CRONOGRAMA
    df_bd_cronograma_aux = df_bd_cronograma[['CLAVE','FECHA']]
    df_bd_cronograma_dpr = df_bd_cronograma_aux.rename(columns = {'CLAVE':'CLAVEDATEDPR','FECHA':'FECHADPR2'}).reset_index(drop=True)
    df_bd_cronograma_pais = df_bd_cronograma_aux.rename(columns = {'CLAVE':'CLAVEDATE','FECHA':'FECHAPAIS'}).reset_index(drop=True)
    df = df.reset_index(drop=True).merge(df_bd_cronograma_dpr, on='CLAVEDATEDPR', how='left')
    df['CLAVEDATE'] = np.where(df['TEMA'] == 'DPR',
                                np.where(df['FECHADPR2'].isna() | df['FPROV'].isna(),
                                         "error",
                                         np.where(df['FECHADPR2'] <= df['FPROV'],
                                                  "DPR"+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                  "DPR"+df['ANO'].astype(str)+df['TEMP'].astype(str)+'CIERRE PREV')),
                                                  np.where(df['CODISAP'].str[:2].isin(['15', '25']),
                                                           df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                           np.where(df['TEMA'] == 'COL',
                                                                    np.where(df['COLE'] == 'ADV',
                                                                             df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'ADV',
                                                                             df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR'),
                                                                             np.where(df['TEMA'] == 'PER',
                                                                                      np.where(df['COLE'] == 'ADV',
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'AER'),
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'AER'))))
    df = df.reset_index(drop=True).merge(df_bd_cronograma_pais, on='CLAVEDATE', how='left')
    df = df.drop(['CLAVEDATE','CLAVEDATEDPR','FECHADPR2'], axis=1)
    df['FCOMP'] = df['FECHAPAIS']    
    df['TIPO'] = np.where(df['TEMA'].isna() | df['COLE'].isna() | df['FCOMP'].isna() | df['FECHAPAIS'].isna(),
                          "error",
                          np.where(df['TEMA'] == 'DPR',
                                   'NAL',
                                   np.where((df['COLE'] == 'ADV') | (df['COLE'] == 'ADV-NAL'),
                                            np.where(df['TEMA'] == 'PER',
                                                     'PER-ADV',
                                                     np.where(df['COLE'] == 'ADV',
                                                     'ADV',
                                                      df['COLE'])),
                                            np.where((df['TEMA'] == 'PER') & (df['CODISAP'].str[:2].isin(['15', '25'])),
                                                              df['COLE'],
                                                     np.where((df['TEMA'] == 'ECU') | (df['TEMA'] == 'GUA') | (df['TEMA'] == 'BOL') | (df['TEMA'] == 'PER'),
                                                     'REA',
                                                     'NAL')))))
    df['PLEAD'] = np.where((date_now - df['FPROV']) < pd.Timedelta(days=5),
                           0,
                           np.where((date_now - df['FPROV']) < pd.Timedelta(days=10),
                                    2,
                                    np.where((date_now - df['FPROV']) < pd.Timedelta(days=20),
                                             3,
                                             np.where((date_now - df['FPROV']) < pd.Timedelta(days=30),
                                                      4,
                                                      5))))
    df['REF'] = df['CODISAP'].str[:7]
    df_zdm_pp_hrutas_aux = df_zdm_pp_hrutas.copy()
    df = df.merge(df_zdm_pp_hrutas_aux, on='REF', how='left')
    
    df['PDUE'] = np.where(df['LIN'] == 'IND',
                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                  parametros_dict['df_lead_due_ind'].loc[10][0],
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=20),
                                           parametros_dict['df_lead_due_ind'].loc[20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=30),
                                                    parametros_dict['df_lead_due_ind'].loc[30][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=40),
                                                             parametros_dict['df_lead_due_ind'].loc[40][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=80),
                                                                      parametros_dict['df_lead_due_ind'].loc[80][0],
                                                                      parametros_dict['df_lead_due_ind'].loc[100][0]))))),
                         np.where(((df['OPER'] == 'P.I.') | (df['NAMESAP'].str[:4] == 'POLO')),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                           parametros_dict['df_lead_due_proc'].loc[0][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                    parametros_dict['df_lead_due_proc'].loc[10][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=14),
                                                             parametros_dict['df_lead_due_proc'].loc[14][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=21),
                                                                      parametros_dict['df_lead_due_proc'].loc[21][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_proc'].loc[39][0],
                                                                               parametros_dict['df_lead_due_proc'].loc[100][0]))))),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=-20),
                                           parametros_dict['df_lead_due_max'].loc[-20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                                    parametros_dict['df_lead_due_max'].loc[0][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                             parametros_dict['df_lead_due_max'].loc[10][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=19),
                                                                      parametros_dict['df_lead_due_max'].loc[19][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_max'].loc[39][0],
                                                                               parametros_dict['df_lead_due_max'].loc[100][0])))))))    
    df_ppais_aux = parametros_dict['df_ppais']
    df_ptipo_aux = parametros_dict['df_ptipo']
    df = df.reset_index(drop=True)
    df.insert(loc=len(df.columns), column = 'PPAIS_PUNTAJE', value = df.merge(df_ppais_aux, left_on='TEMA', right_on='PAIS', how='left')['PUNTAJE'].tolist())
    df.insert(loc=len(df.columns), column = 'PTIPO_PUNTAJE', value = df.merge(df_ptipo_aux, on='TIPO', how='left')['PUNTAJE'].tolist())
    df['PUNTAJE'] = df['PLEAD'].astype('float64') + df['PDUE'].astype('float64') + df['PPAIS_PUNTAJE'].astype('float64') + df['PTIPO_PUNTAJE'].astype('float64')    
    df['CANT_REAL'] = df['CANT_REAL'].astype('float64')
    df = df.sort_values(['PUNTAJE','FCOMP','REF','CANT_REAL'], ascending=[False, True, True, True])
    df = df.loc[:,["TEMA", "ANO", "TEMP", "OPREV", "CODISAP", "NAMESAP", "LIN", "CANT_REAL", "FPROV", "FCOMP", "FECHAPAIS", "TIPO", "PLEAD", "REF", "OPER", "PDUE", "PPAIS_PUNTAJE", "PTIPO_PUNTAJE", "PUNTAJE"]]
    df = df.reset_index(drop=True)        
    global df_cargue
    df_cargue = df.copy()
    return df

# =============================================================================
# ZPP_SUSTITUTOS
# -Verificar orden para creación de sustitutos en ZDM_PP_COMP
# =============================================================================
def zpp_sustitutos():
        df = pd.read_csv(root_main+"ZPP_SUSTITUTOS.TXT", sep='\t', encoding='cp1252', skiprows=6, dtype='str')
        df = df.loc[:,['TpMt','Material_origen','Material_reempl','NºMate_anti_origen','NºMate_anti_reempl']]
        df = df[~df['Material_origen'].isna()]
        df = df[df['Material_origen'] != '2017642001']
        df = df[df['Material_origen'] != '2017657001']
        df = df[df['Material_origen'] != '2017658001']
        df = df[df['Material_reempl'] != '2017642001']
        df = df.loc[:,['Material_origen','Material_reempl']]
        df = df.sort_values(['Material_origen'], ascending=False)
        df = df.reset_index(drop=True)
        global df_zpp_sustitutos
        df_zpp_sustitutos = df.copy()
        return df
    
# =============================================================================
# ZPP_SUSTITUTOS_NUMERAL
# =============================================================================
def zpp_sustitutos_numeral():
    df = df_zpp_sustitutos.copy()
    df.insert(loc=2, column='NUMERAL', value=(df.groupby('Material_origen').cumcount() + 1).tolist())
    df['NUMERAL'] = 'SUSTITUTO ' + df['NUMERAL'].astype(str)
    df = df[df['Material_origen'].str.startswith('6') | df['Material_origen'].str.startswith('8')]
    df = df.pivot(index='Material_origen', columns='NUMERAL', values='Material_reempl').reset_index()
    df = df.rename_axis(None, axis=1)
    # global df_zpp_sustitutos_numeral
    # df_zpp_sustitutos_numeral = df.copy()
    global df_zpp_sustitutos_numeral
    df_zpp_sustitutos_numeral = df
    return df
    
# =============================================================================
# ZDM_PP_COMP
# =============================================================================
def zdm_pp_comp():
    df = pd.read_csv(root_main+"ZDM_PP_COMP.TXT", sep='\t', encoding='cp1252', skiprows=6, dtype='str')
    df = df.drop_duplicates()
    df = df[~df['Ord.prev.'].isna()]
    df = df.loc[:,['Causante','Ord.prev.','Material','Texto breve de material','Ctd.necesaria','Ctd.orden']]
    df[['Ctd.necesaria','Ctd.orden']] = df[['Ctd.necesaria','Ctd.orden']].applymap(lambda x: x.strip().replace(",",""))
    df[['Ctd.necesaria','Ctd.orden']] = df[['Ctd.necesaria','Ctd.orden']].astype(float)
    df['CONSUMO'] = df['Ctd.necesaria'] / df['Ctd.orden']
    df = df.rename(columns = {'Material':'CODISAPCOMP',
                              'Texto breve de material':'NAMESAPCOMP',
                              'Ord.prev.':'OPREV',
                              'Causante':'CODISAP'})
    names_to_drop = ['CAJA','BOLSA','MARQUILLA','ETIQUETA','ALMA','STICKER TRANSF TERMIC','CINTA NYLON 8.5CM','CINTA RESINA 9CM','PAPEL','CINTA NYLON DP661T']
    for i in names_to_drop:
        df = df[~df.NAMESAPCOMP.str.startswith(i)]
    
    df_zpp_sustitutos_numeral_aux = df_zpp_sustitutos_numeral.copy().rename(columns={'Material_origen':'CODISAPCOMP'})
    df = df.reset_index(drop=True).merge(df_zpp_sustitutos_numeral_aux, on='CODISAPCOMP', how='left')
    df['OPREV'] = df['OPREV'].astype('int64')
    df = df.sort_values('OPREV')
    global df_zdm_pp_comp
    df_zdm_pp_comp = df
    return df
        
# =============================================================================
# ZREPO_LIST_MATE_FULL
# =============================================================================
def zrepo_list_mate_full():
    df = pd.read_csv(root_main+"ZREPO_LIST_MATE_FULL.TXT", sep='\t', encoding='cp1252', skiprows=6, dtype='str')
    df = df[~(df.Material.isna()) & (df.Material != '')]
    df.columns = [i.strip() for i in df.columns]
    df = df.loc[:,['Material','Texto breve de material','Componente','LMatAl','Cantidad','Cantidad base']]
    df = df.rename(columns={"Material":"CODISAP",
                            "Componente":"CODISAPCOMP",
                            'Texto breve de material':'NAMESAPCOMP'})
    df[['Cantidad', 'Cantidad base']] = df[['Cantidad', 'Cantidad base']].applymap(lambda x: x.strip().replace(",",""))
    df[['Cantidad', 'Cantidad base']] = df[['Cantidad', 'Cantidad base']].astype(float)
    df['CONSUMO'] = df['Cantidad'] / df['Cantidad base']
    df = df.drop(['Cantidad', 'Cantidad base'], axis=1).drop_duplicates().reset_index(drop=True)
    names_to_drop = ['CAJA','BOLSA','MARQUILLA','ETIQUETA','ALMA','STICKER TRANSF TERMIC','CINTA NYLON 8.5CM','CINTA RESINA 9CM','PAPEL','CINTA NYLON DP661T']
    df.NAMESAPCOMP = df.NAMESAPCOMP.str.strip()
    for i in names_to_drop:
        df = df[~df['NAMESAPCOMP'].str.startswith(i)]
    
    df_zpp_sustitutos_numeral_aux = df_zpp_sustitutos_numeral.rename(columns={'Material_origen':'CODISAPCOMP'})
    df = df.reset_index(drop=True).merge(df_zpp_sustitutos_numeral_aux, on='CODISAPCOMP', how='left')
    df = df.sort_values('CODISAP').reset_index(drop=True)
    df = df.drop('LMatAl', axis=1)
    global df_zrepo_list_mate_full
    df_zrepo_list_mate_full = df
    return df
    

# =============================================================================
# ZMD04 - STOCK DIS
# =============================================================================
def zmd04():
    df = pd.read_csv(root_main+"ZMD04.TXT", sep='\t', encoding='cp1252', skiprows=3, dtype='str')
    df = df.drop('   Disponible', axis=1)
    df.columns = df.columns.str.strip()
    df = df.loc[:,['Material', 'Texto breve material', 'Unidad', 'Disponible', 'Res.O.Prod', 'Alm. 2001', 'Alm. 2008', 'Lb.util.real']]
    df = df[~(df.Material.isna()) & (df.Material != '')]
    df.columns = [i.strip() for i in df.columns]
    df = df.applymap(lambda x: x.strip())
    cols = ["Res.O.Prod","Lb.util.real","Disponible","Alm. 2001","Alm. 2008"]
    df[cols] = df[cols].applymap(lambda x: x.replace(",",""))
    df[cols] = df[cols].astype(float)
    df['Lb.util.Real'] = np.maximum(df['Disponible'] - df['Res.O.Prod'] - df['Alm. 2001'] - df['Alm. 2008'], 0)
    df['DIF_REVISAR_MD04'] = df['Lb.util.real'] - df['Lb.util.Real']
    global df_stockdis
    df_stockdis = df.copy()
    return df.reset_index(drop=True)
    
# =============================================================================
# Generar COOIS_CABE_PREV
# =============================================================================
def coois_cabe_prev():
    df = pd.read_csv(root_main+"COOIS_CABE_PREV.TXT", sep='\t', encoding='cp1252', skiprows=1, dtype='str')
    df = df.rename(columns = {"Tema":"TEMA",
                              "Año temp.":"ANO",
                              "Temporada":"TEMP",
                              "Ctd.teór.":"CANT_REAL",
                              "Material":"CODISAP",
                              "Orden":"OPREV",
                              "Texto breve material":"NAMESAP"})
    df = df[['TEMA','ANO','TEMP','OPREV','CODISAP','NAMESAP','CANT_REAL']]
    #Insertar ZDM_MAT_ART
    df_zdm_mat_art_aux = df_zdm_mat_art.copy()
    df_zdm_mat_art_aux = df_zdm_mat_art_aux[['CODISAP','LINPROD']].reset_index(drop=True)
    df = df.reset_index(drop=True).merge(df_zdm_mat_art_aux, on='CODISAP', how='left')
    df = df.rename(columns = {'LINPROD':'LIN'})
    df['CANT_REAL'] = df['CANT_REAL'].apply(lambda x: x.strip().replace(".00","").replace(",","")).astype(int)
    df = df.loc[:,['TEMA','ANO','TEMP','OPREV','CODISAP','NAMESAP','LIN','CANT_REAL']].drop_duplicates()
    df['OPREV'] = df['OPREV'].astype('int64')
    #Insertar ZPP_PLAN_OPREV_2020
    df_zpp_plan_oprev = zpp_plan_oprev()[['FPROV','FCOMP','COLE','OPREV']].reset_index(drop=True)
    df = df.reset_index(drop=True).merge(df_zpp_plan_oprev, on='OPREV', how='left')
    df['COLE'] = df['COLE'].fillna('NAL')
    df['CLAVEDATEDPR'] = (df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'DOC C.A.2')
    #Insertar BD_CRONOGRAMA
    df_bd_cronograma_aux = df_bd_cronograma[['CLAVE','FECHA']]
    df_bd_cronograma_dpr = df_bd_cronograma_aux.rename(columns = {'CLAVE':'CLAVEDATEDPR','FECHA':'FECHADPR2'}).reset_index(drop=True)
    df_bd_cronograma_pais = df_bd_cronograma_aux.rename(columns = {'CLAVE':'CLAVEDATE','FECHA':'FECHAPAIS'}).reset_index(drop=True)
    df = df.reset_index(drop=True).merge(df_bd_cronograma_dpr, on='CLAVEDATEDPR', how='left')
    df['CLAVEDATE'] = np.where(df['TEMA'] == 'DPR',
                                np.where(df['FECHADPR2'].isna() | df['FPROV'].isna(),
                                         "error",
                                         np.where(df['FECHADPR2'] <= df['FPROV'],
                                                  "DPR"+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                  "DPR"+df['ANO'].astype(str)+df['TEMP'].astype(str)+'CIERRE PREV')),
                                                  np.where(df['CODISAP'].str[:2].isin(['15', '25']),
                                                           df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                           np.where(df['TEMA'] == 'COL',
                                                                    np.where(df['COLE'] == 'ADV',
                                                                             df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'ADV',
                                                                             df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR'),
                                                                             np.where(df['TEMA'] == 'PER',
                                                                                      np.where(df['COLE'] == 'ADV',
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'MAR',
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'AER'),
                                                                                               df['TEMA'].astype(str)+df['ANO'].astype(str)+df['TEMP'].astype(str)+'AER'))))
    df = df.reset_index(drop=True).merge(df_bd_cronograma_pais, on='CLAVEDATE', how='left')
    df = df.drop(['CLAVEDATE','CLAVEDATEDPR','FECHADPR2'], axis=1)
    df['TIPO'] = np.where(df['TEMA'].isna() | df['COLE'].isna() | df['FCOMP'].isna() | df['FECHAPAIS'].isna(),
                          "error",
                          np.where(df['TEMA'] == 'DPR',
                                   'NAL',
                                   np.where((df['COLE'] == 'ADV') | (df['COLE'] == 'ADV-NAL'),
                                            np.where(df['TEMA'] == 'PER',
                                                     'PER-ADV',
                                                     np.where(df['COLE'] == 'ADV',
                                                     'ADV',
                                                     np.where(df['FCOMP'] > df['FECHAPAIS'],
                                                              'REA',
                                                              df['COLE']))),
                                            np.where((df['TEMA'] == 'PER') & (df['CODISAP'].str[:2].isin(['15', '25'])),
                                                     np.where(df['FCOMP'] > df['FECHAPAIS'],
                                                              'REA',
                                                              df['COLE']),
                                                     np.where((df['TEMA'] == 'ECU') | (df['TEMA'] == 'GUA') | (df['TEMA'] == 'BOL') | (df['TEMA'] == 'PER'),
                                                     'REA',
                                                     np.where(df['FCOMP'] > df['FECHAPAIS'],
                                                              'REA',
                                                              'NAL'))))))
    
    # df['PLEAD'] = np.where((date_now - df['FPROV']) < pd.Timedelta(days=5+1),
    #                        0,
    #                        np.where((date_now - df['FPROV']) < pd.Timedelta(days=10+1),
    #                                 2,
    #                                 np.where((date_now - df['FPROV']) < pd.Timedelta(days=20+1),
    #                                          3,
    #                                          np.where((date_now - df['FPROV']) < pd.Timedelta(days=30+1),
    #                                                   4,
    #                                                   5))))
    df['PLEAD'] = np.where((date_now - df['FPROV']) < pd.Timedelta(days=5),
                           0,
                           np.where((date_now - df['FPROV']) < pd.Timedelta(days=10),
                                    2,
                                    np.where((date_now - df['FPROV']) < pd.Timedelta(days=20),
                                             3,
                                             np.where((date_now - df['FPROV']) < pd.Timedelta(days=30),
                                                      4,
                                                      5))))
    df['REF'] = df['CODISAP'].str[:7]
    #Insertar ZDM_PP_HRUTAS
    df_zdm_pp_hrutas_aux = df_zdm_pp_hrutas.copy()
    df = df.reset_index(drop=True).merge(df_zdm_pp_hrutas_aux, on='REF', how='left')
    # df['PDUE'] = np.where(df['LIN'] == 'IND',
    #                       np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10+1),
    #                                20,
    #                                np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=20+1),
    #                                         16,
    #                                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=30+1),
    #                                                  12,
    #                                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=40+1),
    #                                                           8,
    #                                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=80+1),
    #                                                                    4,
    #                                                                    0))))),
    #                       np.where(((df['OPER'] == 'P.I.') | (df['NAMESAP'].str[:4] == 'POLO')),
    #                                np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0+1),
    #                                         20,
    #                                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10+1),
    #                                                  16,
    #                                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=14+1),
    #                                                           12,
    #                                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=21+1),
    #                                                                    8,
    #                                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39+1),
    #                                                                             4,
    #                                                                             0))))),
    #                                np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=-20+1),
    #                                         20,
    #                                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0+1),
    #                                                  16,
    #                                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10+1),
    #                                                           12,
    #                                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=19+1),
    #                                                                    8,
    #                                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39+1),
    #                                                                             4,
    #                                                                             0)))))))
    df['PDUE'] = np.where(df['LIN'] == 'IND',
                         np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                  parametros_dict['df_lead_due_ind'].loc[10][0],
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=20),
                                           parametros_dict['df_lead_due_ind'].loc[20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=30),
                                                    parametros_dict['df_lead_due_ind'].loc[30][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=40),
                                                             parametros_dict['df_lead_due_ind'].loc[40][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=80),
                                                                      parametros_dict['df_lead_due_ind'].loc[80][0],
                                                                      parametros_dict['df_lead_due_ind'].loc[100][0]))))),
                         np.where(((df['OPER'] == 'P.I.') | (df['NAMESAP'].str[:4] == 'POLO')),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                           parametros_dict['df_lead_due_proc'].loc[0][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                    parametros_dict['df_lead_due_proc'].loc[10][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=14),
                                                             parametros_dict['df_lead_due_proc'].loc[14][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=21),
                                                                      parametros_dict['df_lead_due_proc'].loc[21][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_proc'].loc[39][0],
                                                                               parametros_dict['df_lead_due_proc'].loc[100][0]))))),
                                  np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=-20),
                                           parametros_dict['df_lead_due_max'].loc[-20][0],
                                           np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=0),
                                                    parametros_dict['df_lead_due_max'].loc[0][0],
                                                    np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=10),
                                                             parametros_dict['df_lead_due_max'].loc[10][0],
                                                             np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=19),
                                                                      parametros_dict['df_lead_due_max'].loc[19][0],
                                                                      np.where((df['FECHAPAIS'] - date_now) <= pd.Timedelta(days=39),
                                                                               parametros_dict['df_lead_due_max'].loc[39][0],
                                                                               parametros_dict['df_lead_due_max'].loc[100][0])))))))
    df['TPMT'] = df['CODISAP'].str[:2]
    #Insertar PPAIS
    df_ppais_aux = parametros_dict['df_ppais']
    df_ptipo_aux = parametros_dict['df_ptipo']
    df_ptpmt_aux = parametros_dict['df_ptpmt']
    df = df.reset_index(drop=True)
    df.insert(loc=len(df.columns), column = 'PPAIS_PUNTAJE', value = df.merge(df_ppais_aux, left_on='TEMA', right_on='PAIS', how='left')['PUNTAJE'].tolist())
    df.insert(loc=len(df.columns), column = 'PTIPO_PUNTAJE', value = df.merge(df_ptipo_aux, on='TIPO', how='left')['PUNTAJE'].tolist())
    df.insert(loc=len(df.columns), column = 'PTPMT_PUNTAJE', value = df.merge(df_ptpmt_aux, on='TPMT', how='left')['PUNTAJE'].tolist())
    df[['PLEAD','PDUE','PPAIS_PUNTAJE','PTIPO_PUNTAJE','PTPMT_PUNTAJE']] = df[['PLEAD','PDUE','PPAIS_PUNTAJE','PTIPO_PUNTAJE','PTPMT_PUNTAJE']].astype('float64').fillna(0)
    df['PUNTAJE'] = df['PLEAD'] + df['PDUE'] + df['PPAIS_PUNTAJE'] + df['PTIPO_PUNTAJE'] + df['PTPMT_PUNTAJE']
    df = df.drop(['PTPMT_PUNTAJE', 'TPMT'], axis=1)
    df = df.sort_values(['PUNTAJE','FCOMP','REF','CANT_REAL'], ascending=[False,True,True,True]).reset_index(drop=True)
    global df_coois_cabe_prev
    df_coois_cabe_prev = df.copy()
    return df

# =============================================================================
# Generar COOIS_CABE_PREV_MATE
# =============================================================================
def coois_cabe_prev_mate(): 
    df = df_coois_cabe_prev.copy()
    df_consulta_aux = df_consulta.copy()
    df_cargue_aux = df_cargue.copy()    
    df = df.append(df_consulta_aux)
    df = df.append(df_cargue_aux)    
    df = df.sort_values(['PUNTAJE','FCOMP','REF','CANT_REAL'], ascending=[False,True,True,True]).reset_index(drop=True)
    df = df.reset_index(drop=False).rename(columns={'index':'PRIORIDAD'})
    df['PRIORIDAD'] = df['PRIORIDAD'] + 1
    df = df.loc[:,['OPREV','CODISAP','CANT_REAL','OPER','PRIORIDAD']]
    #Insertar ZDM_PP_COMP
    df_zdm_pp_comp_aux = df_zdm_pp_comp.copy()
    df_zdm_pp_comp_aux = df_zdm_pp_comp_aux[['OPREV',"CODISAPCOMP", "CONSUMO", "SUSTITUTO 1", "SUSTITUTO 2", "SUSTITUTO 3", "SUSTITUTO 4"]]
    df_zdm_pp_comp_aux.columns = ['OPREV',"CODISAPCOMP1", "CONSUMO1", "SUSTITUTO 11", "SUSTITUTO 21", "SUSTITUTO 31", "SUSTITUTO 41"]
    df = df.reset_index(drop=True).merge(df_zdm_pp_comp_aux, on='OPREV', how='left')
    df['CANT_REAL'] = df['CANT_REAL'].astype('float64')
    # df['CONSUMO1'] = np.round(df['CONSUMO1']*100).fillna(0).astype(int)/100
    # df['CONSUMO1'] = df['CONSUMO1'].astype(int)
    df['NECESIDAD1'] = df['CANT_REAL'] * df['CONSUMO1']
    df['SIN PPCOMP'] = np.where((df['CODISAPCOMP1'] == '') | (df['CODISAPCOMP1'].isna()),
                                df['CODISAP'],
                                'OK')
    #Insertar ZREPO_LIST_MATE_FULL
    df_zrepo_list_mate_full_aux = df_zrepo_list_mate_full.copy()[["CODISAP", "CODISAPCOMP", "CONSUMO", "SUSTITUTO 1", "SUSTITUTO 2", "SUSTITUTO 3", "SUSTITUTO 4"]]
    df_zrepo_list_mate_full_aux.columns = ["CODISAP", "CODISAPCOMP2", "CONSUMO2", "SUSTITUTO 12", "SUSTITUTO 22", "SUSTITUTO 32", "SUSTITUTO 42"]
    df = df.reset_index(drop=True).merge(df_zrepo_list_mate_full_aux, left_on='SIN PPCOMP', right_on='CODISAP', how='left')
    df = df.drop('CODISAP_y', axis=1).rename(columns={'CODISAP_x':'CODISAP'})
    # df['CONSUMO2'] = np.round(df['CONSUMO2']*100).fillna(0).astype(int)/100#np.ceil(df['CONSUMO2']*100)/100
    df['CONSUMO2'] = df['CONSUMO2'].astype('float64')#np.ceil(df['CONSUMO2']*100)/100
    df['NECESIDAD2'] = df['CANT_REAL'] * df['CONSUMO2']
    df['CODISAPCOMP'] = np.where(df['SIN PPCOMP'] != 'OK', df['CODISAPCOMP2'], df['CODISAPCOMP1'])
    df['CONSUMO'] = np.where(df['SIN PPCOMP'] != 'OK', df['CONSUMO2'], df['CONSUMO1'])
    df['SUSTITUTO 1'] = np.where(df['SIN PPCOMP'] != 'OK', df['SUSTITUTO 12'], df['SUSTITUTO 11'])
    df['SUSTITUTO 2'] = np.where(df['SIN PPCOMP'] != 'OK', df['SUSTITUTO 22'], df['SUSTITUTO 21'])
    df['SUSTITUTO 3'] = np.where(df['SIN PPCOMP'] != 'OK', df['SUSTITUTO 32'], df['SUSTITUTO 31'])
    df['SUSTITUTO 4'] = np.where(df['SIN PPCOMP'] != 'OK', df['SUSTITUTO 42'], df['SUSTITUTO 41'])
    df['NECESIDAD'] = np.where(df['SIN PPCOMP'] != 'OK', df['NECESIDAD2'], df['NECESIDAD1'])
    df = df.drop(["CODISAPCOMP1", "CONSUMO1", "SUSTITUTO 11", "SUSTITUTO 21", "SUSTITUTO 31", "SUSTITUTO 41", "NECESIDAD1", "OPER",
                  "CODISAPCOMP2", "CONSUMO2", "SUSTITUTO 12", "SUSTITUTO 22", "SUSTITUTO 32", "SUSTITUTO 42", "NECESIDAD2"], axis=1)
    df = df.sort_values(['PRIORIDAD', 'CODISAPCOMP'], ascending=[True, True])
    df = df[~((df['CODISAPCOMP'] == '') | (df['CODISAPCOMP'].isna()))]
    
    #CALCULOS DE LA HOJA EXCEL
    df['NECESIDAD'] = df['NECESIDAD'].fillna(0)
    #Necesidades
    df_necesidades = df[['PRIORIDAD','CODISAPCOMP','NECESIDAD']].groupby(['PRIORIDAD','CODISAPCOMP'], as_index=False).sum()
    df_necesidades = df_necesidades.reset_index(drop=True)
    df_necesidades['CUMSUM_NECESIDADES'] = df_necesidades.sort_values('PRIORIDAD').groupby('CODISAPCOMP', as_index=False)['NECESIDAD'].cumsum(axis=0)
    df_necesidades['CUMCOUNT_CODISAPCOMP'] = df_necesidades.groupby('CODISAPCOMP').cumcount()+1
    #Merge con lo ya asignado en una prioridad mayor
    df_aux = df_necesidades.copy().drop(['PRIORIDAD','NECESIDAD'], axis=1)
    df_aux = df_aux.rename(columns={'CUMSUM_NECESIDADES':'CUMSUM_NECESIDADES+1'})
    df_aux['CUMCOUNT_CODISAPCOMP'] = df_aux['CUMCOUNT_CODISAPCOMP'] + 1
    df_necesidades = df_necesidades.merge(df_aux, on=['CUMCOUNT_CODISAPCOMP','CODISAPCOMP'], how='left')
    df_necesidades = df_necesidades.rename(columns={'CUMSUM_NECESIDADES+1':'NO_DISP'}).fillna(0).reset_index(drop=True)
    
    #Insertar StockDIS (ZMD04)
    df_stockdis_aux = df_stockdis.copy()
    df_stockdis_aux = df_stockdis_aux[['Material','Lb.util.Real']].rename(columns={'Material':'CODISAPCOMP','Lb.util.Real':'STOCK_DISP'})
    df_stockdis_aux = df_stockdis_aux.groupby('CODISAPCOMP', as_index=False).sum().reset_index(drop=True)
    df.insert(loc=df.columns.shape[0], column='SALDO', value=df.merge(df_stockdis_aux, on='CODISAPCOMP', how='left')['STOCK_DISP'].tolist())
    df.insert(loc=df.columns.shape[0], column='SALDO_S1', value=df.merge(df_stockdis_aux, left_on='SUSTITUTO 1', right_on='CODISAPCOMP', how='left')['STOCK_DISP'].tolist())
    df.insert(loc=df.columns.shape[0], column='SALDO_S2', value=df.merge(df_stockdis_aux, left_on='SUSTITUTO 2', right_on='CODISAPCOMP', how='left')['STOCK_DISP'].tolist())
    df.insert(loc=df.columns.shape[0], column='SALDO_S3', value=df.merge(df_stockdis_aux, left_on='SUSTITUTO 3', right_on='CODISAPCOMP', how='left')['STOCK_DISP'].tolist())
    df.insert(loc=df.columns.shape[0], column='SALDO_S4', value=df.merge(df_stockdis_aux, left_on='SUSTITUTO 4', right_on='CODISAPCOMP', how='left')['STOCK_DISP'].tolist())
    cols = ['SALDO','SALDO_S1','SALDO_S2','SALDO_S3','SALDO_S4']
    df[cols] = df[cols].fillna(0)
    df['SALDO_TOTAL'] = df['SALDO'] + df['SALDO_S1'] + df['SALDO_S2'] + df['SALDO_S3'] + df['SALDO_S4']
    df = df.drop(cols, axis=1)
    #Necesidades
    df = df.merge(df_necesidades[['PRIORIDAD','CODISAPCOMP','NO_DISP']], on=['PRIORIDAD','CODISAPCOMP'], how='left')
    df['NO_DISP'] = df['NO_DISP'].fillna(0)
    df['SALDO_DISP'] = np.maximum(df['SALDO_TOTAL'] - df['NO_DISP'], 0)
    df['CANT_ASIG'] = np.minimum(df['NECESIDAD'], df['SALDO_DISP'])
    df['SALDO_SOBR'] = df['SALDO_DISP'] - df['CANT_ASIG']
    df = df.reset_index(drop=True)
    global df_coois_cabe_prev_mate
    df_coois_cabe_prev_mate = df.copy()
    return df

# =============================================================================
# CONSULTAYCOOIS
# =============================================================================
def consultaycoois():
    df = df_coois_cabe_prev.copy()
    df_consulta_aux = df_consulta.copy()
    df_cargue_aux = df_cargue.copy()
    df = df.append(df_consulta_aux)
    df = df.append(df_cargue_aux)
    df = df.sort_values(['PUNTAJE','FCOMP','REF','CANT_REAL'], ascending=[False,True,True,True]).reset_index(drop=True).reset_index(drop=False)
    df = df.rename(columns={'index':'PRIORIDAD'})
    df['PRIORIDAD'] = df['PRIORIDAD']+1
    global df_consultaycoois
    df_consultaycoois = df.copy()
    return df

# =============================================================================
# RESULTADO_PREV
# =============================================================================
def resultado_prev():
    df = df_coois_cabe_prev_mate.copy()
    #CONSULTAYCOOIS
    df_consultaycoois_aux = df_consultaycoois.copy()
    df_consultaycoois_aux = df_consultaycoois_aux[['TEMA','ANO','TEMP','OPREV','NAMESAP','LIN','TIPO','OPER','FPROV']]
    df = df.reset_index(drop=True).merge(df_consultaycoois_aux, on='OPREV', how='left')
    # df['UND_DISP'] = np.floor(np.round(df['CANT_ASIG']/df['CONSUMO']*100)/100).fillna(0).astype(int)
    df['UND_DISP'] = np.floor(np.round(df['CANT_ASIG']/df['CONSUMO'], 1))
    df = df.groupby(["TEMA", "ANO", "TEMP", "LIN", "OPREV", "CODISAP", "NAMESAP", "PRIORIDAD", "CANT_REAL", "TIPO","OPER","FPROV"],
                    as_index=False, dropna=False)['UND_DISP'].min()
    df['% DE COBERTURA SKU'] = np.round(df['UND_DISP']/df['CANT_REAL']*100)/100
    df['REFERENCIA'] = df['CODISAP'].str[:7]
    df = df.loc[:,["TEMA", "ANO", "TEMP", "TIPO", "OPER","LIN", "FPROV", "REFERENCIA", "OPREV",
                   "CODISAP", "NAMESAP", "PRIORIDAD", "CANT_REAL", "UND_DISP", "% DE COBERTURA SKU"]]
    df = df.sort_values('PRIORIDAD', ascending=True)
    df['CATALOGO'] = np.where(df['REFERENCIA'].str[:2].isin(['15','25']),
                              'LIVVA',
                              'AZZORTI')
    df = df.drop_duplicates()
    df = df.reset_index(drop=True)
    #Agregar coberturas
    df_cant_real_om = df.groupby(['REFERENCIA','FPROV','TIPO','TEMP','ANO','TEMA'], as_index=False, dropna=False)['CANT_REAL'].sum()
    df_cant_real_om = df_cant_real_om.rename(columns={'CANT_REAL':'CANT_REAL_OM'}).reset_index(drop=True)
    df_cobertura_ref = df.groupby(['REFERENCIA','FPROV','TIPO','TEMP','ANO','TEMA'], as_index=False, dropna=False)['UND_DISP'].sum()
    df_cobertura_ref = df_cobertura_ref.rename(columns={'UND_DISP':'COBERTURA_REF'}).reset_index(drop=True)
    df = df.merge(df_cant_real_om.reset_index(drop=True), on=['REFERENCIA','FPROV','TIPO','TEMP','ANO','TEMA'], how='left')
    df = df.merge(df_cobertura_ref.reset_index(drop=True), on=['REFERENCIA','FPROV','TIPO','TEMP','ANO','TEMA'], how='left')
    assert df['CANT_REAL_OM'].min() > 0, 'Columna "CANT_REAL_OM" contiene ceros o negativos'
    df['% DE COBERTURA REF'] = df['COBERTURA_REF']/df['CANT_REAL_OM']
    df = df.drop('COBERTURA_REF', axis=1).reset_index(drop=True)
    df = df[['TEMA', 'ANO', 'TEMP', 'TIPO', 'CATALOGO', 'OPER', 'LIN', 'FPROV', 'REFERENCIA',
             'OPREV', 'CODISAP', 'NAMESAP', 'PRIORIDAD', 'CANT_REAL', 'UND_DISP',
             '% DE COBERTURA SKU', 'CANT_REAL_OM', '% DE COBERTURA REF']]
    
    global df_resultado_prev
    df_resultado_prev = df.copy()
    return df

def namesapcomp():
    df_zdm_pp_comp_aux = df_zdm_pp_comp.copy()
    df_zrepo_list_mate_full_aux = df_zrepo_list_mate_full.copy()
    df = df_zdm_pp_comp_aux.append(df_zrepo_list_mate_full_aux)
    df = df.loc[:,['CODISAPCOMP','NAMESAPCOMP']].drop_duplicates()
    df = df.sort_values('CODISAPCOMP', ascending=True)
    global df_namesapcomp
    df_namesapcomp = df
    return df

# =============================================================================
# REPORTE_OK
# Los redondeos causan que las líneas a liberar sean diferentes (algunas más)
# Ignorados
# 23 y 18 días con Indigo en las condiciones de "RANGO_PRIORIDAD"
# REVISAR PARTE EXCEL
# No son días 360 al año en lista_ok
# =============================================================================
def reporte_ok():
    df = df_resultado_prev.copy()
    
    #Algoritmos de PowerQuery
    df['OBS'] = np.where(df['% DE COBERTURA REF'] >= 1,
                         'LIBERAR', 
                         np.where((df['% DE COBERTURA REF'] >= 0.95) & ((df['CANT_REAL_OM']-df['CANT_REAL_OM']*df['% DE COBERTURA REF'])<=120),
                                  'GENERAR CURVA',
                                  np.where(df['% DE COBERTURA REF'] >= 0.80,
                                           'CONSULTAR CURVA',
                                           '')))
    df = df[df['OBS'] != '']
    df = df[~df['OPREV'].astype('str').str.startswith('9')]
    df = df.drop(['FPROV','OPER'], axis=1)
    #Agregar ignorados
    # df_ignorados = pd.read_excel(root_diario+"XMDVPv2.1.xlsm", sheet_name='IGNORADOS')
    # df_ignorados = df_ignorados.loc[:,['PREVISIONAL']].drop_duplicates().reset_index(drop=True)
    # df_ignorados['PREVISIONAL_'] = df_ignorados['PREVISIONAL']
    df_ignorados_aux = df_ignorados.copy()
    df_ignorados_aux.columns = ['OPREV', 'PREVISIONAL']
    df = df.merge(df_ignorados_aux, on='OPREV', how='left')
    df = df[df['PREVISIONAL'].isna()]
    df = df.drop('PREVISIONAL', axis=1)
    df = df.sort_values('PRIORIDAD', ascending=True).reset_index(drop=True)
    df = df.reset_index(drop=False).rename(columns={'index':'PRIORIDAD LIB'})
    df['PRIORIDAD LIB'] = df['PRIORIDAD LIB'] + 1
    df = df.drop(['PRIORIDAD','CANT_REAL_OM'], axis=1)
    df_coois_cabe_prev_aux = df_coois_cabe_prev.copy()
    df_coois_cabe_prev_aux = df_coois_cabe_prev_aux[['OPREV','FECHAPAIS','FCOMP']].drop_duplicates().reset_index(drop=True)
    df = df.merge(df_coois_cabe_prev_aux, on='OPREV', how='left')
    df['RANGO_PRIORIDAD'] = np.where(df['FECHAPAIS'] <= date_now,
                                     'PRIORIDAD ALTA',
                                     np.where(((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=23)) & (df['LIN'] == 'IND'),
                                              'PRIORIDAD ALTA',
                                              np.where(((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=18)) & (df['LIN'] != 'IND'),
                                                       'PRIORIDAD ALTA',
                                                       np.where(((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=30)) & (df['LIN'] == 'IND'),
                                                                'PRIORIDAD MEDIA',
                                                                np.where(((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=25)) & (df['LIN'] != 'IND'),
                                                                         'PRIORIDAD MEDIA',
                                                                         'SIN PRIORIDAD')))))
    #CALCULOS DE LA HOJA EXCEL
    df_prioridad_om = pd.DataFrame()
    df_prioridad_om['GRUPO'] = df['TEMA'].astype(str) + df['ANO'].astype(str) + df['TEMP'].astype(str) + df['TIPO'].astype(str) + df['REFERENCIA'].astype(str)
    df_prioridad_om = df_prioridad_om.drop_duplicates()
    df_prioridad_om = df_prioridad_om.reset_index(drop=True).reset_index(drop=False).rename(columns={'index':'PRIORIDAD_OM'})
    df_prioridad_om['PRIORIDAD_OM'] = df_prioridad_om['PRIORIDAD_OM'] + 1
    
    df['GRUPO'] = df['TEMA'].astype(str) + df['ANO'].astype(str) + df['TEMP'].astype(str) + df['TIPO'].astype(str) + df['REFERENCIA'].astype(str)
    df = df.merge(df_prioridad_om, on='GRUPO', how='left')
    
    #calculos de horizonte liberación
    df_hz1 = df.query("OBS != 'CONSULTAR CURVA' & LIN != 'IND'").reset_index(drop=True)
    df_hz1 = df_hz1.groupby(['LIN','PRIORIDAD_OM','OBS'], as_index=False)['UND_DISP'].sum()
    df_hz1 = df_hz1.sort_values('PRIORIDAD_OM')
    df_hz1['UND_DISP_ACUM'] = df_hz1['UND_DISP'].cumsum()
    df_hz2 = df_hz1.copy()
    df_hz1['PRIORIDAD_OM'] = df_hz1['PRIORIDAD_OM'] + 1
    
    df.insert(loc=df.columns.shape[0], column='HORIZONTE_LIBERACION_SI', value=df.merge(df_hz1, on=['LIN','PRIORIDAD_OM','OBS'], how='left')['UND_DISP_ACUM'].tolist())
    df.insert(loc=df.columns.shape[0], column='HORIZONTE_LIBERACION_NO', value=df.merge(df_hz2, on=['LIN','PRIORIDAD_OM','OBS'], how='left')['UND_DISP_ACUM'].tolist())
    df = df.sort_values('PRIORIDAD LIB', ascending=True)
    df[['HORIZONTE_LIBERACION_SI','HORIZONTE_LIBERACION_NO']] = df[['HORIZONTE_LIBERACION_SI','HORIZONTE_LIBERACION_NO']].fillna(method='ffill')
    df[['HORIZONTE_LIBERACION_SI','HORIZONTE_LIBERACION_NO']] = df[['HORIZONTE_LIBERACION_SI','HORIZONTE_LIBERACION_NO']].fillna(0)
    
    df['HORIZONTE_LIBERACION'] = np.where(df['LIN'] == 'IND',
                                          0,
                                          np.where(df['OBS'] == 'CONSULTAR CURVA',
                                                   df['HORIZONTE_LIBERACION_SI'],
                                                   df['HORIZONTE_LIBERACION_NO']))
    df = df.drop(['HORIZONTE_LIBERACION_SI','HORIZONTE_LIBERACION_NO'], axis=1)
    df = df.reset_index(drop=True)
    
    #Agregamos SAM de tiempos de procesos
    df = df.merge(df_tiempos_confeccion, on='REFERENCIA', how='left')
    df = df.merge(df_est_sub, on='REFERENCIA', how='left')
    
    #Reporte_ok_lista
    df_ok = df.copy()
    df_ok = df_ok[df_ok['HORIZONTE_LIBERACION'] <= parametros_dict['horizonte_liberacion']]
    df_ok = df_ok.reset_index(drop=True)
    df_ok['DIAS'] = (df_ok['FECHAPAIS'] - date_now).apply(lambda x: x.days)
    df_ok['DIAS'] = np.maximum(df_ok['DIAS'], 0)
    
    df_ok['RANGO'] = np.where(df_ok['DIAS'] <= 15,
                           '0-15',
                           np.where(df_ok['DIAS'] <= 30,
                                    '16-30',
                                    np.where(df_ok['DIAS'] <= 45,
                                             '31-45',
                                             np.where(df_ok['DIAS'] <= 60,
                                                      '46-60',
                                                      '+61'))))
    return [df, df_ok]
# =============================================================================
# FALTANTE_MP
# =============================================================================
def faltante_mp():
    df = df_coois_cabe_prev_mate.copy()
    df = df.sort_values('PRIORIDAD', ascending=True)
    df = df[~df.OPREV.astype(str).str.startswith('9')]
    df_consultaycoois_aux = df_consultaycoois.copy()
    df_consultaycoois_aux = df_consultaycoois_aux[["OPREV","TEMA","ANO","TEMP","NAMESAP","LIN","TIPO","FECHAPAIS"]].drop_duplicates().reset_index(drop=True)
    df = df.merge(df_consultaycoois_aux, on='OPREV', how='left')
    df['FALTANTE'] = df['NECESIDAD'] - df['CANT_ASIG']
    df = df[df['FALTANTE'] > 0]
    df_namesapcomp_aux = df_namesapcomp.copy()
    df = df.merge(df_namesapcomp_aux, on='CODISAPCOMP', how='left')
    df = df.drop_duplicates()
    df = df.drop(["SALDO_TOTAL", "SALDO_DISP", "CANT_ASIG", "SALDO_SOBR", "SUSTITUTO 1", "SUSTITUTO 2", "SUSTITUTO 3", "SUSTITUTO 4"], axis=1)
    df['UNDS QUE AFECTA'] = df['FALTANTE'] / df['CONSUMO']
    df['RANGO'] = np.where(df['FECHAPAIS'].isna(),
                           'PRIORIDAD BAJA',
                           np.where((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=22+1),
                                    'PRIORIDAD NEGRA',
                                    np.where((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=40+1),
                                             'PRIORIDAD ALTA',
                                             np.where((date_now - df['FECHAPAIS'])*-1 <= pd.Timedelta(days=60+1),
                                                      'PRIORIDAD MEDIA',
                                                      'PRIORIDAD BAJA',))))
    df = df.drop(["FECHAPAIS", "TEMA", "ANO", "TEMP", "CODISAP", "NAMESAP", "LIN", "TIPO", "CANT_REAL", "CONSUMO", "NECESIDAD"], axis=1)
    df = df.drop_duplicates()
    df = df.pivot_table(index=['OPREV', 'PRIORIDAD', 'SIN PPCOMP', 'CODISAPCOMP', 'NO_DISP','NAMESAPCOMP', 'UNDS QUE AFECTA'],
                        columns='RANGO', values='FALTANTE', aggfunc='sum').reset_index()
    df = df.drop_duplicates()
    df = df.sort_values('PRIORIDAD', ascending=True).reset_index(drop=True)
    df['SKUSNEG'] = np.where((df['PRIORIDAD NEGRA'] == 0) | (df['PRIORIDAD NEGRA'].isna()), 0, 1)
    df = df.groupby(['CODISAPCOMP','NAMESAPCOMP'], as_index=False).agg(P_URG = ('PRIORIDAD NEGRA', 'sum'),
                                                                       P_ALTA = ('PRIORIDAD ALTA', 'sum'),
                                                                       P_MEDIA = ('PRIORIDAD MEDIA', 'sum'),
                                                                       P_BAJA = ('PRIORIDAD BAJA', 'sum'),
                                                                       SKUS_AFECT_URG = ('SKUSNEG', 'sum'),
                                                                       PRIORIDAD = ('PRIORIDAD', 'min'),
                                                                       UNDS_QUE_AFECTA_ESTA_MP = ('UNDS QUE AFECTA', 'sum'))
    df['TOTAL'] = df['P_URG'] + df['P_ALTA'] + df['P_MEDIA'] + df['P_BAJA']
    df = df.sort_values('PRIORIDAD', ascending=True).reset_index(drop=True)
    df = df.reset_index(drop=False).rename(columns={'index':'PRIORIDAD_'})
    df['PRIORIDAD_'] = df['PRIORIDAD_'] +1
    df = df.drop('PRIORIDAD', axis=1)
    df = df.loc[:,["CODISAPCOMP", "NAMESAPCOMP", "P_URG", "P_ALTA", "P_MEDIA", "P_BAJA", "TOTAL", "SKUS_AFECT_URG", "UNDS_QUE_AFECTA_ESTA_MP", "PRIORIDAD_"]]
    global df_faltante_mp
    df_faltante_mp = df.copy()
    return df          

# =============================================================================
# FALTANTE_SKU                                                                   
# =============================================================================
def faltante_sku():
    df = df_coois_cabe_prev_mate.copy()
    df_consultaycoois_aux = df_consultaycoois.copy()
    df_consultaycoois_aux = df_consultaycoois_aux[["OPREV","TEMA","ANO","TEMP","NAMESAP","LIN","FPROV","TIPO"]].drop_duplicates().reset_index(drop=True)
    df = df.merge(df_consultaycoois_aux, on='OPREV', how='left')
    df['FALTANTE'] = df['NECESIDAD'] - df['CANT_ASIG']
    df = df[~(df['FALTANTE'].isna() | df['FALTANTE']=='')]
    df = df[df['FALTANTE'] > 0].reset_index(drop=True)
    df_namesapcomp_aux = df_namesapcomp.copy()
    df = df.merge(df_namesapcomp_aux, on='CODISAPCOMP', how='left')
    df = df.drop_duplicates()
    df = df.drop(["SALDO_TOTAL", "SALDO_DISP", "CANT_ASIG", "SALDO_SOBR", "SUSTITUTO 1", "SUSTITUTO 2", "SUSTITUTO 3", "SUSTITUTO 4"], axis=1)
    df = df.sort_values('PRIORIDAD', ascending=True).reset_index(drop=True)
    df['OBSERVACION'] = np.where(df['FALTANTE'] <= (df['NECESIDAD']*0.1),
                                 'AJUSTAR',
                                 '')
    df['REFNAME'] = df['NAMESAP'].str.split(',',expand=True).loc[:,0]
    # df['Texto breve de material'] = df['NAMESAP'].str.split(',',expand=True).loc[:,1]
    df = df.drop('NO_DISP', axis=1)
    df = df.reset_index(drop=True)
    global df_faltante_sku
    df_faltante_sku = df.copy()
    return df
    
#%% Escribir

parametros()
zpp_leader_list_adv()
zpp_plan_oprev()
zdm_mat_art()
bd_cronograma()
zdm_pp_hrutas()
consulta()
cargue()
zpp_sustitutos()
zpp_sustitutos_numeral()
zdm_pp_comp()
zrepo_list_mate_full()
zmd04()
coois_cabe_prev()
coois_cabe_prev_mate()
consultaycoois()
resultado_prev()
namesapcomp()
reporte_ok()
faltante_mp()
faltante_sku()

df_coois_cabe_prev_mate = df_coois_cabe_prev_mate.drop('NO_DISP', axis=1)

#%%
df_reporte = reporte_ok()
df_reporte_ok = df_reporte[0]
df_reporte_ok_lista = df_reporte[1]
# df_reporte_ok.to_excel('C:/Users/liamd/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP/Test/reporte.xlsx', index=False)

df_requerimiento = df_coois_cabe_prev_mate.copy()
df_faltante_mp = df_faltante_mp.copy()
# df_faltante_mp.to_excel('C:/Users/liamd/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP/Test/faltante_mp.xlsx', index=False)
df_faltante_sku = df_faltante_sku.copy()

#%%
consolidado = {'RESULTADO_PREV':df_resultado_prev,
               'STOCK_DIS':df_stockdis,
               'REPORTE_OK':df_reporte_ok,
               'REPORTE_OK_LISTA':df_reporte_ok_lista,
               'COOIS_CABE_PREV':df_coois_cabe_prev,
               'CONSULTA':df_consulta,
               'REQUERIMIENTO':df_requerimiento,
               'FALTANTE_MP':df_faltante_mp,
               'FALTANTE_SKU':df_faltante_sku}

#%%
#writer = pd.ExcelWriter('C:/Cloud/Dropbox/Dupree/Tablas/Pruebas' + 'reporte_mdvp' + '.xlsx')
# writer = pd.ExcelWriter('C:/Users/' + name_user +'/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP/' + 'reporte_mdvp.xlsx')
#try:
#    writer = pd.ExcelWriter('C:/Users/' + name_user +'/OneDrive - 35159_147728_DUPREE_VENTA_DIRECTA_S_A/PLAN_PROD/VALIDACIONES/NUEVA MDVP/AJUSTE PEDIDO/' + 'reporte_mdvp.xlsx')
#except:
#    pass
#if name_user == 'liamd':
#    writer = pd.ExcelWriter('C:/Users/' + name_user +'/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP/Test/' + 'reporte_mdvp.xlsx')
writer = pd.ExcelWriter('C:/Cloud/Dropbox/Dupree/Tablas/' + 'reporte_mdvp.xlsx')
for i in consolidado.keys():
    print(i)
    consolidado[i].to_excel(writer, sheet_name=i, index=False)
writer.save()
    
# df_coois_cabe_prev_mate.to_excel('C:/Users/' + name_user +'/35159_147728_DUPREE_VENTA_DIRECTA_S_A/CADENA DE SUMINISTROS - Documentos/Projects/15. MDVP/' + 'df_coois_cabe_prev_mate.xlsx', index=False)