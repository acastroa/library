# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
#%%% Generador Campañas y rezagos
def create_camp(rezagos=0):
    """
    NAME
        creador de campañas

    Descripcion
        función que genera las campañas de AZZ (18 del año) y sus rezagos. No excluye la 202011 que no salió en ningún pais

    Parametros
    ----------
        rezagos: el número de rezagos que desea aparezca en la campaña. Por defecto es cero.
    Returns
    -------
    Retorna la campaña acompañada de su anterior campaña según el rezago especificado. En caso sea cero, solo la columna campaña.
    """

    df= pd.DataFrame({'Año': [i for i in range(2008,2045)]*18,
                   'Camp':["%02d" % i for i in range (1,19)]*37})
    df["CAMP"]= df.apply(lambda x: "".join(map(str,x)),axis=1)
    ANOS_2= df.sort_values('CAMP').reset_index(drop=True)
    CAMP= ANOS_2['CAMP'].astype(int).unique()

    if rezagos>0:
        rezaguitos= rezagos
        lastito= int(ANOS_2.CAMP.count() -rezaguitos)
        CAMP_MAS= ANOS_2.CAMP[rezaguitos:]
        CAMP_MAS.reset_index(drop=True, inplace=True)
        df_camp= pd.DataFrame({'CAMP_ANT': ANOS_2.CAMP[0:lastito],
                               'CAMP': CAMP_MAS})
    else:
        df_camp= pd.DataFrame(df['CAMP'])
    df_camp = df_camp.sort_values(["CAMP"])
    df_camp = df_camp.reset_index(drop= True)

    return(df_camp)
