import setuptools

setuptools.setup(name='AzzLib',
      version='0.0.1',
      description='Library for Azzorti Projects',
      url='https://gitlab.com/a6333/library',
      author='Supply Chain Projects Team & Friends - AZZ+LVV',
      author_email='user@whatever.com',
      license='All rights reserved',
      scripts=[
          'AzzLib/LoadData.py',
          'MDVP/main.py'
      ],
      packages=setuptools.find_packages(),
      classifiers=[
          "Programming Language :: Python :: 3 :: Only"
      ],
      install_requires=[
          'numpy>=0.1',
          'pandas>=0.1',
          'pymssql>=2.2',
          'selenium>=4.0.0',
          'webdriver_manager>=3.5.3',
          'openpyxl>=3.0.9'
      ],
      zip_safe=False)
